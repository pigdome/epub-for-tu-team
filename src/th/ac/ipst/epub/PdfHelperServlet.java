package th.ac.ipst.epub;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.http.*;

import th.ac.ipst.epub.gcs.EpubHelper;

import com.google.gson.GsonBuilder;

@SuppressWarnings("serial")
public class PdfHelperServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		String request=req.getParameter("req");
		
		if(request.equals("getAllHtmlsInSpineOrder")) {
			Long publicationId=Long.valueOf(req.getParameter("publicationId"));

			ArrayList<String> htmls=EpubHelper.getInstance().getAllHtmlsInSpineOrder(publicationId);
			
	        resp.setContentType("application/json");
	        resp.setCharacterEncoding("UTF-8");
	        if(htmls.size()>0)
	        	resp.getWriter().write(new GsonBuilder().create().toJson(htmls));
	        else
	        	resp.setStatus(404);
		}
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
	}
}
