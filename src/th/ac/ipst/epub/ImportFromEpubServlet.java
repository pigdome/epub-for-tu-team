package th.ac.ipst.epub;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.channels.Channels;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.servlet.http.*;

import th.ac.ipst.epub.gcs.EpubHelper;

import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsInputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;
import com.google.common.io.ByteStreams;
import com.google.gson.GsonBuilder;

@SuppressWarnings("serial")
public class ImportFromEpubServlet extends HttpServlet {
	
	private static final Logger log = Logger.getLogger(ImportFromEpubServlet.class.getName());

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String request=req.getParameter("req");
		
		if(request.equals("extractEpub")) {
			String epubGcsKey=req.getParameter("epubGcsKey");
			Long authorId=Long.valueOf(req.getParameter("authorId"));

			List publicationAndEpubPackage=EpubHelper.getInstance().extractFilesFromEpub(epubGcsKey, authorId);
			
	        resp.setContentType("application/json");
	        resp.setCharacterEncoding("UTF-8");
	        resp.getWriter().write(new GsonBuilder().create().toJson(publicationAndEpubPackage));
		} 
	}
}
