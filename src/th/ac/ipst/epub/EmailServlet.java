package th.ac.ipst.epub;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.channels.Channels;
import java.util.Properties;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;

import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import javax.servlet.http.*;

import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsInputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;
import com.google.common.io.ByteStreams;

@SuppressWarnings("serial")
public class EmailServlet extends HttpServlet {
	
	private static final Logger log = Logger.getLogger(EmailServlet.class.getName());

	private final GcsService gcsService = GcsServiceFactory.createGcsService(RetryParams.getDefaultInstance());
	
	private static final int BUFFER_SIZE = 2 * 1024 * 1024;	
	private static final String BUCKET="ipst-epub";
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String request=req.getParameter("req");
		
		if(request.equals("sendEpub")) {
			String publicationId=req.getParameter("publicationId");
			String email=req.getParameter("email");
			String senderName=req.getParameter("senderName");
			String subject=req.getParameter("subject");			
			String message=req.getParameter("message");
			
			Publication publication=PublicationConnector.getById(Long.valueOf(publicationId));
			
			GcsInputChannel inputChannel = this.gcsService.openPrefetchingReadChannel(new GcsFilename(BUCKET, publication.getEpubRelativeUrl()), 0, BUFFER_SIZE);
			byte[] attachment=ByteStreams.toByteArray(Channels.newInputStream(inputChannel));
			String fileName=publication.getTitle()+".epub";
			String mimeType="application/epub+zip";
			
			sendWithAttachment(email, senderName, subject, message, fileName, mimeType, attachment);
		} 
		else if(request.equals("sendPdf")) { // flow คือ ต้องมีหน้าให้อัพ pdf ขึ้น GCS ก่อนแล้วค่อยเลือก
			String email=req.getParameter("email");
			String senderName=req.getParameter("senderName");
			String subject=req.getParameter("subject");			
			String message=req.getParameter("message");
			String pdfUrl=req.getParameter("pdfUrl");
			
			String gcsObjectPath=pdfUrl.split("/ipst-epub/")[1]; // ตัด url ด้วย bucket name เพื่อเอาเฉพาะ gcs object path"
			String fileName=pdfUrl.split("/temp-attachments/")[1]; 
			String mimeType="application/pdf";
			
			GcsFilename gcsFileName=new GcsFilename(BUCKET, gcsObjectPath);
			GcsInputChannel inputChannel = this.gcsService.openPrefetchingReadChannel(gcsFileName, 0, BUFFER_SIZE);
			byte[] attachment=ByteStreams.toByteArray(Channels.newInputStream(inputChannel));
			
			sendWithAttachment(email, senderName, subject, message, fileName, mimeType, attachment);			
			
			// delete uploaded file to claim some space back
			gcsService.delete(gcsFileName);
		}
	}
	
	private void sendWithAttachment(String email, String senderName, String subject, String message, String fileName, String attachmentMimeType, byte[] attachment)
	{
		Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);

        try {
        	// http://stackoverflow.com/questions/4897836/problem-with-subject-encoding-when-sending-an-email
        	MimeMessage msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress("ipst-epub@appspot.gserviceaccount.com", "IPST ePub Editor"));
            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
            msg.setSubject(subject, "UTF-8");
            
            Multipart mp = new MimeMultipart();
            
            MimeBodyPart htmlPart = new MimeBodyPart();
            String htmlBody=message + "<br /><br />" + "- "+senderName+" (ส่งผ่านทางแอพ IPST ePub Editor)";
            htmlPart.setContent(htmlBody, "text/html");
            mp.addBodyPart(htmlPart);
            
            MimeBodyPart attachmentPart = new MimeBodyPart();
            attachmentPart.setFileName(fileName);

            // attachmentPart.setContent(attachment, attachmentMimeType); ทำตาม document ไม่ work
            // ต้องทำแบบนี้ http://stackoverflow.com/questions/5765059/app-engine-create-and-email-audio-file
            DataSource src = new ByteArrayDataSource(attachment, attachmentMimeType);
            attachmentPart.setDataHandler(new DataHandler(src));
            
            mp.addBodyPart(attachmentPart);
            msg.setContent(mp);
            
            Transport.send(msg);
        } catch (AddressException e) {
        	log.severe("AddressException: "+e.getLocalizedMessage());
        } catch (MessagingException e) {
        	log.severe("MessagingException: "+e.getLocalizedMessage());
        } catch (UnsupportedEncodingException e) {
        	log.severe("MessagingException: "+e.getLocalizedMessage());        	
        }
	}
}
