package th.ac.ipst.epub;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import com.google.appengine.api.datastore.Text;

@PersistenceCapable
public class Tag 
{
    @PrimaryKey
    @Persistent ( valueStrategy =  IdGeneratorStrategy. IDENTITY )
    Long id;

    Long authorId;
    String name; 
    Long count;

    public Long getId()
    {
    	return this.id;
    }
    public void setId(Long id)
    {
    	this.id=id;
    }

    public Long getAuthorId()
    {
    	return this.authorId;
    }
    public void setAuthorId(Long authorId)
    {
    	this.authorId=authorId;
    }

    public String getName()
    {
    	return this.name;
    }
    public void setName(String name)
    {
    	this.name=name;
    }
    
    public Long getCount()
    {
    	return this.count;
    }
    public void setCount(Long count)
    {
    	this.count=count;
    }
}
