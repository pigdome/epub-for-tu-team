package th.ac.ipst.epub;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@SuppressWarnings("serial")
public class MediaServlet extends HttpServlet {

	private static final Logger log = Logger.getLogger(MediaServlet.class.getName());

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String request=req.getParameter("req");
		
		if(request.equals("list")) {
			Long authorId=Long.valueOf(req.getParameter("authorId"));
			String type=req.getParameter("type");
			String cursorString=req.getParameter("cursor");
			String limitString=req.getParameter("limit");
			Integer limit=null; 
			if(limitString!=null)
				limit=Integer.valueOf(req.getParameter("limit"));
			List mediasWithCursor=MediaConnector.list(authorId, type, cursorString, limit);
	        resp.setContentType("application/json");
	        resp.setCharacterEncoding("UTF-8");
	        resp.getWriter().write(new GsonBuilder().create().toJson(mediasWithCursor));
		}
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String request=req.getParameter("req");
		
		if(request.equals("insert")) {
			Media media=new Gson().fromJson(req.getParameter("media"), Media.class);
			Media insertedMedia=MediaConnector.insert(media);
	        resp.setContentType("application/json");
	        resp.setCharacterEncoding("UTF-8");
	        resp.getWriter().write(new GsonBuilder().create().toJson(insertedMedia));
		}
		else if(request.equals("remove")) {
			Long mediaId=Long.valueOf(req.getParameter("mediaId"));
			if(MediaConnector.remove(mediaId))
				resp.setStatus(200);
			else
				resp.setStatus(404);				
		}
	}
}
