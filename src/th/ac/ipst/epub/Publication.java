package th.ac.ipst.epub;

import java.util.ArrayList;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Text;

@PersistenceCapable(detachable="true")
public class Publication
{
	@PrimaryKey
    @Persistent ( valueStrategy =  IdGeneratorStrategy. IDENTITY )
    Long id;
	
	// ไม่จำเป็นต้องเก็บ เพราะ reconstruct ได้จาก field อื่น และ request ได้แบบ dynamic/on demand (return signed url กลับไป)
	//String objectNamesPrefix; // path ของ file objects ใน Google Cloud Storage (ยังไม่รวม path ของ version)
	//String epubUrl; // path ของไฟล์หนังสือในรูปแบบ EPUB
	//String pdfUrl; // path ของไฟล์หนังสือในรูปแบบ PDF 

	// TODO: check if the title is safe to construct a GCS path
	// root path ของ asset ทั้งหลายอยู่ที่ -> /authorId/publicationId/
	// epub file -> /authorId/publicationId/version/{title}.epub     <-- gcs ยังมี bug เกี่ยวกับ encoding ต้องใช้ hash ของ title ไปก่อน String.hashCode()
	// pdf file -> /authorId/publicationId/version/{title}.pdf
	// epub package -> /authorId/publicationId/version/epub/...
	// pdf -> /authorId/publicationId/version/pdf/...
	// media -> /authorId/publicationId/version/media/png(video/etc.)  specific กับ version เวลา save as new version ก็ให้ clone มาด้วย
	// cover -> /authorId/covers ไม่ขึ้นกับ book และ version
	
	Long publicationGroupId; // ใช้เป็นตัวผูกหนังสือเล่มเดียวกันแต่คนละ version เข้าด้วยกัน, ใช้ dateCreated ได้เพราะ unique among Publications of an Author 
							 // แต่ตอนนี้คงยังไม่ได้ใช้เพราะยังไม่ทำ versioning
	String version; // v1, v2, v3, etc.
	Long versionDateCreated; // เผื่อเอาไว้ sort version ได้ ดูได้ว่าสร้าง version ไหน เมื่อไหร่
	boolean isLatestVersion; // ใช้ตอน select เฉพาะ latest version
	String status; // in progress, completed   เรียกดู publication ตาม status พวกนี้ได้บน Web Portal!
	String type; // textbook, concept
	Long authorId; // map ได้กับ publisherID ใน Digital Marketplace's Item entity
	ArrayList <Long> collaboratorIds; // IDs ของผู้ช่วยแต่งหนังสือ (role เหมือนกับ Owner อาจมีบางอย่างที่ทำไม่ได้)
	Long dateCreated;
	String coverUrl; // รูปปกหนังสือ
	String themeId; // theme1, theme2, etc.

	// ส่วนที่ follow ตาม Digital Marketplace's Item entity
	String title = "untitled"; // ชื่อหนังสือ (default ไว้เป็น untitled เผื่อกรณีไม่ใส่เข้ามา เวลา gen ไฟล์ epub จะได้ untitled.epub)
    ArrayList<String> otherTitles; // ชื่อเรื่องอื่นๆ 
    ArrayList<String> authors; // ผู้แต่ง
    ArrayList<String> authorsTypes; // ประเภทผู้แต่ง
    ArrayList<String> mediaTypes; // ชนิดของสื่อ
    ArrayList<String> resourceTypes; // ประเภทสื่อการเรียนรู้
    ArrayList<String> disciplines; // กลุ่มสาระวิชา
    ArrayList<String> grades; // ระดับชั้นเรียน    
    Text description; // รายละเอียดของสื่อ
    ArrayList <String> subjectHeadings; // หัวเรื่อง
	// ArrayList <String> screenshotURLs; // ภาพตัวอย่างในหนังสือ     ไว้ค่อยใส่ตอน publish ใน Digital Marketplace	

    public Long getPublicationGroupId()
    {
    	return this.publicationGroupId;
    }
    public void setPublicationGroupId(Long publicationGroupId)
    {
    	this.publicationGroupId=publicationGroupId;
    }

    public String getThemeId()
    {
    	return this.themeId;
    }
    public void setThemeId(String themeId)
    {
    	this.themeId=themeId;
    }
    
    public Long getId()
    {
    	return this.id;
    }
    public void setId(Long id)
    {
    	this.id=id;
    }

    public String getVersion()
    {
    	return this.version;
    }
    public void setVersion(String version)
    {
    	this.version=version;
    }
    
    public Long getVersionDateCreated()
    {
    	return this.versionDateCreated;
    }
    public void setVersionDateCreated(Long versionDateCreated)
    {
    	this.versionDateCreated=versionDateCreated;
    }

    public boolean getIsLatestVersion()
    {
    	return this.isLatestVersion;
    }
    public void setIsLatestVersion(boolean isLatestVersion)
    {
    	this.isLatestVersion=isLatestVersion;
    }
    
    public String getGcsRootPath()
    {
    	// authorId/dateCreated/title/version/
    	//return this.authorId+"/"+this.dateCreated.toString()+"/"+this.title+"/"+this.version+"/";
    	
    	// authorId/publicationId/version/
    	return this.authorId.toString()+"/"+this.id.toString()+"/"+this.version+"/";    	
    }
    
    // เก็บรวม ไม่แยกตาม publicationId (และไม่ copy ไปตามแต่ละ publicationId folder เพราะเปลือง gcs space)
    public String getMediaRootPath()
    {
    	return this.authorId.toString()+"/media/";
    }

    public String getGcsEpubPackagePath()
    {
    	return getGcsRootPath()+"epub/";
    }
    
    public String getEpubRelativeUrl()
    {
    	// authorId/bookId/version/{title}.epub
    	// IMPORTANT: TODO: gcs ยังมี bug เกี่ยวกับ encoding ต้องใช้ hash ของ title ไปก่อน String.hashCode()
    	String filename=String.valueOf(this.title.hashCode())+".epub";
    	return getGcsRootPath()+filename;
    }

    public String getPdfRelativeUrl()
    {
    	// authorId/bookId/version/{title}.pdf
    	String filename=this.title+".pdf";
    	return getGcsRootPath()+filename;
    }

    public String getStatus()
    {
    	return this.status;
    }
    public void setStatus(String status)
    {
    	this.status=status;
    }

    public String getType()
    {
    	return this.type;
    }
    public void setType(String type)
    {
    	this.type=type;
    }

    public Long getAuthorId()
    {
    	return this.authorId;
    }
    public void setAuthorId(Long authorId)
    {
    	this.authorId=authorId;
    }

    public ArrayList<Long> getCollaboratorIds()
    {
    	return this.collaboratorIds;
    }
    public void setCollaboratorIds(ArrayList<Long> collaboratorIds)
    {
    	this.collaboratorIds=collaboratorIds;
    }

    public Long getDateCreated()
    {
    	return this.dateCreated;
    }
    public void setDateCreated(Long dateCreated)
    {
    	this.dateCreated=dateCreated;
    }

    public String getCoverFileName()
    {
    	String fileName = this.coverUrl.substring(this.coverUrl.lastIndexOf('/')+1, this.coverUrl.length());
    	return fileName;
    }
    
    public String getCoverGcsObjectName()
    {
    	String objectName=this.coverUrl.split("ipst-epub/")[1]; // splitting right at the bucket name e.g. http://commondatastorage.googleapis.com/ipst-epub/admin/template-covers/book-cover-0.png
    	return objectName;
    }
    
    public String getCoverUrl()
    {
    	return this.coverUrl;
    }
    public void setCoverUrl(String coverUrl)
    {
    	this.coverUrl=coverUrl;
    }
    
    // เริ่มส่วน metadata ตาม สสวท. กำหนด
    public String getTitle()
    {
    	return this.title;
    }
    public void setTitle(String title)
    {
    	this.title=title;
    }

    public ArrayList<String> getOtherTitles()
    {
    	return this.otherTitles;
    }
    public void setOtherTitles(ArrayList<String> otherTitles)
    {
    	this.otherTitles=otherTitles;
    }

    public ArrayList<String> getMediaTypes()
    {
    	return this.mediaTypes;
    }
    public void setMediaTypes(ArrayList<String> mediaTypes)
    {
    	this.mediaTypes=mediaTypes;
    }

    public ArrayList<String> getAuthors()
    {
    	return this.authors;
    }
    public void setAuthors(ArrayList<String> authors)
    {
    	this.authors=authors;
    }

    public ArrayList<String> getAuthorsTypes()
    {
    	return this.authorsTypes;
    }
    public void setAuthorsTypes(ArrayList<String> authorsTypes)
    {
    	this.authorsTypes=authorsTypes;
    }

    public ArrayList<String> getResourceTypes()
    {
    	return this.resourceTypes;
    }
    public void setResourceTypes(ArrayList<String> resourceTypes)
    {
    	this.resourceTypes=resourceTypes;
    }
    
    public ArrayList<String> getDisciplines()
    {
    	return this.disciplines;
    }
    public void setDisciplines(ArrayList<String> disciplines)
    {
    	this.disciplines=disciplines;
    }

    public ArrayList<String> getGrades()
    {
    	return this.grades;
    }
    public void setGrades(ArrayList<String> grades)
    {
    	this.grades=grades;
    }
    
    public Text getDescription()
    {
    	return this.description;
    }
    public void setDescription(Text description)
    {
    	this.description=description;
    }

    public ArrayList<String> getSubjectHeadings()
    {
    	return this.subjectHeadings;
    }
    public void setSubjectHeadings(ArrayList<String> subjectHeadings)
    {
    	this.subjectHeadings=subjectHeadings;
    }

}