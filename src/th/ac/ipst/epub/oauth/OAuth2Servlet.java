package th.ac.ipst.epub.oauth;

import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import org.json.simple.JSONObject;

import java.util.logging.Logger;

@SuppressWarnings("serial")
public class OAuth2Servlet extends HttpServlet {
	
	private static final Logger log=Logger.getLogger(OAuth2Servlet.class.getName());

	protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {

		// https://developers.google.com/accounts/docs/OAuth2Login
		// Create a state token to prevent request forgery.
		// Store it in the session for later validation.
		String security_token = new BigInteger(130, new SecureRandom()).toString(32);
		req.getSession().setAttribute("security_token", security_token);
		
		String provider=req.getParameter("provider");
		String action=req.getParameter("action"); // upload, login, etc. ต่างกันที่ถ้า upload จะ gen upload form จาก server ให้เลย
		
	    // ส่งผ่าน JSON นี้ไปใน URL, identity provider จะส่งกลับมาอีกทีใน callback เพื่อทำ validation (i.e., security_token ว่า match ค่าที่เก็บไว้ใน session รึเปล่า และให้เข้า logic ต่างกันสำหรับแต่ละ provider)
	    // ต้องส่งผ่าน parameter "state" (ดู http://stackoverflow.com/questions/6463152/facebook-oauth-custom-callback-uri-parameters)
		JSONObject statesJSON=new JSONObject(); 
	    statesJSON.put("security_token", security_token);
	    statesJSON.put("action", action);

		if(provider.equals("google")) {
		    statesJSON.put("provider", "google");	// สำหรับ implement logic ต่างกัน ใน callback	    
		    res.sendRedirect(GoogleOAuthHelper.getInstance().getAuthorizationUrlWithJSONParameters(statesJSON));
		}
		else if(provider.equals("facebook")) {
		    statesJSON.put("provider", "facebook");	// สำหรับ implement logic ต่างกัน ใน callback	    
			res.sendRedirect(FacebookOAuthHelper.getInstance().getAuthorizationUrlWithJSONParameters(statesJSON));
		}
		else if(provider.equals("microsoft")) {
		    statesJSON.put("provider", "microsoft");	// สำหรับ implement logic ต่างกัน ใน callback	    
			res.sendRedirect(MicrosoftOAuthHelper.getInstance().getAuthorizationUrlWithJSONParameters(statesJSON));			
		}
    }
}
