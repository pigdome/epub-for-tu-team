package th.ac.ipst.epub.oauth;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.client.util.Charsets;
import com.google.common.io.Resources;

import java.io.IOException;
import java.util.Arrays;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * A helper class for Google's OAuth2 authentication API.
 * @version 20130224
 * @author Matyas Danter
 */

// Modified from http://ocpsoft.org/java/setting-up-google-oauth2-with-java/ by Pao 

public final class GoogleOAuthHelper {

	private final static GoogleOAuthHelper instance = new GoogleOAuthHelper();

	private String clientId;
	private String clientSecret;
	private String userInfoUrl;
	private String redirectUri;
	private Iterable<String> scopes;
	
	private final JsonFactory JSON_FACTORY = new JacksonFactory();
	private final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
	
	private GoogleAuthorizationCodeFlow flow;
	
	private GoogleOAuthHelper() 
	{
		JSONObject secretsDictionary=null;
		try {
			secretsDictionary = (JSONObject)new JSONParser().parse(Resources.toString(Resources.getResource("google_client_secrets.json"), Charsets.UTF_8));
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		JSONObject webSecretsDictionary=(JSONObject)secretsDictionary.get("web");
		clientId=(String)webSecretsDictionary.get("client_id");
		clientSecret=(String)webSecretsDictionary.get("client_secret");
		redirectUri=(String)webSecretsDictionary.get("redirect_uri");
		userInfoUrl=(String)webSecretsDictionary.get("user_info_url");
		String scopesString=(String)webSecretsDictionary.get("scopes");
		scopes=Arrays.asList(scopesString.split(";"));

		flow=new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY, clientId, clientSecret, scopes)
			.setAccessType("online") // Sets the access type ("online" to request online access or "offline" to request offline access) or null for the default behavior of "online". By default this has the value "offline".
			.setApprovalPrompt("auto") // Sets the approval prompt behavior ("auto" to request auto-approval or "force" to force the approval UI to show) or null for the default behavior of "auto". By default this has the value "force".
			.build();
	}

	@SuppressWarnings("unchecked")
	public String getAuthorizationUrlWithJSONParameters(JSONObject statesJSON) {
		
	    // ส่งผ่าน JSON นี้ไปใน URL, identity provider จะส่งกลับมาอีกทีใน callback เพื่อทำ validation (i.e., security_token ว่า match ค่าที่เก็บไว้ใน session รึเปล่า และให้เข้า logic ต่างกันสำหรับแต่ละ provider)

		final GoogleAuthorizationCodeRequestUrl url = flow.newAuthorizationUrl();
		
		return url.setRedirectUri(redirectUri).setState(statesJSON.toJSONString()).build();
	}
	
	public String getUserInfoForVerifierCode(String code) throws IOException
	{
		GoogleTokenResponse response = flow.newTokenRequest(code).setRedirectUri(redirectUri).execute();
		Credential credential = flow.createAndStoreCredential(response, null); // userId - user ID or null if not using a persisted credential store
		HttpRequestFactory requestFactory = HTTP_TRANSPORT.createRequestFactory(credential);
		// Make an authenticated request
		GenericUrl url = new GenericUrl(userInfoUrl);
		HttpRequest request = requestFactory.buildGetRequest(url);
		request.getHeaders().setContentType("application/json");
		return request.execute().parseAsString();
	}

	public static GoogleOAuthHelper getInstance() { return instance; }
}
