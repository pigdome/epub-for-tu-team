package th.ac.ipst.epub.oauth;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.FacebookApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

import com.google.api.client.util.Charsets;
import com.google.common.io.Resources;

// สาเหตุที่ไม่เก็บ OAuthService ไว้ใน session แบบนี้ --> req.getSession().setAttribute("oauth_service", service);
// https://github.com/fernandezpablo85/scribe-java/pull/238

public class FacebookOAuthHelper {

	private static final Logger log=Logger.getLogger(FacebookOAuthHelper.class.getName());

	// http://stackoverflow.com/questions/3635396/pattern-for-lazy-thread-safe-singleton-instantiation-in-java

	private final static FacebookOAuthHelper instance = new FacebookOAuthHelper();

	private final static Token EMPTY_TOKEN = null; // ไม่ต้องใช้ request token เพราะ Facebook ใช้ OAuth2.0 (ดู http://stackoverflow.com/questions/8573272/oauth-problems-with-scribe)

	private OAuthService service;
	private String clientId;
	private String clientSecret;
	private String userInfoUrl;
	private String redirectUri;
	private String scopes;

	private FacebookOAuthHelper() {

		JSONObject secretsDictionary=null;
		try {
			secretsDictionary = (JSONObject)new JSONParser().parse(Resources.toString(Resources.getResource("facebook_client_secrets.json"), Charsets.UTF_8));
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		JSONObject webSecretsDictionary=(JSONObject)secretsDictionary.get("web");
		clientId=(String)webSecretsDictionary.get("client_id");
		clientSecret=(String)webSecretsDictionary.get("client_secret");
		redirectUri=(String)webSecretsDictionary.get("redirect_uri");
		userInfoUrl=(String)webSecretsDictionary.get("user_info_url");
		scopes=(String)webSecretsDictionary.get("scopes");

		service = new ServiceBuilder()
	    	.provider(FacebookApi.class)
	    	.apiKey(clientId)
	    	.apiSecret(clientSecret)
	    	.callback(redirectUri)
	    	.scope(scopes)
	    	.build();
	}
		
	@SuppressWarnings("unchecked")
	public String getAuthorizationUrlWithJSONParameters(JSONObject statesJSON) {


	    // ต้องเพิ่ม parameter เข้าไปดื้อๆ แบบนี้ เพราะ https://github.com/fernandezpablo85/scribe-java/issues/219
	    String encodedStatePayload=null;
		try {
			encodedStatePayload=URLEncoder.encode(statesJSON.toJSONString(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return service.getAuthorizationUrl(EMPTY_TOKEN) + "&state=" + encodedStatePayload;
	}
	
	public String getUserInfoForVerifierCode(String code)
	{
		Verifier verifier = new Verifier(code);
	    Token accessToken = service.getAccessToken(EMPTY_TOKEN, verifier);
		OAuthRequest request = new OAuthRequest(Verb.GET, userInfoUrl);
	    service.signRequest(accessToken, request);
	    Response response = request.send();
	    return response.getBody();
	}
	
	public static FacebookOAuthHelper getInstance() { return instance; }
}
