package th.ac.ipst.epub.oauth;

import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.google.appengine.api.datastore.Text;
import com.google.gson.Gson;

import th.ac.ipst.epub.Author;
import th.ac.ipst.epub.AuthorConnector;

@SuppressWarnings("serial")
public class OAuth2CallbackServlet extends HttpServlet {
	
	private static final Logger log=Logger.getLogger(OAuth2CallbackServlet.class.getName());

	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

		/* TODO: error handling
		String error=req.getParameter("error");
		
		if(error.equals("access_denied")) {
			redirect to the original page or login page
		}
		*/		

		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");

		String verifierCode=req.getParameter("code");
		String state=req.getParameter("state");

		JSONObject stateJSON=null;
		try {
			stateJSON = (JSONObject)new JSONParser().parse(state);
		} catch (ParseException e1) { e1.printStackTrace(); }
		String provider=(String)stateJSON.get("provider");
		String security_token=(String)stateJSON.get("security_token");

		if(verifierCode==null ||
		   !req.getSession().getAttribute("security_token").equals(security_token)) {

			resp.getWriter().println("Invalid parameter");
			resp.setStatus(401);	
			
			return;
		}

		JSONObject userInfo=null;
		String email=null;
		String identityProvider=null;
		String displayName=null;
		boolean emailVerified=false;

		if(provider.equals("google")) {

				// หลังจาก sign-in ดึงข้อมูลในโปรไฟล์ของ user
				try {
					userInfo = (JSONObject)new JSONParser().parse(GoogleOAuthHelper.getInstance().getUserInfoForVerifierCode(verifierCode));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				email=(String)userInfo.get("email");
				displayName=(String)userInfo.get("name");
				identityProvider="google";
				emailVerified=(Boolean)userInfo.get("verified_email");
				
		} else if(provider.equals("facebook")) {

				// หลังจาก sign-in ดึงข้อมูลในโปรไฟล์ของ user
				try {
					userInfo = (JSONObject)new JSONParser().parse(FacebookOAuthHelper.getInstance().getUserInfoForVerifierCode(verifierCode));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				email=(String)userInfo.get("email");
				displayName=(String)userInfo.get("name");
				identityProvider="facebook";
				emailVerified=(Boolean)userInfo.get("verified");

		} else if(provider.equals("microsoft")) {

				// หลังจาก sign-in ดึงข้อมูลในโปรไฟล์ของ user
				try {
					userInfo = (JSONObject)new JSONParser().parse(MicrosoftOAuthHelper.getInstance().getUserInfoForVerifierCode(verifierCode));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				email=(String)((JSONObject)userInfo.get("emails")).get("account");
				displayName=(String)userInfo.get("name");
				identityProvider="microsoft";
				emailVerified=true; // microsoft forces users to verify email prior to OAuth2.0 use
		}
		
		if(!emailVerified) {

			resp.sendRedirect("/email-verification-needed.html");
			
			return;
		}
		
		// เช็คว่าเคยลงทะเบียนเข้าระบบไปรึยัง (ใช้ email ค้นหา)
		Author a=AuthorConnector.getByEmail(email); 

		// กรณีที่เป็นสมาชิกใหม่ ให้สร้าง entity Author ใหม่เลย พร้อมใส่ข้อมูลเท่าที่รู้เข้าไป
		boolean newRegistration=false;
		if(a==null) {
			newRegistration=true;
			a=new Author();
			a.setEmail(email);
			a.setEmailVerified(emailVerified); // เชื่อตาม IdP เลย
			a.setIdentityProvider(identityProvider);
			a.setDisplayName(displayName);
			a.setUserInfo(new Text(userInfo.toJSONString()));
			AuthorConnector.insert(a);
		} 
		
		if(newRegistration) 
			req.getSession().setAttribute("newMember", Boolean.TRUE);
		else
			req.getSession().setAttribute("newMember", Boolean.FALSE);
		req.getSession().setAttribute("authorDisplayName", a.getDisplayName());
		req.getSession().setAttribute("authorId", a.getId());
		
		String action=(String)stateJSON.get("action");
		resp.setContentType("text/html;charset=UTF-8");
		if(action!=null && action.equals("close-after-logged-in")) { // generate หน้า upload จากฝั่ง server เพื่อกัน unauthorized accesss
			resp.sendRedirect("/close-myself.html");
		} else {
			// resp.sendRedirect("/"); // ทำแบบนี้ได้เพราะใส่ index.jsp ใน welcome-file แล้ว (อยู่ใน web.xml)
			resp.sendRedirect("http://ipst-epub.appspot.com/"); // IMPORTANT: ใช้แบบด้านบนไม่ได้ เพราะหลังจาก oauth เสร็จมันจะ redirect โดยใช้ protocol https ดังนั้นต้องกำหนด absolute url ไปเลย (TODO: ถ้าจะเปลี่ยน domain ทีหลัง อย่าลืมมาแก้ตรงนี้ด้วย!)
		}

    }
}
