package th.ac.ipst.epub;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.channels.Channels;
import java.util.Arrays;

import javax.servlet.http.*;

/* Cloud Storage API */
import com.google.appengine.api.files.AppEngineFile;
import com.google.appengine.api.files.FileReadChannel;
import com.google.appengine.api.files.FileService;
import com.google.appengine.api.files.FileServiceFactory;
import com.google.appengine.api.files.FileWriteChannel;
import com.google.appengine.api.files.GSFileOptions.GSFileOptionsBuilder;
/* End */

/* Google APIs Client Library for Java */
import com.google.api.client.googleapis.extensions.appengine.auth.oauth2.AppIdentityCredential;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
/* End */

@SuppressWarnings("serial")
public class IPST_ePublishingServlet extends HttpServlet {
	
	private static final String GCS_URI = "http://commondatastorage.googleapis.com";

	/** Global configuration of Google Cloud Storage OAuth 2.0 scope. */
	private static final String STORAGE_SCOPE = "https://www.googleapis.com/auth/devstorage.read_write";

	/** Global instance of the HTTP transport. */
	private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();

	/** Global instance of HTML reference to XSL style sheet. */
	String XSL = "\n<?xml-stylesheet href=\"/xsl/listing.xsl\" type=\"text/xsl\"?>\n";	
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		/*
		// Get the file service
		FileService fileService = FileServiceFactory.getFileService();
		
		boolean lockForRead = false;
		String filename = "/gs/ipst-epub/test/test.txt";
		AppEngineFile readableFile = new AppEngineFile(filename);
		FileReadChannel readChannel = fileService.openReadChannel(readableFile, lockForRead);

		// Read the file in whichever way you'd like
		BufferedReader reader = new BufferedReader(Channels.newReader(readChannel, "UTF8"));
		String line = reader.readLine();
		resp.getWriter().println("READ:" + line);

		readChannel.close();
		*/
		
		try {
		      AppIdentityCredential credential = new AppIdentityCredential(Arrays.asList(STORAGE_SCOPE));
		      
		      // Set up and execute Google Cloud Storage request.
		      String bucketName = req.getRequestURI();
		      //String bucketName = req.getPathInfo();
		      if (bucketName.equals("/")) {
		        resp.sendError(404, "No bucket specified - append /bucket-name to the URL and retry.");
		        return;
		      }
		      // Remove any trailing slashes, if found.
		      String cleanBucketName = bucketName.replaceAll("/$", "");
		      String URI = GCS_URI + cleanBucketName;
		      HttpRequestFactory requestFactory = HTTP_TRANSPORT.createRequestFactory(credential);
		      GenericUrl url = new GenericUrl(URI);
		      HttpRequest request = requestFactory.buildGetRequest(url);
		      HttpResponse response = request.execute();
		      String content = response.parseAsString();

		      // Display the output XML.
		      resp.setContentType("text/xml");
		      BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(resp.getOutputStream()));
		      String formattedContent = content.replaceAll("(<ListBucketResult)", XSL + "$1");
		      writer.append(formattedContent);
		      writer.flush();
		      resp.setStatus(200);
		    } catch (Throwable e) {
		      resp.sendError(404, e.getMessage());
		    }
	
	}
}
