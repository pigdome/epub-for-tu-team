package th.ac.ipst.epub.gcs;

import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.api.client.util.Charsets;
import com.google.common.io.Resources;

public final class CredentialHelper {

	private final static CredentialHelper instance = new CredentialHelper();

	private String bucket;
	private String success_action_status;
	private String googleAccessId;
	private String action;
	private String enctype;
	
	private CredentialHelper() 
	{
		JSONObject credentialDictionary=null;
		try {
			credentialDictionary = (JSONObject)new JSONParser().parse(Resources.toString(Resources.getResource("gcs_credential.json"), Charsets.UTF_8));
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.bucket=(String)credentialDictionary.get("bucket");
		this.success_action_status=(String)credentialDictionary.get("success_action_status");
		this.googleAccessId=(String)credentialDictionary.get("googleAccessId");
		this.action=(String)credentialDictionary.get("action");
		this.enctype=(String)credentialDictionary.get("enctype");
		
	}
	
	public String getBucket() { return this.bucket; }
	public String getSuccessActionStatus() { return this.success_action_status; }
	public String getGoogleAccessId() { return this.googleAccessId; }
	public String getAction() { return this.action; }
	public String getEnctype() { return this.enctype; }
	
	public static CredentialHelper getInstance() { return instance; }
}
