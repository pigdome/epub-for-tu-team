package th.ac.ipst.epub.gcs;

import org.apache.commons.codec.binary.Base64;  // From Apache Commons Codec.

// http://commons.apache.org/codec/

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.UnrecoverableKeyException;
import java.util.logging.Logger;

public final class UrlSigner {
	
	private static final Logger log = Logger.getLogger(UrlSigner.class.getName());
	static final long ONE_MINUTE_IN_MILLIS=60000;

	public static void main(String[] args) throws Exception {

		PrivateKey key = loadKeyFromPkcs12(UrlSigner.class.getClassLoader().getResourceAsStream("gcs-privatekey.p12"), "notasecret".toCharArray());
		
		// This string-to-sign is for PUT, GET, and DELETE. The string-to-sign for POST is the "policy" field in HTML upload form
		/*
		String signature = signData(key, "GET"+
				"rmYdCNHKFXam78uCt7xQLw=="+
				"text/plain"+
				"1361993085"+
				"x-goog-foo:bar,baz"+
				"x-goog-var:value"+
				"/bucket/objectname");
		System.out.println(signature);
		*/
		String stringToSign="eyJjb25kaXRpb25zIjpbeyJidWNrZXQiOiJpcHN0LW1hcmtldCJ9LFsic3RhcnRzLXdpdGgiLCIka2V5IiwiIl0sWyJjb250ZW50LWxlbmd0aC1yYW5nZSIsMCwyMDAwMDAwMF0seyJzdWNjZXNzX2FjdGlvbl9zdGF0dXMiOiIyMDEifV0sImV4cGlyYXRpb24iOiIyMDEzLTA2LTA5VDEyOjE5OjQ0WiJ9";
		String signature = signData(key, stringToSign);
		System.out.println(signature);

		URL url=null;
		try {
			url = parseUrl("http://commondatastorage.googleapis.com/ipst-epub/002001/010/Screen Shot 2013-05-22 at 2.36.50 PM.png");
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println(url.getPath());
	}
	
	public static String getSignedUrl(String originalUrl) throws UnsupportedEncodingException
	{
		URL url=null;
		try {
			url = parseUrl(originalUrl);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String bucketPlusKey=url.getPath(); // extract bucket/key from the url
		
		String httpVerb="GET";		
		Long expirationEpoch=System.currentTimeMillis()/1000L + 20 * ONE_MINUTE_IN_MILLIS / 1000L;
		String expiration=expirationEpoch.toString();
		String canonicalizedResource=bucketPlusKey;
		String contentType="";
		String contentMD5="";
		String canonicalizedExtensionHeaders="";
	
		// https://developers.google.com/storage/docs/accesscontrol#Construct-the-String
		String stringToSign = httpVerb + "\n" +
				contentMD5 + "\n" +
				contentType + "\n" +
			    expiration + "\n" +
			    canonicalizedExtensionHeaders + 
			    canonicalizedResource;

		String encodedSignature=UrlSigner.getUrlSignature(stringToSign);
		String urlEncodedSignature=URLEncoder.encode(encodedSignature, "UTF-8");
		
		String authenticatedUrl = url.toString() + "?GoogleAccessId=" + CredentialHelper.getInstance().getGoogleAccessId() + "&Signature=" + urlEncodedSignature + "&Expires=" + expiration;
		return authenticatedUrl;
	}
	
	public static URL parseUrl(String s) throws Exception {
	     URL u = new URL(s);
	     return new URI(
	            u.getProtocol(), 
	            u.getAuthority(), 
	            u.getPath(),
	            u.getQuery(), 
	            u.getRef()).
	            toURL();
	}
	
	public static String getUrlSignature(String stringToSign)
	{
		PrivateKey key;
		String signature = null;
		try {
			key = loadKeyFromPkcs12(UrlSigner.class.getClassLoader().getResourceAsStream("gcs-privatekey.p12"), "notasecret".toCharArray());
			signature = signData(key, stringToSign);		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return signature;
	}
	
	private static PrivateKey loadKeyFromPkcs12(InputStream is, char[] password) throws Exception {

		KeyStore ks = KeyStore.getInstance("PKCS12");
		try {
			ks.load(is, password);
		} catch (IOException e) {
			if (e.getCause() != null && e.getCause() instanceof UnrecoverableKeyException) {
				System.err.println("Incorrect password");
			}
			throw e;
		}
		return (PrivateKey)ks.getKey("privatekey", password);
	}

	private static String signData(PrivateKey key, String data) throws Exception {
		Signature signer = Signature.getInstance("SHA256withRSA");
		signer.initSign(key);
		signer.update(data.getBytes("UTF-8"));
		byte[] rawSignature = signer.sign();
		String encodedSignature = new String(Base64.encodeBase64(rawSignature, false), "UTF-8");
		return encodedSignature;
	}
}