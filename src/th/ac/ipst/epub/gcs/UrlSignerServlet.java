package th.ac.ipst.epub.gcs;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.security.PrivateKey;

import javax.servlet.http.*;

@SuppressWarnings("serial")
public class UrlSignerServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		// IMPORTANT: TODO: validate the user first by using session!
		
		String originalUrl=req.getParameter("originalUrl");
		
		String authenticatedUrl = UrlSigner.getSignedUrl(originalUrl);

	    resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");
	    resp.getWriter().write(authenticatedUrl);
	}

}
