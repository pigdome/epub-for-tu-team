package th.ac.ipst.epub.gcs;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.security.PrivateKey;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.*;

import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import th.ac.ipst.epub.BookCoverConnector;

import com.google.gson.GsonBuilder;

@SuppressWarnings("serial")
public class FormServlet extends HttpServlet {

	static final long ONE_MINUTE_IN_MILLIS=60000;
	static final Long UPLOAD_QUOTA=150000000L; // 150MB
	
	@SuppressWarnings("unchecked")
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String request=req.getParameter("req");
		
		if(request.equals("renewFormSignatureAndPolicy")) {
			String policy=getPolicy(null); // null indicates default expire date
			String signature=getSignature(policy);
			
			JSONObject formFields=new JSONObject();
			formFields.put("policy", policy);
			formFields.put("signature", signature);
					
			resp.setContentType("application/json");
			resp.getWriter().print(formFields.toJSONString());
		} else if(request.equals("renewFormSignatureAndPolicyForImport")) {
			long now=new Date().getTime();
			String policy=getPolicy(new Date(now + (24 * 60 * ONE_MINUTE_IN_MILLIS)));
			String signature=getSignature(policy);
			
			JSONObject formFields=new JSONObject();
			formFields.put("policy", policy);
			formFields.put("signature", signature);
					
			resp.setContentType("application/json");
			resp.getWriter().print(formFields.toJSONString());
		}
	}

	@SuppressWarnings("unchecked")
	private String getPolicy(Date expireDate) {
		
		// https://developers.google.com/storage/docs/reference-methods#policydocument
		// 
		// The following is an example of a typical policy document:
		/*
			{"expiration": "2010-06-16T11:11:11Z",
			 "conditions": [
			  ["starts-with", "$key", "" ],
			  {"acl": "bucket-owner-read" },
			  {"bucket": "travel-maps"},
			  {"success_action_redirect": "http://www.example.com/success_notification.html" },
			  ["eq", "$Content-Type", "image/jpeg" ],
			  ["content-length-range", 0, 1000000]
			  ]
			}
		*/
		
		// TODO: ถ้าดูตามนี้ policy น่าจะมีประโยชน์ กัน cross-site scripting ได้ ถ้าเรากำหนด condition ให้ว่า key ต้อง starts-with <user-id> !!!

		JSONObject policyDictionary=new JSONObject();
		
		if(expireDate==null) {
			long now=new Date().getTime();
			expireDate=new Date(now + (20 * ONE_MINUTE_IN_MILLIS)); // default 20 minutes
		}

		// The expiration time of the policy document. An expired policy document will cause the HTML form to break. This must be in ISO8601 format.
		DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX"); // Java7 already supports this (see http://stackoverflow.com/questions/10614771/java-simpledateformat-pattern-for-w3c-xml-dates-with-timezone)
		//dateFormat.setTimeZone(TimeZone.getTimeZone("UTC")); // or should it be "GMT+7"?
				
		policyDictionary.put("expiration", dateFormat.format(expireDate));

		JSONArray conditions=new JSONArray();
		JSONObject bucket=new JSONObject();
		bucket.put("bucket", CredentialHelper.getInstance().getBucket());
		JSONArray keyCondition=new JSONArray();
		keyCondition.add("starts-with"); keyCondition.add("$key"); keyCondition.add("");
		JSONArray contentLength=new JSONArray();
		contentLength.add("content-length-range"); contentLength.add(0); contentLength.add(UPLOAD_QUOTA);		
		JSONObject successActionStatus=new JSONObject();
		successActionStatus.put("success_action_status", CredentialHelper.getInstance().getSuccessActionStatus());

		conditions.add(bucket);
		conditions.add(keyCondition);
		conditions.add(contentLength);
		conditions.add(successActionStatus);
		
		policyDictionary.put("conditions", conditions);
		
		String encodedPolicyString=Base64.encodeBase64String(policyDictionary.toJSONString().getBytes()); // to be used later for getting signed in "getSignature()"
		
		//return policyDictionary.toJSONString(); // readable text for debugging
		return encodedPolicyString;
	}
	
	private String getSignature(String encodedPolicy) {
		
		return UrlSigner.getUrlSignature(encodedPolicy);
		
	}
}
