package th.ac.ipst.epub;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.inject.Named;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.datanucleus.query.JDOCursorHelper;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

public class PublicationConnector {

	private static final Integer DEFAULT_LIMIT = 25;
	
	@SuppressWarnings("unchecked")
	public static List list(Long authorId, String cursorString, Integer limit) {

		PersistenceManager mgr = null;
		Cursor cursor = null;
		List<Publication> publications = null;
		List<Publication> detachedPublications=null;
		
		try {
			mgr = getPersistenceManager();
			Query query = mgr.newQuery(Publication.class);
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				HashMap<String, Object> extensionMap = new HashMap<String, Object>();
				extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
				query.setExtensions(extensionMap);
			}

			if   ( limit == null )   {
            	limit=DEFAULT_LIMIT;
            }
        	query. setRange ( 0 , limit ) ;
        	
        	query.setOrdering("dateCreated desc");

    	    boolean isLatestVersion=true;
    	    query.setFilter("authorId==authorIdParam && isLatestVersion==isLatestVersionParam");
            query.declareParameters("Long authorIdParam, boolean isLatestVersionParam");
            
			publications = (List<Publication>) query.execute(authorId, isLatestVersion);
			cursor = JDOCursorHelper.getCursor(publications);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();
			
			detachedPublications=new ArrayList<Publication>(mgr.detachCopyAll(publications));

		} finally {
			mgr.close();
		}
		
		return Arrays.asList(detachedPublications, cursorString);
	}
	
	public static Publication getById(Long id) {

		PersistenceManager mgr = getPersistenceManager();
		Publication publication = null;

		try {
			publication = mgr.getObjectById(Publication.class, id);
		} finally {
			mgr.close();
		}
		return publication;		
	}
		
	public static Publication insert(Publication publication) 
	{
		PersistenceManager mgr = getPersistenceManager();
		Publication detachedPublication=null;
		try {
			if(publication.getId() != null){ // ถ้าเป็น null assume ได้ว่า เป็น entity ใหม่ ไม่งั้นต้องเช็คว่ามีรึยัง
				if (containsPublication(publication)) {
					throw new EntityExistsException("Publication entity already exists");
				}
			}

			Long now=System.currentTimeMillis()/1000L;
			
			// TODO: ตอนนี้ยังไม่ support versioning fix ไว้ เป็น "v1" กับ "true" ไปก่อน
			if(publication.getDateCreated()==null) // กรณีไม่ null แปลว่า แค่เปลี่ยน version ใหม่
				publication.setDateCreated(now);
			publication.setVersionDateCreated(now);
			publication.setType("textbook"); // TODO: textbook, or concept แต่ตอนนี้มีแบบเดียวไปก่อน
			if(publication.getVersion()==null) {
				publication.setVersion("v1");
				publication.setPublicationGroupId(now); // TODO: implement versioning capability with this id
			}
			
			publication.setStatus("in progress");
			publication.setIsLatestVersion(true); // assume ว่าการ insert ใหม่คือ version ใหม่ล่าสุด (แต่ตอน update แล้วแต่ user ตั้งค่า ซึ่งสามารถ set logic จัดการไว้ในฝั่ง client)
			
			mgr.makePersistent(publication);
			
			detachedPublication=mgr.detachCopy(publication);
		} finally {
			mgr.close();
		}
		
		return detachedPublication;
	}
	
	public static void remove(Long id) 
	{
		PersistenceManager mgr = getPersistenceManager();
		Publication publication = null;
		try {
			publication = mgr.getObjectById(Publication.class, id);
			mgr.deletePersistent(publication);
		} finally {
			mgr.close();
		}
	}

	// TODO: If we want to protect some fields from modification, do this: fetch old entity by id and set new values from the entity parameter 
	public static Publication update(Publication publication) {
		PersistenceManager mgr = getPersistenceManager();
		Publication detachedPublication=null;
		try {
			if (!containsPublication(publication)) {
				throw new EntityNotFoundException("Publication entity does not exist");
			}
			mgr.makePersistent(publication);
			detachedPublication=mgr.detachCopy(publication);
		} finally {
			mgr.close();
		}
		return detachedPublication;
	}
	
	private static boolean containsPublication(Publication publication) {
		PersistenceManager mgr = getPersistenceManager();
		boolean contains = true;
		try {
			mgr.getObjectById(Publication.class, publication.getId());
		} catch (javax.jdo.JDOObjectNotFoundException ex) {
			contains = false;
		} finally {
			mgr.close();
		}
		return contains;
	}
	
	private static PersistenceManager getPersistenceManager() {
		return PMF.get().getPersistenceManager();
	}
}
