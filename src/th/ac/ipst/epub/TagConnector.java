package th.ac.ipst.epub;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

public class TagConnector {

	private static final Integer DEFAULT_LIMIT = 25;
	
	public static Tag getById(Long id) {

		PersistenceManager mgr = getPersistenceManager();
		Tag tag = null;

		try {
			tag = mgr.getObjectById(Tag.class, id);
		} finally {
			mgr.close();
		}
		return tag;		
	}

	public static Tag getTagInfo(Long authorId, String tagName) {	
		PersistenceManager mgr = null;
		List<Tag> tags = null;
		Tag detachedTag=null;
		Query query=null;
		
		try {
			mgr = getPersistenceManager();
			query = mgr.newQuery(Tag.class);

        	query.setRange(0, 1) ;
        	
    	    query.setFilter("authorId==authorIdParam && name==tagNameParam");
            query.declareParameters("Long authorIdParam, String tagNameParam");
            
			tags = (List<Tag>) query.execute(authorId, tagName);
			
			if(tags.size()>0)
				detachedTag=mgr.detachCopy(tags.get(0));
			else {
				detachedTag=new Tag();
				detachedTag.setAuthorId(authorId);
				detachedTag.setName(tagName);
				detachedTag.setCount(0L);
			}

		} finally {
			query.closeAll();
			mgr.close();
		}
		
		return detachedTag;
	}

	public static Tag insert(Tag tag) 
	{
		PersistenceManager mgr = getPersistenceManager();
		Tag detachedTag=null;
		try {
			if(tag.getId() != null){ // ถ้าเป็น null assume ได้ว่า เป็น entity ใหม่ ไม่งั้นต้องเช็คว่ามีรึยัง
				if (containsTag(tag)) {
					throw new EntityExistsException("Tag entity already exists");
				}
			}

			mgr.makePersistent(tag);
			
			detachedTag=mgr.detachCopy(tag);
		} finally {
			mgr.close();
		}
		
		return detachedTag;
	}
	
	public static void remove(Long id) 
	{
		PersistenceManager mgr = getPersistenceManager();
		Tag tag = null;
		try {
			tag = mgr.getObjectById(Tag.class, id);
			mgr.deletePersistent(tag);
		} finally {
			mgr.close();
		}
	}

	// TODO: If we want to protect some fields from modification, do this: fetch old entity by id and set new values from the entity parameter 
	public static Tag update(Tag tag) {
		PersistenceManager mgr = getPersistenceManager();
		Tag detachedTag=null;
		try {
			if (!containsTag(tag)) {
				throw new EntityNotFoundException("Tag entity does not exist");
			}
			mgr.makePersistent(tag);
			detachedTag=mgr.detachCopy(tag);
		} finally {
			mgr.close();
		}
		return detachedTag;
	}
	
	private static boolean containsTag(Tag tag) {
		PersistenceManager mgr = getPersistenceManager();
		boolean contains = true;
		try {
			mgr.getObjectById(Tag.class, tag.getId());
		} catch (javax.jdo.JDOObjectNotFoundException ex) {
			contains = false;
		} finally {
			mgr.close();
		}
		return contains;
	}
	
	private static PersistenceManager getPersistenceManager() {
		return PMF.get().getPersistenceManager();
	}
}
