package th.ac.ipst.epub;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import com.google.appengine.api.datastore.Text;

@PersistenceCapable(detachable="true")
public class BookCover 
{
    @PrimaryKey
    @Persistent ( valueStrategy =  IdGeneratorStrategy. IDENTITY )
    Long id;

    String coverUrl; 
    Long authorId;
    Long dateAdded;

    public Long getId()
    {
    	return this.id;
    }

    public void setId(Long id)
    {
    	this.id=id;
    }
    
    public Long getAuthorId()
    {
    	return this.authorId;
    }

    public void setAuthorId(Long authorId)
    {
    	this.authorId=authorId;
    }

    public Long getDateAdded()
    {
    	return this.dateAdded;
    }
    
    public void setDateAdded(Long dateAdded)
    {
    	this.dateAdded=dateAdded;
    }
    
    public String getCoverUrl()
    {
    	return this.coverUrl;
    }
    
    public void setCoverUrl(String coverUrl)
    {
    	this.coverUrl=coverUrl;
    }

}
