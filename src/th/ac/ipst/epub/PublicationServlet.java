package th.ac.ipst.epub;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.*;

import th.ac.ipst.epub.gcs.EpubHelper;

import com.google.appengine.datanucleus.query.JDOCursorHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

@SuppressWarnings("serial")
public class PublicationServlet extends HttpServlet {
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		String request=req.getParameter("req");
		
		if(request.equals("list")) {
			Long authorId=Long.valueOf(req.getParameter("authorId"));
			String cursorString=req.getParameter("cursor");
			String limitString=req.getParameter("limit");
			Integer limit=null; 
			if(limitString!=null)
				limit=Integer.valueOf(req.getParameter("limit"));
			List<Publication> publicationsWithCursor=PublicationConnector.list(authorId, cursorString, limit);
	        resp.setContentType("application/json");
	        resp.setCharacterEncoding("UTF-8");
	        resp.getWriter().write(new GsonBuilder().create().toJson(publicationsWithCursor));
		} 
		else if(request.equals("getById")) {
			
			// IMPORTANT: TODO: ต้องเช็คว่า authorId นี้ (ยังไม่ได้ใส่เข้ามา) มีสิทธิ์ดู/แก้ไข publicationId นี้หรือไม่ ไม่งั้น user ไหนก็แก้ publication ใครก็ได้ ขอแค่รู้ publicationId
			
			Long id=Long.valueOf(req.getParameter("id"));
			Publication publication=PublicationConnector.getById(id);
	        resp.setContentType("application/json");
	        resp.setCharacterEncoding("UTF-8");
	        resp.getWriter().write(new GsonBuilder().create().toJson(publication));			
		}
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String request=req.getParameter("req");
		
		if(request.equals("insert")) {
			Publication publication=new Gson().fromJson(req.getParameter("publication"), Publication.class);
			Publication insertedPublication=PublicationConnector.insert(publication);
			
			String epubPackagePath=insertedPublication.getGcsEpubPackagePath();
			uploadEpubSetupFilesToGCS(epubPackagePath);
			
	        resp.setContentType("application/json");
	        resp.setCharacterEncoding("UTF-8");
			resp.getWriter().write(new GsonBuilder().create().toJson(insertedPublication));
		} 
		else if(request.equals("update")) {
			Publication publication=new Gson().fromJson(req.getParameter("publication"), Publication.class);
			Publication updatedPublication=PublicationConnector.update(publication);			
	        resp.setContentType("application/json");
	        resp.setCharacterEncoding("UTF-8");
			resp.getWriter().write(new GsonBuilder().create().toJson(updatedPublication));
		}
		else if(request.equals("remove")) {
			Long publicationId=Long.valueOf(req.getParameter("publicationId"));
			PublicationConnector.remove(publicationId);
			
			// TODO: perform cascading delete for all relevant items of target publicationId
		}
	}
	
	private void uploadEpubSetupFilesToGCS(String epubPackagePath) throws IOException
	{
		EpubHelper.getInstance().postMimetypeFile(epubPackagePath);
		EpubHelper.getInstance().postContainerFile(epubPackagePath);
	}
}
