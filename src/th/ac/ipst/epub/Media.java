package th.ac.ipst.epub;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import com.google.appengine.api.datastore.Text;

@PersistenceCapable(detachable="true")
public class Media 
{
    @PrimaryKey
    @Persistent ( valueStrategy =  IdGeneratorStrategy. IDENTITY )
    Long id;

    String mediaUrl; 
    Long authorId;
    Long publicationId; // เอาไว้ filter ดูเฉพาะ media file ที่อัพโหลดมาใน publicationId นี้เท่านั้น (หนังสือเล่มนี้เท่านั้น)
    Long dateAdded;
    String type; // img, video, audio

    public String getType()
    {
    	return this.type;
    }
    
    public void setType(String type) 
    {
    	this.type=type;
    }
    
    public Long getId()
    {
    	return this.id;
    }

    public void setId(Long id)
    {
    	this.id=id;
    }
    
    public Long getAuthorId()
    {
    	return this.authorId;
    }

    public void setAuthorId(Long authorId)
    {
    	this.authorId=authorId;
    }

    public Long getPublicationId()
    {
    	return this.publicationId;
    }

    public void setPublicationId(Long publicationId)
    {
    	this.publicationId=publicationId;
    }

    public Long getDateAdded()
    {
    	return this.dateAdded;
    }
    
    public void setDateAdded(Long dateAdded)
    {
    	this.dateAdded=dateAdded;
    }
    
    public String getMediaUrl()
    {
    	return this.mediaUrl;
    }
    
    public void setMediaUrl(String mediaUrl)
    {
    	this.mediaUrl=mediaUrl;
    }

}
