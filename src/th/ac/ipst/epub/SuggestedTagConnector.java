package th.ac.ipst.epub;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Named;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.datanucleus.query.JDOCursorHelper;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.jdo.Transaction;

public class SuggestedTagConnector {

	private static final Integer DEFAULT_LIMIT = 25;
	private static final Logger log = Logger.getLogger(SuggestedTagConnector.class.getName());
		
	@SuppressWarnings("unchecked")
	public static List<SuggestedTag> list(String queryString, Integer limit) {

		PersistenceManager mgr = null;
		List<SuggestedTag> suggestedTags = null;
		List<SuggestedTag> detachedSuggestedTags=null;
		Query query=null;
		
		try {
			mgr = getPersistenceManager();

		    query = mgr.newQuery(SuggestedTag.class);
        	
	        String queryStringParam=queryString.toLowerCase(); // lower case!
	        String queryStringParam2=queryStringParam+"\ufffd";
	        	
		    // this technique is from http://stackoverflow.com/questions/47786/google-app-engine-is-it-possible-to-do-a-gql-like-query
		    query.setFilter("name >= queryStringParam && name < queryStringParam2");
		    query.declareParameters("String queryStringParam, String queryStringParam2");

        	query.setOrdering("name asc");

		    if   ( limit == null )   {
            	limit=DEFAULT_LIMIT;
            }
        	query.setRange(0 , limit) ;

			suggestedTags = (List<SuggestedTag>) query.execute(queryStringParam, queryStringParam2);

			detachedSuggestedTags=new ArrayList<SuggestedTag>(mgr.detachCopyAll(suggestedTags));

		} finally {
			query.closeAll();
			mgr.close();
		}
		
		return detachedSuggestedTags;
	}
	
	public static SuggestedTag getByName(String name) {
		PersistenceManager mgr = null;
		List<SuggestedTag> suggestedTags = null;
		SuggestedTag detachedSuggestedTag=null;
		Query query=null;
		
		try {
			mgr = getPersistenceManager();
			query = mgr.newQuery(SuggestedTag.class);

        	query.setRange(0, 1) ;
        	
    	    query.setFilter("name==nameParam");
            query.declareParameters("String nameParam");
            
			suggestedTags = (List<SuggestedTag>) query.execute(name);
			
			if(suggestedTags.size()>0)
				detachedSuggestedTag=mgr.detachCopy(suggestedTags.get(0));
			else { // ถ้าไม่มี สร้างใหม่เลย
				detachedSuggestedTag=new SuggestedTag();
				detachedSuggestedTag.setName(name);
				detachedSuggestedTag.setTagCount(0L);
				detachedSuggestedTag.setUserCount(0L);
			}

		} finally {
			query.closeAll();
			mgr.close();
		}
		
		return detachedSuggestedTag;	
	}
		
	public static SuggestedTag insert(SuggestedTag suggestedTag) 
	{
		PersistenceManager mgr = getPersistenceManager();
		SuggestedTag detachedSuggestedTag=null;
		try {
			if(suggestedTag.getId() != null){ // ถ้าเป็น null assume ได้ว่า เป็น entity ใหม่ ไม่งั้นต้องเช็คว่ามีรึยัง
				if (containsSuggestedTag(suggestedTag)) {
					throw new EntityExistsException("SuggestedTag entity already exists");
				}
			}

			mgr.makePersistent(suggestedTag);
			
			detachedSuggestedTag=mgr.detachCopy(suggestedTag);
		} finally {
			mgr.close();
		}
		
		return detachedSuggestedTag;
	}
	
	public static void remove(Long id) 
	{
		PersistenceManager mgr = getPersistenceManager();
		SuggestedTag suggestedTag = null;
		try {
			suggestedTag = mgr.getObjectById(SuggestedTag.class, id);
			mgr.deletePersistent(suggestedTag);
		} finally {
			mgr.close();
		}
	}

	// TODO: If we want to protect some fields from modification, do this: fetch old entity by id and set new values from the entity parameter 
	public static SuggestedTag update(SuggestedTag suggestedTag) {
		PersistenceManager mgr = getPersistenceManager();
		SuggestedTag detachedSuggestedTag=null;
		try {
			if (!containsSuggestedTag(suggestedTag)) {
				throw new EntityNotFoundException("SuggestedTag entity does not exist");
			}
			mgr.makePersistent(suggestedTag);
			detachedSuggestedTag=mgr.detachCopy(suggestedTag);
		} finally {
			mgr.close();
		}
		return detachedSuggestedTag;
	}

	private static void updateInTransaction(SuggestedTag suggestedTag) {
		PersistenceManager mgr = getPersistenceManager();
		Transaction tx = mgr.currentTransaction();

        try {
        	tx.begin();
        	mgr.makePersistent(suggestedTag);
            tx.commit();
        }
        finally {
            if (tx.isActive()) {
				log.severe("Fail to updateInTransaction with" +
				        " tagName: "+suggestedTag.getName()+
						" tagCount: "+suggestedTag.getTagCount()+
						" userCount: "+suggestedTag.getUserCount());
                tx.rollback();
            }
            mgr.close();
        }		
	}

	public static void updates(ArrayList<String> tagsToAdd, ArrayList<String> tagsToRemove, Long authorId) {
		// TODO: IMPORTANT:
		// assuming that only SuggestedTag is subject to simultaneous update so we embrace it within Transaction, and a user never updates the same Tag across machines at the same time
		// if so, we should create an entity group (XG transaction only allow 5 entities to be updated simultaneously)
		// http://stackoverflow.com/questions/2185763/how-to-create-entities-in-one-entity-group
		// of course, this poses a risk of update anomaly (e.g. some non-trasactional updates fail, and can't rollback)

		ArrayList entitiesForBatchUpdate=new ArrayList();
		
		if(tagsToAdd!=null) {
			for(String tag : tagsToAdd) {
				SuggestedTag suggestedTag=SuggestedTagConnector.getByName(tag);
				suggestedTag.setTagCount(suggestedTag.getTagCount()+1);
	
				Tag userTag=TagConnector.getTagInfo(authorId, tag);
				if(userTag.getCount()==0) {
					suggestedTag.setUserCount(suggestedTag.getUserCount()+1);
				}
				userTag.setCount(userTag.getCount()+1);
				entitiesForBatchUpdate.add(userTag);
	
				// entities.add(suggestedTag); ต้องครอบด้วย transaction ตามที่อธิบายไว้ด้านบน
				SuggestedTagConnector.updateInTransaction(suggestedTag);
			}
		}
		
		if(tagsToRemove!=null) {
			for(String tag : tagsToRemove) {
				SuggestedTag suggestedTag=SuggestedTagConnector.getByName(tag);
				if(suggestedTag.getTagCount()>0)
					suggestedTag.setTagCount(suggestedTag.getTagCount()-1);
				
				Tag userTag=TagConnector.getTagInfo(authorId, tag);
				if(userTag.getCount()>0)
					userTag.setCount(userTag.getCount()-1);
				if(userTag.getCount()==0) {
					if(suggestedTag.getUserCount()>0)
						suggestedTag.setUserCount(suggestedTag.getUserCount()-1);
				}
				entitiesForBatchUpdate.add(userTag);
	
				// entities.add(suggestedTag); ต้องครอบด้วย transaction ตามที่อธิบายไว้ด้านบน
				SuggestedTagConnector.updateInTransaction(suggestedTag);
			}
		}

		PersistenceManager mgr = getPersistenceManager();
		try {
			mgr = getPersistenceManager();
			mgr.makePersistentAll(entitiesForBatchUpdate);
		} finally {
			mgr.close();
		}

	}
	
	private static boolean containsSuggestedTag(SuggestedTag suggestedTag) {
		PersistenceManager mgr = getPersistenceManager();
		boolean contains = true;
		try {
			mgr.getObjectById(SuggestedTag.class, suggestedTag.getId());
		} catch (javax.jdo.JDOObjectNotFoundException ex) {
			contains = false;
		} finally {
			mgr.close();
		}
		return contains;
	}
	
	private static PersistenceManager getPersistenceManager() {
		return PMF.get().getPersistenceManager();
	}
}
