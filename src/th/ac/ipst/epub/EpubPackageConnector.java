package th.ac.ipst.epub;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.inject.Named;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.datanucleus.query.JDOCursorHelper;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

public class EpubPackageConnector {

	public static EpubPackage getById(Long id) {

		PersistenceManager mgr = getPersistenceManager();
		EpubPackage epubPackage = null;

		try {
			epubPackage = mgr.getObjectById(EpubPackage.class, id);
		} finally {
			mgr.close();
		}
		return epubPackage;		
	}

	public static EpubPackage getByPublicationId(Long publicationId) {
		PersistenceManager mgr = null;
		EpubPackage detachedEpubPackage = null;
		
		try {
			mgr = getPersistenceManager();
			Query query = mgr.newQuery(EpubPackage.class);

    	    query.setFilter("publicationId==publicationIdParam");
            query.declareParameters("Long publicationIdParam");
            
			List<EpubPackage> epubPackages = (List<EpubPackage>) query.execute(publicationId);

			if(epubPackages!=null && epubPackages.size()>0)
				detachedEpubPackage=mgr.detachCopy(epubPackages.get(0));

		} finally {
			mgr.close();
		}
		
		return detachedEpubPackage;
	}

	public static EpubPackage insert(EpubPackage epubPackage) 
	{
		PersistenceManager mgr = getPersistenceManager();
		EpubPackage detachedEpubPackage=null;
		try {
			if(epubPackage.getId() != null){ // ถ้าเป็น null assume ได้ว่า เป็น entity ใหม่ ไม่งั้นต้องเช็คว่ามีรึยัง
				if (containsEpubPackage(epubPackage)) {
					throw new EntityExistsException("EpubPackage entity already exists");
				}
			}

			mgr.makePersistent(epubPackage);
			detachedEpubPackage=mgr.detachCopy(epubPackage);
		
		} finally {
			mgr.close();
		}
		
		return detachedEpubPackage;
	}
	
	public static void remove(Long id) 
	{
		PersistenceManager mgr = getPersistenceManager();
		EpubPackage epubPackage = null;
		try {
			epubPackage = mgr.getObjectById(EpubPackage.class, id);
			mgr.deletePersistent(epubPackage);
		} finally {
			mgr.close();
		}
	}

	// TODO: If we want to protect some fields from modification, do this: fetch old entity by id and set new values from the entity parameter 
	public static EpubPackage update(EpubPackage epubPackage) {
		PersistenceManager mgr = getPersistenceManager();
		EpubPackage detachedEpubPackage=null;
		try {
			if (!containsEpubPackage(epubPackage)) {
				throw new EntityNotFoundException("EpubPackage entity does not exist");
			}
			mgr.makePersistent(epubPackage);
			detachedEpubPackage=mgr.detachCopy(epubPackage);
		} finally {
			mgr.close();
		}
		return detachedEpubPackage;
	}
	
	private static boolean containsEpubPackage(EpubPackage epubPackage) {
		PersistenceManager mgr = getPersistenceManager();
		boolean contains = true;
		try {
			mgr.getObjectById(EpubPackage.class, epubPackage.getId());
		} catch (javax.jdo.JDOObjectNotFoundException ex) {
			contains = false;
		} finally {
			mgr.close();
		}
		return contains;
	}
	
	private static PersistenceManager getPersistenceManager() {
		return PMF.get().getPersistenceManager();
	}
}
