package th.ac.ipst.epub;

import java.io.IOException;
import java.nio.channels.Channels;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.datanucleus.query.JDOCursorHelper;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsInputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;
import com.google.common.io.ByteStreams;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

public class MediaConnector {

	private static final Logger log = Logger.getLogger(MediaConnector.class.getName());

	private static final Integer DEFAULT_LIMIT = 1000;
	
	private final static GcsService gcsService = GcsServiceFactory.createGcsService(RetryParams.getDefaultInstance());
	private static final String BUCKET="ipst-epub";
	
	@SuppressWarnings("unchecked")
	public static List list(Long authorId, String type, String cursorString, Integer limit) {

		PersistenceManager mgr = null;
		Cursor cursor = null;
		List<Media> medias = null;
		List<Media> detachedMedias=null;

		try {
			mgr = getPersistenceManager();
			Query query = mgr.newQuery(Media.class);
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				HashMap<String, Object> extensionMap = new HashMap<String, Object>();
				extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
				query.setExtensions(extensionMap);
			}

			if   ( limit == null )   {
            	limit=DEFAULT_LIMIT;
            }
        	query. setRange ( 0 , limit ) ;
        	
	    	query.setOrdering("dateAdded desc");

    	    query.setFilter("authorId==authorIdParam && type==typeParam");
            query.declareParameters("Long authorIdParam, String typeParam");

			medias = (List<Media>) query.execute(authorId, type);
			cursor = JDOCursorHelper.getCursor(medias);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();

			detachedMedias=new ArrayList<Media>(mgr.detachCopyAll(medias));
		} finally {
			mgr.close();
		}
		
		return Arrays.asList(detachedMedias, cursorString);
	}
	
	public static Media getById(Long id) {

		PersistenceManager mgr = getPersistenceManager();
		Media media = null;

		try {
			media = mgr.getObjectById(Media.class, id);
		} finally {
			mgr.close();
		}
		return media;		
	}
	
	public static Media insert(Media media) 
	{
		PersistenceManager mgr = getPersistenceManager();
		Media detachedMedia=null;
		try {
			if(media.getId() != null){ // ถ้าเป็น null assume ได้ว่า เป็น entity ใหม่ ไม่งั้นต้องเช็คว่ามีรึยัง
				if (containsMedia(media)) {
					throw new EntityExistsException("Media entity already exists");
				}
			}
			
			media.setDateAdded(System.currentTimeMillis()/1000L);

			mgr.makePersistent(media);
			
			detachedMedia=mgr.detachCopy(media);
		} finally {
			mgr.close();
		}
		
		return detachedMedia;
	}

	private static void removeMediaFile(String url) throws IOException 
	{
		String gcsObjectPath=url.split("/ipst-epub/")[1]; // ตัด url ด้วย bucket name เพื่อเอาเฉพาะ gcs object path"
		
		GcsFilename gcsFileName=new GcsFilename(BUCKET, gcsObjectPath);
		gcsService.delete(gcsFileName);
	}
	
	public static boolean remove(Long id) 
	{
		PersistenceManager mgr = getPersistenceManager();
		boolean succeeded=false;
		try {
			if (containsMediaForId(id)) {
				succeeded=false;
			}

			Media media = mgr.getObjectById(Media.class, id);

			removeMediaFile(media.getMediaUrl());

			mgr.deletePersistent(media);

			succeeded=true;
		} 
		catch(Exception e) {
			succeeded=false;
		}
		finally {
			mgr.close();
		}		
	
		return succeeded;
	}

	/* TODO: เช็คว่า update แบบนี้ work รึเปล่า จำได้ว่า ต้อง get ออกมา (จาก author.getId()) แล้ว set ค่ากลับเข้าไป จริงๆแล้ว แบบนี้จะ safe กว่าด้วย เพราะบังคับได้ว่า field ไหนเปลี่ยนได้บ้าง ignore field ไหนบ้าง (แค่เสียเวลา get entity เก่าออกมาก่อน)
	public Author updateAuthor(Author author) {
		PersistenceManager mgr = getPersistenceManager();
		try {
			if (!containsAuthor(author)) {
				throw new EntityNotFoundException("Object does not exist");
			}
			mgr.makePersistent(author);
		} finally {
			mgr.close();
		}
		return author;
	}
	*/
	
	private static boolean containsMediaForId(Long id) {
		PersistenceManager mgr = getPersistenceManager();
		boolean contains = true;
		try {
			mgr.getObjectById(Media.class, id);
		} catch (javax.jdo.JDOObjectNotFoundException ex) {
			contains = false;
		} finally {
			mgr.close();
		}
		return contains;		
	}
	
	private static boolean containsMedia(Media media) {
		PersistenceManager mgr = getPersistenceManager();
		boolean contains = true;
		try {
			mgr.getObjectById(Media.class, media.getId());
		} catch (javax.jdo.JDOObjectNotFoundException ex) {
			contains = false;
		} finally {
			mgr.close();
		}
		return contains;
	}
	
	private static PersistenceManager getPersistenceManager() {
		return PMF.get().getPersistenceManager();
	}
}
