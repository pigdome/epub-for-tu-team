package th.ac.ipst.epub;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.*;

import com.google.appengine.datanucleus.query.JDOCursorHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

@SuppressWarnings("serial")
public class AuthorServlet extends HttpServlet {
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String request=req.getParameter("req");
		
		if(request.equals("logout")) {
			HttpSession session=req.getSession();
			session.invalidate();
		}
	}
}
