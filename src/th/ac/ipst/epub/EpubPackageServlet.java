package th.ac.ipst.epub;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.*;

import th.ac.ipst.epub.gcs.EpubHelper;
import th.ac.ipst.epub.gcs.UrlSigner;

import com.google.appengine.datanucleus.query.JDOCursorHelper;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

@SuppressWarnings("serial")
public class EpubPackageServlet extends HttpServlet { // จัดการเกี่ยวกับ metadata อย่างเดียว ส่วนการจัดการเกี่ยวกับอัพโหลดไฟล์ทำจากฝั่ง client ได้เลย
	
	private static final String GCS_ROOT_URL="http://commondatastorage.googleapis.com/ipst-epub/";

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		String request=req.getParameter("req");
		
		if(request.equals("get")) {
			Long publicationId=Long.valueOf(req.getParameter("publicationId"));

			EpubPackage epubPackage=EpubPackageConnector.getByPublicationId(publicationId);
	        resp.setContentType("application/json");
	        resp.setCharacterEncoding("UTF-8");
	        if(epubPackage!=null)
	        	resp.getWriter().write(new GsonBuilder().create().toJson(epubPackage));
	        else
	        	resp.setStatus(404);
		} else if(request.equals("genEpub")) {
			Long publicationId=Long.valueOf(req.getParameter("publicationId"));

			Publication publication=PublicationConnector.getById(publicationId);
			EpubPackage epubPackage=EpubPackageConnector.getByPublicationId(publicationId);
			
			boolean succeeded=EpubHelper.getInstance().genEpub(publication, epubPackage);
			if(succeeded) {
				String signedEpubUrl=UrlSigner.getSignedUrl(GCS_ROOT_URL + publication.getEpubRelativeUrl());
				resp.setContentType("text/html");
		        resp.setCharacterEncoding("UTF-8");
			    resp.getWriter().write(signedEpubUrl);
			} else {
	        	resp.setStatus(404);				
			}
		}
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String request=req.getParameter("req");
		
		if(request.equals("insert")) {
			EpubPackage epubPackage=new Gson().fromJson(req.getParameter("epubPackage"), EpubPackage.class);
			EpubPackage insertedEpubPackage=EpubPackageConnector.insert(epubPackage);
			
	        resp.setContentType("application/json");
	        resp.setCharacterEncoding("UTF-8");
			resp.getWriter().write(new GsonBuilder().create().toJson(insertedEpubPackage));
		} else if(request.equals("update")) {
			EpubPackage epubPackage=new Gson().fromJson(req.getParameter("epubPackage"), EpubPackage.class);
			EpubPackage updatedEpubPackage=EpubPackageConnector.update(epubPackage);
			
	        resp.setContentType("application/json");
	        resp.setCharacterEncoding("UTF-8");
			resp.getWriter().write(new GsonBuilder().create().toJson(updatedEpubPackage));
		}
	}
}
