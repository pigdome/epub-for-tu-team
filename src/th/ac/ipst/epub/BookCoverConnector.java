package th.ac.ipst.epub;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.datanucleus.query.JDOCursorHelper;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

public class BookCoverConnector {

	private static final Logger log = Logger.getLogger(BookCoverConnector.class.getName());

	private static final Integer DEFAULT_LIMIT = 25;
	
	@SuppressWarnings("unchecked")
	public static List list(Long authorId, String cursorString, Integer limit) {

		PersistenceManager mgr = null;
		Cursor cursor = null;
		List<BookCover> bookCovers = null;
		List<BookCover> detachedBookCovers=null;

		try {
			mgr = getPersistenceManager();
			Query query = mgr.newQuery(BookCover.class);
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				HashMap<String, Object> extensionMap = new HashMap<String, Object>();
				extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
				query.setExtensions(extensionMap);
			}

			if   ( limit == null )   {
            	limit=DEFAULT_LIMIT;
            }
        	query. setRange ( 0 , limit ) ;
        	
	    	query.setOrdering("dateAdded desc");

    	    query.setFilter("authorId==authorIdParam");
            query.declareParameters("Long authorIdParam");

			bookCovers = (List<BookCover>) query.execute(authorId);
			cursor = JDOCursorHelper.getCursor(bookCovers);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();

			detachedBookCovers=new ArrayList<BookCover>(mgr.detachCopyAll(bookCovers));
		} finally {
			mgr.close();
		}
		
		return Arrays.asList(detachedBookCovers, cursorString);
	}
	
	public static BookCover getById(Long id) {

		PersistenceManager mgr = getPersistenceManager();
		BookCover bookCover = null;

		try {
			bookCover = mgr.getObjectById(BookCover.class, id);
		} finally {
			mgr.close();
		}
		return bookCover;		
	}
	
	public static void insert(BookCover bookCover) 
	{
		PersistenceManager mgr = getPersistenceManager();
		try {
			if(bookCover.getId() != null){ // ถ้าเป็น null assume ได้ว่า เป็น entity ใหม่ ไม่งั้นต้องเช็คว่ามีรึยัง
				if (containsBookCover(bookCover)) {
					throw new EntityExistsException("BookCover entity already exists");
				}
			}
			
			bookCover.setDateAdded(System.currentTimeMillis()/1000L);

			mgr.makePersistent(bookCover);
		} finally {
			mgr.close();
		}
	}
	
	/* TODO: เช็คว่า update แบบนี้ work รึเปล่า จำได้ว่า ต้อง get ออกมา (จาก author.getId()) แล้ว set ค่ากลับเข้าไป จริงๆแล้ว แบบนี้จะ safe กว่าด้วย เพราะบังคับได้ว่า field ไหนเปลี่ยนได้บ้าง ignore field ไหนบ้าง (แค่เสียเวลา get entity เก่าออกมาก่อน)
	public Author updateAuthor(Author author) {
		PersistenceManager mgr = getPersistenceManager();
		try {
			if (!containsAuthor(author)) {
				throw new EntityNotFoundException("Object does not exist");
			}
			mgr.makePersistent(author);
		} finally {
			mgr.close();
		}
		return author;
	}
	*/
	
	private static boolean containsBookCover(BookCover bookCover) {
		PersistenceManager mgr = getPersistenceManager();
		boolean contains = true;
		try {
			mgr.getObjectById(BookCover.class, bookCover.getId());
		} catch (javax.jdo.JDOObjectNotFoundException ex) {
			contains = false;
		} finally {
			mgr.close();
		}
		return contains;
	}
	
	private static PersistenceManager getPersistenceManager() {
		return PMF.get().getPersistenceManager();
	}
}
