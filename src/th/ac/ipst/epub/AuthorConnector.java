package th.ac.ipst.epub;

import java.util.HashMap;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.datanucleus.query.JDOCursorHelper;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

public class AuthorConnector {

	private static final Integer DEFAULT_LIMIT = 25;
	
	@SuppressWarnings("unchecked")
	public static List<Author> list(String cursorString, Integer limit, String order) {

		PersistenceManager mgr = null;
		Cursor cursor = null;
		List<Author> authors = null;

		try {
			mgr = getPersistenceManager();
			Query query = mgr.newQuery(Author.class);
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				HashMap<String, Object> extensionMap = new HashMap<String, Object>();
				extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
				query.setExtensions(extensionMap);
			}

			if   ( limit == null )   {
            	limit=DEFAULT_LIMIT;
            }
        	query. setRange ( 0 , limit ) ;
        	
    	    if (order != null) {
    	    	if (order.equals("new")) {
    	    		query.setOrdering("dateOfMember desc");
    		    } else if (order.equals("publication-count")) {
    		        query.setOrdering("publicationCount desc");
    		    }
    	    } else {
	    		query.setOrdering("dateOfMember desc");
    	    }

			authors = (List<Author>) query.execute();
			cursor = JDOCursorHelper.getCursor(authors);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();

		} finally {
			mgr.close();
		}
		
		return authors;
	}
	
	public static Author getById(Long id) {

		PersistenceManager mgr = getPersistenceManager();
		Author author = null;

		try {
			author = mgr.getObjectById(Author.class, id);
		} finally {
			mgr.close();
		}
		return author;		
	}
	
	@SuppressWarnings("unchecked")
	public static Author getByEmail(String email) 
	{
		PersistenceManager mgr = getPersistenceManager();
		Author author = null;
		try {
          Query query =  mgr. newQuery ( Author. class ) ;
          query.setFilter("email==emailParam");
          query.declareParameters("String emailParam");
          List<Author> results = (List<Author>) query.execute(email);
          if(results.size()>0) {
          	author=results.get(0);
          }
          
		} finally {
			mgr.close();
		}
		
		return author;		
	}
	
	public static void insert(Author author) 
	{
		PersistenceManager mgr = getPersistenceManager();
		try {
			if(author.getId() != null){ // ถ้าเป็น null assume ได้ว่า เป็น entity ใหม่ ไม่งั้นต้องเช็คว่ามีรึยัง
				if (containsAuthor(author)) {
					throw new EntityExistsException("Author entity already exists");
				}
			}
			
			author.setDateOfMember(System.currentTimeMillis()/1000L);
			author.setPublicationCount(0L);

			mgr.makePersistent(author);
		} finally {
			mgr.close();
		}
	}
	
	/* TODO: เช็คว่า update แบบนี้ work รึเปล่า จำได้ว่า ต้อง get ออกมา (จาก author.getId()) แล้ว set ค่ากลับเข้าไป จริงๆแล้ว แบบนี้จะ safe กว่าด้วย เพราะบังคับได้ว่า field ไหนเปลี่ยนได้บ้าง ignore field ไหนบ้าง (แค่เสียเวลา get entity เก่าออกมาก่อน)
	public Author updateAuthor(Author author) {
		PersistenceManager mgr = getPersistenceManager();
		try {
			if (!containsAuthor(author)) {
				throw new EntityNotFoundException("Object does not exist");
			}
			mgr.makePersistent(author);
		} finally {
			mgr.close();
		}
		return author;
	}
	*/
	
	private static boolean containsAuthor(Author author) {
		PersistenceManager mgr = getPersistenceManager();
		boolean contains = true;
		try {
			mgr.getObjectById(Author.class, author.getId());
		} catch (javax.jdo.JDOObjectNotFoundException ex) {
			contains = false;
		} finally {
			mgr.close();
		}
		return contains;
	}
	
	private static PersistenceManager getPersistenceManager() {
		return PMF.get().getPersistenceManager();
	}
}
