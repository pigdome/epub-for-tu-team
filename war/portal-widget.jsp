<!DOCTYPE html> 
<%@page import="th.ac.ipst.epub.oauth.UserValidator"%>
<%@page contentType="text/html;charset=UTF-8" %>
<%
	UserValidator validator=new UserValidator();
%>
<html lang="en">

  	<% if(validator.validate(request.getSession())) { 
			Long authorId=(Long)request.getSession().getAttribute("authorId");
			String authorDisplayName=(String)request.getSession().getAttribute("authorDisplayName");
			Boolean newMember=(Boolean)request.getSession().getAttribute("newMember");
	%>
	
  <head>
  
    <meta charset="utf-8">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

  </head>

  <body style="background: #9c4274;">

    <div id="epubEditorWidgetRegion">
    </div>

		<input type="hidden" id="authorId" value="<%= authorId %>">
  		<input type="hidden" id="authorDisplayName" value="<%= authorDisplayName %>">
  		<input type="hidden" id="newMember" value="<%= newMember %>">

    <!-- Placed at the end of the document so the pages load faster -->

  		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

		<script src="/js/imagesloaded.pkgd.min.js"></script>
		<script src="/js/imagesloaded.js"></script>
		<script src="/js/utils.js"></script>
		
		<script src="/js/handlebars/handlebars.runtime.js"></script>

		<script src="/tmpl/portal/portal-widget-item.js"></script>

		<script src="/js/app/portal/widget.js"></script>
		<script src="/js/app/publication/endpoint.js"></script>

		<script>
			(function() {
				PortalWidget.init();
			})();
		</script>

	</body>
  
  	<% } else { %>
	
	<body style="background: #9c4274;">
		<div id="wrap" style="margin-top: 60px;">
	      <div style="text-align:center; color: white; font-size: 16px;">
	      	<img id="epubEditorLogin" src="/img/loginSocial.png" />
	      </div>
		</div>
		
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script>
		
			// IMPORTANT: ไม่สามารถ display ปุ่ม login ภายใน widget นี้ได้ เพราะ 3rd party IdP กัน clickjacking ไว้ด้วยการ set X-Frame-Options ให้เป็น DENY (http://en.wikipedia.org/wiki/Clickjacking)
			// ดังนั้นที่ทำได้คือ popup new window ให้ login ในหน้า epub editor แล้ว send redirect ไปที่ page ที่ปิดตัวเอง และให้ refresh หน้า portal อัตโนมัติ

			var openDialog = function(uri, name, options, closeCallback) {
			    var win = window.open(uri, name, options);
			    var interval = window.setInterval(function() {
			        try {
			            if (win == null || win.closed) {
			                window.clearInterval(interval);
			                closeCallback(win);
			            }
			        }
			        catch (e) {
			        }
			    }, 1000);
			    return win;
			};
			
			(function() {
				$("#epubEditorLogin").on("click", function() {
					openDialog("/portal-login.html", "ePub Editor", null, function(popupWindow) {
						location.href="/portal-widget.jsp"; // refresh page
					});			
				});
			})();

		</script>
	</body>

	<% } %>
	
</html>
