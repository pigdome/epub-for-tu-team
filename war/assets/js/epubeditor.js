jQuery.noConflict();

jQuery(document).ready(function(){


	// .navbar #epub-container .show() เพื่อให้ show เมื่อ document ready แล้ว

	jQuery(".navbar").show();
	
	jQuery("#epub-container").show();

	// jQuery('#myTabContent').slimScroll({

 //        size: '5px',
 //        color: '#999999',
 //        distance: '0px',
 //        start:'#myTabContent'
 //    });

	jQuery("#btnNewBook").click(function () {
		
		$('#myModal').modal({
  			keyboard: false,
  			backdrop: true,
  			show:true

		});
		
		$('#myTab a[href="#tab-book-cover"]').tab('show');

	});

	// jQuery(window).resize(function() {
	// 	if(jQuery(".og-grid").hasClass("expanded")){
 //  		//jQuery(".book-item-position-expanded").find(".og-close").trigger('click');
	// 	/*  	var liBookItemExpanded = jQuery(".book-item-position-expanded");
 //  				var liBookItemNowSelected = jQuery(".book-item-now-selected");


	// 			jQuery("#og-grid").find(".book-item-now-selected").removeClass("book-item-now-selected");
	// 			jQuery("#og-grid").find(".book-item-position-expanded").removeClass("book-item-position-expanded");
	// 			jQuery("#og-grid").removeClass("expanded");

	// 			liBookItemExpanded.css( "height", "150px" );

	// 			liBookItemExpanded.find(".og-expander").remove();
	// 			liBookItemNowSelected.find("#divExArrow").remove();*/

	// 		var liBookItemExpanded = jQuery(".book-item-position-expanded");
			
	// 		liBookItemExpanded.animate({
	// 				opacity:'1.0',
	// 				height:'150px',
	// 				queue:false

	// 		},350,"linear",function() {

	// 		});

	// 		liBookItemExpanded.find(".og-expander").animate({
	// 				opacity:'0.1',
	// 				height:'0px',
	// 				queue:false
	// 		},350,"linear",function() {
				
	// 			//liBookItemExpanded.css( "height", "150px" );
				
	// 		});

	// 		var liBookItemNowSelected = jQuery(".book-item-now-selected");
	// 		// จะส่วนโดยหาจาก .book-icon ที่มีการ click ไม่ใช่จากตำแหน่งที่แสดง expanded ซึ่งอาจจะไม่ใช่ item เดียวกัน
	// 		liBookItemNowSelected.find("#exArrow").animate({opacity:'0.1'},200,function() {

	// 			jQuery("#og-grid").find(".book-item-now-selected").removeClass("book-item-now-selected");
	// 			jQuery("#og-grid").find(".book-item-position-expanded").removeClass("book-item-position-expanded");
	// 			jQuery("#og-grid").removeClass("expanded");

	// 			liBookItemExpanded.find(".og-expander").remove();
	// 			liBookItemNowSelected.find("#divExArrow").remove();
				
	// 		});
 //  		}
	// });

	jQuery(document).on('click', '.og-close', function(){

		// น่าจะใช้ .book-item-position-expanded ในการหา item ที่จะปิด expan ได้เลย 
		// เพราะยังไงก็ต้องอยู่ที่ตำแหน่ง ที่มี class book-item-position-expanded อยู่แล้ว
		var liBookItemExpanded = jQuery(".book-item-position-expanded");
		
		liBookItemExpanded.animate({
				opacity:'1.0',
				height:'150px',
				queue:false

		},350,"linear",function() {

		});

		liBookItemExpanded.find(".og-expander").animate({
				opacity:'1.0',
				height:'0px',
				queue:false
		},350,"linear",function() {
			
			//liBookItemExpanded.css( "height", "150px" );

			var liBookItemNowSelected = jQuery(".book-item-now-selected");
			// จะส่วนโดยหาจาก .book-icon ที่มีการ click ไม่ใช่จากตำแหน่งที่แสดง expanded ซึ่งอาจจะไม่ใช่ item เดียวกัน
			liBookItemNowSelected.find("#exArrow").hide("blind",150,function() {

				jQuery("#og-grid").find(".book-item-now-selected").removeClass("book-item-now-selected");
				jQuery("#og-grid").find(".book-item-position-expanded").removeClass("book-item-position-expanded");
				jQuery("#og-grid").removeClass("expanded");

				liBookItemExpanded.find(".og-expander").remove();
				liBookItemNowSelected.find("#divExArrow").remove();
			});
			
			

		});

	});


	jQuery(document).on('click', '.book-item', function(){


	});

	jQuery(document).on('click', '.book-theme-selection', function(){

		if(jQuery(this).hasClass("theme-selected")){

			return;
		}
		else{

			var getThemeTypeSelectd = jQuery(this).attr("theme-type");

			var getThemeNameSelectd = jQuery(this).attr("theme-name");

			jQuery("#book-theme-selected-header").html(getThemeNameSelectd+ " <b class='caret'></b>");


			jQuery(".preview-theme-selected").hide();

			jQuery(".preview-theme-selected").removeClass("preview-theme-selected");

			jQuery(".theme-selected").removeClass("theme-selected");

			jQuery(this).addClass("theme-selected");

			jQuery("#preview-theme-"+getThemeTypeSelectd).fadeIn();

			jQuery("#preview-theme-"+getThemeTypeSelectd).addClass("preview-theme-selected");

		}

	});

	// jQuery("#item-cover-templates img").hover(
 //            function(){ // Change the input image's source when we "roll on"
 //            //alert("AAA");
 //            	jQuery(this).parent().find(".icon-trash").hide();
 //            },
 //            function(){ // Change the input image's source back to the default on "roll off"
 //            	jQuery(this).parent().find(".icon-trash").show();             
 //            }
 //    );

	jQuery(document).on('click', '#nav-cover-templates', function(){

		if(jQuery(this).hasClass("active")){

		}
		else{
			jQuery("#nav-my-cover").removeClass("active");
			jQuery(this).addClass("active");
			jQuery("#item-my-covers").hide();
			jQuery("#item-cover-templates").fadeIn();

		}
	});

	jQuery(document).on('click', '#nav-my-cover', function(){

		if(jQuery(this).hasClass("active")){

		}
		else{
			jQuery("#nav-cover-templates").removeClass("active");
			jQuery(this).addClass("active");
			jQuery("#item-cover-templates").hide();
			jQuery("#item-my-covers").fadeIn();
		}
	});



	jQuery(document).on('click', '#item-cover-templates img', function(){


		if(jQuery(this).hasClass("book-cover-selected")){

		}
		else
		{
			jQuery("#item-cover-templates").find(".book-cover-selected").removeClass("book-cover-selected").css('box-shadow', '1px 1px 2px 1px #BABABA');

			jQuery("#item-my-covers").find(".book-cover-selected").removeClass("book-cover-selected").css('box-shadow', '1px 1px 2px 1px #BABABA');

			jQuery(this).css("box-shadow", "")

			jQuery(this).addClass("book-cover-selected");

			jQuery('#preview-book-cover-create-new img').attr('src', jQuery(this).attr("src")).hide().fadeIn();

		}
		
	});

	
	jQuery(document).on('click', '#item-my-covers img', function(){


		if(jQuery(this).hasClass("book-cover-selected")){

		}
		else
		{
			jQuery(this).parent().find(".icon-trash").hide();

			jQuery("#item-cover-templates").find(".book-cover-selected").removeClass("book-cover-selected").css('box-shadow', '1px 1px 2px 1px #BABABA');

			jQuery("#item-my-covers").find(".book-cover-selected").removeClass("book-cover-selected").css('box-shadow', '1px 1px 2px 1px #BABABA');

			jQuery(this).css("box-shadow", "")

			jQuery(this).addClass("book-cover-selected");

			jQuery('#preview-book-cover-create-new img').attr('src', jQuery(this).attr("src")).hide().fadeIn();

		}
		
	});


	$("#item-my-covers img").on(
	{// .on ในกรณีที่ content อาจเกิดขึ้นใหม่ แบบ dynamic
		mouseenter: function() 
		{
			if(jQuery(this).hasClass("book-cover-selected")){
				//กรณีปกนั้นถูกเลือกอยู่จะยังไม่ให้ลบได้
			}
			else
			{
		        //stuff to do on mouseover
		        jQuery(this).parent().find(".icon-trash").show();
		    }
		},
		mouseleave: function()
		{
			if(jQuery(this).hasClass("book-cover-selected")){
				//กรณีปกนั้นถูกเลือกอยู่จะยังไม่ให้ลบได้
			}
			else
			{
	        //stuff to do on mouseleave
	        jQuery(this).parent().find(".icon-trash").hide();  
	    	}
		}
	});

	$(".icon-trash-book-cover").on(
	{// .on ในกรณีที่ content อาจเกิดขึ้นใหม่ แบบ dynamic
	    mouseenter: function() 
	    {
	        //stuff to do on mouseover
	        jQuery(this).show();
	    },
	    mouseleave: function()
	    {
	        //stuff to do on mouseleave
	        jQuery(this).hide();  
	    }
	});

	jQuery(document).on('click', '.icon-trash-book-cover', function(){




		jQuery(this).parent().find(".form-btn-delete-my-cover").fadeIn(200);

		
	});

	jQuery(document).on('click', '.btn-del-my-covers', function(){

		jQuery(this).parent().parent().fadeOut(300, function () {
   			jQuery(this).remove();
  		});
	});

	jQuery(document).on('click', '.btn-cancle-my-covers', function(){

		jQuery(this).parent().fadeOut(200);
		
	});

	jQuery(document).on('click', '.book-icon', function(){
		//alert(jQuery(this).position().top);

		//jQuery(this).parent() -> .book-item (คือ li)


		if(jQuery(".og-grid").hasClass("expanded"))
		{	//แสดงว่ามี book ที่ expan อยู่ จะต้องทำการหาตำแหน่งของตัวที่ expan ก่อนอยู่แล้วว่าอยู่ไหน position เท่ากันไหม
			//บรรดทัดเดียวกันหรือเปล่า ถ้าบรรทัดเดียวกันจะทำการ reuse ใช้ expan ของตัวเดิมที่เปิดไว้อยู่แล้ว แต่เปลี่ยน
			//นำข้อมูลใหม่ที่กดเพื่อดูรายละเอียดไปใส่แทนที่ของเก่า

				if(jQuery(".og-grid").hasClass("waiting")){
				
					//กรณีรอยังไม่ต้องทำอะไร
					return false;
				}
				else if(jQuery(this).parent().hasClass("book-item-now-selected"))
				{
					// กรณีถ้า click ที่ item ของตัวเองอีกครั้งนึง ก็ทำการ close
					jQuery(".book-item-position-expanded").find(".og-close").trigger('click');
				}
				else
				{
					//กรณีเมื่อกดที่ item อื่นๆ ซึ่งจะต้องตรวจสอบดูก่อนว่าเป็น row เดียวกันไหม เทียบจาก position() 
					//ถ้า row เดียวกันจะใช้ div expan ของตัวที่เปิดอยู่เดิม แต่ลบข้อมูลออกใช้เฉพาะ div expan แล้ว
					//นำข้อมูลของ book item ตัวใหม่ที่มีการเลือกดึงข้อมูลมาใส่ใน div expan ของตัวเดิมที่เปิดอยู่แล้ว

					var getPreviousExpanPosition = jQuery(".book-item-position-expanded").position().top;

					var getNewExpanPosition = jQuery(this).parent().position().top;


					// เอา position ทั้ง 2 อันมาเทียบกัน
					// 1. ถ้า เท่ากันแปลว่าอยู่ row เดียวกัน ก็ให้ใช้ reuse div expan ของอันเดิม นำข้อมูลใหม่มาใส่
					// 2. ถ้า ไม่เท่ากันแปลว่าอยู่คนละ row กัน ให้สร้าง div expan ใหม่ได้เลย แล้ว ซ่อนตัวเก่าแล้วลบทิ้งได้เลย

					if(getPreviousExpanPosition == getNewExpanPosition)
					{// 1. ถ้า เท่ากันแปลว่าอยู่ row เดียวกัน ก็ให้ใช้ reuse div expan ของอันเดิม นำข้อมูลใหม่มาใส่
						
						var elementBookItemPreviousExpan = jQuery(".book-item-position-expanded");

						// จะทำการลบ div .og-expander-inner ออกไป แล้วนำข้อมูลใหม่มาใส่แทน

						elementBookItemPreviousExpan.find(".og-expander-inner").remove();

						var getNewDataToBookItemExpan = getBookDetailForReuseExpan(jQuery(this).parent().attr("bookid"));
						
						elementBookItemPreviousExpan.find(".og-expander").append(getNewDataToBookItemExpan);

						elementBookItemPreviousExpan.find("#bookCoverL").fadeIn();

						jQuery(".book-item-now-selected").find("#divExArrow").remove();

						var strDivArrow = "<div id='divExArrow'style='margin-top:-15px; margin-left:25px; position:absolute;'><img id='exArrow'src='assets/img/expanArrow.png'/></div>";
						
						jQuery(this).parent().append(strDivArrow);


						// ลบ class .book-item-now-selected ออกจากตัวเดิม แล้วไปใส่ให้ตัวใหม่ที่มีการกดแสดงข้อมูล

						jQuery(".book-item-now-selected").removeClass("book-item-now-selected");

						jQuery(this).parent().addClass("book-item-now-selected");


						// var getNewDataToBookItemExpan ="<img id='bookCoverL'src='assets/images/bookCoverL2.png' alt='newBookCover' style='display:none;'/>";
						// elementBookItemPreviousExpan.find(".og-fullimg img").remove();
						// elementBookItemPreviousExpan.find(".og-fullimg").append(getNewDataToBookItemExpan);
						// elementBookItemPreviousExpan.find("#bookCoverL").fadeIn();



					}
					else
					{// 2. ถ้า ไม่เท่ากันแปลว่าอยู่คนละ row กัน ให้สร้าง div expan ใหม่ได้เลย แล้ว ซ่อนตัวเก่าแล้วลบทิ้งได้เลย



						jQuery(".og-grid").addClass("waiting"); // ไว้เพื่อกันตอนกดซ้ำตอนที่ expan ยังทำงานไม่เสร็จ แต่เท่าที่ทดลองไม่มีก็ไม่เป็นไร
							
						var newBookExpander = getBookDetail(jQuery(this).parent().attr("bookid"));



					    jQuery(this).parent().append(newBookExpander);

					    //jQuery(this).addClass("book-item-expanded");
					    
					    jQuery(this).parent().css( "height", "650px" );

					    var that=jQuery(this).parent();


						// แก้ใหม่สำหรับให้ใช้ได้บน safari ด้วย
						jQuery("#bookCoverL",jQuery(this).parent()).animate({
							width:'300px',
							queue:false
						},350,"linear"); 
						//

						jQuery(".og-expander",jQuery(this).parent()).animate({
							height:'500px',
							queue:false
						},350,"linear",function() {

							//jQuery("#exArrow",jQuery(this)).show();
							//ไม่สามารถทำตามบรรทัดบนได้ เพราะว่า this ตัวนี้จะเป็นคนละตัวกับตัวบน this ตัวนี้จะหมายถึง .og-expander
							//ซึ่ง exArrow นั้น ไม่ใข่ ลูก ของ .og-expander จึง .find หาไม่เจอ 
							//จะต้องใช้ that ซึ่งหมายถึง this ของ #bookItem นั้นเอง

							jQuery(that).find("#exArrow").show();


							//jQuery("#bookCoverL",jQuery(this)).show("scale", 350 );

							/*jQuery('html, body').animate({
								scrollTop: jQuery(".book-icon",jQuery(that)).offset().top
							}, 250);*/
							//jQuery.scrollTo('.book-item-now-selected', 250 ,{queue:false} );
						});


						// ทำการ ลบ book item ที่ expan ก่อนหน้านี้ทิ้งซะก่อนที่จะ add class book-item-now-selected และ book-item-position-expanded

						//removePreviousExpanForCaseItemDifferenceRow();
						var liBookItemPreviousExpanded = jQuery(".book-item-position-expanded");
						var liBookItemPreviousNowSelected = jQuery(".book-item-now-selected");

						jQuery("#og-grid").find(".book-item-now-selected").removeClass("book-item-now-selected");
						jQuery("#og-grid").find(".book-item-position-expanded").removeClass("book-item-position-expanded");
						//jQuery("#og-grid").removeClass("expanded");

		
						//

						// book-item-now-expanded ใช้ระบุ item ที่กำลังแสดงข้อมูลอยู่
						// book-item-position-expanded  ใช้ระบบ position ที่มีการแสดงข้อมูลอยู่
						// ซึ่ง 2 ตัวนี้ อาจจะอยู่ใน คนละ item ก็ได้ คือ กรณี ที่กดดูรายละเอียดตัวอื่นๆ ในแถวเดียวกันนั้นเอง

						jQuery(this).parent().addClass("book-item-now-selected"); 
						jQuery(this).parent().addClass("book-item-position-expanded"); // ใช้ระบบ position ที่มีการแสดงข้อมูลอยู่
						//jQuery(this).parent().parent().addClass("expanded");
						// ซึ่ง 
						jQuery(".og-grid").removeClass("waiting");	


						/*jQuery('html, body').animate({
								scrollTop: jQuery(".book-item-now-selected",jQuery(".og-grid")).offset().top,
								queue:true
						}, 450, function(){


						});*/

						//jQuery.scrollTo('.book-item-now-selected', 350 ,{queue:false} );

						liBookItemPreviousExpanded.find(".og-expander").remove();
						liBookItemPreviousNowSelected.find("#divExArrow").remove();

						liBookItemPreviousExpanded.animate({
							height:'150px',
							queue:false
						},250,"linear",function() {
							jQuery.scrollTo('.book-item-now-selected', 350 ,{queue:false} );
						});

					}
				}

		}
		else
		{//กรณีที่ยังไม่มี book เล่มไหนที่ expan อยู่เลย ก็ให้ สร้าง .og-expander ที่ item นั้นได้เลย

			if(jQuery(".og-grid").hasClass("waiting")){
				
				return false;
			}
			else if(jQuery(this).parent().hasClass("book-item-now-selected"))
			{
				jQuery(this).parent().find(".og-close").trigger('click');
			}
			else
			{

				jQuery(".og-grid").addClass("waiting"); // ไว้เพื่อกันตอนกดซ้ำตอนที่ expan ยังทำงานไม่เสร็จ แต่เท่าที่ทดลองไม่มีก็ไม่เป็นไร
					
				var newBookExpander = getBookDetail(jQuery(this).parent().attr("bookid"));



			    jQuery(this).parent().append(newBookExpander);

			    //jQuery(this).addClass("book-item-expanded");
			    
			    jQuery(this).parent().css( "height", "650px" );

			    var that=jQuery(this).parent();
					

				jQuery('html, body').animate({
					scrollTop: jQuery(".book-icon",jQuery(that)).offset().top,
					queue:false
				}, 350);

				// แก้ใหม่สำหรับให้ใช้ได้บน safari ด้วย
				jQuery("#bookCoverL",jQuery(this).parent()).animate({
					width:'300px',
					queue:false
				},350,"linear"); 
				//

				jQuery(".og-expander",jQuery(this).parent()).animate({
					height:'500px',
					queue:false
				},350,"linear",function() {

					//jQuery("#exArrow",jQuery(this)).show();
					//ไม่สามารถทำตามบรรทัดบนได้ เพราะว่า this ตัวนี้จะเป็นคนละตัวกับตัวบน this ตัวนี้จะหมายถึง .og-expander
					//ซึ่ง exArrow นั้น ไม่ใข่ ลูก ของ .og-expander จึง .find หาไม่เจอ 
					//จะต้องใช้ that ซึ่งหมายถึง this ของ #bookItem นั้นเอง

					jQuery(that).find("#exArrow").show();
					//jQuery("#bookCoverL",jQuery(this)).show("scale", 350 );



				});

				// book-item-now-selected ใช้ระบุ item ที่กำลังแสดงข้อมูลอยู่
				// book-item-position-expanded  ใช้ระบบ position ที่มีการแสดงข้อมูลอยู่
				// ซึ่ง 2 ตัวนี้ อาจจะอยู่ใน คนละ item ก็ได้ คือ กรณี ที่กดดูรายละเอียดตัวอื่นๆ ในแถวเดียวกันนั้นเอง

				jQuery(this).parent().addClass("book-item-now-selected");
				jQuery(this).parent().addClass("book-item-position-expanded");
				jQuery(this).parent().parent().addClass("expanded");
				// ซึ่ง 
				jQuery(".og-grid").removeClass("waiting");	




			}

		}
	});	

	$('#myModal').on('show', function () {
  		// do something…
  		//jQuery("#bookCoverBigNew").hide();

  		//$('#myTab a[href="#profile"]').tab('show');
	})

	$('#myModal').on('hidden', function () {
  		// do something…
  		//jQuery("#bookCoverBigNew").hide();
		//jQuery("#og-grid").prepend("<li id='newBookItem'><a href='#'><img id='newBookIcon' src='assets/images/newBookCoverS.png' style='display:none;'/></a></li>");

		jQuery("#bookItem").show();
		jQuery("#bookIcon").show("scale", 400 );
		//jQuery("#newBookIcon").fadeTo(1000,1.0,function() {

		//});

	})


	jQuery("#btnImportBook").click(function () {
		//jQuery('#tbBookList').animate({opacity: 1.0});
		//jQuery("#tbBookList").show();
		//$("#myModal").modal("show");

	});

	jQuery("#btnSaveNewBook").click(function () {
		//jQuery('#tbBookList').animate({opacity: 1.0});
		//jQuery("#tbBookList").show();

		//jQuery('#preview-book-cover-create-new img').attr('src', jQuery(this).attr("src")).hide().fadeIn();

		var getPathImgForDisplay = jQuery('#preview-book-cover-create-new img').attr('src');

		var getBookItemForDisplay = "";

		$("#myModal").modal("hide");

		jQuery("#og-grid").prepend("<li id='bookItem' class='book-item' bookid='EP000'style='display:none;'><img id='bookIcon' class='book-icon' src='assets/img/book-covers/book-cover-0.png' style='display:none; cursor:pointer;'/></li>");

		//jQuery("#newBookIcon").show("scale", 800 );


	});


	jQuery("#btnEditBookCover").click(function () {
		//jQuery('#tbBookList').animate({opacity: 1.0});
		//jQuery("#tbBookList").show();


		$('#myTab a[href="#tab-description"]').tab('show');


	});


	jQuery("#btnSearchBook").click(function () {
		//jQuery('#tbBookList').animate({opacity: 1.0});
		//jQuery("#tbBookList").show();


		// jQuery('html, body').animate({
		// 	scrollTop: jQuery("#bookIcon").offset().top
		// }, 200);

		// jQuery("#bookCoverL").hide("blind",100,function() {

		// });

		// jQuery(".og-expander").hide("blind",300,function() {


		// });
		
		// jQuery("#exArrow").hide("scale",300,function() {
		// 	jQuery(".og-expander").remove();
		// 	jQuery("#divExArrow").remove();
		// });

		var testalert = "test"+
						"test"+
						jQuery("#bookItem").attr("booktitle");

		alert(testalert);
	});


	//socialIcon Sign in Hover
		jQuery("#imgFacebookIcon").hover(
	            function(){ // Change the input image's source when we "roll on"
	            	jQuery(this).attr({ src : 'assets/img/idp-logos/facebookIconHover.png'});
	            },
	            function(){ // Change the input image's source back to the default on "roll off"
	            	jQuery(this).attr({ src : 'assets/img/idp-logos/facebookIcon.png'});             }
	    );

		jQuery("#imgTwitterIcon").hover(
	            function(){ // Change the input image's source when we "roll on"
	            	jQuery(this).attr({ src : 'assets/img/idp-logos/twitterIconHover.png'});
	            },
	            function(){ // Change the input image's source back to the default on "roll off"
	            	jQuery(this).attr({ src : 'assets/img/idp-logos/twitterIcon.png'});             }
	    );

	    jQuery("#imgGoogleIcon").hover(
	            function(){ // Change the input image's source when we "roll on"
	            	jQuery(this).attr({ src : 'assets/img/idp-logos/googleIconHover.png'});
	            },
	            function(){ // Change the input image's source back to the default on "roll off"
	            	jQuery(this).attr({ src : 'assets/img/idp-logos/googleIcon.png'});             }
	    );
    //

});

function getBookDetail(bookId){

	var bookDetail = "";

	if(bookId == "EP000"){

		bookDetail = "<div class='og-expander'>"+
                     "<div class='og-expander-inner'>"+
                     "<span class='og-close'></span>"+
                     "<div class='og-fullimg'><img id='bookCoverL'src='assets/img/book-covers/book-cover-0.png' alt='newBookCover' width='0px'/></div>"+
                     "<div class='og-details'><h3>Coriander artichoke</h3><p>Catsear celery turnip greens bush tomato okra parsley bell pepper cabbage bamboo shoot dulse kombu shallot quandong lettuce seakale.</p><a href='http://cargocollective.com/jaimemartinez/'>Edit</a></div>"+
                     "</div>"+
                     "</div>"+
                     "<div id='divExArrow'style='margin-top:-15px; margin-left:25px; position:absolute;'><img id='exArrow'src='assets/img/expanArrow.png' style='display:none;'/>"+
                     "</div>";

	}
	else if(bookId == "EP101"){

		bookDetail = "<div class='og-expander'>"+
                     "<div class='og-expander-inner'>"+
                     "<span class='og-close'></span>"+
                     "<div class='og-fullimg'><img id='bookCoverL'src='assets/img/book-covers/book-cover-1.png' alt='newBookCover' width='0px'/></div>"+
                     "<div class='og-details'><h3>มหัศจรรย์ทฤษฎีสตริง (THE ELEGANT UNIVERSE)</h3><div class='og-text-desc'>BRIAN GREENE</div><a href='#'>Edit</a><a href='#'>Go Inside</a><a class='delete-book'href='#'>Delete</a></div>"+
                     "</div>"+
                     "</div>"+
                     "<div id='divExArrow'style='margin-top:-15px; margin-left:25px; position:absolute;'><img id='exArrow'src='assets/img/expanArrow.png' style='display:none;'/>"+
                     "</div>";

	}
	else if(bookId == "EP102"){

		bookDetail = "<div class='og-expander'>"+
                     "<div class='og-expander-inner'>"+
                     "<span class='og-close'></span>"+
                     "<div class='og-fullimg'><img id='bookCoverL'src='assets/img/book-covers/book-cover-2.png' alt='newBookCover' width='0px'/></div>"+
                     "<div class='og-details'><h3>ทฤษฏีสัมพัทธภาพทั่วไป เบื้องต้น (AN INTRODUCTION TO GENERAL RELATIVITY)</h3><div class='og-text-desc'>ปิยบุตร บุรีคำ</div><a href='#'>Edit</a><a href='#'>Go Inside</a><a class='delete-book'href='#'>Delete</a></div>"+
                     "</div>"+
                     "</div>"+
                     "<div id='divExArrow'style='margin-top:-15px; margin-left:25px; position:absolute;'><img id='exArrow'src='assets/img/expanArrow.png' style='display:none;'/>"+
                     "</div>";

	}
	else if(bookId == "EP103"){

		bookDetail = "<div class='og-expander'>"+
                     "<div class='og-expander-inner'>"+
                     "<span class='og-close'></span>"+
                     "<div class='og-fullimg'><img id='bookCoverL'src='assets/img/book-covers/book-cover-3.png' alt='newBookCover' width='0px'/></div>"+
                     "<div class='og-details'><h3>Dandelion horseradish</h3><p>Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.</p><a href='http://cargocollective.com/jaimemartinez/'>Edit</a></div>"+
                     "</div>"+
                     "</div>"+
                     "<div id='divExArrow'style='margin-top:-15px; margin-left:25px; position:absolute;'><img id='exArrow'src='assets/img/expanArrow.png' style='display:none;'/>"+
                     "</div>";

	}	
	else if(bookId == "EP104"){

		bookDetail = "<div class='og-expander'>"+
                     "<div class='og-expander-inner'>"+
                     "<span class='og-close'></span>"+
                     "<div class='og-fullimg'><img id='bookCoverL'src='assets/img/book-covers/book-cover-5.png' alt='newBookCover' width='0px'/></div>"+
                     "<div class='og-details'><h3>Sea lettuce</h3><p>Bell pepper eggplant water spinach bell pepper radicchio kale artichoke earthnut pea beet greens carrot celtuce peanut radish mustard jícama tomato bamboo shoot quandong.</p><a href='http://cargocollective.com/jaimemartinez/'>Edit</a></div>"+
                     "</div>"+
                     "</div>"+
                     "<div id='divExArrow'style='margin-top:-15px; margin-left:25px; position:absolute;'><img id='exArrow'src='assets/img/expanArrow.png' style='display:none;'/>"+
                     "</div>";

	}
	else if(bookId == "EP105"){

		bookDetail = "<div class='og-expander'>"+
                     "<div class='og-expander-inner'>"+
                     "<span class='og-close'></span>"+
                     "<div class='og-fullimg'><img id='bookCoverL'src='assets/img/book-covers/book-cover-6.png' alt='newBookCover' width='0px'/></div>"+
                     "<div class='og-details'><h3>Catsear celery turnip</h3><p>Kohlrabi fennel taro salsify bok choy arugula bell pepper gourd groundnut sierra leone bologi pumpkin.</p><a href='http://cargocollective.com/jaimemartinez/'>Edit</a></div>"+
                     "</div>"+
                     "</div>"+
                     "<div id='divExArrow'style='margin-top:-15px; margin-left:25px; position:absolute;'><img id='exArrow'src='assets/img/expanArrow.png' style='display:none;'/>"+
                     "</div>";

	}
	else if(bookId == "EP106"){

		bookDetail = "<div class='og-expander'>"+
                     "<div class='og-expander-inner'>"+
                     "<span class='og-close'></span>"+
                     "<div class='og-fullimg'><img id='bookCoverL'src='assets/img/book-covers/book-cover-7.png' alt='newBookCover' width='0px'/></div>"+
                     "<div class='og-details'><h3>Cabbage bamboo shoot</h3><p>Catsear celery turnip greens bush tomato okra parsley bell pepper cabbage bamboo shoot dulse kombu shallot quandong lettuce seakale.</p><a href='http://cargocollective.com/jaimemartinez/'>Edit</a></div>"+
                     "</div>"+
                     "</div>"+
                     "<div id='divExArrow'style='margin-top:-15px; margin-left:25px; position:absolute;'><img id='exArrow'src='assets/img/expanArrow.png' style='display:none;'/>"+
                     "</div>";

	}
	else if(bookId == "EP107"){

		bookDetail = "<div class='og-expander'>"+
                     "<div class='og-expander-inner'>"+
                     "<span class='og-close'></span>"+
                     "<div class='og-fullimg'><img id='bookCoverL'src='assets/img/book-covers/book-cover-8.png' alt='newBookCover' width='0px'/></div>"+
                     "<div class='og-details'><h3>Asparagus nori burdock</h3><p>Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.</p><a href='http://cargocollective.com/jaimemartinez/'>Edit</a></div>"+
                     "</div>"+
                     "</div>"+
                     "<div id='divExArrow'style='margin-top:-15px; margin-left:25px; position:absolute;'><img id='exArrow'src='assets/img/expanArrow.png' style='display:none;'/>"+
                     "</div>";

	}
	else if(bookId == "EP108"){

		bookDetail = "<div class='og-expander'>"+
                     "<div class='og-expander-inner'>"+
                     "<span class='og-close'></span>"+
                     "<div class='og-fullimg'><img id='bookCoverL'src='assets/img/book-covers/book-cover-9.png' alt='newBookCover' width='0px'/></div>"+
                     "<div class='og-details'><h3>Dandelion horseradish</h3><p>Bell pepper eggplant water spinach bell pepper radicchio kale artichoke earthnut pea beet greens carrot celtuce peanut radish mustard jícama tomato bamboo shoot quandong.</p><a href='http://cargocollective.com/jaimemartinez/'>Edit</a></div>"+
                     "</div>"+
                     "</div>"+
                     "<div id='divExArrow'style='margin-top:-15px; margin-left:25px; position:absolute;'><img id='exArrow'src='assets/img/expanArrow.png' style='display:none;'/>"+
                     "</div>";

	}			    
    return bookDetail;
}

function getBookDetailForReuseExpan(bookId){

	var bookDetail = "";
	
	if(bookId == "EP000"){

		bookDetail = "<div class='og-expander-inner'>"+
                     "<span class='og-close'></span>"+
                     "<div class='og-fullimg'><img id='bookCoverL'src='assets/img/book-covers/book-cover-0.png' alt='newBookCover' style='display:none;'/></div>"+
                     "<div class='og-details'><h3>Coriander artichoke</h3><p>Catsear celery turnip greens bush tomato okra parsley bell pepper cabbage bamboo shoot dulse kombu shallot quandong lettuce seakale.</p><a href='http://cargocollective.com/jaimemartinez/'>Edit</a></div>"+
                     "</div>";

	}
	else if(bookId == "EP101"){

		bookDetail = "<div class='og-expander-inner'>"+
                     "<span class='og-close'></span>"+
                     "<div class='og-fullimg'><img id='bookCoverL'src='assets/img/book-covers/book-cover-1.png' alt='newBookCover' style='display:none;'/></div>"+
                     "<div class='og-details'><h3>มหัศจรรย์ทฤษฎีสตริง (THE ELEGANT UNIVERSE)</h3><div class='og-text-desc'>BRIAN GREENE</div><a href='http://cargocollective.com/jaimemartinez/'>Edit</a></div>"+
                     "</div>";

	}
	else if(bookId == "EP102"){

		bookDetail = "<div class='og-expander-inner'>"+
                     "<span class='og-close'></span>"+
                     "<div class='og-fullimg'><img id='bookCoverL'src='assets/img/book-covers/book-cover-2.png' alt='newBookCover' style='display:none;'/></div>"+
                     "<div class='og-details'><h3>ทฤษฏีสัมพัทธภาพทั่วไป เบื้องต้น (AN INTRODUCTION TO GENERAL RELATIVITY)</h3><div class='og-text-desc'>ปิยบุตร บุรีคำ</div><a href='http://cargocollective.com/jaimemartinez/'>Edit</a></div>"+
                     "</div>";

	}
	else if(bookId == "EP103"){

		bookDetail = "<div class='og-expander-inner'>"+
                     "<span class='og-close'></span>"+
                     "<div class='og-fullimg'><img id='bookCoverL'src='assets/img/book-covers/book-cover-3.png' alt='newBookCover' style='display:none;'/></div>"+
                     "<div class='og-details'><h3>Dandelion horseradish</h3><p>Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.</p><a href='http://cargocollective.com/jaimemartinez/'>Edit</a></div>"+
                     "</div>";

	}	
	else if(bookId == "EP104"){

		bookDetail = "<div class='og-expander-inner'>"+
                     "<span class='og-close'></span>"+
                     "<div class='og-fullimg'><img id='bookCoverL'src='assets/img/book-covers/book-cover-5.png' alt='newBookCover' style='display:none;'/></div>"+
                     "<div class='og-details'><h3>Sea lettuce</h3><p>Bell pepper eggplant water spinach bell pepper radicchio kale artichoke earthnut pea beet greens carrot celtuce peanut radish mustard jícama tomato bamboo shoot quandong.</p><a href='http://cargocollective.com/jaimemartinez/'>Edit</a></div>"+
                     "</div>";

	}
	else if(bookId == "EP105"){

		bookDetail = "<div class='og-expander-inner'>"+
                     "<span class='og-close'></span>"+
                     "<div class='og-fullimg'><img id='bookCoverL'src='assets/img/book-covers/book-cover-6.png' alt='newBookCover' style='display:none;'/></div>"+
                     "<div class='og-details'><h3>Catsear celery turnip</h3><p>Kohlrabi fennel taro salsify bok choy arugula bell pepper gourd groundnut sierra leone bologi pumpkin.</p><a href='http://cargocollective.com/jaimemartinez/'>Edit</a></div>"+
                     "</div>";

	}
	else if(bookId == "EP106"){

		bookDetail = "<div class='og-expander-inner'>"+
                     "<span class='og-close'></span>"+
                     "<div class='og-fullimg'><img id='bookCoverL'src='assets/img/book-covers/book-cover-7.png' alt='newBookCover' style='display:none;'/></div>"+
                     "<div class='og-details'><h3>Cabbage bamboo shoot</h3><p>Catsear celery turnip greens bush tomato okra parsley bell pepper cabbage bamboo shoot dulse kombu shallot quandong lettuce seakale.</p><a href='http://cargocollective.com/jaimemartinez/'>Edit</a></div>"+
                     "</div>";

	}
	else if(bookId == "EP107"){

		bookDetail = "<div class='og-expander-inner'>"+
                     "<span class='og-close'></span>"+
                     "<div class='og-fullimg'><img id='bookCoverL'src='assets/img/book-covers/book-cover-8.png' alt='newBookCover' style='display:none;'/></div>"+
                     "<div class='og-details'><h3>Asparagus nori burdock</h3><p>Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.</p><a href='http://cargocollective.com/jaimemartinez/'>Edit</a></div>"+
                     "</div>";

	}
	else if(bookId == "EP108"){

		bookDetail = "<div class='og-expander-inner'>"+
                     "<span class='og-close'></span>"+
                     "<div class='og-fullimg'><img id='bookCoverL'src='assets/img/book-covers/book-cover-9.png' alt='newBookCover' style='display:none;'/></div>"+
                     "<div class='og-details'><h3>Dandelion horseradish</h3><p>Bell pepper eggplant water spinach bell pepper radicchio kale artichoke earthnut pea beet greens carrot celtuce peanut radish mustard jícama tomato bamboo shoot quandong.</p><a href='http://cargocollective.com/jaimemartinez/'>Edit</a></div>"+
                     "</div>";

	}			    
    return bookDetail;
}

function removePreviousExpanForCaseItemDifferenceRow(){


		var liBookItemPreviousExpanded = jQuery(".book-item-position-expanded");
		var liBookItemPreviousNowSelected = jQuery(".book-item-now-selected");

		jQuery("#og-grid").find(".book-item-now-selected").removeClass("book-item-now-selected");
		jQuery("#og-grid").find(".book-item-position-expanded").removeClass("book-item-position-expanded");
				//jQuery("#og-grid").removeClass("expanded");

		liBookItemPreviousExpanded.find(".og-expander").remove();
		liBookItemPreviousNowSelected.find("#divExArrow").remove();
		liBookItemPreviousExpanded.css( "height", "150px" );
}