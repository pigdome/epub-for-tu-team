jQuery.noConflict();

jQuery(document).ready(function(){


    // manage chapter list

        var updateOutput = function(e)
        {
            var list   = e.length ? e : jQuery(e.target),
                output = list.data('output');
            if (window.JSON) {
                output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
            } else {
                output.val('JSON browser support required for this demo.');
            }
        };

        // activate Nestable for list 1
        jQuery('#nestable').nestable({
            group: 6,
            maxDepth: 2,
            expandBtnHTML: "",
            collapseBtnHTML: "",

        })
        .on('change', updateOutput);
        

        // output initial serialised data
        updateOutput(jQuery('#nestable').data('output', jQuery('#nestable-output')));


        jQuery('#nestable-menu').on('click', function(e)
        {
            var target = jQuery(e.target),
                action = target.data('action');
            if (action === 'expand-all') {
                jQuery('.dd').nestable('expandAll');
            }
            if (action === 'collapse-all') {
                jQuery('.dd').nestable('collapseAll');
            }
        });

        jQuery(".dd-list").on('click', '.dd-item', function(e){

            var that = jQuery(this);

            e.stopPropagation();

            jQuery(".dd-selected-item").removeClass('dd-selected-item');//ลบตัวก่อนหน้าก่อนถ้ามีการ click hilight ตัวอื่นมาก่อนแล้ว
            jQuery(that).find("> .dd-handle").addClass('dd-selected-item');


            jQuery('#chapter-list-loading').show(1).delay(2000).hide(1,function () 
            {
                if(jQuery("#content-panel").hasClass("opened"))
                {


                    var getChapterListName = jQuery(that).find("> .dd-handle").text();
                    jQuery("#txt-display-chapter-list").val(getChapterListName);



                }
                else
                {


                    jQuery("#content-panel").show();

                    jQuery("#content-panel").addClass("opened");


                    var getChapterListName = jQuery(that).find("> .dd-handle").text();
                    jQuery("#txt-display-chapter-list").val(getChapterListName);

                }

            });


        });

        jQuery(document).on('click', '#btn-expand-all', function(){

            jQuery('.dd').nestable('expandAll');
        });

        jQuery(document).on('click', '#btn-collapse-all', function(){

            jQuery('.dd').nestable('collapseAll');
        });
    

        jQuery("#btn-confirm-create-chapter").click(function () {

            var getChapterName = jQuery("#txt-chapter-name").val();

            var getTimeStamp = new Date().getTime();

            var appendNewChapter = "<li class='dd-item new-item-added' data-timestamp='"+ getTimeStamp + "' style='display:none;'>" +
                                "<div class='dd-handle'>" + getChapterName + "</div>" +
                                "</li>";

            jQuery(".dd > .dd-list").append(appendNewChapter);


            var getSerialize = JSON.stringify(jQuery('#nestable').nestable('serialize'));

            jQuery(".dd").attr("data-serialize-toc",getSerialize);

            
            jQuery(".ipst-epub-template-selected").hide();
            jQuery(".ipst-epub-template-selected").removeClass("ipst-epub-template-selected");

            
            //CKEDITOR.instances.editable.setData(jQuery("#template-editor-1").html());

            var getValueSelectedTemplate = jQuery("input[name='rb-chapter-template']:checked").val();

            var getSplitValueSelectedTemplate = getValueSelectedTemplate.split("-");

            var getTemplateType = getSplitValueSelectedTemplate[0];

            var getThemeNo = getSplitValueSelectedTemplate[1];

            var getTemplateNo  = getSplitValueSelectedTemplate[2];


            jQuery(".ipst-epub-"+getTemplateType+"-theme-"+ getThemeNo +"-template-"+ getTemplateNo + " .ipst-epub-header-desc").text(getChapterName);

            $("#modal-create-chapter").modal("hide");



        });

        $('#modal-create-chapter').on('hidden', function () {

            jQuery(".new-item-added").fadeIn();
            
            var getChapterListHeight = jQuery(".dd > .dd-list").height();

            jQuery('.dd').slimScroll({ scrollTo: getChapterListHeight });
        
            jQuery(".new-item-added").removeClass("new-item-added");



            var getValueSelectedTemplate = jQuery("input[name='rb-chapter-template']:checked").val();

            var getSplitValueSelectedTemplate = getValueSelectedTemplate.split("-");

            var getTemplateType = getSplitValueSelectedTemplate[0];

            var getThemeNo = getSplitValueSelectedTemplate[1];

            var getTemplateNo  = getSplitValueSelectedTemplate[2];


            jQuery(".ipst-epub-"+getTemplateType+"-theme-"+getThemeNo+"-template-"+getTemplateNo).fadeIn();
            jQuery(".ipst-epub-"+getTemplateType+"-theme-"+getThemeNo+"-template-"+getTemplateNo).addClass("ipst-epub-template-selected");

        });

        jQuery(document).on('click', '#btn-confirm-create-section', function(){

            var getSectionName = jQuery("#txt-section-name").val();

            //var getType = jQuery(".dd-selected-item").parent().attr("data-type");

            var getTimeStamp = new Date().getTime();

            var appendNewSection = "";


            var getTypeSelectedItem = "";

            if( jQuery(".dd-selected-item").parent().parent().parent().is("div") ){

                getTypeSelectedItem = "isChapter";
                console.log("ischap");
            }

            if( jQuery(".dd-selected-item").parent().parent().parent().is("li") ){

                getTypeSelectedItem = "isSection";
                console.log("issec");
            }


            if(getTypeSelectedItem == "isChapter"){

                

                var getDDlistSize = jQuery(".dd-selected-item").parent().find(".dd-list").length;

                

                if(getDDlistSize > 0){

                    var appendNewSection = "<li class='dd-item new-item-added' data-timestamp='"+ getTimeStamp + "'  style='display:none;'>" +
                                    "<div class='dd-handle'>" + getSectionName + "</div>" +
                                    "</li>";

                    jQuery(".dd-selected-item").parent().find("> .dd-list").append(appendNewSection);

                    //$("#modal-create-section").modal("hide");

                }
                else{
                    
                    var appendNewSection = "<ol class='dd-list new-item-added' style='display:none;'>" +
                                    "<li class='dd-item' data-timestamp='"+ getTimeStamp + "' >" +
                                    "<div class='dd-handle'>" + getSectionName + "</div>" +
                                    "</li> </ol>";

                    jQuery(".dd-selected-item").parent().append(appendNewSection);

                    //$("#modal-create-section").modal("hide");
                }

                var getSerialize = JSON.stringify(jQuery('#nestable').nestable('serialize'));

                jQuery(".dd").attr("data-serialize-toc",getSerialize);


                jQuery(".ipst-epub-template-selected").hide();
                jQuery(".ipst-epub-template-selected").removeClass("ipst-epub-template-selected");

                var getValueSelectedTemplate = jQuery("input[name='rb-section-template']:checked").val();

                var getSplitValueSelectedTemplate = getValueSelectedTemplate.split("-");

                var getTemplateType = getSplitValueSelectedTemplate[0];

                var getThemeNo = getSplitValueSelectedTemplate[1];

                var getTemplateNo  = getSplitValueSelectedTemplate[2];


                jQuery(".ipst-epub-"+getTemplateType+"-theme-"+ getThemeNo +"-template-"+ getTemplateNo + " .ipst-epub-header-desc").text(getSectionName);

                $("#modal-create-section").modal("hide");
                        
            }
            else if(getTypeSelectedItem == "isSection"){
                

                var appendNewSection = "<li class='dd-item new-item-added' data-timestamp='"+ getTimeStamp + "'  style='display:none;'>" +
                                "<div class='dd-handle'>" + getSectionName + "</div>" +
                                "</li>";

                jQuery(".dd-selected-item").parent().parent().append(appendNewSection);


                var getSerialize = JSON.stringify(jQuery('#nestable').nestable('serialize'));

                jQuery(".dd").attr("data-serialize-toc",getSerialize);


                var getValueSelectedTemplate = jQuery("input[name='rb-section-template']:checked").val();

                var getSplitValueSelectedTemplate = getValueSelectedTemplate.split("-");

                var getTemplateType = getSplitValueSelectedTemplate[0];

                var getThemeNo = getSplitValueSelectedTemplate[1];

                var getTemplateNo  = getSplitValueSelectedTemplate[2];


                jQuery(".ipst-epub-"+getTemplateType+"-theme-"+ getThemeNo +"-template-"+ getTemplateNo + " .ipst-epub-header-desc").text(getSectionName);

                $("#modal-create-section").modal("hide");
            }

            


        });

        $('#modal-create-section').on('hidden', function () {

            jQuery(".new-item-added").fadeIn();

            jQuery(".new-item-added").removeClass("new-item-added");

            var getValueSelectedTemplate = jQuery("input[name='rb-section-template']:checked").val();

            var getSplitValueSelectedTemplate = getValueSelectedTemplate.split("-");

            var getTemplateType = getSplitValueSelectedTemplate[0];

            var getThemeNo = getSplitValueSelectedTemplate[1];

            var getTemplateNo  = getSplitValueSelectedTemplate[2];


            jQuery(".ipst-epub-"+getTemplateType+"-theme-"+getThemeNo+"-template-"+getTemplateNo).fadeIn();
            jQuery(".ipst-epub-"+getTemplateType+"-theme-"+getThemeNo+"-template-"+getTemplateNo).addClass("ipst-epub-template-selected");

        });


        jQuery("#btn-confirm-create-copyright").click(function () {
            
            //var getChapterName = jQuery("#txt-copyright-name").val();

            var getChapterName = "ลิขสิทธิ์"

            var getTimeStamp = new Date().getTime();

            var appendNewChapter = "<li class='dd-item new-item-added' data-timestamp='"+ getTimeStamp + "' style='display:none;'>" +
                                "<div class='dd-handle'>" + getChapterName + "</div>" +
                                "</li>";

            jQuery(".dd > .dd-list").append(appendNewChapter);


            var getSerialize = JSON.stringify(jQuery('#nestable').nestable('serialize'));

            jQuery(".dd").attr("data-serialize-toc",getSerialize);

            
            jQuery(".ipst-epub-template-selected").hide();
            jQuery(".ipst-epub-template-selected").removeClass("ipst-epub-template-selected");


            $("#modal-create-copyright").modal("hide");



        });

        $('#modal-create-copyright').on('hidden', function () {

            jQuery(".new-item-added").fadeIn();
            
            var getChapterListHeight = jQuery(".dd > .dd-list").height();

            jQuery('.dd').slimScroll({ scrollTo: getChapterListHeight });
        
            jQuery(".new-item-added").removeClass("new-item-added");



            var getValueSelectedTemplate = jQuery("input[name='rb-copyright-template']:checked").val();

            var getSplitValueSelectedTemplate = getValueSelectedTemplate.split("-");

            var getTemplateType = getSplitValueSelectedTemplate[0];

            var getThemeNo = getSplitValueSelectedTemplate[1];

            var getTemplateNo  = getSplitValueSelectedTemplate[2];


            jQuery(".ipst-epub-"+getTemplateType+"-theme-"+getThemeNo+"-template-"+getTemplateNo).fadeIn();
            jQuery(".ipst-epub-"+getTemplateType+"-theme-"+getThemeNo+"-template-"+getTemplateNo).addClass("ipst-epub-template-selected");

        });


        jQuery("#btn-confirm-create-dedication").click(function () {
            
            //var getChapterName = jQuery("#txt-copyright-name").val();

            var getChapterName = "คำอุทิศ";

            var getTimeStamp = new Date().getTime();

            var appendNewChapter = "<li class='dd-item new-item-added' data-timestamp='"+ getTimeStamp + "' style='display:none;'>" +
                                "<div class='dd-handle'>" + getChapterName + "</div>" +
                                "</li>";

            jQuery(".dd > .dd-list").append(appendNewChapter);


            var getSerialize = JSON.stringify(jQuery('#nestable').nestable('serialize'));

            jQuery(".dd").attr("data-serialize-toc",getSerialize);

            
            jQuery(".ipst-epub-template-selected").hide();
            jQuery(".ipst-epub-template-selected").removeClass("ipst-epub-template-selected");


            $("#modal-create-dedication").modal("hide");



        });

        $('#modal-create-dedication').on('hidden', function () {

            jQuery(".new-item-added").fadeIn();
            
            var getChapterListHeight = jQuery(".dd > .dd-list").height();

            jQuery('.dd').slimScroll({ scrollTo: getChapterListHeight });
        
            jQuery(".new-item-added").removeClass("new-item-added");



            var getValueSelectedTemplate = jQuery("input[name='rb-dedication-template']:checked").val();

            var getSplitValueSelectedTemplate = getValueSelectedTemplate.split("-");

            var getTemplateType = getSplitValueSelectedTemplate[0];

            var getThemeNo = getSplitValueSelectedTemplate[1];

            var getTemplateNo  = getSplitValueSelectedTemplate[2];


            jQuery(".ipst-epub-"+getTemplateType+"-theme-"+getThemeNo+"-template-"+getTemplateNo).fadeIn();
            jQuery(".ipst-epub-"+getTemplateType+"-theme-"+getThemeNo+"-template-"+getTemplateNo).addClass("ipst-epub-template-selected");

        });


        jQuery("#btn-confirm-create-forward").click(function () {
            
            //var getChapterName = jQuery("#txt-copyright-name").val();

            var getChapterName = "คำนิยม";

            var getTimeStamp = new Date().getTime();

            var appendNewChapter = "<li class='dd-item new-item-added' data-timestamp='"+ getTimeStamp + "' style='display:none;'>" +
                                "<div class='dd-handle'>" + getChapterName + "</div>" +
                                "</li>";

            jQuery(".dd > .dd-list").append(appendNewChapter);


            var getSerialize = JSON.stringify(jQuery('#nestable').nestable('serialize'));

            jQuery(".dd").attr("data-serialize-toc",getSerialize);

            
            jQuery(".ipst-epub-template-selected").hide();
            jQuery(".ipst-epub-template-selected").removeClass("ipst-epub-template-selected");


            $("#modal-create-forward").modal("hide");



        });

        $('#modal-create-forward').on('hidden', function () {

            jQuery(".new-item-added").fadeIn();
            
            var getChapterListHeight = jQuery(".dd > .dd-list").height();

            jQuery('.dd').slimScroll({ scrollTo: getChapterListHeight });
        
            jQuery(".new-item-added").removeClass("new-item-added");



            var getValueSelectedTemplate = jQuery("input[name='rb-forward-template']:checked").val();

            var getSplitValueSelectedTemplate = getValueSelectedTemplate.split("-");

            var getTemplateType = getSplitValueSelectedTemplate[0];

            var getThemeNo = getSplitValueSelectedTemplate[1];

            var getTemplateNo  = getSplitValueSelectedTemplate[2];


            jQuery(".ipst-epub-"+getTemplateType+"-theme-"+getThemeNo+"-template-"+getTemplateNo).fadeIn();
            jQuery(".ipst-epub-"+getTemplateType+"-theme-"+getThemeNo+"-template-"+getTemplateNo).addClass("ipst-epub-template-selected");

        });


        jQuery("#btn-confirm-create-preface").click(function () {
            
            //var getChapterName = jQuery("#txt-copyright-name").val();

            var getChapterName = "คำนำ";

            var getTimeStamp = new Date().getTime();

            var appendNewChapter = "<li class='dd-item new-item-added' data-timestamp='"+ getTimeStamp + "' style='display:none;'>" +
                                "<div class='dd-handle'>" + getChapterName + "</div>" +
                                "</li>";

            jQuery(".dd > .dd-list").append(appendNewChapter);


            var getSerialize = JSON.stringify(jQuery('#nestable').nestable('serialize'));

            jQuery(".dd").attr("data-serialize-toc",getSerialize);

            
            jQuery(".ipst-epub-template-selected").hide();
            jQuery(".ipst-epub-template-selected").removeClass("ipst-epub-template-selected");


            $("#modal-create-preface").modal("hide");



        });

        $('#modal-create-preface').on('hidden', function () {

            jQuery(".new-item-added").fadeIn();
            
            var getChapterListHeight = jQuery(".dd > .dd-list").height();

            jQuery('.dd').slimScroll({ scrollTo: getChapterListHeight });
        
            jQuery(".new-item-added").removeClass("new-item-added");



            var getValueSelectedTemplate = jQuery("input[name='rb-preface-template']:checked").val();

            var getSplitValueSelectedTemplate = getValueSelectedTemplate.split("-");

            var getTemplateType = getSplitValueSelectedTemplate[0];

            var getThemeNo = getSplitValueSelectedTemplate[1];

            var getTemplateNo  = getSplitValueSelectedTemplate[2];


            jQuery(".ipst-epub-"+getTemplateType+"-theme-"+getThemeNo+"-template-"+getTemplateNo).fadeIn();
            jQuery(".ipst-epub-"+getTemplateType+"-theme-"+getThemeNo+"-template-"+getTemplateNo).addClass("ipst-epub-template-selected");

        });


        jQuery("#btn-confirm-create-bibliography").click(function () {
            
            //var getChapterName = jQuery("#txt-copyright-name").val();

            var getChapterName = "บรรณานุกรม";

            var getTimeStamp = new Date().getTime();

            var appendNewChapter = "<li class='dd-item new-item-added' data-timestamp='"+ getTimeStamp + "' style='display:none;'>" +
                                "<div class='dd-handle'>" + getChapterName + "</div>" +
                                "</li>";

            jQuery(".dd > .dd-list").append(appendNewChapter);


            var getSerialize = JSON.stringify(jQuery('#nestable').nestable('serialize'));

            jQuery(".dd").attr("data-serialize-toc",getSerialize);

            
            jQuery(".ipst-epub-template-selected").hide();
            jQuery(".ipst-epub-template-selected").removeClass("ipst-epub-template-selected");


            $("#modal-create-bibliography").modal("hide");



        });

        $('#modal-create-bibliography').on('hidden', function () {

            jQuery(".new-item-added").fadeIn();
            
            var getChapterListHeight = jQuery(".dd > .dd-list").height();

            jQuery('.dd').slimScroll({ scrollTo: getChapterListHeight });
        
            jQuery(".new-item-added").removeClass("new-item-added");



            var getValueSelectedTemplate = jQuery("input[name='rb-bibliography-template']:checked").val();

            var getSplitValueSelectedTemplate = getValueSelectedTemplate.split("-");

            var getTemplateType = getSplitValueSelectedTemplate[0];

            var getThemeNo = getSplitValueSelectedTemplate[1];

            var getTemplateNo  = getSplitValueSelectedTemplate[2];


            jQuery(".ipst-epub-"+getTemplateType+"-theme-"+getThemeNo+"-template-"+getTemplateNo).fadeIn();
            jQuery(".ipst-epub-"+getTemplateType+"-theme-"+getThemeNo+"-template-"+getTemplateNo).addClass("ipst-epub-template-selected");

        });

        jQuery(document).on('click', '#btn-delete-chapterlist', function(){

            jQuery(".dd-selected-item").parent().remove();

            jQuery("#content-panel").fadeOut();

            jQuery("#content-panel").removeClass("opened");

            var getSerialize = JSON.stringify(jQuery('#nestable').nestable('serialize'));

            jQuery(".dd").attr("data-serialize-toc",getSerialize);
        });


        jQuery("#btn-create-chapter").click(function () {
            
            $('#modal-create-chapter').modal({
                keyboard: false,
                backdrop: true,
                show:true

            });
            

        });

        jQuery("#btn-create-section").click(function () {
            
            $('#modal-create-section').modal({
                keyboard: false,
                backdrop: true,
                show:true

            });


        });

        jQuery("#btn-create-copyright").click(function () {
            
            $('#modal-create-copyright').modal({
                keyboard: false,
                backdrop: true,
                show:true

            });


        });

        jQuery("#btn-create-dedication").click(function () {
            
            $('#modal-create-dedication').modal({
                keyboard: false,
                backdrop: true,
                show:true

            });


        });

        jQuery("#btn-create-forward").click(function () {
            
            $('#modal-create-forward').modal({
                keyboard: false,
                backdrop: true,
                show:true

            });


        });

        jQuery("#btn-create-preface").click(function () {
            
            $('#modal-create-preface').modal({
                keyboard: false,
                backdrop: true,
                show:true

            });


        });

        jQuery("#btn-create-bibliography").click(function () {
            
            $('#modal-create-bibliography').modal({
                keyboard: false,
                backdrop: true,
                show:true

            });


        });

        jQuery('.dd').on('change', function(e) {
            /* on change event */

            //alert(e.type); // "click"


            // ทำการตรวจสอบถ้าไม่มีการเปลี่ยนแปลงตำแหน่งก็ไม่ต้องทำอะไร

            var getPreviousSerialize =  jQuery(".dd").attr("data-serialize-toc");

            var getSerialize = JSON.stringify(jQuery('#nestable').nestable('serialize'));


            if(getPreviousSerialize == getSerialize){
                //alert("no re-ordering");
            }

            if(getPreviousSerialize != getSerialize){
                //alert("re-ordering");

                var getAllDDhandle = jQuery(".dd").find(".dd-handle");

                getAllDDhandle.each(function(){
                    jQuery(this).removeClass("dd-handle");
                    jQuery(this).addClass("dd-disable");
                });

                jQuery('.dd').animate({opacity: 0.4});

                jQuery("#btn-create-chapter-section").prop('disabled',true);
                jQuery("#btn-delete-chapterlist").prop('disabled',true);
                jQuery("#btn-expand-all").prop('disabled',true);
                jQuery("#btn-collapse-all").prop('disabled',true);


                jQuery('#chapter-list-reordering').show(1).delay(2000).hide(1,function () 
                {  

                    var getAllDDhandle = jQuery(".dd").find(".dd-disable");

                    getAllDDhandle.each(function(){
                        jQuery(this).removeClass("dd-disable");
                        jQuery(this).addClass("dd-handle");
                    });

                    jQuery('.dd').animate({opacity: 1.0});

                    jQuery("#btn-create-chapter-section").prop('disabled',false);
                    jQuery("#btn-delete-chapterlist").prop('disabled',false);
                    jQuery("#btn-expand-all").prop('disabled',false);
                    jQuery("#btn-collapse-all").prop('disabled',false);


                    // เมื่อมีการเปลี่ยนแปลงตำแหน่งเรียบร้อยแล้ว update ค่าตำแหน่งใหม่กลับเข้าไป
                    jQuery(".dd").attr("data-serialize-toc",getSerialize);
                });

            }
            




        });


    // end manage chapter list


    //CKEDITOR.replace( 'editable');

	jQuery('.dd').slimScroll({
        height: '300px',
        width: '100%',
        size: '5px',
        color: '#999999',
        distance: '0px',
        start:'.dd'
    });
	jQuery(".navbar").show();


    jQuery("#btnInline").click(function () {
        // CKEDITOR.replace( 'editable' );


         var editor_data = CKEDITOR.instances.editable.getData();

         alert(editor_data);

        CKEDITOR.instances.editableinline.setData(editor_data);

    //     jQuery('.editor-active').hide();
    //     jQuery('.editor-unactive').show();

    //     CKEDITOR.disableAutoInline = true;
    // CKEDITOR.inline( 'editableinline' );

    //jQuery('#editBoxUninline').hide();

    });

    jQuery("#btn-gen-math-jax").click(function () {
        
        $("#display-math-jax").html("$\\frac{1}{2}$");
         MathJax.Hub.Queue(["Typeset", MathJax.Hub, "display-math-jax"]);
    });

    jQuery("#btnNewChapter").click(function () {
        
        $('#myModal').modal({
            keyboard: false,
            backdrop: true,
            show:true

        });
        
        $('#myTab a[href="#home"]').tab('show');

    });

	jQuery("#btnImportChapter").click(function () {
		// var getSizeOfTOC = jQuery("#ch-list-sortable li").size();
		
  //       var insertContentTB = 	"<li style='display:none;' class='ui-ch-list-item'>"+
  //                               "<div style='position:relative; float:right;'>"+
  //                               "<i class='icon-align-justify icon-white'></i>"+
  //                               "</div>"+
  //                               "<p style='width=230px;'>"+
  //                               "<span>"+(getSizeOfTOC+1)+". </span>"+
  //                               "<span>"+ "Chapter 3  Points, lines and planes" + "</span>"+
  //                               "</p>"+
  //                               "</li>";

  //       jQuery("#ch-list-sortable").append(insertContentTB);

        
        

  //       jQuery("#ch-list-sortable li:eq("+(getSizeOfTOC)+")").fadeIn();

  //       var getChapterListHeight = jQuery("#ch-list-sortable").height();

  //       jQuery('.scrollspy-chapterlist').slimScroll({ scrollTo: getChapterListHeight });
  
        //jQuery("#tbChapterList tbody tr:eq(6)").fadeIn();
	});

	jQuery(document).on('click', 'tr', function(){

		jQuery(".select-item").removeClass('info select-item');//ลบตัวก่อนหน้าก่อนถ้ามีการ click hilight ตัวอื่นมาก่อนแล้ว
		jQuery(this).addClass('info select-item');
		//alert(jQuery(this).find("td:eq(0)").text());
	});


    jQuery("#btnSendEmail").click(function () {
        
        $('#modal-send-email').modal({
            keyboard: false,
            backdrop: true,
            show:true

        });
        
        $('#myTab a[href="#tab-email-epub"]').tab('show');

    });

    jQuery("#btnCreateTemplate").click(function () {
        
        $('#modal-create-template').modal({
            keyboard: false,
            backdrop: true,
            show:true

        });
        

    });
});

