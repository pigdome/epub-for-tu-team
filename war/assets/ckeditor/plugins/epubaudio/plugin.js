/**
 * Basic sample plugin inserting matheviation elements into CKEditor editing area.
 *
 * Created out of the CKEditor Plugin SDK:
 * http://docs.ckeditor.com/#!/guide/plugin_sdk_sample_1
 */

// Register the plugin within the editor.
CKEDITOR.plugins.add( 'epubaudio', {

	// Register the icons.
	icons: 'epubaudio',

	// The plugin initialization logic goes inside this method.
	init: function( editor ) {

		// Define an editor command that opens our dialog.
		editor.addCommand( 'epubaudio', new CKEDITOR.dialogCommand( 'epubaudioDialog' ) );

		// Create a toolbar button that executes the above command.
		editor.ui.addButton( 'epubaudio', {

			// The text part of the button (if available) and tooptip.
			label: 'Add audio',

			// The command to execute on click.
			command: 'epubaudio',

			// The button placement in the toolbar (toolbar group name).
			toolbar: 'media'
		});

		if ( editor.contextMenu ) {
			editor.addMenuGroup( 'epubMediaGroup' );
			editor.addMenuItem( 'epubaudioItem', {
				label: 'Change audio',
				icon: this.path + 'icons/epubaudio.png',
				command: 'epubaudio',
				group: 'epubMediaGroup'
			});

			editor.contextMenu.addListener( function( element ) {
				if ( element.getAscendant( 'audio', true ) ) { // ให้ plugin intercept tag <audio/> เพื่อให้ใช้ context menu กับ element นี้ได้ (i.e., edit/change) 
					return { epubaudioItem: CKEDITOR.TRISTATE_OFF };
				}
			});
		}

		// Register our dialog file. this.path is the plugin folder path.
		CKEDITOR.dialog.add( 'epubaudioDialog', this.path + 'dialogs/epubaudio.js' );
	}
});

