/**
 * The math dialog definition.
 *
 * Created out of the CKEditor Plugin SDK:
 * http://docs.ckeditor.com/#!/guide/plugin_sdk_sample_1
 */

// Our dialog definition.
CKEDITOR.dialog.add( 'epubaudioDialog', function( editor ) {

	var $el={
		//myCKAudios: $("#myCKAudios"), <-- ไม่สามารถ cache ได้ เพราะถูกสร้างแบบ dynamic     TODO: cache ใน onShow ได้ 
		authorId: $("#authorId"),
	};
	
	var myCKAudioTemplate=Handlebars.templates["my-ck-audio"];

	var loadMyCKAudios=function(audioIdToHighlight) {
		
		$(".myCKAudios").empty(); 

		MediaEndpoint.list({authorId: $el.authorId.val(), type: "audio"}, function(data) {
			var audios=data[0]; 
			var cursorString=data[1]; // TODO: ทำ infinite scrolling!
			$(".myCKAudios").append(myCKAudioTemplate(audios)); 
			MediaEndpoint.loadAudioAsync($(".myCKAudios").find("audio.myCKAudio"));
			
			$("#"+audioIdToHighlight).addClass("selected-audio");
			
			$(".myCKAudios").on("click", "audio.myCKAudio", function() {
				$(".selected-audio").removeClass("selected-audio"); // clear previously selected
				$(this).addClass("selected-audio");
			});
		});
	};
	
	return {

		// Basic properties of the dialog window: title, minimum size.
		title: 'เลือกไฟล์เสียง',
		minWidth: 500,
		minHeight: 400,

		// Dialog window contents definition.
		contents: [
			{
				// Definition of the Basic Settings dialog tab (page).
				id: 'tab-basic',
				label: 'Basic Settings',

				// bootstrap ไม่ได้ เพราะ include css ไม่ได้
				
				elements: [
					{
						// ทำไมถึงต้องใช้ class แทน id --> https://sites.google.com/a/punsarn.asia/ipst/learning-space-phase-1/epub-editor/caveats/ckeditor/multiple-inline-editors
					    type: 'html',
					    html: '<div class="myCKAudios" style="overflow:auto; height:400px; white-space: normal;"></div>',
						setup: function(audioId) {
							var id;
							if(audioId)
								id=audioId.split("id")[1]; // take out "id"
							loadMyCKAudios(id);
						}

					}

				]

			}
		],

		// Invoked when the dialog is loaded.
		onShow: function() {

			//loadMyCKAudios(); ยัง load ตอนนี้ไม่ได้ เพราะยังไม่ได้สร้าง elements ของ dialog นี้เลย เรียกข้างล่างสุด
			
			// Get the selection in the editor.
			var selection = editor.getSelection();

			// Get the element at the start of the selection.
			var element = selection.getStartElement();

			var targetId=null;
			if(element)
				targetId=element.getId();
			
			this.setupContent(targetId); // send the id of the target audio to be highlighted
		},

		// This method is invoked once a user clicks the OK button, confirming the dialog.
		onOk: function() {

			$(".myCKAudios audio.myCKAudio").off("click"); // prevent event listeners to be added repeatedly

			var originalId=$(".selected-audio").attr("id");
			var $audio=$(".selected-audio")
					.removeClass("selected-audio") // remove selected-video to take out the shadow box
					.removeClass("img-polaroid")
					.attr("style", "width: 300px;")  // set fixed width 
					.attr("id", "id"+originalId); // have to make it different otherwise id crash and adding box-shadow at dialog launch won't work
			var epubaudio=new CKEDITOR.dom.element($audio.get(0));
			editor.insertElement(epubaudio);
		}
	};
});