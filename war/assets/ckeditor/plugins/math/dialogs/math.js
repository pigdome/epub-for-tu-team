/**
 * The math dialog definition.
 *
 * Created out of the CKEditor Plugin SDK:
 * http://docs.ckeditor.com/#!/guide/plugin_sdk_sample_1
 */

// Our dialog definition.
CKEDITOR.dialog.add( 'mathDialog', function( editor ) {

	var insertStringAtPosition=function(insertedString, originalString, position) {
		return [originalString.slice(0, position), insertedString, originalString.slice(position)].join('');
	};
	
	var previewMath=function(latex) {
    	$("#math-live-preview").html("$"+latex+"$");
    	MathJax.Hub.Queue(["Typeset", MathJax.Hub, "math-live-preview"]);		
	};
	
	var LaTeX={
	
	//common tab
		equalTo: "=",
		plus: "+",
		minus: "-",
		multiply: "\\times",
		asterisk: "*",
		percentage: "\\%",
		divide: "/",
		division: "\\div",
		plusOrMinus: "\\pm",
		minusOrPlus: "\\mp",
		squareRoot: "\\sqrt{2}",
		nthRootOf2: "\\sqrt[n]{2}",
		fraction: "\\frac{a}{b}",
		superscriptX2: "x^2",
		naturalLogarithm: "\\ln",
		logarithmToBaseE: "\\log e",
		logarithmToBase10: "\\log_{10} m",
		parenthesis: "\\left ( \\right )",
		squareBrackets: "\\left [ \\right ]",
		lessThan: "<",
		lessThanOrEqualTo: "\\le",
		greaterThanOrEqual: "\\ge",
		greaterThan: ">",
		isAproximatelyEqual: "\\approx",
		pi: "\\pi",
		angle: "\\angle",
		perpendicular: "\\perp",
		isParallelTo: "\\|",
		btn45Degree: "45^\\circ ",
		therefore: "\\therefore",
		congruence: "\\cong",
		overRightArrow: "\\overrightarrow{a b}",
		overLeftArrow: "\\overleftarrow{c d}",
		overLeftRightArrow: "\\overleftrightarrow{a b}",
		lineOver: "\\overline{abc}",
		theta: "\\theta",
		sine: "\\sin",
		cosine: "\\cos",
		tangent: "\\tan",
		secant: "\\sec",
		cosecant: "\\csc",
		cotangent: "\\cot",
		booleanAnd: "\\land",
		booleanOr: "\\lor",
		booleanNot: "\\lnot",
		forAll: "\\forall",
		exists: "\\exists",
		doubleLeftArrow: "\\Leftarrow",
		doubleRightArrow: "\\Rightarrow",
		summation: "\\sum_{k=1}^N",
		limitFromNToInfinity: "\\lim_{n \\to \\infty}",
		derivative: "x'",
		leibnizNotation: "\\frac{dy}{dx}\\,x",
		leibnizInline: "dy/dx",
		integral: "\\int_{1}^{3}",
		emptySet: "\\emptyset",
		elementOf: "\\in",
		notElementOf: "\\notin",
		subset: "\\subset",
		subsetOrEqualTo: "\\subseteq",
		superSet: "\\supset",
		superSetOrEqualTo: "\\supseteq",
		intersection: "\\cap",
		union: "\\cup",
	//


	};

	var LaTeXDiffInt={

	// derivative & integration Tab
		nabia: "\\nabla \\,",
		partialX: "\\partial x\\,",
		dx: "dx",
		leibnizInline: "dy/dx",
		dotX: "\\dot x\\,",
		dotY: "\\dot y\\,",
		leibnizNotation: "\\frac{dy}{dx}\\,x",
		integral: "\\int_{1}^{3}",
		textStyleIntegrationLimitNToNegN: "\\textstyle \\int\\limits_{-N}^{N}",
		textStyleIntegrationLimitNegNToNDiffPresentation: "\\textstyle \\int_{-N}^{N}",
		integrationLimit1To3: "\\int\\limits_{1}^{3}",
		doubleIntegrationLowerLimitD: "\\iint\\limits_D",
		tripleIntegrationLowerLimitE: "\\iiint\\limits_E",
		quadrupleIntegrationLowerLimitF: "\\iiiint\\limits_F",
		zeroIntegrationLowerLimitC: "\\oint_C",


	//

	}

	var LaTeXMatrix={
	// matrix Tab
		matrix: "\\begin{matrix}a1 & a2 & a3\\\\b1 & b2 & b3\\end{matrix}",
		barMatrix: "\\begin{vmatrix}a1 & a2 & a3\\\\b1 & b2 & b3\\end{vmatrix}",
		doubleBarMatrix: "\\begin{Vmatrix}a1 & a2 & a3\\\\b1 & b2 & b3\\end{Vmatrix}",
		parenthesisMatrix: "\\begin{pmatrix}a1 & a2 & a3\\\\b1 & b2 & b3\\end{pmatrix}",
		bracketMatrix: "\\begin{bmatrix}a1 & a2 & a3\\\\b1 & b2 & b3\\end{bmatrix}",
		braceMatrix: "\\begin{Bmatrix}a1 & a2 & a3\\\\b1 & b2 & b3\\end{Bmatrix}",
		smallMatrix: "\\begin{smallmatrix}a1 & a2 & a3\\\\b1 & b2 & b3\\end{smallmatrix}",
		rightSidedMatrix: "\\left.\\begin{matrix}a1 & a2 & a3\\\\b1 & b2 & b3\\end{matrix}\\right|"

	//
	}

	var onEventsInLaTeXPanel=function(thisDialog) {
		
	// common tab
		var equalTo=CKEDITOR.document.getById("cm-equal-to");
		equalTo.on("click", function() {
			updateWithLaTeX(LaTeX.equalTo, thisDialog);
		});

		var plus=CKEDITOR.document.getById("cm-plus");
		plus.on("click", function() {
			updateWithLaTeX(LaTeX.plus, thisDialog);
		});

		var minus=CKEDITOR.document.getById("cm-minus");
		minus.on("click", function() {
			updateWithLaTeX(LaTeX.minus, thisDialog);
		});

		var multiply=CKEDITOR.document.getById("cm-multiply");
		multiply.on("click", function() {
			updateWithLaTeX(LaTeX.multiply, thisDialog);
		});

		var asterisk=CKEDITOR.document.getById("cm-asterisk");
		asterisk.on("click", function() {
			updateWithLaTeX(LaTeX.asterisk, thisDialog);
		});

		var percentage=CKEDITOR.document.getById("cm-percentage");
		percentage.on("click", function() {
			updateWithLaTeX(LaTeX.percentage, thisDialog);
		});

		var divide=CKEDITOR.document.getById("cm-divide");
		divide.on("click", function() {
			updateWithLaTeX(LaTeX.divide, thisDialog);
		});

		var division=CKEDITOR.document.getById("cm-division");
		division.on("click", function() {
			updateWithLaTeX(LaTeX.division, thisDialog);
		});

		var plusOrMinus=CKEDITOR.document.getById("cm-plus-or-minus");
		plusOrMinus.on("click", function() {
			updateWithLaTeX(LaTeX.plusOrMinus, thisDialog);
		});

		var minusOrPlus=CKEDITOR.document.getById("cm-minus-or-plus");
		minusOrPlus.on("click", function() {
			updateWithLaTeX(LaTeX.minusOrPlus, thisDialog);
		});

		var squareRoot=CKEDITOR.document.getById("cm-square-root");
		squareRoot.on("click", function() {
			updateWithLaTeX(LaTeX.squareRoot, thisDialog);
		});

		var nthRootOf2=CKEDITOR.document.getById("cm-nth-root-of-2");
		nthRootOf2.on("click", function() {
			updateWithLaTeX(LaTeX.nthRootOf2, thisDialog);
		});

		var fraction=CKEDITOR.document.getById("cm-fraction");
		fraction.on("click", function() {
			updateWithLaTeX(LaTeX.fraction, thisDialog);
		});

		var superscriptX2=CKEDITOR.document.getById("cm-superscript-x^2");
		superscriptX2.on("click", function() {
			updateWithLaTeX(LaTeX.superscriptX2, thisDialog);
		});

		var logarithmToBaseE=CKEDITOR.document.getById("cm-logarithm-to-base-e");
		logarithmToBaseE.on("click", function() {
			updateWithLaTeX(LaTeX.logarithmToBaseE, thisDialog);
		});

		var naturalLogarithm=CKEDITOR.document.getById("cm-natural-logarithm");
		naturalLogarithm.on("click", function() {
			updateWithLaTeX(LaTeX.naturalLogarithm, thisDialog);
		});

		var logarithmToBase10=CKEDITOR.document.getById("cm-logarithm-to-base-10");
		logarithmToBase10.on("click", function() {
			updateWithLaTeX(LaTeX.logarithmToBase10, thisDialog);
		});

		var parenthesis=CKEDITOR.document.getById("cm-parenthesis");
		parenthesis.on("click", function() {
			updateWithLaTeX(LaTeX.parenthesis, thisDialog);
		});

		var squareBrackets=CKEDITOR.document.getById("cm-square-brackets");
		squareBrackets.on("click", function() {
			updateWithLaTeX(LaTeX.squareBrackets, thisDialog);
		});

		var lessThan=CKEDITOR.document.getById("cm-less-than");
		lessThan.on("click", function() {
			updateWithLaTeX(LaTeX.lessThan, thisDialog);
		});

		var lessThanOrEqualTo=CKEDITOR.document.getById("cm-less-than-or-equal-to");
		lessThanOrEqualTo.on("click", function() {
			updateWithLaTeX(LaTeX.lessThanOrEqualTo, thisDialog);
		});

		var greaterThanOrEqual=CKEDITOR.document.getById("cm-greater-than-or-equal-to");
		greaterThanOrEqual.on("click", function() {
			updateWithLaTeX(LaTeX.greaterThanOrEqual, thisDialog);
		});

		var greaterThan=CKEDITOR.document.getById("cm-greater-than");
		greaterThan.on("click", function() {
			updateWithLaTeX(LaTeX.greaterThan, thisDialog);
		});

		var isAproximatelyEqual=CKEDITOR.document.getById("cm-is-aproximately-equal");
		isAproximatelyEqual.on("click", function() {
			updateWithLaTeX(LaTeX.isAproximatelyEqual, thisDialog);
		});

		var pi=CKEDITOR.document.getById("cm-pi");
		pi.on("click", function() {
			updateWithLaTeX(LaTeX.pi, thisDialog);
		});

		var angle=CKEDITOR.document.getById("cm-angle");
		angle.on("click", function() {
			updateWithLaTeX(LaTeX.angle, thisDialog);
		});

		var perpendicular=CKEDITOR.document.getById("cm-perpendicular");
		perpendicular.on("click", function() {
			updateWithLaTeX(LaTeX.perpendicular, thisDialog);
		});

		var isParallelTo=CKEDITOR.document.getById("cm-is-parallel-to");
		isParallelTo.on("click", function() {
			updateWithLaTeX(LaTeX.isParallelTo, thisDialog);
		});

		var btn45Degree=CKEDITOR.document.getById("cm-45-degree");
		btn45Degree.on("click", function() {
			updateWithLaTeX(LaTeX.btn45Degree, thisDialog);
		});

		var therefore=CKEDITOR.document.getById("cm-therefore");
		therefore.on("click", function() {
			updateWithLaTeX(LaTeX.therefore, thisDialog);
		});

		var congruence=CKEDITOR.document.getById("cm-congruence");
		congruence.on("click", function() {
			updateWithLaTeX(LaTeX.congruence, thisDialog);
		});

		var overRightArrow=CKEDITOR.document.getById("cm-over-right-arrow");
		overRightArrow.on("click", function() {
			updateWithLaTeX(LaTeX.overRightArrow, thisDialog);
		});

		var overLeftArrow=CKEDITOR.document.getById("cm-over-left-arrow");
		overLeftArrow.on("click", function() {
			updateWithLaTeX(LaTeX.overLeftArrow, thisDialog);
		});

		var overLeftRightArrow=CKEDITOR.document.getById("cm-over-left-right-arrow");
		overLeftRightArrow.on("click", function() {
			updateWithLaTeX(LaTeX.overLeftRightArrow, thisDialog);
		});

		var lineOver=CKEDITOR.document.getById("cm-line-over");
		lineOver.on("click", function() {
			updateWithLaTeX(LaTeX.lineOver, thisDialog);
		});

		var theta=CKEDITOR.document.getById("cm-theta");
		theta.on("click", function() {
			updateWithLaTeX(LaTeX.theta, thisDialog);
		});

		var sine=CKEDITOR.document.getById("cm-sine");
		sine.on("click", function() {
			updateWithLaTeX(LaTeX.sine, thisDialog);
		});

		var cosine=CKEDITOR.document.getById("cm-cosine");
		cosine.on("click", function() {
			updateWithLaTeX(LaTeX.cosine, thisDialog);
		});

		var tangent=CKEDITOR.document.getById("cm-tangent");
		tangent.on("click", function() {
			updateWithLaTeX(LaTeX.tangent, thisDialog);
		});

		var secant=CKEDITOR.document.getById("cm-secant");
		secant.on("click", function() {
			updateWithLaTeX(LaTeX.secant, thisDialog);
		});

		var cosecant=CKEDITOR.document.getById("cm-cosecant");
		cosecant.on("click", function() {
			updateWithLaTeX(LaTeX.cosecant, thisDialog);
		});

		var cotangent=CKEDITOR.document.getById("cm-cotangent");
		cotangent.on("click", function() {
			updateWithLaTeX(LaTeX.cotangent, thisDialog);
		});

		var booleanAnd=CKEDITOR.document.getById("cm-boolean-and");
		booleanAnd.on("click", function() {
			updateWithLaTeX(LaTeX.booleanAnd, thisDialog);
		});

		var booleanOr=CKEDITOR.document.getById("cm-boolean-or");
		booleanOr.on("click", function() {
			updateWithLaTeX(LaTeX.booleanOr, thisDialog);
		});

		var booleanNot=CKEDITOR.document.getById("cm-boolean-not");
		booleanNot.on("click", function() {
			updateWithLaTeX(LaTeX.booleanNot, thisDialog);
		});

		var forAll=CKEDITOR.document.getById("cm-for-all");
		forAll.on("click", function() {
			updateWithLaTeX(LaTeX.forAll, thisDialog);
		});

		var exists=CKEDITOR.document.getById("cm-exists");
		exists.on("click", function() {
			updateWithLaTeX(LaTeX.exists, thisDialog);
		});

		var doubleLeftArrow=CKEDITOR.document.getById("cm-double-left-arrow");
		doubleLeftArrow.on("click", function() {
			updateWithLaTeX(LaTeX.doubleLeftArrow, thisDialog);
		});

		var doubleRightArrow=CKEDITOR.document.getById("cm-double-right-arrow");
		doubleRightArrow.on("click", function() {
			updateWithLaTeX(LaTeX.doubleRightArrow, thisDialog);
		});

		var summation=CKEDITOR.document.getById("cm-summation");
		summation.on("click", function() {
			updateWithLaTeX(LaTeX.summation, thisDialog);
		});

		var limitFromNToInfinity=CKEDITOR.document.getById("cm-limit-from-n-to-infinity");
		limitFromNToInfinity.on("click", function() {
			updateWithLaTeX(LaTeX.limitFromNToInfinity, thisDialog);
		});

		var derivative=CKEDITOR.document.getById("cm-derivative");
		derivative.on("click", function() {
			updateWithLaTeX(LaTeX.derivative, thisDialog);
		});

		var leibnizNotation=CKEDITOR.document.getById("cm-leibniz-notation");
		leibnizNotation.on("click", function() {
			updateWithLaTeX(LaTeX.leibnizNotation, thisDialog);
		});

		var leibnizInline=CKEDITOR.document.getById("cm-leibniz-inline");
		leibnizInline.on("click", function() {
			updateWithLaTeX(LaTeX.leibnizInline, thisDialog);
		});

		var integral=CKEDITOR.document.getById("cm-integral");
		integral.on("click", function() {
			updateWithLaTeX(LaTeX.integral, thisDialog);
		});

		var emptySet=CKEDITOR.document.getById("cm-empty-set");
		emptySet.on("click", function() {
			updateWithLaTeX(LaTeX.emptySet, thisDialog);
		});

		var elementOf=CKEDITOR.document.getById("cm-element-of");
		elementOf.on("click", function() {
			updateWithLaTeX(LaTeX.elementOf, thisDialog);
		});

		var notElementOf=CKEDITOR.document.getById("cm-not-element-of");
		notElementOf.on("click", function() {
			updateWithLaTeX(LaTeX.notElementOf, thisDialog);
		});

		var subset=CKEDITOR.document.getById("cm-subset");
		subset.on("click", function() {
			updateWithLaTeX(LaTeX.subset, thisDialog);
		});

		var subsetOrEqualTo=CKEDITOR.document.getById("cm-subset-or-equal-to");
		subsetOrEqualTo.on("click", function() {
			updateWithLaTeX(LaTeX.subsetOrEqualTo, thisDialog);
		});

		var superSet=CKEDITOR.document.getById("cm-super-set");
		superSet.on("click", function() {
			updateWithLaTeX(LaTeX.superSet, thisDialog);
		});

		var superSetOrEqualTo=CKEDITOR.document.getById("cm-super-set-or-equal-to");
		superSetOrEqualTo.on("click", function() {
			updateWithLaTeX(LaTeX.superSetOrEqualTo, thisDialog);
		});

		var intersection=CKEDITOR.document.getById("cm-intersection");
		intersection.on("click", function() {
			updateWithLaTeX(LaTeX.intersection, thisDialog);
		});

		var union=CKEDITOR.document.getById("cm-union");
		union.on("click", function() {
			updateWithLaTeX(LaTeX.union, thisDialog);
		});
	//

	// derivative & integration Tab
		var nabia=CKEDITOR.document.getById("di-nabia");
		nabia.on("click", function() {
			updateWithLaTeX(LaTeXDiffInt.nabia, thisDialog);
		});

		var partialX=CKEDITOR.document.getById("di-partial-x");
		partialX.on("click", function() {
			updateWithLaTeX(LaTeXDiffInt.partialX, thisDialog);
		});

		var dx=CKEDITOR.document.getById("di-dx");
		dx.on("click", function() {
			updateWithLaTeX(LaTeXDiffInt.dx, thisDialog);
		});

		var leibnizInline=CKEDITOR.document.getById("di-leibniz-inline");
		leibnizInline.on("click", function() {
			updateWithLaTeX(LaTeXDiffInt.leibnizInline, thisDialog);
		});

		var dotX=CKEDITOR.document.getById("di-dot-x");
		dotX.on("click", function() {
			updateWithLaTeX(LaTeXDiffInt.dotX, thisDialog);
		});

		var dotY=CKEDITOR.document.getById("di-dot-y");
		dotY.on("click", function() {
			updateWithLaTeX(LaTeXDiffInt.dotY, thisDialog);
		});

		var leibnizNotation=CKEDITOR.document.getById("di-leibniz-notation");
		leibnizNotation.on("click", function() {
			updateWithLaTeX(LaTeXDiffInt.leibnizNotation, thisDialog);
		});

		var integral=CKEDITOR.document.getById("di-integral");
		integral.on("click", function() {
			updateWithLaTeX(LaTeXDiffInt.integral, thisDialog);
		});

		var textStyleIntegrationLimitNToNegN=CKEDITOR.document.getById("di-text-style-integration-limit-n-to--n");
		textStyleIntegrationLimitNToNegN.on("click", function() {
			updateWithLaTeX(LaTeXDiffInt.textStyleIntegrationLimitNToNegN, thisDialog);
		});

		var textStyleIntegrationLimitNegNToNDiffPresentation=CKEDITOR.document.getById("di-text-style-integration-limit--n-to-n-diff-presentation");
		textStyleIntegrationLimitNegNToNDiffPresentation.on("click", function() {
			updateWithLaTeX(LaTeXDiffInt.textStyleIntegrationLimitNegNToNDiffPresentation, thisDialog);
		});

		var integrationLimit1To3=CKEDITOR.document.getById("di-integration-limit-1-to-3");
		integrationLimit1To3.on("click", function() {
			updateWithLaTeX(LaTeXDiffInt.integrationLimit1To3, thisDialog);
		});

		var doubleIntegrationLowerLimitD=CKEDITOR.document.getById("di-double-integration-lower-limit-d");
		doubleIntegrationLowerLimitD.on("click", function() {
			updateWithLaTeX(LaTeXDiffInt.doubleIntegrationLowerLimitD, thisDialog);
		});

		var tripleIntegrationLowerLimitE=CKEDITOR.document.getById("di-triple-integration-lower-limit-e");
		tripleIntegrationLowerLimitE.on("click", function() {
			updateWithLaTeX(LaTeXDiffInt.tripleIntegrationLowerLimitE, thisDialog);
		});

		var quadrupleIntegrationLowerLimitF=CKEDITOR.document.getById("di-quadruple-integration-lower-limit-f");
		quadrupleIntegrationLowerLimitF.on("click", function() {
			updateWithLaTeX(LaTeXDiffInt.quadrupleIntegrationLowerLimitF, thisDialog);
		});

		var zeroIntegrationLowerLimitC=CKEDITOR.document.getById("di-0-integration-lower-limit-c");
		zeroIntegrationLowerLimitC.on("click", function() {
			updateWithLaTeX(LaTeXDiffInt.zeroIntegrationLowerLimitC, thisDialog);
		});
	//

	// matrix Tab
		var matrix=CKEDITOR.document.getById("mt-matrix");
		matrix.on("click", function() {
			updateWithLaTeX(LaTeXMatrix.matrix, thisDialog);
		});

		var barMatrix=CKEDITOR.document.getById("mt-bar-matrix");
		barMatrix.on("click", function() {
			updateWithLaTeX(LaTeXMatrix.barMatrix, thisDialog);
		});

		var doubleBarMatrix=CKEDITOR.document.getById("mt-double-bar-matrix");
		doubleBarMatrix.on("click", function() {
			updateWithLaTeX(LaTeXMatrix.doubleBarMatrix, thisDialog);
		});

		var parenthesisMatrix=CKEDITOR.document.getById("mt-parenthesis-matrix");
		parenthesisMatrix.on("click", function() {
			updateWithLaTeX(LaTeXMatrix.parenthesisMatrix, thisDialog);
		});

		var bracketMatrix=CKEDITOR.document.getById("mt-bracket-matrix");
		bracketMatrix.on("click", function() {
			updateWithLaTeX(LaTeXMatrix.bracketMatrix, thisDialog);
		});

		var braceMatrix=CKEDITOR.document.getById("mt-brace-matrix");
		braceMatrix.on("click", function() {
			updateWithLaTeX(LaTeXMatrix.braceMatrix, thisDialog);
		});

		var smallMatrix=CKEDITOR.document.getById("mt-small-matrix");
		smallMatrix.on("click", function() {
			updateWithLaTeX(LaTeXMatrix.smallMatrix, thisDialog);
		});

		var rightSidedMatrix=CKEDITOR.document.getById("mt-right-sided-matrix");
		rightSidedMatrix.on("click", function() {
			updateWithLaTeX(LaTeXMatrix.rightSidedMatrix, thisDialog);
		});
	//

	};


	var updateWithLaTeX=function(latex, dialog) {
		var mathInput=dialog.getContentElement("tab-basic", "mathInput");

		var caretPosition = document.getElementById(mathInput.getInputElement().getId()).selectionStart; 
		var originalString=mathInput.getValue();
		var updatedString=insertStringAtPosition(latex, originalString, caretPosition);
		mathInput.setValue(updatedString);
    	previewMath(updatedString);		
	};
	

	
	return {

		// Basic properties of the dialog window: title, minimum size.
		title: 'Math Properties',
		minWidth: 630,
		minHeight: 400,

		// Dialog window contents definition.
		contents: [
			{
				// Definition of the Basic Settings dialog tab (page).
				id: 'tab-basic',
				label: 'Basic Settings',

				// The tab contents.
				elements: [
					{
					    type: 'html',
					    html: '<ul id="myTab" class="nav nav-tabs" style="border-bottom:1px solid lightgray;">'+
					    '<li><a href="#tab-symbols-common" data-toggle="tab">Common</a></li>'+
					    '<li><a href="#tab-symbols-derivative-integration" data-toggle="tab">Derivative & Integration</a></li>'+
					    '<li><a href="#tab-symbols-matrix" data-toggle="tab">Matrix</a></li>'+
					    '</ul><div id="myTabContent" class="tab-content">'+
					    '<div class="tab-pane fade" id="tab-symbols-common" style="margin-top:20px; margin-bottom:20px;">'+
					    
					    '<img id="cm-equal-to" title="Equal to" src="assets/img/math-symbols/common/equal-to.png" style="cursor:pointer;">'+
					    '<img id="cm-plus" title="Plus" src="assets/img/math-symbols/common/plus.png" style="cursor:pointer;">'+
					    '<img id="cm-minus" title="Minus" src="assets/img/math-symbols/common/minus.png" style="cursor:pointer;">'+
					    '<img id="cm-multiply" title="Multiply" src="assets/img/math-symbols/common/multiply.png" style="cursor:pointer;">'+
					    '<img id="cm-asterisk" title="Asterisk" src="assets/img/math-symbols/common/asterisk.png" style="cursor:pointer;">'+
					    '<img id="cm-percentage" title="Percentage" src="assets/img/math-symbols/common/percentage.png" style="cursor:pointer;">'+
					    '<img id="cm-divide" title="Divide" src="assets/img/math-symbols/common/divide.png" style="cursor:pointer;">'+
					    '<img id="cm-division" title="Division" src="assets/img/math-symbols/common/division.png" style="cursor:pointer;">'+
					    '<img id="cm-plus-or-minus" title="Plus Or Minus" src="assets/img/math-symbols/common/plus-or-minus.png" style="cursor:pointer;">'+
					    '<img id="cm-minus-or-plus" title="Minus Or Plus" src="assets/img/math-symbols/common/minus-or-plus.png" style="cursor:pointer;">'+
					    '<img id="cm-square-root" title="Square Root" src="assets/img/math-symbols/common/square-root.png" style="cursor:pointer;">'+
					    '<img id="cm-nth-root-of-2" title="Nth Root of 2" src="assets/img/math-symbols/common/nth-root-of-2.png" style="cursor:pointer;">'+
					    '<img id="cm-fraction" title="Fraction" src="assets/img/math-symbols/common/fraction.png" style="cursor:pointer;">'+
						'<img id="cm-superscript-x^2" title="Superscript x^2" src="assets/img/math-symbols/common/superscript-x2.png" style="cursor:pointer;">'+
					    '<img id="cm-natural-logarithm" title="Natural Logarithm" src="assets/img/math-symbols/common/natural-logarithm.png" style="cursor:pointer;">'+
						'<img id="cm-logarithm-to-base-e" title="Logarithm to base e" src="assets/img/math-symbols/common/logarithm-to-base-e.png" style="cursor:pointer;">'+
						'<img id="cm-logarithm-to-base-10" title="Logarithm to base 10" src="assets/img/math-symbols/common/logarithm-to-base-10.png" style="cursor:pointer;">'+
						'<br>'+

						'<img id="cm-parenthesis" title="Parenthesis" src="assets/img/math-symbols/common/parenthesis.png" style="cursor:pointer;">'+
						'<img id="cm-square-brackets" title="Square brackets" src="assets/img/math-symbols/common/square-brackets.png" style="cursor:pointer;">'+
						'<img id="cm-less-than" title="Less than" src="assets/img/math-symbols/common/less-than.png" style="cursor:pointer;">'+
						'<img id="cm-less-than-or-equal-to" title="Less than Or Equal to" src="assets/img/math-symbols/common/less-than-or-equal-to.png" style="cursor:pointer;">'+
						'<img id="cm-greater-than-or-equal-to" title="Greater than Or Equal to" src="assets/img/math-symbols/common/greater-than-or-equal-to.png" style="cursor:pointer;">'+
						'<img id="cm-greater-than" title="Greater than" src="assets/img/math-symbols/common/greater-than.png" style="cursor:pointer;">'+
						'<img id="cm-is-aproximately-equal" title="is Aproximately Equal" src="assets/img/math-symbols/common/is-aproximately-equal.png" style="cursor:pointer;">'+
						'<img id="cm-pi" title="Pi" src="assets/img/math-symbols/common/pi.png" style="cursor:pointer;">'+
						'<img id="cm-angle" title="Angle" src="assets/img/math-symbols/common/angle.png" style="cursor:pointer;">'+
						'<img id="cm-perpendicular" title="Perpendicular" src="assets/img/math-symbols/common/perpendicular.png" style="cursor:pointer;">'+
						'<img id="cm-is-parallel-to" title="is Parallel to" src="assets/img/math-symbols/common/is-parallel-to.png" style="cursor:pointer;">'+
						'<img id="cm-45-degree" title="45 Degree" src="assets/img/math-symbols/common/45-degree.png" style="cursor:pointer;">'+
						'<img id="cm-therefore" title="Therefore" src="assets/img/math-symbols/common/therefore.png" style="cursor:pointer;">'+
						'<img id="cm-congruence" title="Congruence" src="assets/img/math-symbols/common/congruence.png" style="cursor:pointer;">'+
						'<img id="cm-over-right-arrow" title="Over right arrow" src="assets/img/math-symbols/common/over-right-arrow.png" style="cursor:pointer;">'+
						'<img id="cm-over-left-arrow" title="Over left arrow" src="assets/img/math-symbols/common/over-left-arrow.png" style="cursor:pointer;">'+
						'<img id="cm-over-left-right-arrow" title="Over left-right arrow" src="assets/img/math-symbols/common/over-left-right-arrow.png" style="cursor:pointer;">'+
					    '<br>'+

					    '<img id="cm-line-over" title="Line Over" src="assets/img/math-symbols/common/line-over.png" style="cursor:pointer;">'+
					    '<img id="cm-theta" title="Theta" src="assets/img/math-symbols/common/theta.png" style="cursor:pointer;">'+
					    '<img id="cm-sine" title="Sine" src="assets/img/math-symbols/common/sine.png" style="cursor:pointer;">'+
					    '<img id="cm-cosine" title="Cosine" src="assets/img/math-symbols/common/cosine.png" style="cursor:pointer;">'+
					    '<img id="cm-tangent" title="Tangent" src="assets/img/math-symbols/common/tangent.png" style="cursor:pointer;">'+
					    '<img id="cm-secant" title="Secant" src="assets/img/math-symbols/common/secant.png" style="cursor:pointer;">'+
					    '<img id="cm-cosecant" title="Cosecant" src="assets/img/math-symbols/common/cosecant.png" style="cursor:pointer;">'+
					    '<img id="cm-cotangent" title="Cotangent" src="assets/img/math-symbols/common/cotangent.png" style="cursor:pointer;">'+
					    '<img id="cm-boolean-and" title="Boolean And" src="assets/img/math-symbols/common/boolean-and.png" style="cursor:pointer;">'+
					    '<img id="cm-boolean-or" title="Boolean Or" src="assets/img/math-symbols/common/boolean-or.png" style="cursor:pointer;">'+
					    '<img id="cm-boolean-not" title="Boolean Not" src="assets/img/math-symbols/common/boolean-not.png" style="cursor:pointer;">'+
					    '<img id="cm-for-all" title="For All" src="assets/img/math-symbols/common/for-all.png" style="cursor:pointer;">'+
					    '<img id="cm-exists" title="Exists" src="assets/img/math-symbols/common/exists.png" style="cursor:pointer;">'+
					    '<img id="cm-double-left-arrow" title="Double Left Arrow" src="assets/img/math-symbols/common/double-left-arrow.png" style="cursor:pointer;">'+
					    '<img id="cm-double-right-arrow" title="Double Right Arrow" src="assets/img/math-symbols/common/double-right-arrow.png" style="cursor:pointer;">'+
					    '<img id="cm-summation" title="Summation" src="assets/img/math-symbols/common/summation.png" style="cursor:pointer;">'+
					    '<img id="cm-limit-from-n-to-infinity" title="Limit From n to Infinity" src="assets/img/math-symbols/common/limit-from-n-to-infinity.png" style="cursor:pointer;">'+
					    '<br>'+
					    '<img id="cm-derivative" title="Derivative" src="assets/img/math-symbols/common/derivative.png" style="cursor:pointer;">'+
					    '<img id="cm-leibniz-notation" title="Leibniz notation" src="assets/img/math-symbols/common/leibniz-notation.png" style="cursor:pointer;">'+
					    '<img id="cm-leibniz-inline" title="Leibniz inline" src="assets/img/math-symbols/common/leibniz-inline.png" style="cursor:pointer;">'+
					    '<img id="cm-integral" title="Integral" src="assets/img/math-symbols/common/integral.png" style="cursor:pointer;">'+
					    '<img id="cm-empty-set" title="Empty Set" src="assets/img/math-symbols/common/empty-set.png" style="cursor:pointer;">'+
					    '<img id="cm-element-of" title="Element of" src="assets/img/math-symbols/common/element-of.png" style="cursor:pointer;">'+
					    '<img id="cm-not-element-of" title="Not Element of" src="assets/img/math-symbols/common/not-element-of.png" style="cursor:pointer;">'+
					    '<img id="cm-subset" title="Subset" src="assets/img/math-symbols/common/subset.png" style="cursor:pointer;">'+
					    '<img id="cm-subset-or-equal-to" title="Subset Or Equal to" src="assets/img/math-symbols/common/subset-or-equal-to.png" style="cursor:pointer;">'+
					    '<img id="cm-super-set" title="Super set" src="assets/img/math-symbols/common/super-set.png" style="cursor:pointer;">'+
					    '<img id="cm-super-set-or-equal-to" title="Super set Or Equal to" src="assets/img/math-symbols/common/super-set-or-equal-to.png" style="cursor:pointer;">'+
					    '<img id="cm-intersection" title="Intersection" src="assets/img/math-symbols/common/intersection.png" style="cursor:pointer;">'+
					    '<img id="cm-union" title="Union" src="assets/img/math-symbols/common/union.png" style="cursor:pointer;">'+

					    '</div>'+


					    '<div class="tab-pane fade" id="tab-symbols-derivative-integration" style="margin-top:20px; margin-bottom:20px;">'+
					    '<img id="di-nabia" title="Nabia" src="assets/img/math-symbols/derivative-and-integration/nabia.png" style="cursor:pointer;">'+
					    '<img id="di-partial-x" title="Partial x" src="assets/img/math-symbols/derivative-and-integration/partial-x.png" style="cursor:pointer;">'+				    
					    '<img id="di-dx" title="dx" src="assets/img/math-symbols/derivative-and-integration/dx.png" style="cursor:pointer;">'+
					    '<img id="di-leibniz-inline" title="Leibniz inline" src="assets/img/math-symbols/derivative-and-integration/leibniz-inline.png" style="cursor:pointer;">'+
					    '<img id="di-dot-x" title="Dot x" src="assets/img/math-symbols/derivative-and-integration/dot-x.png" style="cursor:pointer;">'+
					    '<img id="di-dot-y" title="Dot y" src="assets/img/math-symbols/derivative-and-integration/dot-y.png" style="cursor:pointer;">'+
					    '<img id="di-leibniz-notation" title="Leibniz notation" src="assets/img/math-symbols/derivative-and-integration/leibniz-notation.png" style="cursor:pointer;">'+
					    '<img id="di-integral" title="Integral" src="assets/img/math-symbols/derivative-and-integration/integral.png" style="cursor:pointer;">'+
					    '<img id="di-text-style-integration-limit-n-to--n" title="Text style Integration Limit N to -N" src="assets/img/math-symbols/derivative-and-integration/text-style-integration-limit-n-to--n.png" style="cursor:pointer;">'+
					    '<img id="di-text-style-integration-limit--n-to-n-diff-presentation" title="Text style Integration Limit -N to N (diff presentation)" src="assets/img/math-symbols/derivative-and-integration/text-style-integration-limit--n-to-n-diff-presentation.png" style="cursor:pointer;">'+
					    '<img id="di-integration-limit-1-to-3" title="Integration limit 1 to 3" src="assets/img/math-symbols/derivative-and-integration/integration-limit-1-to-3.png" style="cursor:pointer;">'+
					    '<img id="di-double-integration-lower-limit-d" title="Double integration lower limit D" src="assets/img/math-symbols/derivative-and-integration/double-integration-lower-limit-d.png" style="cursor:pointer;">'+
					    '<img id="di-triple-integration-lower-limit-e" title="Triple Integration lower limit E" src="assets/img/math-symbols/derivative-and-integration/triple-integration-lower-limit-e.png" style="cursor:pointer;">'+
					    '<img id="di-quadruple-integration-lower-limit-f" title="Quadruple integration lower limit F" src="assets/img/math-symbols/derivative-and-integration/quadruple-integration-lower-limit-f.png" style="cursor:pointer;">'+
					    '<img id="di-0-integration-lower-limit-c" title="0 integration lower limit C" src="assets/img/math-symbols/derivative-and-integration/0-integration-lower-limit-c.png" style="cursor:pointer;">'+
					    '</div>'+


					    '<div class="tab-pane fade" id="tab-symbols-matrix" style="margin-top:20px; margin-bottom:20px;">'+
						'<img id="mt-matrix" title="Matrix" src="assets/img/math-symbols/matrix/matrix.png" style="cursor:pointer;">'+
						'<img id="mt-bar-matrix" title="Bar Matrix" src="assets/img/math-symbols/matrix/bar-matrix.png" style="cursor:pointer;">'+
						'<img id="mt-double-bar-matrix" title="Double Bar Matrix" src="assets/img/math-symbols/matrix/double-bar-matrix.png" style="cursor:pointer;">'+
						'<img id="mt-parenthesis-matrix" title="Parenthesis Matrix" src="assets/img/math-symbols/matrix/parenthesis-matrix.png" style="cursor:pointer;">'+
						'<img id="mt-bracket-matrix" title="Bracket Matrix" src="assets/img/math-symbols/matrix/bracket-matrix.png" style="cursor:pointer;">'+
						'<img id="mt-brace-matrix" title="Brace Matrix" src="assets/img/math-symbols/matrix/brace-matrix.png" style="cursor:pointer;">'+
						'<img id="mt-small-matrix" title="Small Matrix" src="assets/img/math-symbols/matrix/small-matrix.png" style="cursor:pointer;">'+
						'<img id="mt-right-sided-matrix" title="Right Sided Matrix" src="assets/img/math-symbols/matrix/right-sided-matrix.png" style="cursor:pointer;">'+

					    '</div>'+

					    '</div>'

					},
					{
						// Text input field for the matheviation text.
						type: 'textarea',
						id: 'mathInput',
						label: 'input',

						// Validation checking whether the field is not empty.
						validate: CKEDITOR.dialog.validate.notEmpty( "field cannot be empty" ),

						// Called by the main setupContent call on dialog initialization.
						setup: function( element ) {
							console.log("xxxx");
							this.setValue( element.getText() );

						},

						// Called by the main commitContent call on dialog confirmation.
						commit: function( element ) {
							element.setText( this.getValue() );
						},
						onKeyUp: function() {
							//console.log("ok");
							var getMathInput = this.getValue();

							//var getMathInput = $("#txt-math-input").val();
							console.log(getMathInput);
							$("#math-live-preview").html(getMathInput);
							previewMath(getMathInput);
							//MathJax.Hub.Queue(["Typeset", MathJax.Hub, "math-live-preview"]);
						}
					},
					{
                        type: 'html',
                        html: '<label>Output</label><div id="math-live-preview"></div>',
                        setup: function (element) {
							
							console.log("math");
                        	var getMathInput = element.getText();
                        	$("#math-live-preview").html(getMathInput);
							previewMath(getMathInput);
							//MathJax.Hub.Queue(["Typeset", MathJax.Hub, "math-live-preview"]);

                        	/*
                        	console.log("1");
                            var textarea = editor.document.getElementsByTag("textarea")
                            //editor.document.getById("math-live-preview").setText("") // clear preview
                            // register live-preview handler
                            if (textarea) {
                        	console.log("2");
                                var t = textarea.$[0]
                                // initial preview-build up
                                var preview = editor.document.getById("math-live-preview")
                                //preview.setText(t.value)
                                MathJax.Hub.Queue(["Typeset", MathJax.Hub, "math-live-preview"])
                                // render preview after every keychange
                                	$("#math-live-preview").hide();
                                    console.log("user is typing tex: ")
                                    //preview.setText(t.value)
                                    MathJax.Hub.Queue(["Typeset", MathJax.Hub, "math-live-preview"])

                            }
                            */
                        }
                    }

				]
			}
		],
		
		onLoad: function() {
			var thisDialog=this;
			onEventsInLaTeXPanel(thisDialog);
		},

		// Invoked when the dialog is loaded.
		onShow: function() {

			$("#math-live-preview").html(""); //clear preview

			$('#myTab a[href="#tab-symbols-common"]').tab('show');


			// Get the selection in the editor.
			var selection = editor.getSelection();

			// Get the element at the start of the selection.
			var element = selection.getStartElement();

			// Get the <math> element closest to the selection, if any.
			if ( element )
				element = element.getAscendant( 'math', true );

			// Create a new <math> element if it does not exist.
			if ( !element || element.getName() != 'math' ) {
				element = editor.document.createElement( 'math' );

				// Flag the insertion mode for later use.
				this.insertMode = true;
			}
			else
				this.insertMode = false;

			// Store the reference to the <math> element in an internal property, for later use.
			this.element = element;

			// Invoke the setup methods of all dialog elements, so they can load the element attributes.
			if ( !this.insertMode )
				this.setupContent( this.element );

			
            
		},

		// This method is invoked once a user clicks the OK button, confirming the dialog.
		onOk: function() {

			// The context of this function is the dialog object itself.
			// http://docs.ckeditor.com/#!/api/CKEDITOR.dialog
			var dialog = this;

			// Creates a new <math> element.
			var math = this.element;

			// Invoke the commit methods of all dialog elements, so the <math> element gets modified.
			this.commitContent( math );

			// Finally, in if insert mode, inserts the element at the editor caret position.
			if ( this.insertMode )
				editor.insertElement( math );
				//var getContent = $("#mathInput").html("$(x+4) dx$");
				//editor.insertHtml( '<h2>This is a sample header</h2><p>This is a sample paragraph.</p>' );
				
	
		}
	};
});