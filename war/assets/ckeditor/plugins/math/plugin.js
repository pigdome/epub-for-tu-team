/**
 * Basic sample plugin inserting matheviation elements into CKEditor editing area.
 *
 * Created out of the CKEditor Plugin SDK:
 * http://docs.ckeditor.com/#!/guide/plugin_sdk_sample_1
 */

// Register the plugin within the editor.
CKEDITOR.plugins.add( 'math', {

	// Register the icons.
	icons: 'math',

	// The plugin initialization logic goes inside this method.
	init: function( editor ) {

		// Define an editor command that opens our dialog.
		editor.addCommand( 'math', new CKEDITOR.dialogCommand( 'mathDialog' ) );

		// Create a toolbar button that executes the above command.
		editor.ui.addButton( 'math', {

			// The text part of the button (if available) and tooptip.
			label: 'Insert Math',

			// The command to execute on click.
			command: 'math',

			// The button placement in the toolbar (toolbar group name).
			toolbar: 'insert'
		});

		if ( editor.contextMenu ) {
			editor.addMenuGroup( 'mathGroup' );
			editor.addMenuItem( 'mathItem', {
				label: 'Edit Math',
				icon: this.path + 'icons/math.png',
				command: 'math',
				group: 'mathGroup'
			});

			editor.contextMenu.addListener( function( element ) {
				if ( element.getAscendant( 'math', true ) ) {
					return { mathItem: CKEDITOR.TRISTATE_OFF };
				}
			});
		}

		// Register our dialog file. this.path is the plugin folder path.
		CKEDITOR.dialog.add( 'mathDialog', this.path + 'dialogs/math.js' );
	}
});

