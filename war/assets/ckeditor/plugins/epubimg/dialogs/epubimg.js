/**
 * The math dialog definition.
 *
 * Created out of the CKEditor Plugin SDK:
 * http://docs.ckeditor.com/#!/guide/plugin_sdk_sample_1
 */

// Our dialog definition.
CKEDITOR.dialog.add( 'epubimgDialog', function( editor ) {

	/* แบบนี้ก็ไม่ work
    editor.config.allowedContent = true;
    editor.config.contentsCss = ['/bootstrap.min.css', '/jquery.fileupload-ui.css', '/style.css'];
    // Allow all classes for all allowed elements.
    editor.config.extraAllowedContent = '*(*)';
	*/
	
	var $el={
		//myCKImages: $("#myCKImages"), <-- ไม่สามารถ cache ได้ เพราะถูกสร้างแบบ dynamic     TODO: cache ใน onShow ได้ 
		authorId: $("#authorId"),
	};
	
	var myCKImageTemplate=Handlebars.templates["my-ck-image"];

	var loadMyCKImages=function(imgIdToHighlight) {
		
		$(".myCKImages").empty(); 

		MediaEndpoint.list({authorId: $el.authorId.val(), type: "img"}, function(data) {
			var images=data[0]; 
			var cursorString=data[1]; // TODO: ทำ infinite scrolling!
			$(".myCKImages").append(myCKImageTemplate(images)); 
			MediaEndpoint.loadImageAsync($(".myCKImages").find("img.myCKImage"));
			
			$("#"+imgIdToHighlight).addClass("selected-img");
			
			$(".myCKImages").on("click", "img.myCKImage", function() {
				$(".selected-img").removeClass("selected-img"); // clear previously selected
				$(this).addClass("selected-img");
			});
		});
	};
	
	return {

		// Basic properties of the dialog window: title, minimum size.
		title: 'เลือกไฟล์รูปภาพ',
		minWidth: 500,
		minHeight: 400,

		// Dialog window contents definition.
		contents: [
			{
				// Definition of the Basic Settings dialog tab (page).
				id: 'tab-basic',
				label: 'Basic Settings',

				// bootstrap ไม่ได้ เพราะ include css ไม่ได้
				
				elements: [
					{
						// ทำไมถึงต้องใช้ class แทน id --> https://sites.google.com/a/punsarn.asia/ipst/learning-space-phase-1/epub-editor/caveats/ckeditor/multiple-inline-editors
					    type: 'html',
					    html: '<div class="myCKImages" style="overflow: auto; height: 400px; white-space: normal;"></div>',
						setup: function(imgId) {
							var id;
							if(imgId)
								id=imgId.split("id")[1]; // take out "id"
							loadMyCKImages(id);
						}

					}

				]

			}
		],

		// Invoked when the dialog is loaded.
		onShow: function() {

			//loadMyCKImages(); ยัง load ตอนนี้ไม่ได้ เพราะยังไม่ได้สร้าง elements ของ dialog นี้เลย เรียกข้างล่างสุด
			
			// Get the selection in the editor.
			var selection = editor.getSelection();

			// Get the element at the start of the selection.
			var element = selection.getStartElement();

			var targetId=null;
			if(element)
				targetId=element.getId();
			
			this.setupContent(targetId); // send the id of the target img to be highlighted
		},

		// This method is invoked once a user clicks the OK button, confirming the dialog.
		onOk: function() {

			$(".myCKImages img.myCKImage").off("click"); // prevent event listeners to be added repeatedly

			var originalId=$(".selected-img").attr("id");
			var $img=$(".selected-img")
					.removeClass("selected-img") // remove selected-img to take out the shadow box
					.removeClass("img-polaroid")
					.removeAttr("style")  // remove style to return to its original size, which was previously set to preview size
					.attr("id", "id"+originalId); // have to make it different otherwise id crash and adding box-shadow at dialog launch won't work
			var epubimg=new CKEDITOR.dom.element($img.get(0));
			editor.insertElement(epubimg);
		}
	};
});