/**
 * Basic sample plugin inserting matheviation elements into CKEditor editing area.
 *
 * Created out of the CKEditor Plugin SDK:
 * http://docs.ckeditor.com/#!/guide/plugin_sdk_sample_1
 */

// Register the plugin within the editor.
CKEDITOR.plugins.add( 'epubimg', {

	// Register the icons.
	icons: 'epubimg',

	// The plugin initialization logic goes inside this method.
	init: function( editor ) {

		// Define an editor command that opens our dialog.
		editor.addCommand( 'epubimg', new CKEDITOR.dialogCommand( 'epubimgDialog' ) );

		// Create a toolbar button that executes the above command.
		editor.ui.addButton( 'epubimg', {

			// The text part of the button (if available) and tooptip.
			label: 'Add image',

			// The command to execute on click.
			command: 'epubimg',

			// The button placement in the toolbar (toolbar group name).
			toolbar: 'media'
		});

		if ( editor.contextMenu ) {
			editor.addMenuGroup( 'epubMediaGroup' );
			editor.addMenuItem( 'epubimgItem', {
				label: 'Change image',
				icon: this.path + 'icons/epubimg.png',
				command: 'epubimg',
				group: 'epubMediaGroup'
			});

			editor.contextMenu.addListener( function( element ) {
				if ( element.getAscendant( 'img', true ) ) { // ให้ plugin intercept tag <img/> เพื่อให้ใช้ context menu กับ element นี้ได้ (i.e., edit/change) 
					return { epubimgItem: CKEDITOR.TRISTATE_OFF };
				}
			});
		}

		// Register our dialog file. this.path is the plugin folder path.
		CKEDITOR.dialog.add( 'epubimgDialog', this.path + 'dialogs/epubimg.js' );
	}
});

