/**
 * The math dialog definition.
 *
 * Created out of the CKEditor Plugin SDK:
 * http://docs.ckeditor.com/#!/guide/plugin_sdk_sample_1
 */

// Our dialog definition.
CKEDITOR.dialog.add( 'epubvideoDialog', function( editor ) {

	var $el={
		//myCKVideos: $("#myCKVideos"), <-- ไม่สามารถ cache ได้ เพราะถูกสร้างแบบ dynamic     TODO: cache ใน onShow ได้ 
		authorId: $("#authorId"),
	};
	
	var myCKVideoTemplate=Handlebars.templates["my-ck-video"];

	var loadMyCKVideos=function(videoIdToHighlight) {
		
		$("#myCKVideos").empty(); 

		MediaEndpoint.list({authorId: $el.authorId.val(), type: "video"}, function(data) {
			var videos=data[0]; 
			var cursorString=data[1]; // TODO: ทำ infinite scrolling!
			$("#myCKVideos").append(myCKVideoTemplate(videos)); 
			MediaEndpoint.loadVideoAsync($("#myCKVideos").find("video.myCKVideo"));
			
			$("#"+videoIdToHighlight).addClass("selected-video");
			
			$("#myCKVideos").on("click", "video.myCKVideo", function() {
				$(".selected-video").removeClass("selected-video"); // clear previously selected
				$(this).addClass("selected-video");
			});
		});
	};
	
	return {

		// Basic properties of the dialog window: title, minimum size.
		title: 'เลือกวีดีโอ',
		minWidth: 500,
		minHeight: 400,

		// Dialog window contents definition.
		contents: [
			{
				// Definition of the Basic Settings dialog tab (page).
				id: 'tab-basic',
				label: 'Basic Settings',

				// bootstrap ไม่ได้ เพราะ include css ไม่ได้
				
				elements: [
					{
					    type: 'html',
					    html: '<div id="myCKVideos" style="overflow:auto; height:100%;"></div>',
						setup: function(videoId) {
							var id;
							if(videoId)
								id=videoId.split("id")[1]; // take out "id"
							loadMyCKVideos(id);
						},
						// Called by the main commitContent call on dialog confirmation.
						commit: function( element ) {
							// replace existing id with the one currently selected by user
							//$(element.getOuterHtml()).find("video").attr("id", $(".selected-video").attr("id"));
							
							var originalId=$(".selected-video").attr("id");
							var $video=$(".selected-video")
										.removeClass("selected-video")
										.attr("id", "id"+originalId)
										.removeAttr("controls");
							element.setHtml($video.prop("outerHTML")); // TODO: ทำแบบนี้เหมือนต้อง evaluate ใหม่ และ d/l วีดีโอใหม่ทุกครั้ง มีวิธี insert existing element เข้าไปเลยรึเปล่า
							
							//var $epubvideo=$(element.getOuterHtml());
							//console.log($epubvideo);

						},

					}

				]

			}
		],

		// Invoked when the dialog is loaded.
		onShow: function() {

			/*
			//loadMyCKVideos(); ยัง load ตอนนี้ไม่ได้ เพราะยังไม่ได้สร้าง elements ของ dialog นี้เลย เรียกข้างล่างสุด
			
			// Get the selection in the editor.
			var selection = editor.getSelection();

			// Get the element at the start of the selection.
			var element = selection.getStartElement();

			var targetId=null;
			if(element) {
				console.log(element.getOuterHtml());
				targetId=$(element.getOuterHtml()).find("video").attr("id");
			}
			
			this.setupContent(targetId); // send the id of the target video to be highlighted
			*/
			
			// Get the selection in the editor.
			var selection = editor.getSelection();

			// Get the element at the start of the selection.
			var element = selection.getStartElement();

			// Get the <epubvideo> element closest to the selection, if any.
			if ( element )
				element = element.getAscendant( 'epubvideo', true );

			// Create a new <epubvideo> element if it does not exist.
			if ( !element || element.getName() != 'epubvideo' ) {
				//element = editor.document.createElement( 'epubvideo' );


				/*
				var $video=$("<epubvideo><video style='width: 450px; margin: 0 auto;'></video></epubvideo>");
				element=new CKEDITOR.dom.element($video.get(0));
				*/
				
				// Flag the insertion mode for later use.
				this.insertMode = true;
			}
			else
				this.insertMode = false;

			// Store the reference to the <math> element in an internal property, for later use.
			this.element = element;

			// Invoke the setup methods of all dialog elements, so they can load the element attributes.
			if(element)
				this.setupContent( $(element.getOuterHtml()).find("video").attr("id") );
			else 
				this.setupContent( null );
		},

		// This method is invoked once a user clicks the OK button, confirming the dialog.
		onOk: function() {

			/*
			$("#myCKVideos video.myCKVideo").off("click"); // prevent event listeners to be added repeatedly

			//var $videoDiv=$("<div class='ckVideo'></div>");
			var $videoDiv=$("<epubvideo></epubvideo>");
			var originalId=$(".selected-video").attr("id");
			var $video=$(".selected-video")
					.removeClass("selected-video") // remove selected-video to take out the shadow box
					.attr("style", "width: 450px; margin: 0 auto;")  // set fixed width, center-aligned 
					.attr("id", "id"+originalId) // have to make it different otherwise id crash and adding box-shadow at dialog launch won't work
					.removeAttr("controls"); // ถ้ามี controls แล้ว area ของ <epubvideo> จะเหลือแค่่ส่วน controls เท่านั้น ทำให้ตอน right-click ที่ตัว video ไม่สามารถ popup context menu ได้ user จะงง
											// ดังนั้น เอาส่วน controls ออกไปก่อน ชดเชยด้วยการใช้ปุ่ม preview หรือ ทำ mouseover แล้ว autoplay
											// TODO: make a handle so we can drag, or right-click to open CKEditor's context menu
			$videoDiv.append($video);
			var epubvideo=new CKEDITOR.dom.element($videoDiv.get(0));
			editor.insertElement(epubvideo);	
			*/
			
			$("#myCKVideos video.myCKVideo").off("click"); // prevent event listeners to be added repeatedly

			if ( this.insertMode ) {
				var $videoDiv=$("<epubvideo></epubvideo>");
				var originalId=$(".selected-video").attr("id");
				var $video=$(".selected-video")
						.removeClass("selected-video") // remove selected-video to take out the shadow box
						//.attr("style", "width: 450px; margin: 0 auto;")  // set fixed width, center-aligned      <-- no need now as we no longer use <div>
						.attr("id", "id"+originalId) // have to make it different otherwise id crash and adding box-shadow at dialog launch won't work
						.removeAttr("controls"); // ถ้ามี controls แล้ว area ของ <epubvideo> จะเหลือแค่่ส่วน controls เท่านั้น ทำให้ตอน right-click ที่ตัว video ไม่สามารถ popup context menu ได้ user จะงง
												// ดังนั้น เอาส่วน controls ออกไปก่อน ชดเชยด้วยการใช้ปุ่ม preview หรือ ทำ mouseover แล้ว autoplay
												// TODO: make a handle so we can drag, or right-click to open CKEditor's context menu
				$videoDiv.append($video);
				var epubvideo=new CKEDITOR.dom.element($videoDiv.get(0));

				editor.insertElement(epubvideo);	
			} else {				
				
				/*
				// replace existing <video> with the one currently selected by user
				///var $epubvideo=$(this.element.getOuterHtml());
				//$epubvideo.empty();
				var originalId=$(".selected-video").attr("id");
				var $video=$(".selected-video")
							.removeClass("selected-video")
							.attr("id", "id"+originalId)
							.removeAttr("controls");
				this.element.setHtml($video.prop("outerHTML"));
				
				var $epubvideo=$(this.element.getOuterHtml());
				console.log($epubvideo);
				//$epubvideo.append($video);
				*/
				
				this.commitContent( this.element );
			}

			
			/*
			// The context of this function is the dialog object itself.
			// http://docs.ckeditor.com/#!/api/CKEDITOR.dialog
			var dialog = this;

			// Creates a new <epubvideo> element.
			var epubvideo = this.element;

			// Invoke the commit methods of all dialog elements, so the <epubvideo> element gets modified.
			this.commitContent( epubvideo );

			// Finally, in if insert mode, inserts the element at the editor caret position.
			if ( this.insertMode )
				editor.insertElement( epubvideo );
			*/

		}
	};
});