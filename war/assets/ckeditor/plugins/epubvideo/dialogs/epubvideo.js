/**
 * The math dialog definition.
 *
 * Created out of the CKEditor Plugin SDK:
 * http://docs.ckeditor.com/#!/guide/plugin_sdk_sample_1
 */

// Our dialog definition.
CKEDITOR.dialog.add( 'epubvideoDialog', function( editor ) {

	var $el={
		//myCKVideos: $("#myCKVideos"), <-- ไม่สามารถ cache ได้ เพราะถูกสร้างแบบ dynamic     TODO: cache ใน onShow ได้ 
		authorId: $("#authorId"),
	};
	
	var myCKVideoTemplate=Handlebars.templates["my-ck-video"];

	var loadMyCKVideos=function(videoIdToHighlight) {
		
		$(".myCKVideos").empty(); 

		MediaEndpoint.list({authorId: $el.authorId.val(), type: "video"}, function(data) {
			var videos=data[0]; 
			var cursorString=data[1]; // TODO: ทำ infinite scrolling!
			$(".myCKVideos").append(myCKVideoTemplate(videos)); 
			MediaEndpoint.loadVideoAsync($(".myCKVideos").find("video.myCKVideo"));
			
			$("#"+videoIdToHighlight).addClass("selected-video");
			
			$(".myCKVideos").on("click", "video.myCKVideo", function() {
				$(".selected-video").removeClass("selected-video"); // clear previously selected
				$(this).addClass("selected-video");
			});
		});
	};
	
	return {

		// Basic properties of the dialog window: title, minimum size.
		title: 'เลือกไฟล์วีดีโอ',
		minWidth: 500,
		minHeight: 400,

		// Dialog window contents definition.
		contents: [
			{
				// Definition of the Basic Settings dialog tab (page).
				id: 'tab-basic',
				label: 'Basic Settings',

				// bootstrap ไม่ได้ เพราะ include css ไม่ได้
				
				elements: [
					{
						// ทำไมถึงต้องใช้ class แทน id --> https://sites.google.com/a/punsarn.asia/ipst/learning-space-phase-1/epub-editor/caveats/ckeditor/multiple-inline-editors
					    type: 'html',
					    html: '<div class="myCKVideos" style="overflow:auto; height:400px; white-space: normal;"></div>',
						setup: function(videoId) {
							var id;
							if(videoId)
								id=videoId.split("id")[1]; // take out "id"
							loadMyCKVideos(id);
						}

					}

				]

			}
		],

		// Invoked when the dialog is loaded.
		onShow: function() {

			//loadMyCKAudios(); ยัง load ตอนนี้ไม่ได้ เพราะยังไม่ได้สร้าง elements ของ dialog นี้เลย เรียกข้างล่างสุด
			
			// Get the selection in the editor.
			var selection = editor.getSelection();

			// Get the element at the start of the selection.
			var element = selection.getStartElement();

			var targetId=null;
			if(element)
				targetId=element.getId();
			
			this.setupContent(targetId); // send the id of the target audio to be highlighted
		},

		// This method is invoked once a user clicks the OK button, confirming the dialog.
		onOk: function() {

			$(".myCKVideos video.myCKVideo").off("click"); // prevent event listeners to be added repeatedly

			var originalId=$(".selected-video").attr("id");
			var $video=$(".selected-video")
					.removeClass("selected-video") // remove selected-video to take out the shadow box
					.removeClass("img-polaroid")
					.attr("style", "width: 400px;")  // set fixed width 
					.attr("id", "id"+originalId); // have to make it different otherwise id crash and adding box-shadow at dialog launch won't work
			var epubvideo=new CKEDITOR.dom.element($video.get(0));
			editor.insertElement(epubvideo);
		}
	};
});