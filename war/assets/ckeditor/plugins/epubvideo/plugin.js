/**
 * Basic sample plugin inserting matheviation elements into CKEditor editing area.
 *
 * Created out of the CKEditor Plugin SDK:
 * http://docs.ckeditor.com/#!/guide/plugin_sdk_sample_1
 */

// Register the plugin within the editor.
CKEDITOR.plugins.add( 'epubvideo', {

	// Register the icons.
	icons: 'epubvideo',

	// The plugin initialization logic goes inside this method.
	init: function( editor ) {

		// Define an editor command that opens our dialog.
		editor.addCommand( 'epubvideo', new CKEDITOR.dialogCommand( 'epubvideoDialog' ) );

		// Create a toolbar button that executes the above command.
		editor.ui.addButton( 'epubvideo', {

			// The text part of the button (if available) and tooptip.
			label: 'Add video',

			// The command to execute on click.
			command: 'epubvideo',

			// The button placement in the toolbar (toolbar group name).
			toolbar: 'media'
		});

		/* ยังไม่ work ดูต่อใน /epubvideo (failed to implement context menu)
		if ( editor.contextMenu ) {
			editor.addMenuGroup( 'epubMediaGroup' );
			editor.addMenuItem( 'epubvideoItem', {
				label: 'Change video',
				icon: this.path + 'icons/epubvideo.png',
				command: 'epubvideo',
				group: 'epubMediaGroup'
			});

			editor.contextMenu.addListener( function( element ) {
				if ( element.getAscendant( 'video', true ) ) { // ให้ plugin intercept tag <video/> เพื่อให้ใช้ context menu กับ element นี้ได้ (i.e., edit/change) 
					return { epubvideoItem: CKEDITOR.TRISTATE_OFF };
				}
			});
		}
		*/

		// Register our dialog file. this.path is the plugin folder path.
		CKEDITOR.dialog.add( 'epubvideoDialog', this.path + 'dialogs/epubvideo.js' );
	}
});

