<!DOCTYPE html>
<%@page import="th.ac.ipst.epub.oauth.UserValidator" %>
<%@page contentType="text/html;charset=UTF-8" %>
<%
	UserValidator validator=new UserValidator();
%>
<html>

  	<% if(validator.validate(request.getSession())) { 
			Long authorId=(Long)request.getSession().getAttribute("authorId");
			String authorDisplayName=(String)request.getSession().getAttribute("authorDisplayName");
			Boolean newMember=(Boolean)request.getSession().getAttribute("newMember");
			
			Long publicationId=Long.valueOf(request.getParameter("publicationId"));
			String version=request.getParameter("version");
	%>
	

  <head>
    <meta charset="utf-8" />
    <title>IPST ePub Editor</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="" />


    <style type="text/css">
/*      body {
        padding-top: 60px;
        padding-bottom: 40px;
        }*/
        .sidebar-nav {
          padding: 9px 0;
        }
        .ui-draggable, .ui-droppable {
          background-position: top;
        }
        .table-striped tbody > tr:nth-child(even) > td{
          background-color: #f9f9f9;
        }
        .footer-ui-ch-list{
          /*max-width: 340px;*/
          padding: 8px 0;
          border-bottom-left-radius: 5px;
          border-bottom-right-radius: 5px;
          border-top-left-radius: 0px;
          border-top-right-radius: 0px;
          margin-top: -26px;

          min-height: 20px;
          margin-bottom: 20px;
          background-color: #f5f5f5;
          border: 1px solid #e3e3e3;
          -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
          -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
        }
        .ui-ch-list-item-highlight
        { height: 25px; 
          padding: 10px;
          line-height: 20px; 
          border: 1px solid #80CCFF;
          background: #fbf9ee;
          color: #363636;

        }
        .ui-ch-list-item{
          border-top: 1px solid #dddddd;
          padding: 10px;
          line-height: 20px;
          font-weight: normal;
          color: #555555;
          height: 25px;
        }
        .ui-ch-list-sortable{
          list-style-type: none;
          margin: 0;
          padding: 0;
          width: 100%;
        }
        .ui-ch-list-sortable > li:nth-child(odd) {
          background: #f9f9f9;
        }
        .ui-ch-list-sortable > li:nth-child(even) {
          background: #f5f5f5;
        }

        @media (max-width:979px) and (min-width:768px) {

          #btn-group-expand-chapter-list{
            float: left;
          }

          #btn-expand-all {
            margin-top:10px;

          }
          #btn-collapse-all {
            margin-top:10px;

          }
          
        }​
    </style> 

	<script type="text/x-mathjax-config">
      MathJax.Hub.Config({
        tex2jax: {
          inlineMath: [["$","$"],["\\(","\\)"]]
        },
        "HTML-CSS": { scale: 100}
      });
    </script>

	<script type="text/javascript"
      src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML-full">
    </script>
	<script src="assets/ckeditor/ckeditor.js"></script>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/bootstrap-responsive.min.css" rel="stylesheet" />
    <link href="/css/ckeditor-dialog-custom.css" rel="stylesheet" />

    <link href="assets/css/nestable.css" rel="stylesheet" />

    <link href="assets/css/ipst-epub-theme/theme-1/ipst-epub-theme-1.css" rel="stylesheet" />
    <link href="assets/css/ipst-epub-theme/theme-1/ipst-epub-theme-1-responsive.css" rel="stylesheet" />
    <link href="assets/css/ipst-epub-theme/theme-2/ipst-epub-theme-2.css" rel="stylesheet" />
    <link href="assets/css/ipst-epub-theme/theme-3/ipst-epub-theme-3.css" rel="stylesheet" />
    <link href="assets/css/ipst-epub-theme/theme-3/ipst-epub-theme-3-responsive.css" rel="stylesheet" />

  </head>

  <body>

    <div class="navbar" style="display:none;">
      <div class="navbar-inner">
        <div class="container-fluid">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="/">IPST ePub Editor</a>
          <div class="nav-collapse collapse">
            <p id="signin" class="navbar-text pull-right" style="margin-top:5px;">
            
	            <span id="editorStatus" style="margin-right: 10px; display: none;">Unsaved content recovered locally (last save 3 mins ago)</span>
				
				<span id="fileSpinner" style="display: none;">
					<img alt="" class="spinner" src="/img/loading51.gif" style="width: 20px; height: 20px; margin-left: 5px; margin-top: 2px;" />
				</span>

                <button id="btnPreview" class="btn btn-mini" type="button" style="margin-top:-2px; margin-left: -3px;">
                  <i class="icon-search icon-black"></i> พรีวิว
                </button>

                <button id="btnSaveHtml" class="btn btn-mini" type="button" style="margin-top:-2px;">
                  <i class="icon-upload icon-black"></i> บันทึก
                </button>
                
            </p>
            <ul class="nav">
              <li>
                <button id="btnMediaManagement" class="btn btn-mini" type="button" style="margin-top:10px;">
                  <i class="icon-film icon-black"></i> จัดการมัลติมีเดีย
                </button>
              </li>
              <li>
                <button id="btnSendEmail" class="btn btn-mini" type="button" style="margin-top:10px; margin-left: 15px;">
                  <i class="icon-envelope icon-black"></i> ส่งรีวิว
                </button>
              </li>
              <li>
                <button id="btnExportToEpub" class="btn btn-mini" type="button" style="margin-top:10px; margin-left: 15px;">
                  <i class="icon-download icon-black"></i> นำออก (ePub)
                </button>
              </li>
              <li>
                <button id="btnExportToPdf" class="btn btn-mini" type="button" style="margin-top:10px; margin-left: 15px;">
                  <i class="icon-download icon-black"></i> นำออก (PDF)
                </button>
              </li>

            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">

          <div class="row-fluid">
            <div class="span3">

              <div class="well" style="padding: 8px 0; " data-nestable-group="0"> <!-- ใส่ nestable-group เป็นคนละตัวกันไว้ ไม่ให้ลาก toc item เข้ามาใน list นี้ได้ -->
                <div id="navList" class="nav nav-list" data-nestable-group="0">
                  <div>
                    <div class="row-fluid">
                      <div class="span9" id="bookInfo">
                        <img alt="" id="bookCover" src="assets/img/book-covers/book-cover-preview-book-item.png" style="width: 80px; box-shadow: #BABABA 2px 2px 6px; -webkit-box-shadow: #BABABA 2px 2px 6px;" />
                      </div>
                    </div>  
                  </div>

                    <div class="row-fluid">
                      <div class="span12">
                          <div id="book-name" style="color:gray;">
                              <h5></h5>
                          </div>
                      </div>
                    </div>
                
                  <div class="nav-header">
                    <div class="row-fluid">
                      <div class="span12">
                          <div id="chapter-list-reordering" class="pull-right" style="display:none;">
                            <img alt="" src="assets/img/loading-chapter.gif" /> 
                          </div>
                          <div id="chapter-list-loading" class="pull-right" style="display:none;">
                            <img alt="" src="assets/img/loading-chapter.gif" /> 
                          </div>
                      </div>
                    </div>

                  </div>
                  <div>
                    <div class="row-fluid">
                      <div class="span12">
                        <div class="btn-group" style="margin-top:1px;">
                          <button id="btn-create-chapter-section" class="btn btn-mini btn-info dropdown-toggle" data-toggle="dropdown"><i class="icon-file icon-white"></i> สร้าง <span class="caret"></span></button>
                          <ul class="dropdown-menu">
                            <li><a id="btn-create-chapter" ><i class="icon-file icon-black"></i> บทเรียน (chapter)</a></li>
                            <li><a id="btn-create-section" ><i class="icon-file icon-black"></i> หัวข้อ (section)</a></li>
                            <li><a id="btn-create-copyright" ><i class="icon-file icon-black"></i> ลิขสิทธิ์ (copyright)</a></li>
                            <li><a id="btn-create-dedication" ><i class="icon-file icon-black"></i> อุทิศ (dedication)</a></li>
                            <li><a id="btn-create-forward" ><i class="icon-file icon-black"></i> คำนิยม (forward)</a></li>
                            <li><a id="btn-create-preface" ><i class="icon-file icon-black"></i> คำนำ (preface)</a></li>
                          </ul>
                        </div>

                          <button id="btn-delete-chapterlist" class="btn btn-mini btn-info" type="button" style="margin-top:0px;">
                            <i class="icon-trash icon-white"></i> ลบ
                          </button>

                          <div id="btn-group-expand-chapter-list" class="btn-group pull-right">
                            <button id="btn-expand-all" class="btn btn-mini btn-info" title="แสดงหัวข้อ">
                            	<i class="icon-plus icon-white"></i>
                            </button>
                            <button id="btn-collapse-all" class="btn btn-mini btn-info" title="ซ่อนหัวข้อ">
                            	<i class="icon-minus icon-white"></i>
                            </button>
                          </div>
                        </div>
                      </div>

                  </div>
                  
                  <!-- TOC goes here -->
                  <div id="contain-chapter-list">
                  </div>

                </div>
              </div>

              <div class="footer-ui-ch-list" style="padding: 8px 0;">

              </div> 

            </div>

            <div class="span9" id="editingArea" data-epub-package="">
		    </div>

		  </div>
    </div>

			<div id="bookTemplatesList"></div>



    <div id="modal-manage-media" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="manageMediaLabel" aria-hidden="false" style="display: none;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="manageMediaLabel">จัดการมัลติมีเดีย</h3>
      </div>
      <div class="modal-body">
        <ul id="myMediaTab" class="nav nav-tabs">
          <li><a href="#tab-image" data-toggle="tab">รูป</a></li>
          <li><a href="#tab-video" data-toggle="tab">วีดีโอ</a></li>
          <li><a href="#tab-audio" data-toggle="tab">เสียง</a></li>
        </ul>

        <div id="myMediaTabContent" class="tab-content">
			<div class="tab-pane fade" id="tab-image">
				<div id="myImages" style="overflow: auto; height: 100%;">
					<div id="myImageUploadControl">
						<form id="myImageForm" action="http://ipst-epub.storage.googleapis.com/" method="POST" enctype="multipart/form-data">
							<input type="hidden" name="key" id="myImageKey" value="" />
							<input type="hidden" name="bucket" value="ipst-epub" />
							<input type="hidden" name="GoogleAccessId" value="521169348394-fldq88uf0hui6vd2u2cdb2hu18as9ba7@developer.gserviceaccount.com" />
							<input type="hidden" name="policy" id="myImagePolicy" />
							<input type="hidden" name="signature" id="myImageSignature" />
							<input type="hidden" name="success_action_status" value="201" />
							<div>
								<span id="myImageUploadButton" class='btn btn-success fileinput-button'> 
								    <i class='icon-plus icon-white'></i> 
								    <span>Upload</span> 
								    <input name='file' type='file' />
								</span> 
								<span id="myImageFormSpinner" style="display: none;">
									<img alt="" class="spinner" src="/img/loading51.gif" style="width: 20px; height: 20px; margin-left: 5px; margin-top: 2px;" />
								</span>
							</div>
						</form>
					</div>
				</div>
			</div>
			
			<div class="tab-pane fade" id="tab-video">
				<div id="myVideos" style="overflow: auto; height: 100%;">
					<div id="myVideoUploadControl">
						<form id="myVideoForm" action="http://ipst-epub.storage.googleapis.com/" method="POST" enctype="multipart/form-data">
							<input type="hidden" name="key" id="myVideoKey" value="" />
							<input type="hidden" name="bucket" value="ipst-epub" />
							<input type="hidden" name="GoogleAccessId" value="521169348394-fldq88uf0hui6vd2u2cdb2hu18as9ba7@developer.gserviceaccount.com" />
							<input type="hidden" name="policy" id="myVideoPolicy" />
							<input type="hidden" name="signature" id="myVideoSignature" />
							<input type="hidden" name="success_action_status" value="201" />
							<div>
								<span id="myVideoUploadButton" class='btn btn-success fileinput-button'> 
								    <i class='icon-plus icon-white'></i> 
								    <span>Upload</span> 
								    <input name='file' type='file' />
								</span> 
								<span id="myVideoFormSpinner" style="display: none;">
									<img alt="" class="spinner" src="/img/loading51.gif" style="width: 20px; height: 20px; margin-left: 5px; margin-top: 2px;" />
								</span>
							</div>
						</form>
					</div>
				</div>
			</div>

			<div class="tab-pane fade" id="tab-audio">
				<div id="myAudios" style="overflow: auto; height: 100%;">
					<div id="myAudioUploadControl">
						<form id="myAudioForm" action="http://ipst-epub.storage.googleapis.com/" method="POST" enctype="multipart/form-data">
							<input type="hidden" name="key" id="myAudioKey" value="" />
							<input type="hidden" name="bucket" value="ipst-epub" />
							<input type="hidden" name="GoogleAccessId" value="521169348394-fldq88uf0hui6vd2u2cdb2hu18as9ba7@developer.gserviceaccount.com" />
							<input type="hidden" name="policy" id="myAudioPolicy" />
							<input type="hidden" name="signature" id="myAudioSignature" />
							<input type="hidden" name="success_action_status" value="201" />
							<div>
								<span id="myAudioUploadButton" class='btn btn-success fileinput-button'> 
								    <i class='icon-plus icon-white'></i> 
								    <span>Upload</span> 
								    <input name='file' type='file' />
								</span> 
								<span id="myAudioFormSpinner" style="display: none;">
									<img alt="" class="spinner" src="/img/loading51.gif" style="width: 20px; height: 20px; margin-left: 5px; margin-top: 2px;" />
								</span>
							</div>
						</form>
					</div>
				</div>
			</div>
			
		 </div>
	  </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal">ปิด</button>
      </div>
   </div>


    <div id="modal-send-email" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="sendEmailLabel" aria-hidden="false" style="display: none;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="sendEmailLabel">ส่งอีเมล์พร้อมแนบไฟล์หนังสือ</h3>
      </div>
      <div class="modal-body">
        <ul id="myTab" class="nav nav-tabs">
          <li><a href="#tab-email-epub" data-toggle="tab">ePub</a></li>
          <li><a href="#tab-email-pdf" data-toggle="tab">PDF</a></li>
        </ul>

        <div id="myTabContent" class="tab-content">
          
          <div class="tab-pane fade" id="tab-email-epub">

            <form class="form-horizontal">
              <div class="control-group">
                <label class="control-label" for="txt-email-epub">อีเมล์ผู้รับ</label>
                <div class="controls">
                  <input type="text" id="txt-email-epub" />
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="txt-sender-epub">ชื่อผู้ส่ง</label>
                <div class="controls">
                  <input type="text" id="txt-sender-epub" />
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="txt-subject-epub">หัวเรื่อง</label>
                <div class="controls">
                  <input type="text" id="txt-subject-epub" />
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="txt-detail-epub">ข้อความ</label>
                <div class="controls">
                  <textarea rows="6" id="txt-detail-epub" style="width:260px;"></textarea>
                </div>
              </div>                                           
              <div class="control-group">
                <div class="controls">
                  <button id="btn-confirm-send-email-epub" class="btn btn-primary" type="button" style="width:100px;">
                    ส่ง
                  </button>
                </div>
              </div>
            </form>

          </div>

          <div class="tab-pane fade" id="tab-email-pdf">

              <div class="control-group">
                <label class="control-label" for="txt-email-pdf">อีเมล์ผู้รับ</label>
                <div class="controls">
                  <input type="text" id="txt-email-pdf" />
                  
	             	<div>
						<form id="pdfForm" action="http://ipst-epub.storage.googleapis.com/" method="POST" enctype="multipart/form-data">
							<input type="hidden" name="key" id="pdfFormKey" value="" />
							<input type="hidden" name="bucket" value="ipst-epub" />
							<input type="hidden" name="GoogleAccessId" value="521169348394-fldq88uf0hui6vd2u2cdb2hu18as9ba7@developer.gserviceaccount.com" />
							<input type="hidden" name="policy" id="pdfFormPolicy" />
							<input type="hidden" name="signature" id="pdfFormSignature" />
							<input type="hidden" name="success_action_status" value="201" />
						  	<!-- The fileinput-button span is used to style the file input field as button -->
							<div>										  	
						  		<span id="pdfFormUploadButton" class='btn btn-success fileinput-button'>
						    		<i class='icon-plus icon-white'></i> แนบไฟล์
						    		<input name='file' type='file' />
						  		</span>
								<span id="pdfFormSpinner" style="display: none;"><img alt="" class="spinner" src="/img/loading51.gif" style="width: 20px; height: 20px; margin-left: 5px; margin-top: 2px;" /></span>
							</div>
						</form>					      		
			      	</div>
                               
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="txt-sender-pdf">ชื่อผู้ส่ง</label>
                <div class="controls">
                  <input type="text" id="txt-sender-pdf" />
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="txt-subject-pdf">หัวเรื่อง</label>
                <div class="controls">
                  <input type="text" id="txt-subject-pdf" />
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="txt-detail-pdf">ข้อความ</label>
                <div class="controls">
                  <textarea rows="6" id="txt-detail-pdf" style="width:260px;"></textarea>

                </div>
              </div>         
              <div class="control-group">
                <div class="controls">
                  <div id="txt-display-attach-file-name" >
                    <p id="pdfLinkControl">
                    	<img alt="" src="assets/img/paper-clip-icon.png" />
                    	<a id="pdfLink" target="_blank">ตรวจสอบความถูกต้องของไฟล์ก่อนส่ง</a>
                    </p>
                  </div>
                </div>
              </div>                                   
              <div class="control-group">
                <div class="controls">
                  <button id="btn-confirm-send-email-pdf" class="btn btn-primary" type="button" style="width:100px;">
                    ส่ง
                  </button>
                </div>
              </div>



          </div>

        </div>

      </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal">ปิด</button>
      </div>
    </div>


 <input type="hidden" id="nestable-output" />


    <!-- programmatically upload local file (created with Blob object) by Blueimp's jQuery Upload File -->
	<form id="fileForm">
	</form>

	<input type="hidden" id="publicationId" value="<%= publicationId %>" />
	<input type="hidden" id="version" value="<%= version %>" />	
	<input type="hidden" id="authorId" value="<%= authorId %>" />

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/slimscroll/jquery.slimscroll.min.js"></script>

	<link rel='stylesheet' type="text/css" href='/css/jquery-file-upload/jquery.fileupload-ui.css' />
	<script src='/js/jquery-file-upload/vendor/jquery.ui.widget.js'></script>
	<script src='/js/jquery-file-upload/jquery.fileupload.js'></script>
	<link rel='stylesheet' type="text/css" href='/css/email-pdf-progress-bar.css' />		

<!--  
    <script src="assets/js/editcontent.js"></script>
-->
    
    <script src="assets/js/jquery.nestable.js"></script>
	
	<script src="/js/underscore/underscore-min.js"></script>
	
	<script src="/js/xdate.js"></script>
	<script src="/js/utils.js"></script>
	<script src="/js/imagesloaded.pkgd.min.js"></script>
	<script src="/js/imagesloaded.js"></script>

	<script src="/js/handlebars/handlebars.runtime.js"></script>
	<script src="/js/handlebars-helpers.js"></script>
	<script src="/tmpl/my-image.js"></script>
	<script src="/tmpl/my-video.js"></script>
	<script src="/tmpl/my-audio.js"></script>
	<script src="/tmpl/my-ck-image.js"></script>
	<script src="/tmpl/my-ck-video.js"></script>
	<script src="/tmpl/my-ck-audio.js"></script>

	<!-- theme1 -->	
	<script src="/tmpl/book-templates/book-templates-list-for-theme1.js"></script>
	<script src="/tmpl/book-templates/ipst-epub-chapter-theme-1-template-1.js"></script>	
	<script src="/tmpl/book-templates/ipst-epub-chapter-theme-1-template-2.js"></script>	
	<script src="/tmpl/book-templates/ipst-epub-chapter-theme-1-template-3.js"></script>	
	<script src="/tmpl/book-templates/ipst-epub-chapter-theme-1-template-4.js"></script>	
	<script src="/tmpl/book-templates/ipst-epub-section-theme-1-template-1.js"></script>	
	<script src="/tmpl/book-templates/ipst-epub-section-theme-1-template-2.js"></script>	
	<script src="/tmpl/book-templates/ipst-epub-section-theme-1-template-3.js"></script>	
	<script src="/tmpl/book-templates/ipst-epub-section-theme-1-template-4.js"></script>	
	<script src="/tmpl/book-templates/ipst-epub-copyright-theme-1-template-1.js"></script>	
	<script src="/tmpl/book-templates/ipst-epub-dedication-theme-1-template-1.js"></script>	
	<script src="/tmpl/book-templates/ipst-epub-forward-theme-1-template-1.js"></script>	
	<script src="/tmpl/book-templates/ipst-epub-preface-theme-1-template-1.js"></script>	

	<!-- theme2 -->	
	<script src="/tmpl/book-templates/book-templates-list-for-theme2.js"></script>
	<script src="/tmpl/book-templates/ipst-epub-chapter-theme-2-template-1.js"></script>	
	<script src="/tmpl/book-templates/ipst-epub-chapter-theme-2-template-2.js"></script>	
	<script src="/tmpl/book-templates/ipst-epub-section-theme-2-template-1.js"></script>	
	<script src="/tmpl/book-templates/ipst-epub-section-theme-2-template-2.js"></script>	
	<script src="/tmpl/book-templates/ipst-epub-copyright-theme-2-template-1.js"></script>	
	<script src="/tmpl/book-templates/ipst-epub-dedication-theme-2-template-1.js"></script>	
	<script src="/tmpl/book-templates/ipst-epub-forward-theme-2-template-1.js"></script>	
	<script src="/tmpl/book-templates/ipst-epub-preface-theme-2-template-1.js"></script>		
	
	<!-- theme3 -->	
	<script src="/tmpl/book-templates/book-templates-list-for-theme3.js"></script>
	<script src="/tmpl/book-templates/ipst-epub-chapter-theme-3-template-1.js"></script>	
	<script src="/tmpl/book-templates/ipst-epub-chapter-theme-3-template-2.js"></script>	
	<script src="/tmpl/book-templates/ipst-epub-section-theme-3-template-1.js"></script>	
	<script src="/tmpl/book-templates/ipst-epub-section-theme-3-template-2.js"></script>	
	<script src="/tmpl/book-templates/ipst-epub-copyright-theme-3-template-1.js"></script>	
	<script src="/tmpl/book-templates/ipst-epub-dedication-theme-3-template-1.js"></script>	
	<script src="/tmpl/book-templates/ipst-epub-forward-theme-3-template-1.js"></script>	
	<script src="/tmpl/book-templates/ipst-epub-preface-theme-3-template-1.js"></script>
	
	<script src="/tmpl/nestable/toc-item-nestable.js"></script>
	<script src="/tmpl/nestable/toc-subitem-container-nestable.js"></script>
	
	<!-- html components (in epub package) -->
	<script src="/tmpl/html-component/ipst-epub-theme1.js"></script>
	<script src="/tmpl/html-component/ipst-epub-theme2.js"></script>
	<script src="/tmpl/html-component/ipst-epub-theme3.js"></script>

	<script src="/tmpl/preview/preview.js"></script>
	
	<script src="/js/app/ckeditor/widget.js"></script>
	<script src="/js/app/epub-package/model.js"></script>
	<script src="/js/app/epub-package/endpoint.js"></script>
	<script src="/js/app/epub-package/widget.js"></script>
	<script src="/js/app/publication/endpoint.js"></script>	
	<script src="/js/app/email/widget.js"></script>
	<script src="/js/app/email/endpoint.js"></script>	
	<script src="/js/app/email/model.js"></script>
	<script src="/js/app/media/widget.js"></script>
	<script src="/js/app/media/endpoint.js"></script>	
	<script src="/js/app/html-upload/widget.js"></script>
	<script src="/js/app/html-download/endpoint.js"></script>
	<script src="/js/app/export-to-epub/widget.js"></script>
	<script src="/js/app/export-to-pdf/widget.js"></script>
	<script src="/js/app/export-to-pdf/endpoint.js"></script>
	<script src="/js/app/preview-opener/widget.js"></script>
	<script src="/js/app/delete-toc-item/widget.js"></script>
	<script src="/js/app/offline-support/widget.js"></script>	
	<script src="/js/md5.js"></script>

	<script>
		(function() {
			var authorId=$("#authorId").val();
			var publicationId=$("#publicationId").val();
			var version=$("#version").val();
			
			CKEditorWidget.init();
			EpubPackageWidget.init(publicationId);
			EmailWidget.init();
			MediaWidget.init();
			HtmlUploadWidget.init(authorId, publicationId, version);
			ExportToEpubWidget.init();
			ExportToPdfWidget.init();
			PreviewOpenerWidget.init();
			DeleteTocItemWidget.init();
		})();
	</script>

	<script>
	 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	 ga('create', 'UA-50962149-3', 'ipst-epub.appspot.com');
	 ga('send', 'pageview');
	</script> 

  </body>
  
  <% } else { %>
  <body>	
    <script>window.location.href='/login.html'</script>
  </body>
  <% } %>
	
</html>
