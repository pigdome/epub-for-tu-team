function jsonStringify(object)
{
	return JSON.stringify(object);
}

function getFileNameFromUrl(url)
{
	return url.split("/").pop();
}