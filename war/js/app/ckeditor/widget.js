var CKEditorWidget=(function($) {
	
	var $el = {
		editingArea: $("#editingArea"),			
	};
	
	var init=function() {
	};
	
	return {
		init: function() {
			init();
		},
		deactivateAll: function() {
	        // destroy previous ckeditors
	        for(k in CKEDITOR.instances) {
				var instance = CKEDITOR.instances[k];
				instance.destroy();
			}
		},
		activateAll: function() {
	        // make ckeditor available
	        CKEDITOR.disableAutoInline = true;
	        var $ckeditors=$el.editingArea.find('.ckeditor');
	        $ckeditors.each(function(i, ckeditor) {
		        CKEDITOR.inline(ckeditor);    	
		        //ckeditor.removeMenuItem('paste');
		    }); 		
		},
		// TODO: จริงๆ มันน่าจะดักไว้ใน activateAll รึเปล่า?
		onReady: function(callback) {
			CKEDITOR.on("instanceReady", function(ev) {
				console.log("CKEditor is ready.");
			
				if(callback)
					callback();
				/*
				var editor = ev.editor;
				editor.on("focus", function(ev) {
					alert("focused!");
				});
				*/
			});
		},
	};
	
})(jQuery);