var HtmlDownloadEndpoint=(function($) {
	
	// binding to UIs and caching elements
	var $el = {
				publicationId: $("#publicationId"),
				authorId: $("#authorId"),
				version: $("#version"),
	};

	var endpointUrl="http://commondatastorage.googleapis.com/ipst-epub/"+$el.authorId.val()+"/"+$el.publicationId.val()+"/"+$el.version.val()+"/epub/OPS/";
	
	var downloadContent=function(htmlContentUrl, doneCallback, failCallback, alwaysCallback) {
		
		MediaEndpoint.signUrl(htmlContentUrl, function(signedUrl) {

			$.ajax({
				type: 'GET',
				url: signedUrl,
				dataType: 'text'
			})
			.done(function(data) {				
				if(doneCallback) {

					// http://stackoverflow.com/questions/4429851/parse-complete-html-page-with-jquery
					var htmlContent = '<div id="body-mock">' + data.replace(/^[\s\S]*<body.*?>|<\/body>[\s\S]*$/g, '') + '</div>';
					var $htmlContent = $(htmlContent).contents(); // excluding mock div
					doneCallback($htmlContent);

					// approach ข้างล่างไม่ work เลย
					/*
					var htmlString=data.replace(/^<!DOCTYPE html>/, "").trim();
					console.log(htmlString);
					
					console.log($(htmlString).find("body > div"));
					*/
					/*
					console.log(htmlString);
					
					var $dummy=$("<div/>");
					
					console.log($dummy.html(htmlString));
					
					var $htmlContent=$dummy.html(htmlString).find("body > div"); // this $($.parseHTML(data)).find("body > div") doesn't work because parseHTML returns 'a set' of DOMs (parseHTML also requires "dataType" to be "html")
					doneCallback($htmlContent);
					*/
				}
			})
			.fail(function() { 
				if(failCallback)
					failCallback();
			})
			.always(function() {
				if(alwaysCallback)
					alwaysCallback();
			});
			
		}, function() {
			if(failCallback)
				failCallback();
		});
	};	
	
	return {
		downloadContent: function(htmlHref,  doneCallback, failCallback, alwaysCallback) {
			var htmlContentUrl=endpointUrl+htmlHref;
			downloadContent(htmlContentUrl,  doneCallback, failCallback, alwaysCallback);
		},
		/*
		downloadContentInNewWindow: function(htmlHref, authorId, publicationId, version, doneCallback, failCallback, alwaysCallback) {
			var htmlContentUrl="http://commondatastorage.googleapis.com/ipst-epub/"+authorId+"/"+publicationId+"/"+version+"/epub/OPS/"+htmlHref;
			downloadContent(htmlContentUrl,  doneCallback, failCallback, alwaysCallback);			
		}
		*/
	};
	
})(jQuery);
