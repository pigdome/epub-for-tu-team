var PreviewOpenerWidget=(function($) {
	
	var $el = {
		previewButton: $("#btnPreview"),
		
		editingArea: $("#editingArea"),
	};
	
	/*
	var openPreviewPage=function() {
		var $previouslySelectedItem=$(".dd-selected-item");
		if($previouslySelectedItem.length>0) {
			var htmlHref=$previouslySelectedItem.parent().attr("data-html-href");
			
			var params={ htmlHref: htmlHref, authorId: $el.authorId.val(), publicationId: $el.publicationId.val(), version: $el.version.val() }
			
			window.open("/preview.html?" + $.param(params), "_blank");
		}
	};
	*/

	/*
	var preview=function() {
		
		var $previouslySelectedItem=$(".dd-selected-item");
		if($previouslySelectedItem.length==0) {
			return;
		}
		var htmlHref=$previouslySelectedItem.parent().attr("data-html-href");
		
		var previewWindow=window.open("", "พรีวิว", "");
		var previewWindowHtml=[
		'<!DOCTYPE html>',
		'<html>',
			'<head>',
				'<meta charset="utf-8" />',
				'<title>IPST ePub Editor</title>',
				'<meta name="viewport" content="width=device-width, initial-scale=1.0" />',
				'<script type="text/x-mathjax-config">',
					'MathJax.Hub.Config({',
						'tex2jax: {',
							'inlineMath: [["$","$"],["\\(","\\)"]]',
    					'},',
    					'"HTML-CSS": { scale: 100}',
					'});',
				'</script>',
				'<script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML-full"></script>',
				'<link href="assets/css/bootstrap.min.css" rel="stylesheet" />',
				'<link href="assets/css/bootstrap-responsive.min.css" rel="stylesheet" />',
				'<link href="assets/css/ipst-epub-theme/theme-1/ipst-epub-theme-1.css" rel="stylesheet" />',
				'<link href="assets/css/ipst-epub-theme/theme-1/ipst-epub-theme-1-responsive.css" rel="stylesheet" />',
				'<link href="assets/css/ipst-epub-theme/theme-2/ipst-epub-theme-2.css" rel="stylesheet" />',
				'<link href="assets/css/ipst-epub-theme/theme-3/ipst-epub-theme-3.css" rel="stylesheet" />',
				'<link href="assets/css/ipst-epub-theme/theme-3/ipst-epub-theme-3-responsive.css" rel="stylesheet" />',
			'</head>',
			'<body>',
			'</body>',
		'</html>'
		].join("\n");
		
		previewWindow.document.writeln(previewWindowHtml);
		previewWindow.document.close();
		
		HtmlDownloadEndpoint.downloadContent(htmlHref, function($content) {
			
			$(previewWindow.document).find("body").append($content);

		}, function() {
			console.log("failed");			
		}, function() {
			
		});
	};
	*/

	var previewTemplate=Handlebars.templates["preview"];

	var preview=function() {
		
		var $htmlContentClone=$el.editingArea.find("> div").clone(); // ต้อง clone ไม่งั้นมี side effect ตอน render (อย่างน้อยก็พวก math content)
		var $maths=$htmlContentClone.find("math");
		if($maths.length>0) {
			$maths.each(function(index, math) {
				var latex=$(math).text();
				$(math).html("<span class='math'>$"+latex+"$</span>"); // ใส่ class math เข้าไป จะได้ reverse กลับเข้ามา edit ได้อีกที
				$(math).find("span").unwrap();
			})
		}
		var htmlContent=$htmlContentClone.prop("outerHTML");
		
		var previewWindow=window.open("", "พรีวิว", "");
		var previewWindowHtml=previewTemplate(htmlContent);
		previewWindow.document.writeln(previewWindowHtml);
		previewWindow.document.close();
	};
	
	return {
		init: function() {
			$el.previewButton.on("click", function() {
				//openPreviewPage();
				preview();
			});
		}
	};
	
})(jQuery);