var OfflineSupportWidget=(function($) {
	
	// binding to UIs and caching elements
	var $el = {
		publicationId: $("#publicationId"),
		editingArea: $("#editingArea"),
		editorStatus: $("#editorStatus"),
		interval: null, // a timer
	};

	var hideEditorStatus=function() {
		$el.editorStatus.fadeOut();
	};
	
	var getLocalContent=function() {
		return localStorage[$el.publicationId.val()+"htmlContent"];
	};

	var getLocalContentEpoch=function() {
		return localStorage[$el.publicationId.val()+"epoch"];
	};
	
	var setLocalContent=function(localContent) {
		localStorage[$el.publicationId.val()+"htmlContent"]=localContent;
	};

	var setLocalContentEpoch=function(localContentEpoch) {
		localStorage[$el.publicationId.val()+"epoch"]=localContentEpoch;
	};

	var getLocaleString=function(epoch) {
		return new XDate(epoch*1000).toLocaleString();
	};
	
	var editorReplacesWithLocalVersionIfNeeded=function() {
		// check if there is a need to compare content of both versions
		// if localStorage stores nothing, it means that content was saved successfully in the previous session
		var localContent=getLocalContent();
		if(localContent=="" || localContent==null) // meaning the content is up-to-date
			return;
		
		var serverContent=$el.editingArea.find("> div");
		var epoch=getLocalContentEpoch();
		
		if(localContent!=serverContent) {
			$el.editingArea.html(localContent);
			
			// set status message
			$el.editorStatus.html("Unsaved content recovered locally (last save "+ getLocaleString(getLocalContentEpoch()) +")");
			
			// show message for a short period
			$el.editorStatus.fadeIn();			
			setTimeout(hideEditorStatus, 20000);
		}
	};

	/* too slow like Sigil! Try to move this code to a worker instead
	$el.editingArea.find("[contenteditable='true']").on("input", function() {
		console.log("content changed");
	})
	*/
	
	/* Web worker can't access the DOM? WTF? 
	var worker = new Worker("/js/app/offline-support/worker.js");

	worker.onmessage = function (oEvent) {
	  console.log("Called back by the worker!\n");
	};
	
	worker.postMessage(""); // start the worker.
	*/

	var startTimer=function() {
		$el.interval=setInterval(storeHtmlContentLocally, 30000);		
	};
	
	var stopTimer=function() {
		clearInterval($el.interval);
	};
	
	var storeHtmlContentLocally=function() {
		var $content=$el.editingArea.find("> div");
		if($content.length>0) {
			setLocalContent($content.prop("outerHTML"));
			setLocalContentEpoch(getNowEpoch());
		}
	};
	
	var deactivate=function() {
		$(".ipst-epub-body-content").off("input");
		stopTimer();
		setLocalContent("");
		setLocalContentEpoch("");
	};
	
	// ดัก onfocus เพื่อ start timer เก็บ html content periodically
	var activate=function() {
		editorReplacesWithLocalVersionIfNeeded();
		
		$(".ipst-epub-body-content").one("input", function() {
			startTimer();
		});
	};
	
	return {
		activate: function() {
			activate();
		},
		deactivate: function() {
			deactivate();
		},
	};
	
})(jQuery);