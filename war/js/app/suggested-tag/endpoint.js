var SuggestedTagEndpoint=(function($) {
	
	var baseUrl="/suggestedtagendpoint";

	var update = function(servletUrl, req, tagsToAdd, tagsToRemove, authorId, doneCallback, failCallback, alwaysCallback) {
		
		$.ajax({
			type: 'POST',
			contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			url: servletUrl,
			data: { 
				req: req,
				tagsToAdd: JSON.stringify(tagsToAdd),
				tagsToRemove: JSON.stringify(tagsToRemove),
				authorId: authorId
			} 
		})
		.done(function() { 
			if(doneCallback)
				doneCallback();
		})
	    .fail(function() { 
	    	if(failCallback)
	    		failCallback();
	    })
	    .always(function() { 
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });			
	};

	var list = function(servletUrl, doneCallback, failCallback, alwaysCallback) {
		$.ajax({
			  type: 'GET',
			  contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			  url: servletUrl,
			  dataType: 'json'
		})
		.done(function(data) { 
			if(doneCallback)
				doneCallback(data);
		})
	    .fail(function() { 
	    	if(failCallback)
	    		failCallback();
	    })
	    .always(function() {
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });
	};
	
	return {
		list: function(queryString, doneCallback, failCallback, alwaysCallback) {
			var params={ req: "list", queryString: queryString };
			var servletUrl=baseUrl + "?" + $.param(params);
			list(servletUrl, doneCallback, failCallback, alwaysCallback);
		},
		update: function(tagsToAdd, tagsToRemove, authorId, doneCallback, failCallback, alwaysCallback) {
			var servletUrl=baseUrl;
			var req="update";
			update(servletUrl, req, tagsToAdd, tagsToRemove, authorId, doneCallback, failCallback, alwaysCallback);
		},
	};
	
})(jQuery);
