var EpubPackageModel=(function($) {

	// binding to input UIs and caching elements
	var $el = {
		editingArea: $("#editingArea"),
	};

	var getItemHref=function(url, mediaType) {
		var itemHref;
		var fileName=getLastComponentOfUrl(url);
		
		if(mediaType=="image/png") {
			itemHref="images/"+fileName;
		} else if(mediaType=="video/mp4") {
			itemHref="videos/"+fileName;
		} else if(mediaType=="audio/mp3") {
			itemHref="audios/"+fileName;
		}
		
		return itemHref;
	};
	
	// currently use file name as an id
	var generateItemIdFromUrl=function(url) {
		var fileName=getLastComponentOfUrl(url);
		var fileNameWithoutExtension=fileName.split(".")[0];
		return fileNameWithoutExtension;
	};

	var generateItemIdFromHref=function(href) {
		var fileName=href;
		var fileNameWithoutExtension=fileName.split(".")[0];
		return fileNameWithoutExtension;
	};
	
	var addInArray=function(array, value) {
		if(array==null)
			array=new Array();
		array.push(value);
	};
	
	var beginUpdate=function() {
		var epubPackageString=$el.editingArea.attr("data-epub-package");
		if(epubPackageString==null || epubPackageString=="")
			return {};
		else
			return JSON.parse(epubPackageString);
	};
	
	var endUpdate=function(epubPackage) {
		$el.editingArea.attr("data-epub-package", JSON.stringify(epubPackage));
	};
	
	return {
		/*
		setDefaultContentsToLocalDB: function() {
			// e.g. cover, nav, etc. in <manifest /> which have to be stored in itemIds, itemProperties, etc. of entity EpubPackage
		},
		getEpubPackageFromInputs: function() {
			// have to merge with default content stored in Lawnchair's local DB
		},
		*/ 
		addMediaItems: function(urls, mediaType) { // currently fixed to image/png, video/mp4, audio/mp3
			var epubPackage=beginUpdate();

			$.each(urls, function(index, url) {
				var itemHref=getItemHref(url, mediaType);
				
				if(epubPackage.mediaItemHrefs==null)
					epubPackage.mediaItemHrefs=new Array();
				if(epubPackage.mediaItemMediaTypes==null)
					epubPackage.mediaItemMediaTypes=new Array();
				if(epubPackage.mediaItemIds==null)
					epubPackage.mediaItemIds=new Array();
				
			    if( ! _.contains(epubPackage.mediaItemHrefs, itemHref)) {
				    addInArray(epubPackage.mediaItemHrefs, itemHref);
				    addInArray(epubPackage.mediaItemMediaTypes, mediaType);
				    addInArray(epubPackage.mediaItemIds, generateItemIdFromUrl(url));
			    }
			});
			
			endUpdate(epubPackage);
		},
		addHtmlItem: function(itemHref, mediaType) {
			var epubPackage=beginUpdate();

			if(epubPackage.itemHrefs==null)
				epubPackage.itemHrefs=new Array();
			if(epubPackage.itemMediaTypes==null)
				epubPackage.itemMediaTypes=new Array();
			if(epubPackage.itemIds==null)
				epubPackage.itemIds=new Array();
			
		    if( ! _.contains(epubPackage.itemHrefs, itemHref)) {
			    addInArray(epubPackage.itemHrefs, itemHref);
			    addInArray(epubPackage.itemMediaTypes, mediaType);
			    addInArray(epubPackage.itemIds, generateItemIdFromHref(itemHref));
		    }
			
		    endUpdate(epubPackage);
		},
		updateSpineIdRefs: function(itemrefIdRefs, itemrefLinears) {
			var epubPackage=beginUpdate();

			epubPackage.itemrefIdRefs=itemrefIdRefs;
			epubPackage.itemrefLinears=itemrefLinears;
			
			endUpdate(epubPackage);
		},
		updateTocHtmlString: function(tocHtmlString) {
			var epubPackage=beginUpdate();

			epubPackage.tocHtmlString.value = tocHtmlString; // we store tocHtmlString as Google Appengine's Text data-type (>500characters)
			
			endUpdate(epubPackage);
		},
		updateNavHtmlString: function(navHtmlString) {
			var epubPackage=beginUpdate();

			epubPackage.navHtmlString.value = navHtmlString; 
			
			endUpdate(epubPackage);
		},		
		getEpubPackage: function() {
			var epubPackageString=$el.editingArea.attr("data-epub-package");
			if(epubPackageString==null || epubPackageString=="")
				return {};
			else
				return JSON.parse(epubPackageString);
		},
		removeHtmlItem: function(htmlHref) {
			var epubPackage=beginUpdate();
			
			var targetIndex = _.indexOf(epubPackage.itemHrefs, htmlHref);
			var targetItemId = epubPackage.itemIds[targetIndex];
			epubPackage.itemHrefs.splice(targetIndex, 1);
			epubPackage.itemIds.splice(targetIndex, 1);
			epubPackage.itemMediaTypes.splice(targetIndex, 1);
			
			var targetItemRefIndex = _.indexOf(epubPackage.itemrefIdRefs, targetItemId);
			epubPackage.itemrefIdRefs.splice(targetItemRefIndex, 1);
			epubPackage.itemrefLinears.splice(targetItemRefIndex, 1);
			
			endUpdate(epubPackage);
		}
	};
	
})(jQuery);
