var EpubPackageWidget=(function($) {
	
	// binding to UIs and caching elements
	var $el = {
		bookTitle: $("#book-name > h5"),
		bookCover: $("#bookCover"),
		bookInfo: $("#bookInfo"),
		navBar: $(".navbar"),
		
		createChapterButton: $("#btn-create-chapter"),
		createSectionButton: $("#btn-create-section"),
		createCopyrightButton: $("#btn-create-copyright"),
		createDedicationButton: $("#btn-create-dedication"),
		createForwardButton: $("#btn-create-forward"),
		createPrefaceButton: $("#btn-create-preface"),
		
		createChapterConfirmButtonSelector: "#btn-confirm-create-chapter",
		createSectionConfirmButtonSelector: "#btn-confirm-create-section",
		createCopyrightConfirmButtonSelector: "#btn-confirm-create-copyright",
		createDedicationConfirmButtonSelector: "#btn-confirm-create-dedication",
		createForwardConfirmButtonSelector: "#btn-confirm-create-forward",
		createPrefaceConfirmButtonSelector: "#btn-confirm-create-preface",
		
		createChapterCancelButtonSelector: "#btn-cancel-create-chapter",
		createSectionCancelButtonSelector: "#btn-cancel-create-section",
		createCopyrightCancelButtonSelector: "#btn-cancel-create-copyright",
		createDedicationCancelButtonSelector: "#btn-cancel-create-dedication",
		createForwardCancelButtonSelector: "#btn-cancel-create-forward",
		createPrefaceCancelButtonSelector: "#btn-cancel-create-preface",
		
		createChapterModal: null,  // can't initialize here because it's not in the DOM until page load done..
		createChapterModalSelector: "#modal-create-chapter",
		createSectionModal: null, 
		createSectionModalSelector: "#modal-create-section", 
		createCopyrightModal: null,
		createCopyrightModalSelector: "#modal-create-copyright",
		createDedicationModal: null,
		createDedicationModalSelector: "#modal-create-dedication",
		createForwardModal: null,
		createForwardModalSelector: "#modal-create-forward",
		createPrefaceModal: null,
		createPrefaceModalSelector: "#modal-create-preface",
		
		userEnteredChapterNameSelector: "#txt-chapter-name",
		userEnteredSectionNameSelector: "#txt-section-name",
		
		bookTemplatesList: $("#bookTemplatesList"),
		selectedChapterTemplateNameSelector: "input[name='rb-chapter-template']:checked",
		selectedSectionTemplateNameSelector: "input[name='rb-section-template']:checked",
		selectedCopyrightTemplateNameSelector: "input[name='rb-copyright-template']:checked",
		selectedDedicationTemplateNameSelector: "input[name='rb-dedication-template']:checked",
		selectedForwardTemplateNameSelector: "input[name='rb-forward-template']:checked",
		selectedPrefaceTemplateNameSelector: "input[name='rb-preface-template']:checked",

		editingArea: $("#editingArea"),
		titleSelector: ".ipst-epub-header-desc",
		saveHtmlButton: $("#btnSaveHtml"),
		
		/* ไม่ต้องเปลี่ยนเป็น selector หรอก fix ใน code จะเข้าใจง่ายกว่านะ
		nestable: $("#nestable"),
		nestableList: $(".dd > .dd-list"),
		nestableContainer: $(".dd"),
		nestableTocContainerParent: $(".slimScrollDiv"),
		*/
		tocContainer: $("#contain-chapter-list"),
		expandAllButton: $("#btn-expand-all"),
		collapseAllButton: $("#btn-collapse-all"),
	};
	
	var bookTemplatesListForTheme1Template=Handlebars.templates["book-templates-list-for-theme1"];
	var bookTemplatesListForTheme2Template=Handlebars.templates["book-templates-list-for-theme2"];
	var bookTemplatesListForTheme3Template=Handlebars.templates["book-templates-list-for-theme3"];
	var tocItemNestableTemplate=Handlebars.templates["toc-item-nestable"];
	var tocSubitemContainerNestableTemplate=Handlebars.templates["toc-subitem-container-nestable"];

	var didInitializeNestable=false;
	
	var loadToc=function(tocHtmlString) {
		if(tocHtmlString==null || tocHtmlString.value=="") {
			// create empty toc
			var $toc=$("<div class='dd' id='nestable' style='height: 100px; overflow: hidden;'> <ol class='dd-list'></ol> </div>");
			$el.tocContainer.append($toc);
		} else { // load existing toc			
			
			$el.tocContainer.append(tocHtmlString.value); // .value because of the Text's datatype

		}
		
		// TODO: มีวิธีดีกว่านี้มั๊ยเนี่ย
		if(!didInitializeNestable) {
			onNestableEvents(); // ต้องเรียกหลังจากมี element #nestable แล้วเท่านั้น และ initialize แค่ตอนเสร็จ page load (ทำแค่ครั้งเดียว)
			didInitializeNestable=true;
			
			loadLastSavedPage(); // load หน้าที่เปิด save ไว้ครั้งสุดท้าย (ตรงกว่านั้นคือ หน้าที่ highlight ค้างไว้อยู่ใน EpubPackage entity อาจจะเป็นหน้าที่ย้ายตำแหน่งไปล่าสุดก็ได้)
		}
	};
	
	var loadLastSavedPage=function() {
		var $previouslySelectedItem=$(".dd-selected-item");
		if($previouslySelectedItem.length>0) {
			var htmlHref=$previouslySelectedItem.parent().attr("data-html-href");
			loadHtmlContent(htmlHref);			
		}
	};
	
	var storeEpubPackageInEditingAreaElement=function(epubPackage) {
		$el.editingArea.attr("data-epub-package", JSON.stringify(epubPackage));		
	};
	
	var getEpubPackage=function(publicationId) {
		EpubPackageEndpoint.getByPublicationId(publicationId, 
			function(epubPackage) {

				storeEpubPackageInEditingAreaElement(epubPackage);
				loadToc(epubPackage.tocHtmlString);

			}, function() {
				
				EpubPackageEndpoint.insert({ publicationId: publicationId, title: $el.bookTitle.text(), tocHtmlString: { value: "" } , navHtmlString: { value: "" } }, 
					function(insertedEpubPackage) {

						storeEpubPackageInEditingAreaElement(insertedEpubPackage);
						loadToc(insertedEpubPackage.tocHtmlString);

					}
				);
			}
		);
	};
	
	var postProcessHtmlFile=function() {
        var $content=$el.editingArea.find("> div");
        
		// revert background path and style (if they exist) to the original
        var originalStyle=$content.attr("data-original-style");
        $content.attr("style", originalStyle);        

		// change system image (e.g. backgrounds, lines, placeholders) paths
		var $systemImages=$content.find(".ipst-epub-system-image");
		if($systemImages.length>0) {
			$systemImages.each(function(index, image) {
				$(image).attr("src", $(image).attr("data-original-url"));
			});			
		}

		// change media paths to local package
		var $images=$content.find(".myCKImage");
		if($images.length>0) {
			MediaEndpoint.loadImageAsync($images);
		}

		var $videos=$content.find(".myCKVideo");
		if($videos.length>0) {
			MediaEndpoint.loadVideoAsync($videos);
		}

		var $audios=$content.find(".myCKAudio");
		if($audios.length>0) {
			MediaEndpoint.loadAudioAsync($audios);
		}
	};
	
	var renderDownloadedHtmlContent=function($content) {
		
		CKEditorWidget.deactivateAll();
        $el.editingArea.empty();
        
        // remove src of media files to prevent broken-source visualization
        $content.find(".myCKImage, .myCKVideo, .myCKAudio").removeAttr("src");
        $content.find(".ipst-epub-system-image").removeAttr("src");
        
        $el.editingArea.append($content);
        $el.editingArea.find("> div").hide();
        $el.editingArea.find("> div").fadeIn();

        CKEditorWidget.activateAll();

        // window.setTimeout(postProcessHtmlFile, 3000);
        // Previously we use a timer to delay post processing, but it seems to be related to CKEditor which is not yet ready to be used.
        // So handle such an event should solve the problem.
        //
        // *** IMPORTANT: Currently there is only one CKEditor inside the page. If multiple editors are needed, implementation of CKEditorWidget.onReady has to be changed. ***
        // 
        // See http://docs.ckeditor.com/#!/api/CKEDITOR-event-instanceReady
        // See http://stackoverflow.com/questions/19805338/adding-event-handlers-globally-to-all-ckeditor-instances
        CKEditorWidget.onReady(function() {
        	postProcessHtmlFile();
        	// Don't handle title's change on the fly as it will significantly degrade performance; instead, change TOC item's title upon save action 
        	// http://stackoverflow.com/questions/10328102/how-to-detect-content-change-event-on-a-div
        	// handleOnTitleChange();
        });
	};
	
	var handleOnTitleChange=function() {
        var $titleDiv=$el.editingArea.find("> div .title-sync");
        // .. continue here
	};

	var loadHtmlContent=function(htmlHref, doneCallback, failCallback, alwaysCallback) {
		
		disableAllControls();
		
		HtmlDownloadEndpoint.downloadContent(htmlHref, function($content) {
			
			renderDownloadedHtmlContent($content);
			
			if(doneCallback)
				doneCallback();

		}, function() {
			alert("ไม่สามารถแสดงเนื้อหาได้ กรุณาลองใหม่อีกครั้ง");			
			if(failCallback)
				failCallback();
		}, function() {
			if(alwaysCallback)
				alwaysCallback();
			enableAllControls();				
		});
	};
	
	var setup=function(publicationId) {
		
		PublicationEndpoint.getById(publicationId, function(publication) { // ใช้แค่ publicationId ก็พอ ไม่จำเป็นต้องรู้ version เพราะ publicationId คือ key ของ Publication entity ส่วนตอนทำ versioning ต้องใช้ publicationGroupId
			// ดึง publication entity มาเพื่อ set book title กับ cover
			$el.bookInfo.data("book", JSON.stringify(publication));
			$el.bookTitle.text(publication.title);
			$el.bookCover.data("url", publication.coverUrl);
			PublicationEndpoint.loadImageAsync($el.bookCover);
			
			getEpubPackage(publicationId); // ต้องเรียกตอนได้ Publication entity มาแล้ว เผื่อกรณีที่ยังไม่เคยมี EpubPackage entity จะได้สร้าง entity ใหม่ ซึ่งต้องใช้ publication.title
			
			loadTemplatesForTheme(publication.themeId);
		});
	};
	
	var loadTemplatesForTheme=function(themeId) {
		if(themeId=="theme1") {
			$el.bookTemplatesList.append(bookTemplatesListForTheme1Template()); 
		} else if(themeId=="theme2") {
			$el.bookTemplatesList.append(bookTemplatesListForTheme2Template());
		} else if(themeId=="theme3") {
			$el.bookTemplatesList.append(bookTemplatesListForTheme3Template());			
		}
	};

	var fadeInNewItemAndHighlightIt=function() {
        var $newlyAddedItem=$(".new-item-added");
        $newlyAddedItem.fadeIn();
        var itemListHeight = $(".dd > .dd-list").height();
        $(".dd").slimScroll({ scrollTo: itemListHeight });
        $newlyAddedItem.removeClass("new-item-added");
        
        // unhighlight previously highlighted one, and highlight the new item
        $(".dd-selected-item").removeClass("dd-selected-item");
        $newlyAddedItem.find(".dd-handle").addClass("dd-selected-item");
	};
	
	var addNewSubitemInToc=function(title, nestableTemplate, $parent) {
    	var newItemInToc = { 
    		timestamp: new Date().getTime(), 
    		title: title 
    	};
        $parent.append(nestableTemplate(newItemInToc));
        var serializedListInfo = JSON.stringify($("#nestable").nestable('serialize')); // serializedListInfo มีไว้เช็คว่ามีการเปลี่ยนตำแหน่ง (reorder) หรือไม่
        $(".dd").attr("data-serialize-toc", serializedListInfo);
		
        fadeInNewItemAndHighlightIt();
	};
	
	var addNewItemInToc=function(title, nestableTemplate) {
    	var newItemInToc = { 
    		timestamp: new Date().getTime(), 
    		title: title 
    	};
    	$(".dd > .dd-list").append(nestableTemplate(newItemInToc));
        var serializedListInfo = JSON.stringify($("#nestable").nestable('serialize')); // serializedListInfo มีไว้เช็คว่ามีการเปลี่ยนตำแหน่ง (reorder) หรือไม่
        $(".dd").attr("data-serialize-toc", serializedListInfo);
		
        fadeInNewItemAndHighlightIt();
	};

	var areAllControlsDisabled = function() {
		if( $el.saveHtmlButton.attr("disabled") == "disabled" )
			return true;
		else
			return false;
	};
	
	var disableAllControls=function() {
		// all buttons
		$("button").attr("disabled", "disabled");
		
		// toc control panel
		$(".dd").find(".dd-handle").each(function() {
            $(this).removeClass("dd-handle");
            $(this).addClass("dd-disable");
        });
        $('.dd').animate({opacity: 0.5});
	};
	
	var enableAllControls=function() {
		// all buttons
		$("button").removeAttr("disabled");		
		
		// toc control panel
		$(".dd").find(".dd-disable").each(function(){
            $(this).removeClass("dd-disable");
            $(this).addClass("dd-handle");
        });
        $('.dd').animate({opacity: 1.0});
        
        // TODO: enable offline support only after fixing bug of not being able to load media files
		//OfflineSupportWidget.activate();
	};
		
	var showTemplate=function(bookTemplateName, title, isSection) {
		        
    	disableAllControls();
    	// save currently open page, if there is one
    	HtmlUploadWidget.saveCurrentlyOpenPageToCloud(function() {

    		// clear old editing area, if it exists
            $el.editingArea.empty();
            // replace editing area with new page's content
            var template=Handlebars.templates[bookTemplateName];
            $el.editingArea.append(template({title: title}));        
            $("."+bookTemplateName).fadeIn();		
            CKEditorWidget.activateAll();

            addNewTocItem(title, isSection);
            
            // save new page
        	HtmlUploadWidget.saveCurrentlyOpenPageToCloud(function() {
        		
        	}, function() {
        		alert("ไม่สามารถบันทึกการเปลี่ยนแปลงได้ กรุณาลองอีกครั้ง")        		
        	}, function() {
        		enableAllControls();        		
        	});

    	}, function() {
    		alert("ไม่สามารถบันทึกการเปลี่ยนแปลงได้ กรุณาลองอีกครั้ง")
    		enableAllControls();
    	}, function() {
    	});
	};
	
	var addNewTocItem = function(title, isSection) {
		
		if( isSection ) {
			
	        var chapterSelected=$(".dd-selected-item").parent().parent().parent().is("div");
	        var sectionSelected=$(".dd-selected-item").parent().parent().parent().is("li");

	        if(chapterSelected) {
	            var numSections = $(".dd-selected-item").parent().find(".dd-list").length;
	            if(numSections > 0) { // meaning that the list already contains the <ol/> container
	            	var $parent=$(".dd-selected-item").parent().find("> .dd-list");
	            	addNewSubitemInToc(title, tocItemNestableTemplate, $parent);
	            }
	            else { // adding <ol/> container for the first section item
	            	var $parent=$(".dd-selected-item").parent();
	            	addNewSubitemInToc(title, tocSubitemContainerNestableTemplate, $parent);
	            }
	        } else if(sectionSelected) {
	        	var $parent=$(".dd-selected-item").parent().parent();
	        	addNewSubitemInToc(title, tocItemNestableTemplate, $parent);
	        }

	        // expand all items in toc, just to make sure that a new section is shown on toc properly
	        $el.expandAllButton.trigger("click");
	        
		} else { // chapter-level
			
    		addNewItemInToc(title, tocItemNestableTemplate);
    		
		}
		
	};
	
	var showNewChapter=function(event) {
		var userEnteredTitle=event.data["userEnteredTitle"];
		var bookTemplateName=event.data["bookTemplateName"];
		
        // set title and fade in the template, and add the corresponding TOC item
        showTemplate(bookTemplateName, userEnteredTitle, false);
	};

	var didTapOnCreateChapterConfirmButton=function() {

        var bookTemplateName = $($el.selectedChapterTemplateNameSelector).val();
        var userEnteredChapterName=$($el.userEnteredChapterNameSelector).val();
        
        $el.createChapterModal.one("hidden", { "userEnteredTitle": userEnteredChapterName , "bookTemplateName": bookTemplateName}, showNewChapter);
        $el.createChapterModal.modal("hide");
	};

	var showNewCopyright=function(event) {
		var userEnteredTitle=event.data["userEnteredTitle"];
		var bookTemplateName=event.data["bookTemplateName"];

        showTemplate(bookTemplateName, "UNTITLED", false);
	};

	var didTapOnCreateCopyrightConfirmButton=function() {

        var bookTemplateName = $($el.selectedCopyrightTemplateNameSelector).val();

        $el.createCopyrightModal.one("hidden", { "userEnteredTitle": "ลิขสิทธิ์", "bookTemplateName": bookTemplateName }, showNewCopyright);
        $el.createCopyrightModal.modal("hide");
	};

	var showNewDedication=function(event) {
		var userEnteredTitle=event.data["userEnteredTitle"];
		var bookTemplateName=event.data["bookTemplateName"];

        showTemplate(bookTemplateName, "UNTITLED", false);
	};

	var didTapOnCreateDedicationConfirmButton=function() {

        var bookTemplateName = $($el.selectedDedicationTemplateNameSelector).val();

        $el.createDedicationModal.one("hidden", { "userEnteredTitle": "คำอุทิศ", "bookTemplateName": bookTemplateName }, showNewDedication);
        $el.createDedicationModal.modal("hide");
	};

	var showNewForward=function(event) {
		var userEnteredTitle=event.data["userEnteredTitle"];
		var bookTemplateName=event.data["bookTemplateName"];

        showTemplate(bookTemplateName, "UNTITLED", false);
	};

	var didTapOnCreateForwardConfirmButton=function() {

        var bookTemplateName = $($el.selectedForwardTemplateNameSelector).val();

        $el.createForwardModal.one("hidden", { "userEnteredTitle": "คำนิยม", "bookTemplateName": bookTemplateName }, showNewForward);
        $el.createForwardModal.modal("hide");
	};

	var showNewPreface=function(event) {
		var userEnteredTitle=event.data["userEnteredTitle"];
		var bookTemplateName=event.data["bookTemplateName"];

        showTemplate(bookTemplateName, "UNTITLED", false);
	};

	var didTapOnCreatePrefaceConfirmButton=function() {

        var bookTemplateName = $($el.selectedPrefaceTemplateNameSelector).val();

        $el.createPrefaceModal.one("hidden", { "userEnteredTitle": "คำนำ", "bookTemplateName": bookTemplateName }, showNewPreface);
        $el.createPrefaceModal.modal("hide");
	};

	var showNewSection=function(event) {
		var userEnteredTitle=event.data["userEnteredTitle"];
		var bookTemplateName=event.data["bookTemplateName"];

        // set title and fade in the template and add corresponding toc item
        showTemplate(bookTemplateName, userEnteredTitle, true);
	};
	
	var didTapOnCreateSectionConfirmButton=function() {

        var bookTemplateName = $($el.selectedSectionTemplateNameSelector).val();
        var userEnteredSectionName=$($el.userEnteredSectionNameSelector).val();

        $el.createSectionModal.one("hidden", { "userEnteredTitle": userEnteredSectionName, "bookTemplateName": bookTemplateName }, showNewSection);
        $el.createSectionModal.modal("hide");
	};
	
	var onTemplateEvents=function() {
		
        $el.createChapterButton.on("click", function () {
        	$el.createChapterModal=$($el.createChapterModalSelector); // the element is in the DOM now so we can initialize dynamically 
            $el.createChapterModal.modal({
                keyboard: false,
                backdrop: true,
                show:true
            });

            $el.createChapterModal.one("click", $el.createChapterConfirmButtonSelector, function () {
            	didTapOnCreateChapterConfirmButton();
            });
            // ไม่งั้นตอนกด cancel แล้วกลับมากด confirm ใหม่ จะ create เบิ้ลเลย เพราะเข้า event handler 2 ครั้งของ .on
            // ดักแค่ onclick ของปุ่ม cancel ไม่พอ เพราะมันมี event อื่นที่ cancel modal dialog นี้ได้ด้วย เช่น กดปุ่ม x หรือ click ข้างนอก dialog ดังนั้น safe สุดคือดักที่ on hidden ของ dialog เลย
            $el.createChapterModal.one("hidden", null, function() {
            	$el.createChapterModal.off("click", $el.createChapterConfirmButtonSelector);
            });
        });

        $el.createSectionButton.on("click", function () {
        	$el.createSectionModal=$($el.createSectionModalSelector);
            $el.createSectionModal.modal({
                keyboard: false,
                backdrop: true,
                show:true
            });

            $el.createSectionModal.one("click", $el.createSectionConfirmButtonSelector, function () {
            	didTapOnCreateSectionConfirmButton();
            });
            $el.createSectionModal.one("hidden", null, function() {
            	$el.createSectionModal.off("click", $el.createSectionConfirmButtonSelector);
            });
        });

        $el.createCopyrightButton.on("click", function () {
        	$el.createCopyrightModal=$($el.createCopyrightModalSelector);
            $el.createCopyrightModal.modal({
                keyboard: false,
                backdrop: true,
                show:true
            });

            $el.createCopyrightModal.one("click", $el.createCopyrightConfirmButtonSelector, function () {
            	didTapOnCreateCopyrightConfirmButton();
            });
            $el.createCopyrightModal.one("hidden", null, function() {
            	$el.createCopyrightModal.off("click", $el.createCopyrightConfirmButtonSelector);
            });
        });

        $el.createDedicationButton.on("click", function () {
        	$el.createDedicationModal=$($el.createDedicationModalSelector);
            $el.createDedicationModal.modal({
                keyboard: false,
                backdrop: true,
                show:true
            });

            $el.createDedicationModal.one("click", $el.createDedicationConfirmButtonSelector, function () {
            	didTapOnCreateDedicationConfirmButton();
            });
            $el.createDedicationModal.one("hidden", null, function() {
            	$el.createDedicationModal.off("click", $el.createDedicationConfirmButtonSelector);
            });
        });

        $el.createForwardButton.on("click", function () {
        	$el.createForwardModal=$($el.createForwardModalSelector);
            $el.createForwardModal.modal({
                keyboard: false,
                backdrop: true,
                show:true
            });

            $el.createForwardModal.one("click", $el.createForwardConfirmButtonSelector, function () {
            	didTapOnCreateForwardConfirmButton();
            });
            $el.createForwardModal.one("hidden", null, function() {
            	$el.createForwardModal.off("click", $el.createForwardConfirmButtonSelector);
            });
        });

        $el.createPrefaceButton.on("click", function () {
        	$el.createPrefaceModal=$($el.createPrefaceModalSelector);
            $el.createPrefaceModal.modal({
                keyboard: false,
                backdrop: true,
                show:true
            });

            $el.createPrefaceModal.one("click", $el.createPrefaceConfirmButtonSelector, function () {
            	didTapOnCreatePrefaceConfirmButton();
            });
            $el.createPrefaceModal.one("hidden", null, function() {
            	$el.createPrefaceModal.off("click", $el.createPrefaceConfirmButtonSelector);
            });
        });
        
	};
	
	// TODO: จัดการ refactor code ส่วนนี้ของทอส
	var onNestableEvents=function() {

		// ก่อนทำบรรทัดนี้ได้ ต้องมี element นี้ใน DOM แล้ว
		$("#nestable").nestable({
			group: 1,
            maxDepth: 2,
            expandBtnHTML: "",
            collapseBtnHTML: "",
        });		
		
		var updateOutput = function(e) {
	        var list   = e.length ? e : jQuery(e.target),
	            output = list.data('output');
	        if(output)
	        	output.val(JSON.stringify(list.nestable('serialize')));//, null, 2));
	    };

        // output initial serialised data
        updateOutput(jQuery('#nestable').data('output', jQuery('#nestable-output')));

		$(".dd").slimScroll({
	        height: '300px',
	        width: '100%',
	        size: '5px',
	        color: '#999999',
	        distance: '0px',
	        start:'.dd'
	    });

        jQuery('#nestable-menu').on('click', function(e)
        {
            var target = jQuery(e.target),
                action = target.data('action');
            if (action === 'expand-all') {
                jQuery('.dd').nestable('expandAll');
            }
            if (action === 'collapse-all') {
                jQuery('.dd').nestable('collapseAll');
            }
        });

        jQuery(".dd-list").on('click', '.dd-item', function(e){

            e.stopPropagation(); // to prevent undesirable effects on dd-list

        	if( areAllControlsDisabled() ) { // in middle of processing something, so ignoring the click
        		return;        		
        	}
        	
        	console.log("toc clicked");

            disableAllControls(); // disable immediately right after the item was clicked to prevent undesirable new actions

            // remember what to be loaded after saving the currently open page
            var $previouslySelectedItem=$(".dd-selected-item");
            var $currentlySelectedItem=$(this).find("> .dd-disable");
            
        	// first save previously open page, if there is a page opening, and then loading the target page
            HtmlUploadWidget.saveCurrentlyOpenPageToCloud(function() {

        		// load a target html to ckeditor
                var htmlHref=$currentlySelectedItem.parent().attr("data-html-href");
                if(htmlHref)
                	loadHtmlContent(htmlHref, function() {

                		$previouslySelectedItem.removeClass('dd-selected-item');
                        $currentlySelectedItem.addClass('dd-selected-item');
                		
                	}, function() {
                	}, function() {
                	});

            }, function() {
        		alert("ไม่สามารถแสดงเนื้อหาได้ กรุณาลองใหม่อีกครั้ง");
            }, function() {
            	//enableAllControls(); done in "loadHtmlContent"
            });	            	

        });

        jQuery(document).on('click', '#btn-expand-all', function(){
            jQuery('.dd').nestable('expandAll');
        });

        jQuery(document).on('click', '#btn-collapse-all', function(){
            jQuery('.dd').nestable('collapseAll');
        });
        
        jQuery('.dd').on('change', function(e) {

        	console.log("toc reordered");

        	disableAllControls();
        	HtmlUploadWidget.saveCurrentlyOpenPageToCloud(function() {
            	// เมื่อมีการเปลี่ยนแปลงตำแหน่งเรียบร้อยแล้ว update ค่าตำแหน่งใหม่กลับเข้าไป
                $(".dd").attr("data-serialize-toc", JSON.stringify($('#nestable').nestable('serialize'))); // IMPORTANT: แต่เหมือนจะไม่ได้ใช้ data-serialize-toc แล้วนะ คือ saveToCloud ทุกครั้งถึงแม้ตำแหน่งจะไม่เปลี่ยน        		
        	}, function() {
        		alert("ไม่สามารถเปลี่ยนตำแหน่งได้ กรุณาลองอีกครั้ง")
        	}, function() {
        		enableAllControls();
        	});

        });
	};
    
	var init=function(publicationId) {
		setup(publicationId);
		onTemplateEvents();
		$el.navBar.show();
	};
	
	return {
		init: function(publicationId) {
			init(publicationId);
		},
		disableAllControls: function() {
			disableAllControls();
		},
		enableAllControls: function() {
			enableAllControls();
		},
	};
	
})(jQuery);