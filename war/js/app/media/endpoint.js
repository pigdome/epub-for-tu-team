var MediaEndpoint=(function($) {

	var baseUrl="/mediaendpoint";
	var baseUrlForUploadForm="/mediauploadform";
	
	// binding to UIs and caching elements
	var $el = {
		authorId: $("#authorId"),
		publicationId: $("#publicationId"),
	};
	
	var list = function(servletUrl, doneCallback, failCallback, alwaysCallback) {
		$.ajax({
			  type: 'GET',
			  contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			  url: servletUrl,
			  dataType: 'json'
		})
		.done(function(data) { 
			if(doneCallback)
				doneCallback(data);
		})
	    .fail(function() { 
	    	if(failCallback)
	    		failCallback();
	    })
	    .always(function() {
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });
	};

	var insert=function(servletUrl, req, mediaUrl, type, doneCallback, failCallback, alwaysCallback) {
		$.ajax({
			type: 'POST',
			contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			url: servletUrl,
			data: { 
				req: req,
				media: JSON.stringify({
					authorId: $el.authorId.val(),
					publicationId: $el.publicationId.val(),
					mediaUrl: mediaUrl,
					type: type
				})
			} 
		})
		.done(function(data) { 
			if(doneCallback)
				doneCallback(data);
		})
	    .fail(function() { 
	    	if(failCallback)
	    		failCallback();
	    })
	    .always(function() { 
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });
	};

	var remove=function(servletUrl, req, mediaId, doneCallback, failCallback, alwaysCallback) {
		$.ajax({
			type: 'POST',
			contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			url: servletUrl,
			data: {
				req: req,
				mediaId: mediaId
			} 
		})
		.done(function() { 
			if(doneCallback)
				doneCallback();
		})
	    .fail(function() { 
	    	if(failCallback)
	    		failCallback();
	    })
	    .always(function() { 
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });
	};
	
	var renewFormSignatureAndPolicy=function(servletUrl, doneCallback, failCallback, alwaysCallback) {
		$.ajax({
			  type: 'GET',
			  contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			  url: servletUrl,
			  dataType: 'json'
		})
		.done(function(data) { 
			if(doneCallback)
				doneCallback(data);
		})
		.fail(function() { 
			if(failCallback)
				failCallback();
	    })
	    .always(function() {
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });
	};

	var loadImageAsync=function($img) {
		$img.each(function(index, img) {
			var url=$(img).data("url");
			signUrl(url, function(signedUrl) {
				$(img).attr("src", signedUrl);
			});
		});
	};

	var loadVideoAsync=function($video) {
		$video.each(function(index, video) {
			var url=$(video).data("url");
			signUrl(url, function(signedUrl) {
				$(video).attr("src", signedUrl);
			});
		});
	};

	var loadAudioAsync=function($audio) {
		$audio.each(function(index, audio) {
			var url=$(audio).data("url");
			signUrl(url, function(signedUrl) {
				$(audio).attr("src", signedUrl);
			});
		});
	};

	var signUrl=function(url, doneCallback, failCallback, alwaysCallback) {
		
		if(url=="" || url==null)
			return null;
			
		$.ajax({
			type: 'GET',
			contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			url: '/urlsigner?originalUrl=' + url,
			dataType: 'text'
		})
		.done(function(signedUrl) {
			if(doneCallback)
				doneCallback(signedUrl);
		})
		.fail(function() { 
			if(failCallback)
				failCallback();
		})
		.always(function() {
			if(alwaysCallback)
				alwaysCallback();
		});
	};

	return {
		list: function(options, doneCallback, failCallback, alwaysCallback) {
			var params={ req: "list"};
			$.extend(params, options);
			var servletUrl=baseUrl + "?" + $.param(params);
			list(servletUrl, doneCallback, failCallback, alwaysCallback);
		},
		insert: function(mediaUrl, type, doneCallback, failCallback, alwaysCallback) {
			var servletUrl=baseUrl;
			var req="insert";
			insert(servletUrl, req, mediaUrl, type, doneCallback, failCallback, alwaysCallback);
		},
		remove: function(mediaId, doneCallback, failCallback, alwaysCallback) {
			var servletUrl=baseUrl;
			var req="remove";
			remove(servletUrl, req, mediaId, doneCallback, failCallback, alwaysCallback);
		},
		renewFormSignatureAndPolicy: function(doneCallback, failCallback, alwaysCallback) {
			var servletUrl=baseUrlForUploadForm+"?req=renewFormSignatureAndPolicy";
			renewFormSignatureAndPolicy(servletUrl, doneCallback, failCallback, alwaysCallback);			
		},
		loadImageAsync: function($img) {
			loadImageAsync($img);
		},
		loadVideoAsync: function($video) {
			loadVideoAsync($video);
		},
		loadAudioAsync: function($audio) {
			loadAudioAsync($audio);
		},
		signUrl: function(url, doneCallback, failCallback, alwaysCallback) {
			signUrl(url, doneCallback, failCallback, alwaysCallback);
		},
	};
})(jQuery);
