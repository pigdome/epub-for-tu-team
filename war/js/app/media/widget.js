var MediaWidget=(function($) {
	
	// binding to UIs and caching elements
	var $el = {
		mediaManagementButton: $("#btnMediaManagement"),
		mediaDialog: $('#modal-manage-media'),

		imageTab: $('#myMediaTab a[href="#tab-image"]'), 
		videoTab: $('#myMediaTab a[href="#tab-video"]'), 
		audioTab: $('#myMediaTab a[href="#tab-audio"]'), 
		
		myImages: $("#myImages"),
		myVideos: $("#myVideos"),
		myAudios: $("#myAudios"),
		
		authorId: $("#authorId"),
		publicationId: $("publicationId"),
		
		myImageUploadControl: $("#myImageUploadControl"),
		myImageForm: $("#myImageForm"),
		myImageFormKey: $("#myImageKey"),
		myImageFormPolicy: $("#myImagePolicy"),
		myImageFormSignature: $("#myImageSignature"),
		myImageFormSpinner: $("#myImageFormSpinner"),
		myImageFormUploadButton: $("#myImageUploadButton"),

		myVideoUploadControl: $("#myVideoUploadControl"),
		myVideoForm: $("#myVideoForm"),
		myVideoFormKey: $("#myVideoKey"),
		myVideoFormPolicy: $("#myVideoPolicy"),
		myVideoFormSignature: $("#myVideoSignature"),
		myVideoFormSpinner: $("#myVideoFormSpinner"),
		myVideoFormUploadButton: $("#myVideoUploadButton"),

		myAudioUploadControl: $("#myAudioUploadControl"),
		myAudioForm: $("#myAudioForm"),
		myAudioFormKey: $("#myAudioKey"),
		myAudioFormPolicy: $("#myAudioPolicy"),
		myAudioFormSignature: $("#myAudioSignature"),
		myAudioFormSpinner: $("#myAudioFormSpinner"),
		myAudioFormUploadButton: $("#myAudioUploadButton"),
		
	};

	var myImageTemplate=Handlebars.templates["my-image"];
	var myVideoTemplate=Handlebars.templates["my-video"];
	var myAudioTemplate=Handlebars.templates["my-audio"];

	Handlebars.registerHelper ("getFileNameFromUrl", getFileNameFromUrl);
	Handlebars.registerHelper ("jsonStringify", jsonStringify);

	var openMediaDialog=function() {

		$el.mediaDialog.modal({
  			keyboard: false,
  			backdrop: true,
  			show: true
		});
		
		showImageTab();
		$el.imageTab.tab('show');
	};
	
	var setupMyImageForm=function() {
		
		$el.myImageForm.fileupload({
			done: function (e, data) {
	            var $xmlResult = $(data.result); // data.result is already an XML document so no need to use $.parseXML()
	            var $location=$xmlResult.find("Location");
	            
	            var mediaUrl=$location.text();
	            didUploadImageWithImageUrl(mediaUrl);	            
	        },
	        change: function (e, data) {
	        	$el.myImageFormSpinner.css("display", "inline-block");
	        },
	        add: function(e, data) {
                var uploadErrors = [];
                var acceptFileTypes = /(gif|jpe?g|png)$/i;
                if(!acceptFileTypes.test(data.originalFiles[0]['type'])) {
                    uploadErrors.push('อนุญาตให้อัพโหลดเฉพาะไฟล์รูปภาพเท่านั้น');
                }
                if(data.originalFiles[0]['size'] > 150000000) {
                    uploadErrors.push('ไฟล์มีขนาดใหญ่เกินไป ไม่สามารถอัพโหลดได้');
                }
                if(uploadErrors.length > 0) {
                	$("#myImageFormSpinner").css("display", "none");
                    alert(uploadErrors.join("\n"));
                } else {
                    data.submit();
                }
	        },
		});		

		// set GCS key of myImageForm
		var nowEpoch=getNowEpoch();
		var key=$el.authorId.val()+"/media/images/"+nowEpoch+"-${filename}"; // add timestamp to allow images that share the same file name
		$el.myImageFormKey.val(key);
	};

	var setupMyVideoForm=function() {
		
		$el.myVideoForm.fileupload({
			done: function (e, data) {
	            var $xmlResult = $(data.result); // data.result is already an XML document so no need to use $.parseXML()
	            var $location=$xmlResult.find("Location");
	            
	            var mediaUrl=$location.text();
	            didUploadVideoWithVideoUrl(mediaUrl);	            
	        },
	        change: function (e, data) {
	        	$el.myVideoFormSpinner.css("display", "inline-block");
	        },
	        add: function(e, data) {
                var uploadErrors = [];
                var acceptFileTypes = /(mp4)$/i;
                if(!acceptFileTypes.test(data.originalFiles[0]['type'])) {
                    uploadErrors.push('อนุญาตให้อัพโหลดเฉพาะไฟล์ MP4 เท่านั้น');
                }
                if(data.originalFiles[0]['size'] > 150000000) {
                    uploadErrors.push('ไฟล์มีขนาดใหญ่เกินไป ไม่สามารถอัพโหลดได้');
                }
                if(uploadErrors.length > 0) {
                	$("#myVideoFormSpinner").css("display", "none");
                    alert(uploadErrors.join("\n"));
                } else {
                    data.submit();
                }
	        },
		});		

		// set GCS key of myVideoForm
		var nowEpoch=getNowEpoch();
		var key=$el.authorId.val()+"/media/videos/"+nowEpoch+"-${filename}"; // add timestamp to allow videos that share the same file name
		$el.myVideoFormKey.val(key);
	};

	var setupMyAudioForm=function() {
		
		$el.myAudioForm.fileupload({
			done: function (e, data) {
	            var $xmlResult = $(data.result); // data.result is already an XML document so no need to use $.parseXML()
	            var $location=$xmlResult.find("Location");
	            
	            var mediaUrl=$location.text();
	            didUploadAudioWithAudioUrl(mediaUrl);	            
	        },
	        change: function (e, data) {
	        	$el.myAudioFormSpinner.css("display", "inline-block");
	        },
	        add: function(e, data) {
                var uploadErrors = [];
                var acceptFileTypes = /(mp3)$/i;
                if(!acceptFileTypes.test(data.originalFiles[0]['type'])) {
                    uploadErrors.push('อนุญาตให้อัพโหลดเฉพาะไฟล์ MP3 เท่านั้น');
                }
                if(data.originalFiles[0]['size'] > 150000000) {
                    uploadErrors.push('ไฟล์มีขนาดใหญ่เกินไป ไม่สามารถอัพโหลดได้');
                }
                if(uploadErrors.length > 0) {
                	$("#myAudioFormSpinner").css("display", "none");
                    alert(uploadErrors.join("\n"));
                } else {
                    data.submit();
                }
	        },
		});		

		// set GCS key of myVideoForm
		var nowEpoch=getNowEpoch();
		var key=$el.authorId.val()+"/media/audios/"+nowEpoch+"-${filename}"; // add timestamp to allow audios that share the same file name
		$el.myAudioFormKey.val(key);
	};

	var didUploadImageWithImageUrl=function(mediaUrl) {
        // upload new Media entity
		MediaEndpoint.insert(mediaUrl, "img", function(imageData) { 
			//var imageData=[{ mediaUrl: mediaUrl, authorId: $el.authorId.val(), publicationid: $el.publicationId.val(), type: "img" }];
			var $newImage=$($.parseHTML(myImageTemplate([imageData]))); // ต้องใช้ parseHTML เนื่องจากใน template มี empty tag <br /> ซึ่งไม่ใช่ valid XML expression!!!

			// TODO: โชว์ stub image ไปก่อน จะได้ทำ animation แว้บขึ้นมาได้
			// load newly uploaded image
			MediaEndpoint.loadImageAsync($newImage.find("img"));

			$newImage.insertAfter($el.myImageUploadControl);

	        $el.myImageFormSpinner.css("display", "none");
		});
	}

	var didUploadVideoWithVideoUrl=function(mediaUrl) {
        // upload new Media entity
		MediaEndpoint.insert(mediaUrl, "video", function(videoData) { 
			//var videoData=[{ mediaUrl: mediaUrl, authorId: $el.authorId.val(), publicationid: $el.publicationId.val(), type: "video" }];
			var $newVideo=$($.parseHTML(myVideoTemplate([videoData]))); // ต้องใช้ parseHTML เนื่องจากใน template มี empty tag <br /> ซึ่งไม่ใช่ valid XML expression!!!

			MediaEndpoint.loadVideoAsync($newVideo.find("video"));

			$newVideo.insertAfter($el.myVideoUploadControl);

	        $el.myVideoFormSpinner.css("display", "none");
		});
	}

	var didUploadAudioWithAudioUrl=function(mediaUrl) {
        // upload new Media entity
		MediaEndpoint.insert(mediaUrl, "audio", function(audioData) { 
			//var audioData=[{ mediaUrl: mediaUrl, authorId: $el.authorId.val(), publicationid: $el.publicationId.val(), type: "audio" }];
			var $newAudio=$($.parseHTML(myAudioTemplate([audioData]))); // ต้องใช้ parseHTML เนื่องจากใน template มี empty tag <br /> ซึ่งไม่ใช่ valid XML expression!!!

			MediaEndpoint.loadAudioAsync($newAudio.find("audio"));

			$newAudio.insertAfter($el.myAudioUploadControl);

	        $el.myAudioFormSpinner.css("display", "none");
		});
	}

	var loadMyImages=function() {
		$el.myImages.find("> div:not(:first)").remove(); // clear old values except the first div which is the control for uploading	
		
		MediaEndpoint.list({authorId: $el.authorId.val(), type: "img"}, function(data) {
			var images=data[0]; 
			var cursorString=data[1]; // TODO: ทำ infinite scrolling!
			$el.myImages.append(myImageTemplate(images)); 
			MediaEndpoint.loadImageAsync($el.myImages.find("img.myImage"));
		});
	};

	var loadMyVideos=function() {
		$el.myVideos.find("> div:not(:first)").remove(); // clear old values except the first div which is the control for uploading	
		
		MediaEndpoint.list({authorId: $el.authorId.val(), type: "video"}, function(data) {
			var videos=data[0]; 
			var cursorString=data[1]; // TODO: ทำ infinite scrolling!
			$el.myVideos.append(myVideoTemplate(videos)); 
			MediaEndpoint.loadVideoAsync($el.myVideos.find("video.myVideo"));
		});
	};

	var loadMyAudios=function() {
		$el.myAudios.find("> div:not(:first)").remove(); // clear old values except the first div which is the control for uploading	
		
		MediaEndpoint.list({authorId: $el.authorId.val(), type: "audio"}, function(data) {
			var audios=data[0]; 
			var cursorString=data[1]; // TODO: ทำ infinite scrolling!
			$el.myAudios.append(myAudioTemplate(audios)); 
			MediaEndpoint.loadAudioAsync($el.myAudios.find("audio.myAudio"));
		});
	};

	var showImageTab=function() {

		// first hide upload button wait until signature and policy fields are updated
		$el.myImageFormUploadButton.hide();
		
		// show upload button when form fields are ready
		MediaEndpoint.renewFormSignatureAndPolicy(function(data) {
			// update fields
			$el.myImageFormPolicy.val(data.policy);
			$el.myImageFormSignature.val(data.signature);
			
			// show the upload button
			$el.myImageFormUploadButton.show();
			
			setupMyImageForm();
		}, function() {
			console.log("failed in MediaEndpoint.renewFormSignatureAndPolicy");
		});
		
		loadMyImages();
	};
	
	var showVideoTab=function() {
		// first hide upload button wait until signature and policy fields are updated
		$el.myVideoFormUploadButton.hide();
		
		// show upload button when form fields are ready
		MediaEndpoint.renewFormSignatureAndPolicy(function(data) {
			// update fields
			$el.myVideoFormPolicy.val(data.policy);
			$el.myVideoFormSignature.val(data.signature);
			
			// show the upload button
			$el.myVideoFormUploadButton.show();
			
			setupMyVideoForm();
		}, function() {
			console.log("failed in MediaEndpoint.renewFormSignatureAndPolicy");
		});
		
		loadMyVideos();
	};

	var showAudioTab=function() {
		// first hide upload button wait until signature and policy fields are updated
		$el.myAudioFormUploadButton.hide();
		
		// show upload button when form fields are ready
		MediaEndpoint.renewFormSignatureAndPolicy(function(data) {
			// update fields
			$el.myAudioFormPolicy.val(data.policy);
			$el.myAudioFormSignature.val(data.signature);
			
			// show the upload button
			$el.myAudioFormUploadButton.show();
			
			setupMyAudioForm();
		}, function() {
			console.log("failed in MediaEndpoint.renewFormSignatureAndPolicy");
		});
		
		loadMyAudios();
	};
	
	var openMediaInNewTabWithUrl=function(url) {
		window.open(url, '_blank');		
	};
	
	var deleteMediaEntity=function(mediaEntity) {
		var mediaId=mediaEntity.id;
		// โชว์หมดเพราะขี้เกียจแยกฟังก์ชั่นหรือส่ง param เข้ามา และไม่น่าจะมีปัญหาอะไร
		$el.myImageFormSpinner.show();
		$el.myVideoFormSpinner.show();
		$el.myAudioFormSpinner.show();
		MediaEndpoint.remove(mediaId, function() {
			console.log("remove media entity succeeded.");
			
			$("#manageMedia"+mediaId).remove();
		}, function() {
			console.log("remove media entity failed.");			
		}, function() {
			$el.myImageFormSpinner.hide();
			$el.myVideoFormSpinner.hide();
			$el.myAudioFormSpinner.hide();
		});
	};

	return {
		init: function() {
			$el.mediaManagementButton.on("click", function() {
				openMediaDialog();
			});

			$el.imageTab.on("click", function() {
				showImageTab();
			});

			$el.videoTab.on("click", function() {
				showVideoTab();
			});

			$el.audioTab.on("click", function() {
				showAudioTab();
			});
			
			// on export
			$el.myImages.on("click", ".btn-download-img-resource", function() {
				var targetUrl=$(this).parent().parent().find("img").attr("src"); // ทำไมใช้แค่ closest ถึงไม่ work
				openMediaInNewTabWithUrl(targetUrl);
			});
			// on delete
			$el.myImages.on("click", ".btn-delete-img-resource", function() {
				if(confirm("ต้องการลบไฟล์รูปนี้?")) {
					var mediaEntity=$(this).parent().parent().find("img").data("media"); // ทำไมใช้แค่ closest ถึงไม่ work       และไม่ต้องใช้ JSON.parse เพราะ $.data set/get value เป็น object อยู่แล้ว!
					deleteMediaEntity(mediaEntity);					
				}
			});

			// on export
			$el.myVideos.on("click", ".btn-download-video-resource", function() {
				var targetUrl=$(this).parent().parent().find("video").attr("src"); // ทำไมใช้แค่ closest ถึงไม่ work
				openMediaInNewTabWithUrl(targetUrl);
			});
			// on delete
			$el.myVideos.on("click", ".btn-delete-video-resource", function() {
				if(confirm("ต้องการลบไฟล์วีดีโอนี้?")) {
					var mediaEntity=$(this).parent().parent().find("video").data("media"); // ทำไมใช้แค่ closest ถึงไม่ work
					deleteMediaEntity(mediaEntity);					
				}
			});

			// on export
			$el.myAudios.on("click", ".btn-download-audio-resource", function() {
				var targetUrl=$(this).parent().parent().find("audio").attr("src"); // ทำไมใช้แค่ closest ถึงไม่ work
				openMediaInNewTabWithUrl(targetUrl);
			});
			// on delete
			$el.myAudios.on("click", ".btn-delete-audio-resource", function() {
				if(confirm("ต้องการลบไฟล์เสียงนี้?")) {
					var mediaEntity=$(this).parent().parent().find("audio").data("media"); // ทำไมใช้แค่ closest ถึงไม่ work
					deleteMediaEntity(mediaEntity);					
				}
			});
		}
	};
	
})(jQuery);