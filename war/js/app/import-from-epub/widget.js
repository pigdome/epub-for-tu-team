var ImportFromEpubWidget=(function($) {
	
	var gcsBaseUrl="http://commondatastorage.googleapis.com/ipst-epub/";
	
	// binding to UIs and caching elements
	var $el = {
		importButton: $("input.bootstrap-filestyle"),
		file: $("#importFromEpubFile"),
				
		formControl: $("#importFromEpubFormControl"),
		form: $("#importFromEpubForm"),
		formKey: $("#importFromEpubFormKey"),
		formPolicy: $("#importFromEpubFormPolicy"),
		formSignature: $("#importFromEpubFormSignature"),
		
		spinner: $("#spinner"),
		
		authorId: $("#authorId"),
	};

	var showEpubImportButton=function() {
		// first hide upload form wait until signature and policy fields are updated
		$el.formControl.hide();
		
		// show upload button when form fields are ready
		ImportFromEpubEndpoint.renewFormSignatureAndPolicy(function(data) {
			// update fields
			$el.formPolicy.val(data.policy);
			$el.formSignature.val(data.signature);
						
			setupEpubUploadForm();
			
			// show the upload button
			$el.formControl.show();

		}, function() {
			console.log("failed in ImportFromEpubEndpoint.renewFormSignatureAndPolicy");
		});
	};
	
	var extractEpubForUrl=function(epubUrl) {
		//console.log(epubUrl);
		
		var gcsKey=epubUrl.split(gcsBaseUrl)[1];
		
		ImportFromEpubEndpoint.extractEpub(gcsKey, function(publicationAndEpubPackage) {
			//console.log(publicationAndEpubPackage);
			window.location.href="/"; // refresh the page
		}, function() {
			console.log("failed");			
			alert("นำเข้าไฟล์ ePub ไม่สำเร็จ กรุณาลองอีกครั้ง");
		}, function() {
        	$el.spinner.css("display", "none");
		})
	};
	
	var setupEpubUploadForm=function() {
		$el.form.fileupload({
			done: function (e, data) {
	            var $xmlResult = $(data.result); // data.result is already an XML document so no need to use $.parseXML()
	            var $location=$xmlResult.find("Location");
	            
	            var epubUrl=$location.text();
	            extractEpubForUrl(epubUrl);
	        },
	        change: function (e, data) {
	        	$el.spinner.css("display", "inline-block");
	        }, 
	        add: function(e, data) {
	        	//console.log(data);
	        	
                var uploadErrors = [];
                var acceptFileTypes = /epub\+zip$/i; // type คือ mimetype ไม่ใช่แค่ file extension
                if(!acceptFileTypes.test(data.originalFiles[0]['type'])) {
                    uploadErrors.push('อนุญาตให้นำเข้าเฉพาะไฟล์ ePub เท่านั้น');
                }
                if(data.originalFiles[0]['size'] > 150000000) { // 150 MB
                    uploadErrors.push('ไฟล์มีขนาดใหญ่เกินไป ไม่สามารถนำเข้าได้');
                }
                if(uploadErrors.length > 0) {
                	$el.spinner.css("display", "none");
                    alert(uploadErrors.join("\n"));
                } else {
                    data.submit();
                }
	        },
		});		

		// set GCS key of pdfForm
		var key=$el.authorId.val()+"/temp-imported-epub/${filename}"; 
		$el.formKey.val(key);
	};
	
	var adjustImportButtonPosition=function() {
		// workaround to align positions with other buttons
		$("label[for='importFromEpubFile']").attr("style", "margin-top:10px; margin-right:10px; width:75px;");
	};

	
	return {
		init: function() {
			
			/* this simple hack of a button triggering on input's file click doesn't work on the second time, use bootstrap filestyle instead (http://markusslima.github.io/bootstrap-filestyle/)
			$el.importButton.click(function() {
				$el.file.trigger("click");
			});
			*/

			adjustImportButtonPosition();			
			showEpubImportButton();
		}
	};
	
})(jQuery);