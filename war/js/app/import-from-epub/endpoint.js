var ImportFromEpubEndpoint=(function($) {
	
	var baseUrl="/importfromepubendpoint";
	var baseUrlForUploadForm="/mediauploadform";
	
	// binding to UIs and caching elements
	var $el = {
		authorId: $("#authorId"),
	};
	
	var extractEpub = function(servletUrl, req, epubGcsKey, doneCallback, failCallback, alwaysCallback) {
		
		$.ajax({
			type: 'POST',
			contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			url: servletUrl,
			data: { 
				req: req,
				epubGcsKey: epubGcsKey,
				authorId: $el.authorId.val()
			} 
		})
		.done(function(data) { 
			if(doneCallback)
				doneCallback(data);
		})
	    .fail(function() { 
	    	if(failCallback)
	    		failCallback();
	    })
	    .always(function() { 
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });			
	};

	var renewFormSignatureAndPolicy=function(servletUrl, doneCallback, failCallback, alwaysCallback) {
		$.ajax({
			  type: 'GET',
			  contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			  url: servletUrl,
			  dataType: 'json'
		})
		.done(function(data) { 
			if(doneCallback)
				doneCallback(data);
		})
		.fail(function() { 
			if(failCallback)
				failCallback();
	    })
	    .always(function() {
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });
	};
	
	return {
		extractEpub: function(epubGcsKey, doneCallback, failCallback, alwaysCallback) {
			var servletUrl=baseUrl;
			var req="extractEpub";
			extractEpub(servletUrl, req, epubGcsKey, doneCallback, failCallback, alwaysCallback);
		},
		renewFormSignatureAndPolicy: function(doneCallback, failCallback, alwaysCallback) {
			var servletUrl=baseUrlForUploadForm+"?req=renewFormSignatureAndPolicyForImport";
			renewFormSignatureAndPolicy(servletUrl, doneCallback, failCallback, alwaysCallback);			
		}
	};
	
})(jQuery);
