var BookCoverEndpoint=(function($) {

	var baseUrl="/bookcoverendpoint";
	var baseUrlForUploadForm="/mediauploadform";
	
	// binding to UIs and caching elements
	var $el = {
		authorId: $("#authorId")
	};
	
	var list = function(servletUrl, doneCallback, failCallback, alwaysCallback) {
		$.ajax({
			  type: 'GET',
			  contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			  url: servletUrl,
			  dataType: 'json'
		})
		.done(function(data) { 
			if(doneCallback)
				doneCallback(data);
		})
	    .fail(function() { 
	    	if(failCallback)
	    		failCallback();
	    })
	    .always(function() {
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });
	};

	var insert=function(servletUrl, req, coverUrl, doneCallback, failCallback, alwaysCallback) {
		$.ajax({
			type: 'POST',
			contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			url: servletUrl,
			data: { 
				req: req,
				bookCover: JSON.stringify({
					authorId: $el.authorId.val(),
					coverUrl: coverUrl
				})
			} 
		})
		.done(function() { // returns nothing
			if(doneCallback)
				doneCallback();
		})
	    .fail(function() { 
	    	if(failCallback)
	    		failCallback();
	    })
	    .always(function() { 
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });
	};

	var remove=function(servletUrl, req, coverId, doneCallback, failCallback, alwaysCallback) {
		$.ajax({
			type: 'POST',
			contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			url: servletUrl,
			data: {
				req: req,
				coverId: coverId
			} 
		})
		.done(function() { 
			if(doneCallback)
				doneCallback();
		})
	    .fail(function() { 
	    	if(failCallback)
	    		failCallback();
	    })
	    .always(function() { 
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });
	};
	
	var renewFormSignatureAndPolicy=function(servletUrl, doneCallback, failCallback, alwaysCallback) {
		$.ajax({
			  type: 'GET',
			  contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			  url: servletUrl,
			  dataType: 'json'
		})
		.done(function(data) { 
			if(doneCallback)
				doneCallback(data);
		})
		.fail(function() { 
			if(failCallback)
				failCallback();
	    })
	    .always(function() {
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });
	};

	return {
		list: function(options, doneCallback, failCallback, alwaysCallback) {
			var params={ req: "list"};
			$.extend(params, options);
			var servletUrl=baseUrl + "?" + $.param(params);
			list(servletUrl, doneCallback, failCallback, alwaysCallback);
		},
		insert: function(coverUrl, doneCallback, failCallback, alwaysCallback) {
			var servletUrl=baseUrl;
			var req="insert";
			insert(servletUrl, req, coverUrl, doneCallback, failCallback, alwaysCallback);
		},
		remove: function(coverId, doneCallback, failCallback, alwaysCallback) {
			var servletUrl=baseUrl;
			var req="remove";
			remove(servletUrl, req, coverId, doneCallback, failCallback, alwaysCallback);
		},
		renewFormSignatureAndPolicy: function(doneCallback, failCallback, alwaysCallback) {
			var servletUrl=baseUrlForUploadForm+"?req=renewFormSignatureAndPolicy";
			renewFormSignatureAndPolicy(servletUrl, doneCallback, failCallback, alwaysCallback);			
		}

	};
})(jQuery);
