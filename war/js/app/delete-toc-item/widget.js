var DeleteTocItemWidget=(function($) {
	
	// binding to UIs and caching elements
	var $el = {
		deleteTocItemButton: $("#btn-delete-chapterlist"),
		editingArea: $("#editingArea"),
	};
	
	var disableDeleteButton=function() {
		$el.deleteTocItemButton.attr("disabled", "disabled");
	};
	
	var enableDeleteButton=function() {
		$el.deleteTocItemButton.removeAttr("disabled");		
	};
	
	var init=function() {
		$el.deleteTocItemButton.on("click", function(e) {

			if(confirm("ต้องการลบหน้านี้?")) {
				
				var $selectedTocItem=$(".dd-selected-item").parent();
	
				// remove info for the selected item
				var targetHtmlHref=$selectedTocItem.attr("data-html-href");
				EpubPackageModel.removeHtmlItem(targetHtmlHref);
				
				var tocSubItemHtmlHrefs=$selectedTocItem.find("li").map(function() {
					return $(this).attr("data-html-href");
				}).get();
				for(var i=0; i<tocSubItemHtmlHrefs.length; i++) {
					// remove info for selected item's children
					EpubPackageModel.removeHtmlItem(tocSubItemHtmlHrefs[i]);
				}
				
				// delete TOC item and its subitems
				// but first check if it's the last subitem of the parent, if so remove <ol/> container, too
				var $parent=$selectedTocItem.parent();
				if($parent.is("ol") && $parent.children().length==1 && !$parent.parent().is("#nestable")) // condition สุดท้ายเช็คว่า ต้องไม่ใช่ chapter สุดท้าย
					$parent.remove();
				else
					$selectedTocItem.remove();
				
				// find a new default toc item to be open next time by default, if one exists (of class "dd-handle")
				var $tocItem=$(".dd-item .dd-handle").eq(0);
				if($tocItem.length>0) {
					$tocItem.addClass("dd-selected-item");
				}
				
				// update tocHtmlString
				var tocHtmlString=$("#nestable").prop("outerHTML");
				EpubPackageModel.updateTocHtmlString(tocHtmlString);
				
		        // TODO: เช็คความถูกต้องของ EpubPackage entity 
				// update EpubPackage entity
				EpubPackageWidget.disableAllControls();
				EpubPackageEndpoint.update(EpubPackageModel.getEpubPackage(), function(updatedEpubPackage) {
					console.log(updatedEpubPackage);
					
					// TODO: delete corresponding html files
					
					// refresh
					window.location.reload(true); // "true" skips the cache and reload the page from the server
				}, function() {
					console.log("fail updating EpubPackage entity when deleting toc item(s)");
					// refresh
					window.location.reload(true); // "true" skips the cache and reload the page from the server
				}, function() {
					//EpubPackageWidget.enableAllControls(); ไม่จำเป็นเพราะยังไงก็ต้อง refresh ไม่ว่า done/fail
				});
				
			}
			
		});
	};
	
	return {
		init: function() {
			init();
		}
	};
	
})(jQuery);