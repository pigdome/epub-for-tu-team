var ExportToPdfEndpoint=(function($) {
	
	var endpointUrl="/pdfhelperendpoint";
	
	var getAllHtmlsInSpineOrder=function(servletUrl, doneCallback, failCallback, alwaysCallback) {
		$.ajax({
			  type: 'GET',
			  contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			  url: servletUrl,
			  dataType: 'json'
		})
		.done(function(data) { 
			if(doneCallback)
				doneCallback(data);
		})
	    .fail(function() { 
	    	if(failCallback)
	    		failCallback();
	    })
	    .always(function() {
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });
	};	
	
	return {
		getAllHtmlsInSpineOrder: function(publicationId,  doneCallback, failCallback, alwaysCallback) {
			var servletUrl=endpointUrl + "?req=getAllHtmlsInSpineOrder&publicationId="+publicationId;
			getAllHtmlsInSpineOrder(servletUrl,  doneCallback, failCallback, alwaysCallback);
		},
	};
	
})(jQuery);
