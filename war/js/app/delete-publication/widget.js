var DeletePublicationWidget=(function($) {
	
	// binding to UIs and caching elements
	var $el = {
		bookList: $("#og-grid"),
		bookDetailClostButtonSelector: ".og-close",
	};
	
	var disableDeleteButton=function() {
		$el.bookList.find(".delete-book-button").css("color", "gray");
	};
	
	var enableDeleteButton=function() {
		$el.bookList.find(".delete-book-button").removeAttr("style");		
	};
	
	var init=function() {
		$el.bookList.on("click", ".delete-book-button", function(e) {
			e.preventDefault(); // prevent default link click's behavior

			disableDeleteButton();
			
			var $targetBookItem=$el.bookList.find(".book-item-now-selected > img");
			var publicationId=$targetBookItem.data("book").id;
			PublicationEndpoint.remove(publicationId, function() {
				// close book's detail
				$($el.bookDetailClostButtonSelector).trigger("click");
				// remove DOM
				// http://stackoverflow.com/questions/1190642/how-can-i-pass-a-parameter-to-a-settimeout-callback
				window.setTimeout(function() {
					$targetBookItem.fadeOut();
				}, 400);
				
				console.log("succeed deleting publicationId: "+publicationId);
			}, function() {
				console.log("fail deleting publicationId: "+publicationId);
			}, function() {
				enableDeleteButton();				
			})
		});
	};
	
	return {
		init: function() {
			init();
		}
	};
	
})(jQuery);