var PublicationWidget=(function($) {
	
	// binding to UIs and caching elements
	var $el = {
		newBookButton: $("#btnNewBook"),
		bookDialog: $("#myModal"),
		bookDialogTitle: $("#myModalLabel"),
		bookDialogTabs: $("#myTab"),
		bookCoverTab: $("#myTab a[href='#tab-book-cover']"),
		bookDescriptionTab: $("#myTab a[href='#tab-description']"),
		coversContainer: $("#epub-container"),
		navigationBar: $(".navbar"),
		templateCovers: $("#item-cover-templates"),
		templateCoversNavBar: $("#nav-cover-templates"),
		myCoverForm: $("#myCoverForm"),
		myCoverKey: $("#myCoverKey"),
		myCoverPolicy: $("#myCoverPolicy"),
		myCoverSignature: $("#myCoverSignature"),
		myCoverFormSpinner: $("#myCoverFormSpinner"),
		myCoverUploadButton: $("#myCoverUploadButton"),
		myCoverUploadControl: $("#myCoverUploadControl"),
		myCovers: $("#item-my-covers"),
		myCoversNavBar: $("#nav-my-cover"),
		selectedCoverPreview: $("#preview-book-cover-create-new"),
		coverBinButtons: $(".icon-trash-book-cover"),
		coverDeleteButtons: $(".btn-del-my-covers"),
		coverCancelDeleteButtons: $(".btn-cancel-my-covers"),
		logoutButton: $("#logoutButton"),
		authorId: $("#authorId"),
		
		saveBookButton: $("#btnSaveNewBook"), 
		bookList: $("#og-grid"),
		bookCovers: $(".book-icon"),
		//dismissBookDetailsButton: $(".og-close"), // doesn't work this way ... have to bind this dynamically using .on

		addOtherTitleButton: $("#addOtherTitleButton"),
		removeOtherTitleButton: $("#removeOtherTitleButton"),
		addAuthorButton: $("#addAuthorButton"),
		removeAuthorButton: $("#removeAuthorButton"),
		addSubjectHeadingButton: $("#addSubjectHeadingButton"),
		removeSubjectHeadingButton: $("#removeSubjectHeadingButton"),
		
		subjectHeading: $("#subjectHeading"),

		bookThemeTab: $("#myTab a[href='#tab-book-theme']"),
		themeMenu: $("#theme-menu"),
		themeId: $("#themeId"),
		selectedTheme: $("#selectedTheme"),
		themePreviews: $(".preview-theme"),
		defaultTheme: $("#defaultTheme"),
	};
	
	var openDialogToCreateNewBook=true; // false is open dialog to modify (through the edit button)
	
	var coverBaseUrl="http://commondatastorage.googleapis.com/ipst-epub/admin/template-covers/";
	var templateCoverUrls={ urls: [
	                               {url: coverBaseUrl+"book-cover-0.png"}, 
	                               {url: coverBaseUrl+"book-cover-1.png"},
	                               {url: coverBaseUrl+"book-cover-2.png"},
	                               {url: coverBaseUrl+"book-cover-3.png"},
	                               {url: coverBaseUrl+"book-cover-5.png"},
	                               {url: coverBaseUrl+"book-cover-6.png"},
	                               {url: coverBaseUrl+"book-cover-7.png"},
	                               {url: coverBaseUrl+"book-cover-8.png"},
	                               {url: coverBaseUrl+"book-cover-9.png"},
	                               {url: coverBaseUrl+"book-cover-10.png"},
	                               {url: coverBaseUrl+"book-cover-11.png"},
	                               {url: coverBaseUrl+"book-cover-12.png"},
	                               {url: coverBaseUrl+"book-cover-13.png"},
	                               {url: coverBaseUrl+"book-cover-14.png"},
	                               {url: coverBaseUrl+"book-cover-15.png"},
	                               {url: coverBaseUrl+"book-cover-16.png"},
	                               {url: coverBaseUrl+"book-cover-17.png"},
	                               {url: coverBaseUrl+"book-cover-18.png"},
	                               {url: coverBaseUrl+"book-cover-19.png"},
	                               {url: coverBaseUrl+"book-cover-20.png"},
	                               {url: coverBaseUrl+"book-cover-21.png"}
	                               ]
						  };
	var defaultCoverUrl=templateCoverUrls.urls[0].url;

	//var templateCoverTemplate = Handlebars.compile('{{#each urls}}<div class="panel-book-cover" style="display:inline-block; position:relative;"><i class="icon-trash icon-black icon-trash-book-cover" style="display:none;"></i><img data-url="{{url}}" style="box-shadow: 1px 1px 2px 1px #BABABA;"/></div>{{/each}}'); // cache the template of template covers

	// don't forget to include corresponding compiled templates in .jsp
	var templateCoverTemplate = Handlebars.templates["cover"] ; // use precompiled templates for better performance!
	var myCoverTemplate=Handlebars.templates["my-cover"];
	var bookTemplate=Handlebars.templates["book"];
	var bookDetailsTemplate=Handlebars.templates["book-details"];
	var bookDetailsReuseTemplate=Handlebars.templates["book-details-reuse"];
	
	Handlebars.registerHelper ("jsonStringify", jsonStringify);
	
	var beforeTags; // set ตอน load metadata ขึ้นมาเพื่อ edit และส่งไปตอน update SuggestedTag entity

	var showBookDetails=function(bookJSON, $bookContainer) {

		if($el.bookList.hasClass("expanded"))
		{	//แสดงว่ามี book ที่ expan อยู่ จะต้องทำการหาตำแหน่งของตัวที่ expan ก่อนอยู่แล้วว่าอยู่ไหน position เท่ากันไหม
			//บรรดทัดเดียวกันหรือเปล่า ถ้าบรรทัดเดียวกันจะทำการ reuse ใช้ expan ของตัวเดิมที่เปิดไว้อยู่แล้ว แต่เปลี่ยน
			//นำข้อมูลใหม่ที่กดเพื่อดูรายละเอียดไปใส่แทนที่ของเก่า

				if($el.bookList.hasClass("waiting")) {
					return false;
				}
				else if($bookContainer.hasClass("book-item-now-selected")) {
					// กรณีถ้า click ที่ item ของตัวเองอีกครั้งนึง ก็ทำการ close
					$(".book-item-position-expanded").find(".og-close").trigger('click');
				}
				else {
					//กรณีเมื่อกดที่ item อื่นๆ ซึ่งจะต้องตรวจสอบดูก่อนว่าเป็น row เดียวกันไหม เทียบจาก position() 
					//ถ้า row เดียวกันจะใช้ div expan ของตัวที่เปิดอยู่เดิม แต่ลบข้อมูลออกใช้เฉพาะ div expan แล้ว
					//นำข้อมูลของ book item ตัวใหม่ที่มีการเลือกดึงข้อมูลมาใส่ใน div expan ของตัวเดิมที่เปิดอยู่แล้ว

					var getPreviousExpanPosition = $(".book-item-position-expanded").position().top;
					var getNewExpanPosition = $bookContainer.position().top;

					// เอา position ทั้ง 2 อันมาเทียบกัน
					// 1. ถ้า เท่ากันแปลว่าอยู่ row เดียวกัน ก็ให้ใช้ reuse div expan ของอันเดิม นำข้อมูลใหม่มาใส่
					// 2. ถ้า ไม่เท่ากันแปลว่าอยู่คนละ row กัน ให้สร้าง div expan ใหม่ได้เลย แล้ว ซ่อนตัวเก่าแล้วลบทิ้งได้เลย

					if(getPreviousExpanPosition == getNewExpanPosition)
					{// 1. ถ้า เท่ากันแปลว่าอยู่ row เดียวกัน ก็ให้ใช้ reuse div expan ของอันเดิม นำข้อมูลใหม่มาใส่
						
						var elementBookItemPreviousExpan = $(".book-item-position-expanded");
						elementBookItemPreviousExpan.find(".og-expander-inner").remove();
						elementBookItemPreviousExpan.find(".og-expander").append(bookDetailsReuseTemplate(bookJSON));						
						var $bigCover=elementBookItemPreviousExpan.find(".og-fullimg img");
						PublicationEndpoint.loadImageAsync($bigCover);

						var $bigCoverContainer=elementBookItemPreviousExpan.find(".og-fullimg");
						$bigCoverContainer.imagesLoaded(function() {
							elementBookItemPreviousExpan.find(".bookCoverL").fadeIn(); // IMPORTANT: animation พวกนี้ไม่ work เพราะ load img ใหม่ทุกครั้ง ไม่ได้อยู่ใน cache เลย 
							$(".book-item-now-selected").find("#divExArrow").remove();							
							var strDivArrow = "<div id='divExArrow' style='margin-top:-15px; margin-left:25px; position:absolute;'><img id='exArrow' src='assets/img/expanArrow.png'/></div>";
							$bookContainer.append(strDivArrow);
							$(".book-item-now-selected").removeClass("book-item-now-selected");
							$bookContainer.addClass("book-item-now-selected");
						});

					}
					else { // 2. ถ้า ไม่เท่ากันแปลว่าอยู่คนละ row กัน ให้สร้าง div expan ใหม่ได้เลย แล้ว ซ่อนตัวเก่าแล้วลบทิ้งได้เลย

						$el.bookList.addClass("waiting"); // ไว้เพื่อกันตอนกดซ้ำตอนที่ expan ยังทำงานไม่เสร็จ แต่เท่าที่ทดลองไม่มีก็ไม่เป็นไร
							
					    $bookContainer.append(bookDetailsTemplate(bookJSON));
					    var $bigCover=$bookContainer.find(".og-fullimg img");
					    PublicationEndpoint.loadImageAsync($bigCover);
					    $bookContainer.css( "height", "650px" );

					    var $bigCoverContainer=$bookContainer.find(".og-fullimg");
					    $bigCoverContainer.imagesLoaded(function() {
						    // แก้ใหม่สำหรับให้ใช้ได้บน safari ด้วย
							$(".bookCoverL", $bookContainer).animate({ width:'300px', queue:false }, 350, "linear"); 

							$(".og-expander", $bookContainer).animate({ height:'500px', queue:false }, 350, "linear", function() {
								$bookContainer.find("#exArrow").show();
							});

							var liBookItemPreviousExpanded = $(".book-item-position-expanded");
							var liBookItemPreviousNowSelected = $(".book-item-now-selected");

							$el.bookList.find(".book-item-now-selected").removeClass("book-item-now-selected");
							$el.bookList.find(".book-item-position-expanded").removeClass("book-item-position-expanded");

							$bookContainer.addClass("book-item-now-selected"); 
							$bookContainer.addClass("book-item-position-expanded"); // ใช้ระบบ position ที่มีการแสดงข้อมูลอยู่

							$el.bookList.removeClass("waiting");	

							liBookItemPreviousExpanded.find(".og-expander").remove();
							liBookItemPreviousNowSelected.find("#divExArrow").remove();

							liBookItemPreviousExpanded.animate({ height:'150px', queue:false }, 250, "linear", function() {
								$.scrollTo('.book-item-now-selected', 350 ,{queue:false});
							});
					    	
					    });

					}
				}

		}
		else { //กรณีที่ยังไม่มี book เล่มไหนที่ expan อยู่เลย ก็ให้ สร้าง .og-expander ที่ item นั้นได้เลย

			if($el.bookList.hasClass("waiting")) {
				return false;
			}
			else if($bookContainer.hasClass("book-item-now-selected")) {
				$bookContainer.find(".og-close").trigger('click');
			}
			else {
				$el.bookList.addClass("waiting"); // ไว้เพื่อกันตอนกดซ้ำตอนที่ expan ยังทำงานไม่เสร็จ แต่เท่าที่ทดลองไม่มีก็ไม่เป็นไร
				
				$bookContainer.append(bookDetailsTemplate(bookJSON));
				var $bigCover=$bookContainer.find(".og-fullimg img");
				PublicationEndpoint.loadImageAsync($bigCover);
				
				$bookContainer.css( "height", "650px" );

				$('html, body').animate({ scrollTop: $(".book-icon", $bookContainer).offset().top, queue:false }, 350);

				var $bigCoverContainer=$bookContainer.find(".og-fullimg");
				$bigCoverContainer.imagesLoaded(function() {
					$(".bookCoverL", $bookContainer).animate({ width:'300px', queue:false }, 350, "linear");					
					$(".og-expander", $bookContainer).animate({ height:'500px', queue:false }, 350, "linear", function() {
						$bookContainer.find("#exArrow").show();
					});
					$bookContainer.addClass("book-item-now-selected");
					$bookContainer.addClass("book-item-position-expanded");
					$bookContainer.parent().addClass("expanded");
					$el.bookList.removeClass("waiting");					
				});

			}

		}
		
	};
	
	var dismissBookDetails=function() {

		// น่าจะใช้ .book-item-position-expanded ในการหา item ที่จะปิด expan ได้เลย 
		// เพราะยังไงก็ต้องอยู่ที่ตำแหน่ง ที่มี class book-item-position-expanded อยู่แล้ว
		var liBookItemExpanded = $(".book-item-position-expanded");
		
		liBookItemExpanded.animate({ opacity:'1.0', height:'150px', queue:false }, 350, "linear", function() {
		});

		liBookItemExpanded.find(".og-expander").animate({ opacity:'1.0', height:'0px', queue:false }, 350, "linear", function() {
			var liBookItemNowSelected = $(".book-item-now-selected");
			// จะส่วนโดยหาจาก .book-icon ที่มีการ click ไม่ใช่จากตำแหน่งที่แสดง expanded ซึ่งอาจจะไม่ใช่ item เดียวกัน
			liBookItemNowSelected.find("#exArrow").hide("blind", 150, function() {
				$("#og-grid").find(".book-item-now-selected").removeClass("book-item-now-selected");
				$("#og-grid").find(".book-item-position-expanded").removeClass("book-item-position-expanded");
				$("#og-grid").removeClass("expanded");

				liBookItemExpanded.find(".og-expander").remove();
				liBookItemNowSelected.find("#divExArrow").remove();
			});
		});

	};
	
	var openNewBookDialog=function() {

		openDialogToCreateNewBook=true;
		$el.bookDialogTitle.text("สร้าง");

		PublicationModel.clearPublicationInputs();

		// load default cover
		var $coverPreviewImg=$el.selectedCoverPreview.find("img");
		$coverPreviewImg.data("url", defaultCoverUrl);
		PublicationEndpoint.loadImageAsync($coverPreviewImg);
		
		$el.bookDialog.modal({
  			keyboard: false,
  			backdrop: true,
  			show: true
		});
		
		$el.bookCoverTab.tab('show');
	};
	
	var showMyCoversNavBar=function() {

		loadMyCovers();

		// first hide upload button wait until signature and policy fields are updated
		$el.myCoverUploadButton.hide();
		
		$el.templateCoversNavBar.removeClass("active");
		$el.myCoversNavBar.addClass("active");
		$el.templateCovers.hide();
		$el.myCovers.fadeIn();
		
		// show upload button when form fields are ready
		BookCoverEndpoint.renewFormSignatureAndPolicy(function(data) {
			// update fields
			$el.myCoverPolicy.val(data.policy);
			$el.myCoverSignature.val(data.signature);
			
			// show the upload button
			$el.myCoverUploadButton.show();
			
			setupMyCoverForm();
		}, function() {
			console.log("failed in BookCoverEndpoint.renewFormSignatureAndPolicy");
		});
	};
	
	var showTemplateCoversNavBar=function() {
		loadTemplateCovers();
		
		$el.myCoversNavBar.removeClass("active");
		$el.templateCoversNavBar.addClass("active");
		$el.myCovers.hide();
		$el.templateCovers.fadeIn();
	}
	
	var highlightAndSetCoverPreview=function(img) {
		$el.templateCovers.find(".book-cover-selected").removeClass("book-cover-selected").css("box-shadow", "1px 1px 2px 1px #BABABA");
		$el.myCovers.find(".book-cover-selected").removeClass("book-cover-selected").css("box-shadow", "1px 1px 2px 1px #BABABA");
		$(img).css("box-shadow", "");
		$(img).addClass("book-cover-selected");
		var $coverPreview=$el.selectedCoverPreview.find("img");
		$coverPreview.attr("src", $(img).attr("src"));
		$coverPreview.data("url", $(img).data("url"));
		$coverPreview.hide().fadeIn();
	}
	
	var hideCoverBinButton=function(img) {
		$(img).parent().find(".icon-trash").hide();		
	}

	var showCoverBinButton=function(img) {
		$(img).parent().find(".icon-trash").show();		
	}
	
	var showDeleteCoverButton=function(icon) {
		$(icon).parent().find(".form-btn-delete-my-cover").fadeIn(200);
	}
	
	var hideDeleteCoverButton=function(icon) {
		$(icon).parent().fadeOut(200);
	}
	
	var deleteCover=function(button) {
		$(button).parent().parent().fadeOut(300, function () {
   			$(button).remove();
  		});
	}
	
	var loadTemplateCovers=function() {
		$el.templateCovers.empty(); // clear old values
		$el.templateCovers.append(templateCoverTemplate(templateCoverUrls)); // load cover placeholders with unsigned urls in the temporary element data field
		
		// for each placeholder, load cover image asynchronously
		PublicationEndpoint.loadImageAsync($el.templateCovers.find("img"));
	};
	
	var loadMyCovers=function() {
		$el.myCovers.find("> div:not(:first)").remove(); // clear old values except the first div which is the control for uploading	
		
		BookCoverEndpoint.list({authorId: $el.authorId.val()}, function(data) {
			var bookCovers=data[0]; 
			var cursorString=data[1];
			$el.myCovers.append(myCoverTemplate(bookCovers)); 
			PublicationEndpoint.loadImageAsync($el.myCovers.find("img.myCover"));
		});
	};
	
	var logout=function() {
		AuthorEndpoint.logout(function() {
			window.location.href="/login.html";
		})
	};
	
	var didUploadCoverWithCoverUrl=function(coverUrl) {
        // upload new BookCover entity
		BookCoverEndpoint.insert(coverUrl, function() { // TODO: ให้ .insert return JSON entity กลับมา
			var imageData=[{ coverUrl: coverUrl }];
			var $newImage=$($.parseHTML(myCoverTemplate(imageData))); // ต้องใช้ parseHTML เนื่องจากใน template มี empty tag <br /> ซึ่งไม่ใช่ valid XML expression!!!

			// TODO: โชว์ stub image ไปก่อน จะได้ทำ animation แว้บขึ้นมาได้
			// load newly uploaded image
			PublicationEndpoint.loadImageAsync($newImage.find("img"));

			$newImage.insertAfter($el.myCoverUploadControl);

	        $el.myCoverFormSpinner.css("display", "none");
		});
	}
	
	var setupMyCoverForm=function() {
		
		$el.myCoverForm.fileupload({
			done: function (e, data) {
	            var $xmlResult = $(data.result); // data.result is already an XML document so no need to use $.parseXML()
	            var $location=$xmlResult.find("Location");
	            
	            var coverUrl=$location.text();
	            didUploadCoverWithCoverUrl(coverUrl);	            
	        },
	        change: function (e, data) {
	        	//$el.myCoverFileName.html(data.files[0].name);
	        	
	        	$el.myCoverFormSpinner.css("display", "inline-block"); // TODO: should find more appropriate place for this
	        }
		});		

		// set GCS key of myCoverForm
		var nowEpoch=parseInt(new XDate().getTime()/1000, 10);
		var key=$el.authorId.val()+"/covers/"+nowEpoch+"/${filename}"; // add timestamp to allow images that share the same file name
		$el.myCoverKey.val(key);
	};
	
	var updateTags=function(beforeTags, afterTags, authorId) {
		var tagsToAdd=_.difference(afterTags, beforeTags);
		var tagsToRemove=_.difference(beforeTags, afterTags);
		
		SuggestedTagEndpoint.update(tagsToAdd, tagsToRemove, authorId, function(insertedPublication) {
			console.log("SuggestedTagEndpoint.update succeeded.");						
		}, function() {
			console.log("failed in SuggestedTagEndpoint.update");			
		});
	};
	
	var insertBook=function() {
		
		var publication=PublicationModel.getPublicationFromInputs();
		if(publication.themeId==null || publication.themeId=="") // user never select a theme before saving
			publication.themeId="theme1"; // default
		
		PublicationEndpoint.insert(publication, function(insertedPublication) {
			//var signedCoverUrl=$el.selectedCoverPreview.find("img").attr("src");			
			//$.extend(publication, {signedCoverUrl: signedCoverUrl}); // signedCoverUrl อยู่ใน book.handlebars template ใช้กรณีที่รู้ signed url อยู่แล้ว จะได้ load image ได้เลย ไม่ต้อง get signed url อีกต่อ
																	 // TODO: จริงๆ แล้วควรทำระบบ local cache เก็บ signed url ไว้ในเครื่อง แต่คงยุ่งเรื่องจัดการ expired url แทน
			var books=[insertedPublication];
			var $newBook=$($.parseHTML(bookTemplate(books)));
			$el.bookList.prepend($newBook);
			PublicationEndpoint.loadImageAsync($newBook.find("img"));
			$newBook.trigger("didInsertBook");
			
			// update tags (subjectHeadings) in the server
			var afterTags=insertedPublication.subjectHeadings;
			updateTags(beforeTags, afterTags, $el.authorId.val());
		}, function() {
			console.log("failed in insertBook");
		});
		
	};
	
	var updateBook=function() {

		var publication=PublicationModel.getPublicationWithIdFromInputs(); // จะ update ต้องใช้ (publication) id ด้วย

		PublicationEndpoint.update(publication, function(updatedPublication) {
			// update UIs (covers, title, description) to reflect the change
			var $bigCoverImg=$(".og-fullimg > img");
			$bigCoverImg.data("url", publication.coverUrl);
			PublicationEndpoint.loadImageAsync($bigCoverImg);
			
			var $smallCoverImg=$(".book-item-now-selected > img");
			$smallCoverImg.data("url", publication.coverUrl);
			PublicationEndpoint.loadImageAsync($smallCoverImg);
			
			$smallCoverImg.data("book", updatedPublication); // also update data-book stored in <img /> counterpart
			
			var $title=$(".og-details h3");
			$title.text(publication.title);
			
			var $description=$(".og-details p");
			$description.text(publication.description.value);
			
			$el.bookDialog.modal("hide");

			// update tags (subjectHeadings) in the server
			var afterTags=updatedPublication.subjectHeadings;
			updateTags(beforeTags, afterTags, $el.authorId.val());
		}, function() {
			console.log("failed in updateBook");
		});		
	};
	
	var prepareToShowNewBook=function(newBookItem) {
		$el.bookDialog.on("hidden", {"bookItem": newBookItem}, showNewBookAsCover);
		$(newBookItem).imagesLoaded(function() {
			$el.bookDialog.modal("hide");
		});
	};
	
	var showNewBookAsCover=function(event) {
		var bookItem=event.data["bookItem"];
		$(bookItem).show();
		$(bookItem).find("img").show("scale", 400);
		
		$el.bookDialog.off("hidden", showNewBookAsCover);
	};
	
	var loadBookDetails=function(book) {
		PublicationModel.clearPublicationInputs(); // clear old value		
		PublicationModel.setPublicationToInputs(book); // load metadata into the tab
		
		beforeTags=book.subjectHeadings;
		
		$el.bookDescriptionTab.tab("show");
		$el.bookDialog.modal("show");
	};
 	
	var onEventsInMetadataTab=function() {
		$el.addOtherTitleButton.on("click", function() {
			PublicationModel.addOtherTitle();
		});	
		$el.removeOtherTitleButton.on("click", function() {
			PublicationModel.removeSelectedOtherTitle();
		});
		
		$el.addAuthorButton.on("click", function() {
			PublicationModel.addAuthor();
		});	
		$el.removeAuthorButton.on("click", function() {
			PublicationModel.removeSelectedAuthor();
		});

		$el.addSubjectHeadingButton.on("click", function() {
			PublicationModel.addSubjectHeading();
		});	
		$el.removeSubjectHeadingButton.on("click", function() {
			PublicationModel.removeSelectedSubjectHeading();
		});
	};
	
	var onEventsInMyCoversTab=function() {
		$el.myCoversNavBar.on("click", function(){
			if(!$(this).hasClass("active")){
				showMyCoversNavBar();
			}
		});
		
		$el.myCovers.on({
			click: function() {
				if(!$(this).hasClass("book-cover-selected")) {
					highlightAndSetCoverPreview(this);
				}				
			},
			mouseenter: function() {
				if(!$(this).hasClass("book-cover-selected")) { // กรณีปกนั้นถูกเลือกอยู่จะยังไม่ให้ลบได้
					showCoverBinButton(this);
				}
			},
			mouseleave: function() {
				if(!$(this).hasClass("book-cover-selected")) {
					hideCoverBinButton(this);
				}
			}
		}, "img");

		$el.coverBinButtons.on({
			mouseenter: function() {
				$(this).show();
			},
			mouseleave: function() {
				$(this).hide();  
			}
		});

		$el.coverBinButtons.on("click", function() {
			showDeleteCoverButton(this);
		});

		$el.coverCancelDeleteButtons.on("click", function(){
			hideDeleteCoverButton(this);
		});

		$el.coverDeleteButtons.on("click", function() {
			deleteCover(this);
		});
	};
	
	var onEventsInTemplateCoversTab=function() {
		$el.templateCovers.on("click", "img", function() {
			if(!$(this).hasClass("book-cover-selected")) {
				hideCoverBinButton(this);
				highlightAndSetCoverPreview(this);
			}
		});

		$el.templateCoversNavBar.on("click", function(){
			if(!$(this).hasClass("active")){
				showTemplateCoversNavBar();
			}
		});
	};
	
	var onEventsInUserManagement=function() {
		$el.logoutButton.on("click", function() {
			logout();
		});		
	};
	
	var onEventsInBookManagement=function() {
		$el.newBookButton.on("click", function() {
			openNewBookDialog();
		});
		
		$el.saveBookButton.on("click", function() {
			if(openDialogToCreateNewBook)
				insertBook();
			else
				updateBook();
		});
		
		$el.bookList.on("didInsertBook", "li", function() {
			prepareToShowNewBook(this);
		});		
	};
	
	var onEventsInBookDetails=function() {
		$el.bookList.on("click", "li > img", function() {
			var bookJSON=$(this).data("book");
			var bookContainer=$(this).parent();
			showBookDetails(bookJSON, bookContainer);
		});
		
		$el.bookList.on("click", ".og-close", function() {
			dismissBookDetails();
		});		
		
		$el.bookList.on("click", ".edit-metadata-button", function(e) {
			e.preventDefault(); // prevent default link click's behavior
			
			openDialogToCreateNewBook=false;			
			$el.bookDialogTitle.text("แก้ไข");

			var book=$el.bookList.find(".book-item-now-selected > img").data("book");
			loadBookDetails(book);
		});
		
		$el.bookList.on("click", ".go-inside-button", function(e) {
			e.preventDefault(); // prevent default link click's behavior

			var publicationId=$(".book-item-now-selected").attr("id");
			var publication=$("#"+publicationId).find("> img");
			var title=publication.data("book").title;
			var version=publication.data("book").version;

			// save to recent open book
			RecentOpenBookWidget.add(publicationId, title);
			
			var params={publicationId: publicationId, version: version} // จริงๆ ไม่จำเป็นต้องรู้ version เพราะ publicationId เป็น key ของ Publication entity (ถ้าอยากรู้ว่าหนังสือเล่มนี้มี version อะไรบ้าง ใช้ publicationGroupId) แต่ตอนสร้างไฟล์ html, png, mp4, etc. มันต้องรู้ fullpath มี version อยู่ในนั้นด้วย
			location.href="/edit.jsp?"+$.param(params);
		});
		
		$el.bookThemeTab.on("click", function() {
			// show default theme
			$el.defaultTheme.trigger("click");
		});
		
		$el.themeMenu.on("click", "li", function() {

			if($(this).text().trim()=="เรียบง่าย") {
				$el.themeId.val("theme1");	
				$el.selectedTheme.html("เรียบง่าย <b class='caret'></b>");
				
				$el.themePreviews.hide();
				$("#preview-theme-1").fadeIn();
				/*
				jQuery(".preview-theme-selected").hide();
				jQuery(".preview-theme-selected").removeClass("preview-theme-selected");
				jQuery(".theme-selected").removeClass("theme-selected");
				jQuery(this).addClass("theme-selected");
				jQuery("#preview-theme-1").fadeIn();
				jQuery("#preview-theme-1").addClass("preview-theme-selected");
				*/
			} 
			else if($(this).text().trim()=="เรียบง่าย 2") {
				$el.themeId.val("theme2");					
				$el.selectedTheme.html("เรียบง่าย 2 <b class='caret'></b>");

				$el.themePreviews.hide();
				$("#preview-theme-2").fadeIn();
			}
			else if($(this).text().trim()=="คลาสสิค") {
				$el.themeId.val("theme3");					
				$el.selectedTheme.html("คลาสสิค <b class='caret'></b>");

				$el.themePreviews.hide();
				$("#preview-theme-3").fadeIn();
			}
		});
		
	};

	var onEventsInTypeAheadTagSuggestion=function() {
		$el.subjectHeading.typeahead({
			source: function (query, process) {
		        SuggestedTagEndpoint.list(query, function(tags) {
		        	var tagNameInfos=new Array();
		        	$.each(tags, function(i, tag) {
		        		tagNameInfos.push(tag.name+" ("+getCountString(tag.tagCount)+" แท็ก, "+getCountString(tag.userCount)+" ผู้ใช้)");
		        	});
		        	process(tagNameInfos);
		        });
		    },
		    updater: function (item) {
		    	// ignore metadata part i.e. (? แท็ก, ? ผู้ใช้)
		    	var regex = new RegExp(' \\(.* แท็ก, .* ผู้ใช้\\)$');
		        return item.replace( regex, "" );
		    },
		    matcher: function (item) {
		    	// ไม่จำเป็นต้องลบส่วน metadata (เหมือนใน updater) เพราะตอน user พิมพ์เข้าไปใน query string มัน return no result อยู่แล้ว
		    	if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
		            return true;
		        }
		    },
		    sorter: function (items) {
		    	return items; // default sort ตามตัวอักษร (จาก app engine side)
		    },
		    highlighter: function (item) {
		    	var regex = new RegExp( '(' + this.query + ')', 'gi' );
		        return item.replace( regex, "<strong>$1</strong>" );
		    },
		});
	};
	
	var listBooks=function() {
		var options={ limit: 100 }; // TODO: ทำ infinite scrolling
		PublicationEndpoint.list(
		options,
		function(data) {
			var books=data[0]; 
			var cursorString=data[1];
			$el.bookList.append(bookTemplate(books));
			PublicationEndpoint.loadImageAsync($el.bookList.find("img.book-icon"));
			$el.bookList.find("li, img").show(); // the default visibility in template is display:none so make it visible
		}, function() {
			console.log("failed in listBooks");
		});
	};
	
	var init=function() {
		$el.navigationBar.show();
		$el.coversContainer.show();
		loadTemplateCovers();
		listBooks();
	};
	
	return {
		init: function() {

			onEventsInTemplateCoversTab();
			onEventsInMyCoversTab();
			onEventsInMetadataTab();
			onEventsInUserManagement();
			onEventsInBookManagement();
			onEventsInBookDetails();
			onEventsInTypeAheadTagSuggestion();
			
			init();
		}
	}
	
})(jQuery);