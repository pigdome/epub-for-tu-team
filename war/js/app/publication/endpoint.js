var PublicationEndpoint=(function($) {
	
	var baseUrl="/publicationendpoint";
	
	// binding to UIs and caching elements
	var $el = {
		// TODO: ตอนนี้ยังไม่ support versioning fix ไว้ เป็น "v1" กับ "true" (ใน PublicationConnector.java's insert) ไปก่อน
		//version: $(""), // v1, v2, v3, etc. 
		//isLatestVersion: $(""), // ใช้ตอน select เฉพาะ latest version 

		// TODO: ตอนนี้ยังไม่แยก textbook/concept   fix ไว้เป็น textbook ก่อน ตอน insert
		//String type; // textbook, concept

		//String status; // ตอน insert default เป็น "in progress"
		
		authorId: $("#authorId"),
		
		/*
		selectedCover: $("#preview-book-cover-create-new img"), 
	    title: $(".input-book-title"),
	    otherTitles: $("#otherTitles"),
	    authorsTypes_authors: $("#authorsTypes_authors"),
	    mediaTypes: $("#mediaTypes"),
	    resourceTypes: $("#resourceTypes"),
	    disciplines: $("#disciplines"),
	    grades: $("#grades"),
	    description: $("#description"),
	    subjectHeadings: $("#subjectHeadings")
	    */
	};
	
	var remove=function(servletUrl, req, publicationId, doneCallback, failCallback, alwaysCallback) {
		$.ajax({
			type: 'POST',
			contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			url: servletUrl,
			data: { 
				req: req,
				publicationId: publicationId
			}
		})
		.done(function() { 
			if(doneCallback)
				doneCallback();
		})
	    .fail(function() { 
	    	if(failCallback)
	    		failCallback();
	    })
	    .always(function() { 
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });
	};
	
	var insert = function(servletUrl, req, publication, doneCallback, failCallback, alwaysCallback) {
		$.ajax({
			type: 'POST',
			contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			url: servletUrl,
			data: { 
				req: req,
				publication: JSON.stringify({
					authorId: publication.authorId,					
					coverUrl: publication.coverUrl,					
					title: publication.title,						
					otherTitles: publication.otherTitles, 
					authors: publication.authors,
					authorsTypes: publication.authorsTypes,
					mediaTypes: publication.mediaTypes,
					resourceTypes: publication.resourceTypes,
					disciplines: publication.disciplines,
					grades: publication.grades,
					description: publication.description,
					subjectHeadings: publication.subjectHeadings,
					themeId: publication.themeId
				})
			} 
		})
		.done(function(data) { 
			if(doneCallback)
				doneCallback(data);
		})
	    .fail(function() { 
	    	if(failCallback)
	    		failCallback();
	    })
	    .always(function() { 
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });			
	};
	
	var update = function(servletUrl, req, publication, doneCallback, failCallback, alwaysCallback) {
		
		$.ajax({
			type: 'POST',
			contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			url: servletUrl,
			data: { 
				req: req,
				publication: JSON.stringify({
					id: publication.id,
					authorId: publication.authorId,					
					title: publication.title,
					otherTitles: publication.otherTitles, 
					authors: publication.authors,
					authorsTypes: publication.authorsTypes,
					collaboratorIds: publication.collaboratorIds,
					coverUrl: publication.coverUrl,
					dateCreated: publication.dateCreated,
					description: publication.description,
					disciplines: publication.disciplines,
					grades: publication.grades,
					isLatestVersion: publication.isLatestVersion,
					status: publication.status,
					type: publication.type,
					version: publication.version,
					versionDateCreated: publication.versionDateCreated, 
					mediaTypes: publication.mediaTypes,
					resourceTypes: publication.resourceTypes,
					subjectHeadings: publication.subjectHeadings,
					themeId: publication.themeId
				})
			} 
		})
		.done(function(data) { 
			if(doneCallback)
				doneCallback(data);
		})
	    .fail(function() { 
	    	if(failCallback)
	    		failCallback();
	    })
	    .always(function() { 
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });			
	};

	var list = function(servletUrl, doneCallback, failCallback, alwaysCallback) {
		$.ajax({
			  type: 'GET',
			  contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			  url: servletUrl,
			  dataType: 'json'
		})
		.done(function(data) { 
			if(doneCallback)
				doneCallback(data);
		})
	    .fail(function() { 
	    	if(failCallback)
	    		failCallback();
	    })
	    .always(function() {
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });
	};
	
	var getById = function(servletUrl, doneCallback, failCallback, alwaysCallback) {
		$.ajax({
			  type: 'GET',
			  contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			  url: servletUrl,
			  dataType: 'json'
		})
		.done(function(data) { 
			if(doneCallback)
				doneCallback(data);
		})
	    .fail(function() { 
	    	if(failCallback)
	    		failCallback();
	    })
	    .always(function() {
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });
	};
	
	var signUrl=function(url, doneCallback, failCallback, alwaysCallback) {
		
		if(url=="" || url==null)
			return null;
			
		$.ajax({
			type: 'GET',
			contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			url: '/urlsigner?originalUrl=' + url,
			dataType: 'text'
		})
		.done(function(signedUrl) {
			if(doneCallback)
				doneCallback(signedUrl);
		})
		.fail(function() { 
			if(failCallback)
				failCallback();
		})
		.always(function() {
			if(alwaysCallback)
				alwaysCallback();
		});
	};
	
	var loadImageAsync=function($img) {
		$img.each(function(index, img) {
			var url=$(img).data("url");
			signUrl(url, function(signedUrl) {
				$(img).attr("src", signedUrl);
			});
		});
	};
	
	return {
		list: function(options, doneCallback, failCallback, alwaysCallback) {
			var params={ req: "list", authorId: $el.authorId.val() };
			$.extend(params, options);
			var servletUrl=baseUrl + "?" + $.param(params);
			list(servletUrl, doneCallback, failCallback, alwaysCallback);
		},
		insert: function(publication, doneCallback, failCallback, alwaysCallback) {
			var servletUrl=baseUrl;
			var req="insert";
			insert(servletUrl, req, publication, doneCallback, failCallback, alwaysCallback);
		},
		update: function(publication, doneCallback, failCallback, alwaysCallback) {
			var servletUrl=baseUrl;
			var req="update";
			update(servletUrl, req, publication, doneCallback, failCallback, alwaysCallback);
		},
		remove: function(publicationId, doneCallback, failCallback, alwaysCallback) {
			var servletUrl=baseUrl;
			var req="remove";
			remove(servletUrl, req, publicationId, doneCallback, failCallback, alwaysCallback);			
		},
		signUrl: function(mediaUrl, doneCallback, failCallback, alwaysCallback) {
			signUrl(mediaUrl, doneCallback, failCallback, alwaysCallback);
		},
		loadImageAsync: function($img) {
			loadImageAsync($img);
		},
		getById: function(id, doneCallback, failCallback, alwaysCallback) {
			var params={ req: "getById", id: id };
			var servletUrl=baseUrl + "?" + $.param(params);
			getById(servletUrl, doneCallback, failCallback, alwaysCallback);
		},
	};
	
})(jQuery);
