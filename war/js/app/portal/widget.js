var PortalWidget=(function($) {
	
	// binding to UIs and caching elements
	var $el = {
		epubEditorWidgetRegion: $("#epubEditorWidgetRegion"),
	};

	var portalWidgetItemTemplate=Handlebars.templates["portal-widget-item"];

	var listLatestBooks=function() {
		var options={ limit: 10 };
		PublicationEndpoint.list(
		options,
		function(data) {
			var books=data[0]; 
			var cursorString=data[1];
			$el.epubEditorWidgetRegion.append(portalWidgetItemTemplate(books));
			PublicationEndpoint.loadImageAsync($el.epubEditorWidgetRegion.find("img"));
			$el.epubEditorWidgetRegion.find("img").show(); // the default visibility in template is display:none so make it visible
		}, function() {
			console.log("failed in listBooks");
		});
	};

	var init=function() {
		listLatestBooks();
	};
	
	return {
		init: function() {
			init();
		}
	};
	
})(jQuery);