var EmailEndpoint=(function($) {
	
	var baseUrl="/emailendpoint";
	var baseUrlForUploadForm="/mediauploadform";
	
	// binding to UIs and caching elements
	var $el = {
		publicationId: $("#publicationId"),
	};
	
	var sendEpub = function(servletUrl, req, emailObj, doneCallback, failCallback, alwaysCallback) {
		
		$.ajax({
			type: 'POST',
			contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			url: servletUrl,
			data: { 
				req: req,
				publicationId: $el.publicationId.val(), 
				email: emailObj.email,
				senderName: emailObj.senderName,
				subject: emailObj.subject,
				message: emailObj.message
			} 
		})
		.done(function() { 
			if(doneCallback)
				doneCallback();
		})
	    .fail(function() { 
	    	if(failCallback)
	    		failCallback();
	    })
	    .always(function() { 
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });			
	};

	var sendPdf = function(servletUrl, req, emailObj, doneCallback, failCallback, alwaysCallback) {
		
		$.ajax({
			type: 'POST',
			contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			url: servletUrl,
			data: { 
				req: req,
				pdfUrl: emailObj.pdfUrl,
				email: emailObj.email,
				senderName: emailObj.senderName,
				subject: emailObj.subject,
				message: emailObj.message
			} 
		})
		.done(function() { 
			if(doneCallback)
				doneCallback();
		})
	    .fail(function() { 
	    	if(failCallback)
	    		failCallback();
	    })
	    .always(function() { 
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });			
	};

	var signPdfUrl=function(url, doneCallback, failCallback, alwaysCallback) {
		
		if(url=="" || url==null)
			return null;
			
		$.ajax({
			type: 'GET',
			contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			url: '/urlsigner?originalUrl=' + url,
			dataType: 'text'
		})
		.done(function(signedUrl) {
			if(doneCallback)
				doneCallback(signedUrl);
		})
		.fail(function() { 
			if(failCallback)
				failCallback();
		})
		.always(function() {
			if(alwaysCallback)
				alwaysCallback();
		});
	};	
	
	var renewFormSignatureAndPolicy=function(servletUrl, doneCallback, failCallback, alwaysCallback) {
		$.ajax({
			  type: 'GET',
			  contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			  url: servletUrl,
			  dataType: 'json'
		})
		.done(function(data) { 
			if(doneCallback)
				doneCallback(data);
		})
		.fail(function() { 
			if(failCallback)
				failCallback();
	    })
	    .always(function() {
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });
	};
	
	return {
		sendEpub: function(emailObj, doneCallback, failCallback, alwaysCallback) {
			var servletUrl=baseUrl;
			var req="sendEpub";
			sendEpub(servletUrl, req, emailObj, doneCallback, failCallback, alwaysCallback);
		},
		sendPdf: function(emailObj, doneCallback, failCallback, alwaysCallback) {
			var servletUrl=baseUrl;
			var req="sendPdf";
			sendPdf(servletUrl, req, emailObj, doneCallback, failCallback, alwaysCallback);
		},
		signPdfUrl: function(pdfUrl, doneCallback, failCallback, alwaysCallback) {
			signPdfUrl(pdfUrl, doneCallback, failCallback, alwaysCallback);
		},
		renewFormSignatureAndPolicy: function(doneCallback, failCallback, alwaysCallback) {
			var servletUrl=baseUrlForUploadForm+"?req=renewFormSignatureAndPolicy";
			renewFormSignatureAndPolicy(servletUrl, doneCallback, failCallback, alwaysCallback);			
		}
	};
	
})(jQuery);
