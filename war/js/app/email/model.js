var EmailModel=(function($) {

	// binding to input UIs and caching elements
	var $el = {
		epubEmail: $("#txt-email-epub"),
		epubSenderName: $("#txt-sender-epub"),
		epubSubject: $("#txt-subject-epub"),
		epubMessage: $("#txt-detail-epub"),
		pdfEmail: $("#txt-email-pdf"),
		pdfSenderName: $("#txt-sender-pdf"),
		pdfSubject: $("#txt-subject-pdf"),
		pdfMessage: $("#txt-detail-pdf"),
		pdfLink: $("#pdfLink"),
	};

	return {
		clearInputs: function() {
			$el.epubEmail.val("");
			$el.epubSenderName.val("");
			$el.epubSubject.val("");
			$el.epubMessage.val("");
			$el.pdfEmail.val("");
			$el.pdfSenderName.val("");
			$el.pdfSubject.val("");
			$el.pdfMessage.val("");
		},
		getFromEpubInputs: function() {
			return {
				email: $el.epubEmail.val().trim(),
				senderName: $el.epubSenderName.val().trim(),
				subject: $el.epubSubject.val().trim(),
				message: $el.epubMessage.val()
			}
		},
		getFromPdfInputs: function() {
			return {
				email: $el.pdfEmail.val().trim(),
				senderName: $el.pdfSenderName.val().trim(),
				subject: $el.pdfSubject.val().trim(),
				message: $el.pdfMessage.val(),
				pdfUrl: $el.pdfLink.data("url")
			}			
		}
	};
	
})(jQuery);
