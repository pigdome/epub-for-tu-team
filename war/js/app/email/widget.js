var EmailWidget=(function($) {
	
	// binding to UIs and caching elements
	var $el = {
		emailButton: $("#btnSendEmail"),
		emailDialog: $('#modal-send-email'),
		epubTab: $('#myTab a[href="#tab-email-epub"]'), 
		pdfTab: $('#myTab a[href="#tab-email-pdf"]'), 
		
		sendEpubButton: $("#btn-confirm-send-email-epub"),
		sendPdfButton: $("#btn-confirm-send-email-pdf"),

		authorId: $("#authorId"),
		
		pdfForm: $("#pdfForm"),
		pdfFormKey: $("#pdfFormKey"),
		pdfFormPolicy: $("#pdfFormPolicy"),
		pdfFormSignature: $("#pdfFormSignature"),
		pdfFormSpinner: $("#pdfFormSpinner"),
		pdfFormUploadButton: $("#pdfFormUploadButton"),
		pdfLinkControl: $("#pdfLinkControl"),
		pdfLink: $("#pdfLink"),
	};

	var showPdfUploadForm=function() {
		// first hide upload button wait until signature and policy fields are updated
		$el.pdfFormUploadButton.hide();
		$el.pdfLinkControl.hide();
		
		// show upload button when form fields are ready
		EmailEndpoint.renewFormSignatureAndPolicy(function(data) {
			// update fields
			$el.pdfFormPolicy.val(data.policy);
			$el.pdfFormSignature.val(data.signature);
			
			// show the upload button
			$el.pdfFormUploadButton.show();
			
			setupPdfUploadForm();
			
		}, function() {
			console.log("failed in EmailEndpoint.renewFormSignatureAndPolicy");
		});
	};
	
	var setupPdfUploadForm=function() {
		$el.pdfForm.fileupload({
			done: function (e, data) {
	            var $xmlResult = $(data.result); // data.result is already an XML document so no need to use $.parseXML()
	            var $location=$xmlResult.find("Location");
	            
	            var pdfUrl=$location.text();
	            didUploadPdfWithPdfUrl(pdfUrl);	            
	        },
	        change: function (e, data) {
	        	//$el.myCoverFileName.html(data.files[0].name);
	        	
	        	$el.pdfFormSpinner.css("display", "inline-block");
	        }, 
	        add: function(e, data) {
                var uploadErrors = [];
                var acceptFileTypes = /pdf$/i;
                if(!acceptFileTypes.test(data.originalFiles[0]['type'])) {
                    uploadErrors.push('อนุญาตให้อัพโหลดเฉพาะไฟล์ PDF เท่านั้น');
                }
                if(data.originalFiles[0]['size'] > 150000000) {
                    uploadErrors.push('ไฟล์มีขนาดใหญ่เกินไป ไม่สามารถอัพโหลดได้');
                }
                if(uploadErrors.length > 0) {
                	$el.pdfFormSpinner.css("display", "none");
                    alert(uploadErrors.join("\n"));
                } else {
                    data.submit();
                }
	        },
	        // acceptFileTypes: /\.pdf$/i    <-- ไม่ work
		});		

		// set GCS key of pdfForm
		var key=$el.authorId.val()+"/temp-attachments/${filename}"; 
		$el.pdfFormKey.val(key);
	};
	
	var didUploadPdfWithPdfUrl=function(pdfUrl) {
        // show link of uploaded pdf for validation
		EmailEndpoint.signPdfUrl(pdfUrl, function(signedPdfUrl) { 

			$el.pdfLink.attr("href", signedPdfUrl);
			$el.pdfLink.data("url", pdfUrl);
			$el.pdfLinkControl.show();

	        $el.pdfFormSpinner.css("display", "none");
		}, function() {
			console.log("failed in EmailEndpoint.signPdfUrl");
		});
	}

	return {
		init: function() {
			
			$el.pdfTab.click(function() {
				showPdfUploadForm();
			});
			
			$el.sendEpubButton.click(function() {
				var emailObj=EmailModel.getFromEpubInputs();
				EmailEndpoint.sendEpub(emailObj, function() {
					console.log("epub sent");					
					alert("ส่งเรียบร้อย");
				}, function() {
					console.log("failed in EmailEndpoint.sendEpub");
				});
			});

			$el.sendPdfButton.click(function() {
				var emailObj=EmailModel.getFromPdfInputs();
				EmailEndpoint.sendPdf(emailObj, function() {
					console.log("pdf sent");					
					alert("ส่งเรียบร้อย");
				}, function() {
					console.log("failed in EmailEndpoint.sendPdf");
				});
			});
			
		    $el.emailButton.click(function () {

		    	EmailModel.clearInputs();
		    	
		        $el.emailDialog.modal({
		            keyboard: false,
		            backdrop: true,
		            show: true
		        });
		        
		        $el.epubTab.tab('show');
		    });
		}
	};
	
})(jQuery);