var HtmlUploadWidget=(function($) {
	
	// binding to UIs and caching elements
	var $el = {
		saveHtmlButton: $("#btnSaveHtml"),
		fileForm: $("#fileForm"),
		editingArea: $("#editingArea"),
		titleSelector: ".ipst-epub-header-desc",
		/* can't cache these dynamically created elements
		nestableToc: $(".dd"),
		nestableTocContainer: $("#nestable"),
		*/
	};

	var gcsRelativePathPrefix;
	var $content; // clone content จาก editingArea ออกมา เพราะต้อง preprocess และเปลี่ยนอะไรหลายอย่าง clone มันเลย จะได้ไม่ affect กับ original editingArea's content
	var $originalContent; // ใช้ตอนจะ store info อะไรกลับเข้าไป เช่น gcs-relative-path

	var htmlComponentTheme1Template=Handlebars.templates["ipst-epub-theme1"];
	var htmlComponentTheme2Template=Handlebars.templates["ipst-epub-theme2"];
	var htmlComponentTheme3Template=Handlebars.templates["ipst-epub-theme3"];
	
	//var htmlFileType="application/xhtml+xml";
	var htmlMimeType="text/html"; // TODO: ตาม spec ต้องใช้ด้านบน
	var fileExtension=".html";
	var imageMimeType="image/png";
	var videoMimeType="video/mp4";
	var audioMimeType="audio/mp3";

	// จริงๆ แล้วแค่ update EpubPackage entity ไว้เฉยๆ ก่อนก็ได้ ยังไม่จำเป็นต้อง copy ไฟล์ media ต่างๆ จาก media folder ไปยัง epub folder
	// (*** ไม่จำเป็นต้อง copy ไปไว้ใน epub folder ก็ได้ ทิ้งไว้ใน media folder อย่างเดียวก็พอ)
	// นอกจากจะทำให้ latency น้อยตอน user ทำการบันทึกไฟล์แล้ว ยังลด load ให้กับ GCS ได้ด้วย (ผลักไปตอน export to epub/pdf)
	// ยังไงก็ตาม ทำแบบนี้ ไม่ได้แก้ปัญหา unused media files เพราะตอนลบ media file ไม่ได้ตามไป update ใน EpubPackage (และไม่น่ามีปัญหาอะไร เพราะเราจะมี feature optimize epub size อยู่แล้ว)
	var updateEpubPackageEntity=function(doneCallback, failCallback, alwaysCallback) {
		var htmlHref=getLastComponentOfUrl(getGcsRelativePath());
		updateToc(htmlHref);
		updateEpubManifest(htmlHref);
		updateEpubSpine();
		updateEpubNav();
		
		EpubPackageEndpoint.update(EpubPackageModel.getEpubPackage(), function() {
			console.log("succeeded updating EpubPackage entity");
			
			OfflineSupportWidget.deactivate();
			
			if(doneCallback)
				doneCallback();
			
		}, function() {
			console.log("failed updating EpubPackage entity");
			
			if(failCallback)
				failCallback();
		}, function() {
			if(alwaysCallback)
				alwaysCallback();
		});
	};

	var updateEpubNav=function() {

		// สำหรับ TOC ที่แสดงใน editor ให้เก็บตามนี้เลย ส่วน TOC ที่แสดงใน epub reader ต้องใส่ link เข้าไปด้วย
		EpubPackageModel.updateTocHtmlString($("#nestable").prop("outerHTML"));
		
		var $tocContainer=$("#nestable").clone(); // to make further change invisible		
		
		// make TOC items linkable
		$tocContainer.find(".dd-item > div").each(function(index, div) {
			var $a=$("<a></a>");
			$a.attr("href", $(div).parent().attr("data-html-href"));
			$a.css("font-size", "20px");
			$(div).wrapInner($a);
		});
		
		var tocHtmlString=$tocContainer.prop("outerHTML");
		
		EpubPackageModel.updateNavHtmlString(tocHtmlString);
	};
	
	var updateToc=function(htmlHref) {
		
		var $selectedTocItem=$(".dd-selected-item").parent();
		$selectedTocItem.attr("data-html-href", htmlHref); // use .attr to persist info directly in the html tag (round trip between server and client)

	};
	
	var updateEpubSpine=function() {
		var itemrefIdRefs=new Array();
		var itemrefLinears=new Array();
		var $items=$(".dd").find("> ol > li");		
		$items.each(function(index, item) {
			var htmlHref=$(item).attr("data-html-href").split(".")[0];
			itemrefIdRefs.push(htmlHref);
			itemrefLinears.push("yes"); // currently default to "yes", meaning the reading system put this item in linear order unlike "no" where the system will skip rendering it in order, only doing it for internal linking
			var $subitems=$(item).find("> ol > li");
			if($subitems.length>0) {
				$subitems.each(function(index, subitem) {
					var htmlHrefSubitem=$(subitem).attr("data-html-href").split(".")[0];
					itemrefIdRefs.push(htmlHrefSubitem);					
					itemrefLinears.push("yes"); // default to "yes"
				});				
			}
		});
		EpubPackageModel.updateSpineIdRefs(itemrefIdRefs, itemrefLinears);
	};
	
	var updateEpubManifest=function(htmlHref) {
		var $img=$content.find("img");
		var $video=$content.find("video");
		var $audio=$content.find("audio");
		var imgUrls=$img.map(function() {
		    return $(this).data("url");
		}).get();
		var videoUrls=$video.map(function() {
		    return $(this).data("url");
		}).get();
		var audioUrls=$audio.map(function() {
		    return $(this).data("url");
		}).get();
		
		// update epub's manifest (list of files used in epub package)
		if(imgUrls!=null && imgUrls.length>0)
			EpubPackageModel.addMediaItems(imgUrls, imageMimeType);
		if(videoUrls!=null && videoUrls.length>0)
			EpubPackageModel.addMediaItems(videoUrls, videoMimeType);
		if(audioUrls!=null && audioUrls.length>0)
			EpubPackageModel.addMediaItems(audioUrls, audioMimeType);		
		EpubPackageModel.addHtmlItem(htmlHref, htmlMimeType);
	};
	
	var getHtmlStringForThemeId=function(themeId) {
		var fileContentString=$content.prop("outerHTML");
		var htmlString;
		if(themeId=="theme1") {
			htmlString=htmlComponentTheme1Template(fileContentString);
		} else if(themeId=="theme2") {
			htmlString=htmlComponentTheme2Template(fileContentString);			
		} else if(themeId=="theme3") {
			htmlString=htmlComponentTheme3Template(fileContentString);			
		}
		return htmlString;
	};

	var getHtmlTitle=function() {
		var title=$($el.titleSelector).text().trim();
		if(title=="")
			title="untitled";
		return title;
	};
	
	var getGcsRelativePath=function() {
		// check if this html is an existing file on GCS or not by fetching the path from "data-gcs-relative-path", if so return that, if not construct a new one
		//var gcsRelativePath=$originalContent.data("gcs-relative-path");
		var gcsRelativePath=$content.attr("data-gcs-relative-path");
		
		if(gcsRelativePath=="") {
			// TODO: IMPORTANT: https://code.google.com/p/appengine-gcs-client/issues/detail?id=21
			// gcs client lib ยังมี bug อยู่ ทำ encoding ผิด! (แทนที่จะเป็น %20 ดันเป็น +)
			// workaround คือ replace space ด้วย - ไปก่อน
			//gcsRelativePath=gcsRelativePathPrefix + getNowEpoch() + "-" + encodeURI(getHtmlTitle().replace(" ", "-")) + fileExtension; // attach timestamp in front of a file name
			
			gcsRelativePath=gcsRelativePathPrefix + getNowEpoch() + "-" + md5(getHtmlTitle()) + fileExtension; // ยังมีปัญหาเกี่ยวกับ encoding ตอน sign url (gcs bug?) ตัดปัญหาใช้ hash ของชื่อไฟล์ จบ!
			
			/*
			$originalContent.data("gcs-relative-path", gcsRelativePath); 
			$originalContent.data("first-upload", "yes");
			*/
			$content.attr("data-gcs-relative-path", gcsRelativePath);
			$content.attr("data-first-upload", "yes");
			// ต้องใส่กลับเข้าไปใน original content ด้วย ไม่เฉพาะแค่ตัวที่ clone มา (ที่ต้องมีตัว clone เพราะต้องจัดการ src ของพวกไฟล์ media แท้ๆ เล้ย)
			$originalContent.attr("data-gcs-relative-path", gcsRelativePath); // assign back to the content (to be used in next editing)
			$originalContent.attr("data-first-upload", "yes");
		} else {
			// In case of new import, data-gcs-relative-path holds the path of the previous publication
			// The prefix must be updated in that case! (to match "publicationId" part, otherwise it will make update to the wrong file of previous publication!)
			var targetPublicationId=gcsRelativePathPrefix.split("/")[1];
			var publicationIdInGcsRelativePath=gcsRelativePath.split("/")[1];
			if(publicationIdInGcsRelativePath!=targetPublicationId) {
				gcsRelativePath = gcsRelativePath.replace("/"+publicationIdInGcsRelativePath+"/", "/"+targetPublicationId+"/");
				$content.attr("data-gcs-relative-path", gcsRelativePath);
				$originalContent.attr("data-gcs-relative-path", gcsRelativePath);
			}
		}
		
		return gcsRelativePath;
	};
	
	var uploadHtmlFileWithSignatureAndPolicyAndUpdateEpubPackage=function(signature, policy, doneCallback, failCallback, alwaysCallback) {
		var gcsRelativePath=getGcsRelativePath(); // ต้องเรียกบรรทัดนี้ก่อนเลย เพราะอาจมี side-effect เปลี่ยน $content ได้ (คือเพิ่ม data-gcs-relative-path เข้าไป)

		var firstUpload=$content.attr("data-first-upload");
		if(firstUpload=="yes") {
			// ต้อง update attribute ก่อน upload file เพื่อ save ใน server
			$content.attr("data-first-upload", "no");
			$originalContent.attr("data-first-upload", "no");
		}
		
		var themeId=$content.data("theme-id");
		var htmlString=getHtmlStringForThemeId(themeId);

		//console.log(htmlString);		
		//console.log(gcsRelativePath);		
		
		// creating a html file as a blob
		var blob=new Blob([htmlString], { "type": htmlMimeType });		
		var filesList=new Array();
		filesList.push(blob);
		
		// fileupload plugin initialization
		$el.fileForm.fileupload({
			url: "http://ipst-epub.storage.googleapis.com/",
			type: "POST",
			multipart: true,
			paramName: "file",
			formData: {
				key: gcsRelativePath,
				bucket: "ipst-epub",
				GoogleAccessId: "521169348394-fldq88uf0hui6vd2u2cdb2hu18as9ba7@developer.gserviceaccount.com",
				policy: policy,
				signature: signature,
				success_action_status: "201"
			}
		});
		
		// start sending a file programmatically
		var jqXHR = $el.fileForm.fileupload('send', {files: filesList})
						.success(function (result, textStatus, jqXHR) {
							console.log("succeeded uploading a file: "+gcsRelativePath);
							updateEpubPackageEntity(doneCallback, failCallback, alwaysCallback);
						})
						.error(function (jqXHR, textStatus, errorThrown) {
							if(failCallback)
								failCallback();
						})
						.complete(function (result, textStatus, jqXHR) {
						});
	};

	var preprocessHtmlFile=function() {		
		// remove editing-specific styles
		$content.css("border", "");
		$content.css("padding", "");
		$content.css("height", "");
		$content.css("min-height", "");
		
		// change background path (if they exist) to local package
		var containBackground=$content.hasClass("ipst-epub-contain-background");
		if(containBackground) {
			/* ใช้วิธีง่ายๆ แบบนี้ไม่ได้ เพราะ browser มันฉลาดเกิน มัน resolve relative path ด้วย current url กลายเป็น http://ipst-epub.appspot.com/images/... ซึงไม่ใช่ที่เราต้องการ เราต้องการแค่ /images/...
			var originalPath=$content.css("background-image");
			var fileName=getLastComponentOfUrl(originalPath);
			$content.css("background-image", "url(images/"+fileName+")");			
			*/
			var originalPath=$content.css("background-image").replace(/^url\(/, "").replace(/\)$/, "");
			var fileName=getLastComponentOfUrl(originalPath);
			$content.attr("style", "background-image: url(images/"+fileName+");");
		}

		// change system image (e.g. backgrounds, lines, placeholders) paths to local package
		var $systemImages=$content.find(".ipst-epub-system-image");
		if($systemImages.length>0) {
			$systemImages.each(function(index, image) {
				var fileName=getLastComponentOfUrl($(image).attr("src"));
				$(image).attr("src", "images/"+fileName);
			});			
		}

		// change media paths to local package
		var $images=$content.find(".myCKImage");
		if($images.length>0) {
			$images.each(function(index, image) {
				var url=$(image).data("url");
				var fileName=getLastComponentOfUrl(url);
				$(image).attr("src", "images/"+fileName);
			});			
		}

		var $videos=$content.find(".myCKVideo");
		if($videos.length>0) {
			$videos.each(function(index, video) {
				var url=$(video).data("url");
				var fileName=getLastComponentOfUrl(url);
				$(video).attr("src", "videos/"+fileName);
			});			
		}

		var $audios=$content.find(".myCKAudio");
		if($audios.length>0) {
			$audios.each(function(index, audio) {
				var url=$(audio).data("url");
				var fileName=getLastComponentOfUrl(url);
				$(audio).attr("src", "audios/"+fileName);
			});			
		}
	};
	
	var cloneEditingContents=function() {
		$originalContent=$el.editingArea.find("> div");
		$content=$originalContent.clone();
	};
	
	var saveUserAssetsToCloud=function(doneCallback, failCallback, alwaysCallback) {
		
		// first get the signature and policy strings for uploading a html file to GCS
		MediaEndpoint.renewFormSignatureAndPolicy(function(data) {
			// ทำตามลำดับดังนี้
			// 0. preprocess ไฟล์ html เช่น เปลี่ยน contenteditable เป็น false, เปลี่ยน path ของไฟล์ media ต่างๆ ให้เป็น local
			// 1. อัพโหลดไฟล์ html ขึ้น GCS
			// 2. update EpubPackage entity และ copy referenced media files ลงใน local epub folder ของ user
			// *3. ไม่ต้อง update epub's metadata เพราะจะถูก gen ตอน export to epub (โดยใช้ข้อมูลจาก EpubPackage)
			propagateTitleChangeInCKEditorToTOCItem();
			cloneEditingContents();
			preprocessHtmlFile();
			uploadHtmlFileWithSignatureAndPolicyAndUpdateEpubPackage(data.signature, data.policy, doneCallback, failCallback, alwaysCallback);
		}, function() {
		}, function() {
		});
	};
	
	var propagateTitleChangeInCKEditorToTOCItem = function() {
		var $titleDiv = $el.editingArea.find("> div .title-sync");
		if( $titleDiv.length > 0 ) {
			var $tocItem = $(".dd-list .dd-selected-item");
			$tocItem.text( $titleDiv.text().trim() );
		}
	};
	
	var saveCurrentlyOpenPageToCloud=function(doneCallback, failCallback, alwaysCallback) {
		
        var $currentlySelectedItem=$(".dd-selected-item");        
		// first check if there any page currently open, and save it, if not, perform doneCallback
        if($currentlySelectedItem.length > 0 && $el.editingArea.children().length > 0) { // in case of new book followed by new page, nothing in editingArea (actually the latter condition should suffice?)

        	saveUserAssetsToCloud(function() {

            	if(doneCallback)
            		doneCallback();

            }, function() {
            	if(failCallback)
            		failCallback();
            }, function() {
            	if(alwaysCallback)
            		alwaysCallback();
            });	            	
        } else {
        	if(doneCallback)
        		doneCallback();
        }

	};
	
	var init=function(authorId, publicationId, version) {
		
		gcsRelativePathPrefix=authorId+"/"+publicationId+"/"+version+"/"+"epub"+"/"+"OPS"+"/"

		$el.saveHtmlButton.on("click", function() {

			EpubPackageWidget.disableAllControls();
			saveUserAssetsToCloud(function() {
			}, function() {
				alert("ไม่สามารถบันทึกไฟล์ได้ กรุณาลองใหม่อีกครั้ง");				
			}, function() {
				EpubPackageWidget.enableAllControls();				
			});

		});
	};
	
	return {
		init: function(authorId, publicationId, version) {
			init(authorId, publicationId, version);
		},
		saveCurrentlyOpenPageToCloud: function(doneCallback, failCallback, alwaysCallback) {
			saveCurrentlyOpenPageToCloud(doneCallback, failCallback, alwaysCallback);
		},
	};
	
})(jQuery);