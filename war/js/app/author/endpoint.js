var AuthorEndpoint=(function($) {

	var baseUrl="/authorendpoint";
	
	// binding to UIs and caching elements
	var $el = {
	};

	var logout=function(servletUrl, req, doneCallback, failCallback, alwaysCallback) {
		$.ajax({
			  type: 'POST',
			  contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			  url: servletUrl,
			  data: {
				  req: req
			  }
		})
		.done(function() {
			if(doneCallback)
				doneCallback();
		})
		.fail(function () {
			if(failCallback)
				failCallback();
		})
		.alwaysCallback(function() {
			if(alwaysCallback)
				alwaysCallback();
		});
	};

	return {
		logout: function(doneCallback, failCallback, alwaysCallback) {
			var servletUrl=baseUrl;
			var req="logout";
			logout(servletUrl, req, doneCallback, failCallback, alwaysCallback);
		}
	};
	
})(jQuery);
