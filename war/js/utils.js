function getCountString (count) {
  count = count+''; // coerce to string
  if (count < 1000) {
    return count; // return the same number
  }
  if (count < 10000) { // place a comma between
    return count.charAt(0) + ',' + count.substring(1);
  }
  // divide and format
  return (count/1000).toFixed(count % 1000 != 0)+'k';
}

function getNowEpoch() {
	var nowEpoch=parseInt(new XDate().getTime()/1000, 10);
	return nowEpoch;
}

function getLastComponentOfUrl(url) {
	return url.split('/').pop();
}