(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['ipst-epub-dedication-theme-2-template-1'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "          <div data-theme-id=\"theme2\" data-gcs-relative-path=\"";
  if (stack1 = helpers.gcsRelativePath) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.gcsRelativePath; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" class=\"hero-unit ipst-epub-dedication-theme-2-template-1\" data-original-style=\"background-color:transparent; padding:40px; border:whitesmoke 2px solid;\" style=\"background-color:transparent; padding:40px; border:whitesmoke 2px solid; display:none;\">\n\n                <div class=\"ipst-epub-divider-header-1\">\n                </div>\n\n                <div class=\"ipst-epub-header-desc title-sync\" contenteditable=\"true\">\n                  Untitled\n                </div> \n\n                <div class=\"ipst-epub-body-content ckeditor\" contenteditable=\"true\" style=\"width:70%;\">\n\n                  <p><em>ซิลิกาพาราเซตามอลปฏิยานุพันธ์อีโบลาเมาส์ ดีไวซ์พาราโบลาคูลอมบ์ซีดีรอมคลิกริงโทน ทรานแซกชันอะซีโตนอัพเกรด โมเมนตัมแคร็กเอทิลีน รีเลย์อูบันตูเทฟลอนดอสไฟร์วอลล์สแต็ก คีย์สเปซออฟไลน์</em></p>\n\n                  <p><em>\n                  พันธุศาสตร์ซัลฟิวริกเมาส์ ทวีตจีพีเอสไทฟอยด์เครือข่ายเพอร์ออกไซด์ไมโครซอฟท์ ลีนุกซ์เวิร์มแอนดรอยด์เดลไฟเมาส์ พอร์ต ไซบอร์ก อีเมลอะซีโตน\n                  </em></p>\n\n                  <p><em>โปรเซสเซอร์โพรเซสเซอร์ทรานแซกชันไฟร์ฟอกซ์เมตริกซ์ สุริยจักรวาลซัพพอร์ทกราฟิกส์ดีไวซ์ เมลามีนยูเรียไซต์์ ออฟไลน์เวิร์คสเตชั่นเอนจินอาร์กิวเมนต์คูลอมบ์ ฟอร์แมตไทฟอยด์ เวิร์ม เพจฮาร์ดดิสก์แล็ปท็อป</em></p>\n                </div>       \n\n          </div>\n";
  return buffer;
  });
})();