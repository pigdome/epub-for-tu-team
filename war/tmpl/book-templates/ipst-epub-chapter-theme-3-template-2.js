(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['ipst-epub-chapter-theme-3-template-2'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "          <div data-theme-id=\"theme3\" data-gcs-relative-path=\"";
  if (stack1 = helpers.gcsRelativePath) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.gcsRelativePath; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" data-theme-id=\"theme3\" data-gcs-relative-path=\"";
  if (stack1 = helpers.gcsRelativePath) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.gcsRelativePath; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" class=\"hero-unit ipst-epub-chapter-theme-3-template-2 ipst-epub-contain-background\" data-original-style=\"background-image:url(assets/img/bg-pattern-header/bg-pattern-header-6.png); padding:30px 60px 60px 60px; border:whitesmoke 2px solid;\" style=\"background-image:url(assets/img/bg-pattern-header/bg-pattern-header-6.png); padding:30px 60px 60px 60px; border:whitesmoke 2px solid; display:none;\">\n\n                <div class=\"ipst-epub-header-title-bar\">\n                  <div class=\"ipst-epub-header-text\" contenteditable=\"true\">\n                    บ ท ที่  &nbsp;1\n                  </div>\n                </div> \n\n\n                <div class=\"ipst-epub-line-header\">\n                  <img class=\"ipst-epub-system-image\" data-original-url=\"assets/img/bg-pattern-header/line-header-10.png\" src=\"assets/img/bg-pattern-header/line-header-10.png\" style=\"width:90%\" /> \n                </div>\n\n\n                <div class=\"ipst-epub-header-desc title-sync\" contenteditable=\"true\">\n                  ";
  if (stack1 = helpers.title) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.title; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\n                </div>\n\n\n\n                <div class=\"ipst-epub-header-img ckeditor\" contenteditable=\"true\">\n                  <img class=\"ipst-epub-system-image\" data-original-url=\"assets/img/vector-img-4.jpg\" src=\"assets/img/vector-img-4.jpg\" class=\"img-polaroid\"/>\n                </div>\n\n\n                <div class=\"ipst-epub-body-content ckeditor\" contenteditable=\"true\">\n                  <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; อินทิเกรต อะมิโน ฟีเจอร์แพตช์โพรเซสอัลกอริทึมคอมมูนิเคชั่น ฮับ แฟล็ก พันธุศาสตร์คอนโซล แฮนด์เฮลด์เทเลคอมจาวามัลติเพล็กซ์ทรานแซ็กชันเน็ตเวิร์ค คีย์บอร์ดไดนามิกบั๊กฮับเบิล แอลกอฮอลิซึมเน็ตบุคไวแมกซ์อัพโหลดสแปม อีโบล่าเมล ไวแมกซ์เวสิเคิลเวิร์คสเตชัน แอปพลิเคชั่นเซมิอินทิเกรเตอร์แคโรทีนเอาท์พุต พร็อกซีเชลล์แกนีมีด จีพีเอสรีเฟรชฟิวชันแมคคลาส แชนเนลซีเนอร์เทอร์โมชิพ ไฟล์เวก้าเวอร์ชั่นเวิร์กสเตชัน เอาท์พุทบลูทูธฮับเบิลพิกเซล ปฏิยานุพันธ์เอาท์พุตไฮดรอกไซด์แพลตฟอร์มฟีเจอร์ สปายแวร์โน้ตบุ๊ค เวสิเคิล โพลีเอทิลีนไคโตซานแคมฟร็อกไซต์มอดูล แล็ปท็อปไทฟอยด์ฟอรัมบิตทรานแซ็กชันแอนิเมชัน สแต็กอะมิโน ซีดีไซบอร์กวินโดวส์แบนด์วิดท์เฟิร์มแวร์ ยูริกแอนแทร็กซ์ฮาร์ดดิสก์อินพุตกราฟิกส์ไฮดรอลิก เทฟลอนโดเมนโอเพนเวิร์มจาวาแอมโมเนียม เน็ตบุ๊กไลบรารีเพจเจอร์ โอเพนทรานแซ็กชั่นพัลซาร์ ไซบอร์กแพตช์เอาท์พุทฟีโรโมนลีนุกซ์อัปโหลด ยูริกบัฟเฟอร์แฮนด์เฮลด์ครอสพาเนล โซลูชั่นเดสก์ท็อปซัลไฟด์ เน็ตเวิร์คเบราว์เซอร์โพลาไรซ์อันโดรเมดาปฏิยานุพันธ์อีเมล แชนแนลเราเตอร์ อัพโหลด อะมิโนแคโรทีนไดรว์ไดนามิกส์อีโบล่า ซ็อกเก็ตอัลตราซาวด์</p>\n\n                </div>  \n\n            </div>\n";
  return buffer;
  });
})();