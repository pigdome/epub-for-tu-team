(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['ipst-epub-copyright-theme-1-template-1'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "          <div data-theme-id=\"theme1\" data-gcs-relative-path=\"";
  if (stack1 = helpers.gcsRelativePath) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.gcsRelativePath; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" class=\"hero-unit ipst-epub-copyright-theme-1-template-1\" data-original-style=\"background-color:transparent; padding:40px; border:whitesmoke 2px solid;\" style=\"background-color:transparent; padding:40px; border:whitesmoke 2px solid; display:none;\">\n\n                <div class=\"ipst-epub-header-desc title-sync\" contenteditable=\"true\">\n                  ";
  if (stack1 = helpers.title) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.title; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\n                </div>\n\n                <div class=\"ipst-epub-divider-header\">\n                  <hr/>\n                </div>\n\n                <div class=\"ipst-epub-body-space\">\n\n                </div>      \n\n                <div class=\"ipst-epub-body-content\">\n                    <p class=\"ipst-epub-copyright-symbol-content ckeditor\" contenteditable=\"true\">© ไดนามิกส์มอนอกไซด์</p>\n                    <p class=\"ckeditor\" contenteditable=\"true\">\n                      สเกลฟิวชันมัลแวร์ยูนิโค้ด เทฟลอนไซบอร์ก โซลูชั่นแอนิเมชันสเต็ม ไพรเมตเรียลไทม์ฮับเบิล เอาต์พุท ควอนตัม รีเลย์สแต็กโอเซลทามิเวียร์คลิก ซอฟท์แวร์แคมฟรอกซัพพอร์ท ซีเนอร์ ไทฟอยด์โวลต์ฮาร์ดแวร์เคอร์เนลมีเดีย แพลตฟอร์มอะมิโนอินพุตเวกเตอร์ โพรโทคอล ออกเทนอัลคาไลน์ คีย์สปายแวร์อัลตราซาวด์ ฟีเจอร์แคมฟรอก สปีชีส์บรอดแบนด์พาราเซตามอลแอสพาร์แตมฮาร์ดแวร์โมเด็ม สปีชีส์บรอดแบนด์เวอร์ชั่นโทรจันแพกเก็ตแอสเซมบลี\n                    </p>\n                </div>   \n\n          </div>\n";
  return buffer;
  });
})();