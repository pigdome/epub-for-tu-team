(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['ipst-epub-chapter-theme-2-template-1'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "          <div data-theme-id=\"theme2\" data-gcs-relative-path=\"";
  if (stack1 = helpers.gcsRelativePath) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.gcsRelativePath; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" data-theme-id=\"theme2\" data-gcs-relative-path=\"";
  if (stack1 = helpers.gcsRelativePath) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.gcsRelativePath; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" class=\"hero-unit ipst-epub-chapter-theme-2-template-1\" data-original-style=\"background-color:transparent; background-image:url(assets/img/bg-pattern-header/bg-pattern-header-tran.png); padding:40px; border:whitesmoke 2px solid;\" data-original-style=\"background-color:transparent; background-image:url(assets/img/bg-pattern-header/bg-pattern-header-tran.png); padding:40px; border:whitesmoke 2px solid;\" style=\"background-color:transparent; background-image:url(assets/img/bg-pattern-header/bg-pattern-header-tran.png); padding:40px; border:whitesmoke 2px solid; display:none;\">\n\n                <div class=\"ipst-epub-divider-header-1\">\n                </div>\n\n                <div class=\"ipst-epub-header-title-bar\">\n                  <div class=\"ipst-epub-header-img ckeditor\" contenteditable=\"true\">\n                    <img class=\"ipst-epub-system-image\" data-original-url=\"assets/img/vector-img.png\" src=\"assets/img/vector-img.png\" /> \n                  </div>\n                </div>  \n\n                <div class=\"ipst-epub-divider-header-2\">\n                </div>\n\n\n                <div class=\"ipst-epub-body-content\">\n                  <div class=\"row-fluid\">\n                    <div class=\"span4\" >\n                      <!--Sidebar content-->\n                      <div style=\"margin-top:20px;\"></div>\n                      <p class=\"ipst-epub-header-text\" style=\"text-align: justify;\" contenteditable=\"true\">2</p>\n                      <p class=\"ipst-epub-header-desc title-sync\" style=\"text-align: justify;\" contenteditable=\"true\">";
  if (stack1 = helpers.title) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.title; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</p>\n                    </div>\n                    <div class=\"span8 ckeditor\" contenteditable=\"true\">\n                      <!--Body content-->\n                      <p style=\"text-align: justify;\">โซนาร์เนกาตีฟคลิกชิป ออราเคิลไลบรารีดีวีดีอินพุตดาวน์เกรดอินเทล เน็ตโน้ตบุ๊ก พาราโบลามีเดียพิกเซลแอนิเมชัน สแกนเนอร์พิกเซลอีโบล่าเอ็นจิน บล็อกเกอร์สแต็ก เอ็กซ์โพเนนเชียล แกนีมีดธาลัสซีเมียแคโรทีนเลเยอร์ แท็กออโรร่าแอนดรอยด์โพลีเอทิลีนเอาต์พุตกริด พันธุศาสตร์ โครมาโทกราฟีมัลติทัช คอมพิวติ้งลูปอูบุนตูเอ็กซ์โพเนนเชียล เรียลไทม์สัมพัทธภาพอูบุนตูเวิร์ด อินพุตพอลิเมอร์เมทริกซ์ ซิริอุสแอนิเมชัน กิกะไบต์ อะซีติกเคอร์เซอร์สปายแวร์ อินทิเกรตเอ็นจินกลีเซอรีนโปรโตคอล เอ็กซ์โพเนนเชียลแคมฟร็อกอัลกอริทึมยูเรียไทฟอยด์โอเพน คีย์ทรานแซ็คชั่นเนกาตีฟควอนตัมโมไบล์ สแต็กอินพุตทรานแซ็คชันเบราว์เซอร์ พิกเซล โพลิเมอร์ พอลิเมอร์โหลดเอ็กซ์โพเนนเชียล ไดรว์ไอซีสแปม อะมิโนฮาร์ดแวร์ ไดรเวอร์ซ็อกเก็ตคอนดักเตอร์ คูลอมบ์ เวิร์กสเตชั่นไดนามิคส์แคร็กออฟไลน์ไอพอด เพจไคโตซานวินโดว์สแชนแนลไพธอนโพรเซส เนกาตีฟไฟล์ออกเทนกริดสปีชีส์ ฟิชชันแฟล็ก เน็ตบุ๊กคอมไพล์ซีเทนมาสเตอร์เซมิ โหลดแช็ต โพลีเอทิลีนโอเพนแอนแทร็กซ์ทรานแซกชัน</p>\n                      <p style=\"text-align: justify;\">โซนาร์เนกาตีฟคลิกชิป ออราเคิลไลบรารีดีวีดีอินพุตดาวน์เกรดอินเทล เน็ตโน้ตบุ๊ก พาราโบลามีเดียพิกเซลแอนิเมชัน สแกนเนอร์พิกเซลอีโบล่าเอ็นจิน บล็อกเกอร์สแต็ก เอ็กซ์โพเนนเชียล แกนีมีดธาลัสซีเมียแคโรทีนเลเยอร์ แท็กออโรร่าแอนดรอยด์</p>\n                    </div>\n                  </div>\n\n                </div>      \n\n          </div> \n";
  return buffer;
  });
})();