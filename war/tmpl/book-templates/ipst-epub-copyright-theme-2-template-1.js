(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['ipst-epub-copyright-theme-2-template-1'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "          <div data-theme-id=\"theme2\" data-gcs-relative-path=\"";
  if (stack1 = helpers.gcsRelativePath) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.gcsRelativePath; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" class=\"hero-unit ipst-epub-copyright-theme-2-template-1\" data-original-style=\"background-color:transparent; padding:40px; border:whitesmoke 2px solid;\" style=\"background-color:transparent; padding:40px; border:whitesmoke 2px solid; display:none;\">\n\n                <div class=\"ipst-epub-divider-header-1\">\n                </div>\n\n                <div class=\"ipst-epub-header-desc title-sync\" contenteditable=\"true\">\n                  Untitled\n                </div>\n\n                <div class=\"ipst-epub-body-space\">\n\n                </div>      \n\n                <div class=\"ipst-epub-body-content\">\n                    <p class=\"ipst-epub-copyright-symbol-content ckeditor\" contenteditable=\"true\" style=\"width:70%;\">© ไดนามิกส์มอนอกไซด์</p>\n                    <p class=\"ckeditor\" contenteditable=\"true\" style=\"width:70%;\">\n                      สเกลฟิวชันมัลแวร์ยูนิโค้ด เทฟลอนไซบอร์ก โซลูชั่นแอนิเมชันสเต็ม ไพรเมตเรียลไทม์ฮับเบิล เอาต์พุท ควอนตัม รีเลย์สแต็กโอเซลทามิเวียร์คลิก ซอฟท์แวร์แคมฟรอกซัพพอร์ท ซีเนอร์ ไทฟอยด์โวลต์ฮาร์ดแวร์เคอร์เนลมีเดีย แพลตฟอร์มอะมิโนอินพุตเวกเตอร์ โพรโทคอล ออกเทนอัลคาไลน์ คีย์สปายแวร์อัลตราซาวด์ ฟีเจอร์แคมฟรอก สปีชีส์บรอดแบนด์พาราเซตามอลแอสพาร์แตมฮาร์ดแวร์โมเด็ม  พาสเวิร์ดเลเยอร์แอสพาร์แตม เวกเตอร์อัพเกรดอินเทอร์เน็ตคอนโซลริงโทน\n                    </p>\n                </div>       \n\n          </div> ";
  return buffer;
  });
})();