(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['my-ck-audio'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n	<div style=\"margin-left:20px; padding: 10px; height: display:inline-block; position:relative;\">\n		<audio id=\"";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" controls=\"controls\" class=\"myCKAudio img-polaroid\" style=\"width: 200px; height: 30px;\" data-url=\"";
  if (stack1 = helpers.mediaUrl) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.mediaUrl; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" />\n		&nbsp;&nbsp;&nbsp; <span>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.getFileNameFromUrl || depth0.getFileNameFromUrl),stack1 ? stack1.call(depth0, depth0.mediaUrl, options) : helperMissing.call(depth0, "getFileNameFromUrl", depth0.mediaUrl, options)))
    + "</span>\n  	</div>\n";
  return buffer;
  }

  stack1 = helpers.each.call(depth0, depth0, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n";
  return buffer;
  });
})();