(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['my-image'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n	<div style=\"width:100px; margin-left:20px; display:inline-block; position:relative;\" id=\"manageMedia";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">\n		"
    + "\n    	<img data-media='";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.jsonStringify || depth0.jsonStringify),stack1 ? stack1.call(depth0, depth0, options) : helperMissing.call(depth0, "jsonStringify", depth0, options)))
    + "' data-url=\"";
  if (stack2 = helpers.mediaUrl) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.mediaUrl; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" src=\"/assets/img/book-covers/preLoadingImgUploadTemplate.png\" class=\"img-polaroid myImage\" style=\"height: 80px; width:80px; object-fit:contain; -moz-object-fit:contain; -ms-object-fit:contain; -o-object-fit:contain; -webkit-object-fit:contain;\">\n        <div style=\"margin-top:10px; margin-bottom:20px;\">\n            <button class=\"btn btn-mini btn-download-img-resource\" type=\"button\" style=\"width:58px;\">\n            	<i class=\"icon-download-alt icon-black\"></i>\n            </button>\n            <button class=\"btn btn-mini btn-delete-img-resource\" type=\"button\">\n                <i class=\"icon-trash icon-black\"></i>\n       		</button>\n      	</div>  \n  	</div>\n";
  return buffer;
  }

  stack1 = helpers.each.call(depth0, depth0, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  });
})();