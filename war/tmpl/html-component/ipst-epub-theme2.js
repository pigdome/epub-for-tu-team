(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['ipst-epub-theme2'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function";


  buffer += "<!DOCTYPE html>\n<html>\n	<head>\n		<title></title>\n\n		<link href=\"css/bootstrap/bootstrap.min.css\" rel=\"stylesheet\" />\n		<link href=\"css/bootstrap/bootstrap-responsive.min.css\" rel=\"stylesheet\" />\n\n		<link href=\"css/theme-2/ipst-epub-theme-2.css\" rel=\"stylesheet\" />\n		<link href=\"css/theme-2/ipst-epub-theme-2-responsive.css\" rel=\"stylesheet\" />\n\n	</head>\n	<body>\n		"
    + "\n		";
  stack1 = (typeof depth0 === functionType ? depth0.apply(depth0) : depth0);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n		\n	</body>\n</html>";
  return buffer;
  });
})();