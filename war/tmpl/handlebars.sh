#!/bin/bash
handlebars book-details-reuse.handlebars -f book-details-reuse.js
handlebars my-audio.handlebars -f my-audio.js
handlebars my-image.handlebars -f my-image.js
handlebars book-details.handlebars -f book-details.js
handlebars my-ck-audio.handlebars -f my-ck-audio.js
handlebars my-video.handlebars -f my-video.js
handlebars book.handlebars -f book.js
handlebars my-ck-image.handlebars -f my-ck-image.js
handlebars recent-open-book.handlebars -f recent-open-book.js
handlebars cover.handlebars -f cover.js
handlebars my-ck-video.handlebars -f my-ck-video.js
handlebars epub-html-item.handlebars -f epub-html-item.js
handlebars my-cover.handlebars -f my-cover.js
