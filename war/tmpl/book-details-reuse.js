(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['book-details-reuse'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<div class='og-expander-inner'>\n	<span class='og-close'></span>\n	<div class='og-fullimg'>\n		<img class='bookCoverL' data-url='";
  if (stack1 = helpers.coverUrl) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.coverUrl; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "' style='display:none;' src='/assets/img/book-covers/book-cover-preview-expan.png' />\n	</div>\n	<div class='og-details'>\n		<h3>";
  if (stack1 = helpers.title) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.title; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</h3>\n		<p>"
    + escapeExpression(((stack1 = ((stack1 = depth0.description),stack1 == null || stack1 === false ? stack1 : stack1.value)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</p>\n		<a class='go-inside-button' href='#'>เปิดหนังสือ</a>\n		<a class='edit-metadata-button' href='#'>แก้ไขเมทาดาทา</a>\n       	<a class='delete-book-button' href='#'>ลบหนังสือ</a>\n	</div>\n</div>";
  return buffer;
  });
})();