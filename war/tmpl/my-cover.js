(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['my-cover'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n	<div class=\"panel-book-cover\" style=\"display:inline-block; position:relative;\">\n		<div class=\"form-btn-delete-my-cover\">\n			<button class=\"btn btn-mini btn-danger btn-del-my-covers\" type=\"button\" style=\"margin-bottom:7px; width:65px;\">Delete</button><br />\n			<button class=\"btn btn-mini btn-cancel-my-covers\" type=\"button\" style=\"width:65px;\">Cancel</button>\n		</div>\n		<i class=\"icon-trash icon-black icon-trash-book-cover\" style=\"display:none;\"></i>\n		<img class=\"myCover\" data-url=\"";
  if (stack1 = helpers.coverUrl) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.coverUrl; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" src=\"/assets/img/book-covers/book-cover-preview-template.png\" style=\"box-shadow: 1px 1px 2px 1px #BABABA;\"/>\n	</div>\n";
  return buffer;
  }

  stack1 = helpers.each.call(depth0, depth0, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  });
})();