(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['preview'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function";


  buffer += "<!DOCTYPE html>\n<html>\n	<head>\n	    <meta charset=\"UTF-8\" />\n	\n		<title></title>\n\n		<script type=\"text/x-mathjax-config\">\n			MathJax.Hub.Config({\n				tex2jax: {\n					inlineMath: [[\"$\",\"$\"],[\"\\\\(\",\"\\\\)\"]]\n				},\n				\"HTML-CSS\": { scale: 100}\n			});\n		</script>\n		<script type=\"text/javascript\" src=\"http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML-full\"></script>\n		<script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js\"></script>\n		<script src=\"/js/app/media/endpoint.js\"></script>	\n\n		<link href=\"assets/css/bootstrap.min.css\" rel=\"stylesheet\" />\n		<link href=\"assets/css/bootstrap-responsive.min.css\" rel=\"stylesheet\" />\n\n	    <link href=\"assets/css/ipst-epub-theme/theme-1/ipst-epub-theme-1.css\" rel=\"stylesheet\" />\n	    <link href=\"assets/css/ipst-epub-theme/theme-1/ipst-epub-theme-1-responsive.css\" rel=\"stylesheet\" />\n	    <link href=\"assets/css/ipst-epub-theme/theme-2/ipst-epub-theme-2.css\" rel=\"stylesheet\" />\n	    <link href=\"assets/css/ipst-epub-theme/theme-3/ipst-epub-theme-3.css\" rel=\"stylesheet\" />\n	    <link href=\"assets/css/ipst-epub-theme/theme-3/ipst-epub-theme-3-responsive.css\" rel=\"stylesheet\" />\n\n	</head>\n	<body>\n		"
    + "\n		";
  stack1 = (typeof depth0 === functionType ? depth0.apply(depth0) : depth0);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n		\n		<script>\n			(function($) {\n				//\n				// In case of pdf preview, several html files will be combined at client side, and, therefore, \"src\" of media files are not yet signed\n				//\n\n				// change system image (e.g. backgrounds, lines, placeholders) paths\n				var $systemImages=$(\".ipst-epub-system-image\");\n				if($systemImages.length>0) {\n					$systemImages.each(function(index, image) {\n						$(image).attr(\"src\", $(image).attr(\"data-original-url\"));\n					});			\n				}\n		\n				// change media paths to local package\n				var $images=$(\".myCKImage\");\n				if($images.length>0) {\nconsole.log($images);\n					MediaEndpoint.loadImageAsync($images);\n				}\n		\n				var $videos=$(\".myCKVideo\");\n				if($videos.length>0) {\n					MediaEndpoint.loadVideoAsync($videos);\n				}\n		\n				var $audios=$(\".myCKAudio\");\n				if($audios.length>0) {\n					MediaEndpoint.loadAudioAsync($audios);\n				}\n								\n			})(jQuery);\n		</script>\n	</body>\n</html>";
  return buffer;
  });
})();